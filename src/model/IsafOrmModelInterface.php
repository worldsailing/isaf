<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Interface IsafOrmModelInterface
 * @package worldsailing\Isaf\model
 */
interface IsafOrmModelInterface
{

    /**
     * @return array
     */
    public static function getDefaultValues();

    /**
     * @param $key
     * @return mixed|null
     */
    public function getDefaultValue($key);

    /**
     * @return Assert\Collection
     */
    public static function getConstraints();

    /**
     * @param $data
     * @param bool $useDefaults
     * @return IsafOrmModelInterface
     */
    public function setData($data, $useDefaults = false);

    /**
     * @return array
     */
    public function jsonSerialize();
}
