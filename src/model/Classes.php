<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JsonSerializable;

/**
 * Classes
 *
 * @ORM\Table(name="Classes")
 * @ORM\Entity
 */
class Classes extends \worldsailing\Isaf\model\entity\Classes  implements JsonSerializable, IsafOrmModelInterface
{
    /**
     * @return array
     */
    public static function getDefaultValues()
    {
        return [

        ];
    }

    /**
     * @return Assert\Collection
     */
    public static function getConstraints()
    {

        $constraints = new Assert\Collection([
            'fields' => [
                'ClassPhone1' => [new Assert\Length(['max' => 20])],
                'ClassPhone2' => [new Assert\Length(['max' => 20])],
                'ClassFax' => [new Assert\Length(['max' => 20])],

                'ClassHullMat' => [new Assert\Length(['max' => 30])],
                'ClassNumCrew' => [new Assert\Length(['max' => 30])],
                'ClassOptCrewWeight' => [new Assert\Length(['max' => 30])],
                'ClassNumTrapeze' => [new Assert\Length(['max' => 30])],
                'ClassDesigner' => [new Assert\Length(['max' => 30])],
                'ClassNationalOrigins' => [new Assert\Length(['max' => 30])],
                'ClassYearDesigned' => [new Assert\Length(['max' => 30])],
                'ClassIsafStatus' => [new Assert\Length(['max' => 30])],
                'ClassHullLen' => [new Assert\Length(['max' => 30])],
                'ClassBeamLen' => [new Assert\Length(['max' => 30])],
                'ClassDraughtLen' => [new Assert\Length(['max' => 30])],
                'ClassMainsailArea' => [new Assert\Length(['max' => 30])],
                'ClassHeadsailArea' => [new Assert\Length(['max' => 30])],
                'ClassSpinnakerArea' => [new Assert\Length(['max' => 30])],
                'ClassSailAreaUpwind' => [new Assert\Length(['max' => 30])],
                'ClassWeightBoat' => [new Assert\Length(['max' => 30])],
                'ClassWeightHull' => [new Assert\Length(['max' => 30])],
                'ClassWeightBallast' => [new Assert\Length(['max' => 30])],
                'ClassVolume' => [new Assert\Length(['max' => 30])],
                'ClassIsafAdCat' => [new Assert\Length(['max' => 30])]

            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = self::getDefaultValues();
        if (isset($defaults[$key])) {
            if (is_callable($defaults[$key])) {
                return $defaults[$key]();
            } else {
                return $defaults[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param bool $useDefaults
     * @return IsafOrmModelInterface
     */
    public function setData($data, $useDefaults = false)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    if ($useDefaults === true) {
                        $this->{$key} = $this->getDefaultValue($key);
                    }
                } else {
                    $this->{$key} = $data[$key];
                }
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}