<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JsonSerializable;

/**
 * Xrrschemadoc
 *
 * @ORM\Table(name="XRRSchemaDoc")
 * @ORM\Entity
 */
class XrrSchemaDoc extends \worldsailing\Isaf\model\entity\Xrrschemadoc  implements JsonSerializable, IsafOrmModelInterface
{
    /**
     * @return array
     */
    public static function getDefaultValues()
    {
        return [
            'XRRSchDocContent' => '',
            'XRRSchDocType' => '',
            'XRRSchDocVersion' =>'',
            'XRRSchDocFilename' => '',
            'XRRSchDocTitle' => ''
        ];
    }

    /**
     * @return Assert\Collection
     */
    public static function getConstraints()
    {
        $constraints = new Assert\Collection([
            'fields' => [
                'XRRSchDocContent' => [new Assert\NotBlank()],
                'XRRSchDocType' => [new Assert\NotBlank()],
                'XRRSchDocVersion' => [new Assert\NotBlank()],
                'XRRSchDocFilename' => [new Assert\NotBlank()],
                'XRRSchDocTitle' => [new Assert\NotBlank()]

            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = self::getDefaultValues();
        if (isset($defaults[$key])) {
            if (is_callable($defaults[$key])) {
                return $defaults[$key]();
            } else {
                return $defaults[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param bool $useDefaults
     * @return IsafOrmModelInterface
     */
    public function setData($data, $useDefaults = false)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    if ($useDefaults === true) {
                        $this->{$key} = $this->getDefaultValue($key);
                    }
                } else {
                    $this->{$key} = $data[$key];
                }
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}