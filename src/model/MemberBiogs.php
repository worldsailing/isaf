<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JsonSerializable;

/**
 * Memberbiogs
 *
 * @ORM\Table(name="MemberBiogs", indexes={@ORM\Index(name="MembId_Deleted", columns={"BiogMembId", "BiogDeleted"}), @ORM\Index(name="idBiogIsafId", columns={"BiogIsafId"}), @ORM\Index(name="cfcnuserlevel", columns={"BiogClassificationsUserLevel"}), @ORM\Index(name="biogioc", columns={"BiogIOCNum"}), @ORM\Index(name="soticusermap", columns={"BiogSoticIdMap"})})
 * @ORM\Entity
 */
class MemberBiogs extends \worldsailing\Isaf\model\entity\Memberbiogs  implements JsonSerializable, IsafOrmModelInterface
{

    /**
     * @return array
     */
    public static function getDefaultValues()
    {
        return [
            'BiogDeleted' => 'no',
            'BiogShow' => 1,
            'BiogHideInSearch' => 0,
            'BiogEmailList' => 'no',
            'BiogShowClass' => 1,
            'BiogShowGender' => 1,
            'BiogShowDob' => 1,
            'BiogShowEmail' => 0,
            'BiogShowResidence' => 0
        ];
    }

    /**
     * @return Assert\Collection
     */
    public static function getConstraints()
    {
        $constraints = new Assert\Collection([
            'fields' => [
                'BiogIsafId' => [new Assert\NotBlank(), new Assert\Length(['min' => 5])],
                'BiogFirstName' => [new Assert\NotBlank(), new Assert\Type('string')],
                'BiogSurname' => [new Assert\NotBlank(), new Assert\Type('string')],
                'BiogEmail' => [new Assert\NotBlank(), new Assert\Email()],
                'BiogDeleted' => [new Assert\NotBlank(), new Assert\Collection(['yes','no', 'Yes', 'No', 'YES', 'NO'])],
                'BiogEmailList' => [new Assert\NotBlank(), new Assert\Collection(['yes','no', 'Yes', 'No', 'YES', 'NO'])]
            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = self::getDefaultValues();
        if (isset($defaults[$key])) {
            if (is_callable($defaults[$key])) {
                return $defaults[$key]();
            } else {
                return $defaults[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param bool $useDefaults
     * @return IsafOrmModelInterface
     */
    public function setData($data, $useDefaults = false)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    if ($useDefaults === true) {
                        $this->{$key} = $this->getDefaultValue($key);
                    }
                } else {
                    $this->{$key} = $data[$key];
                }
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
