<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JsonSerializable;

/**
 * Regattas
 *
 * @ORM\Table(name="Regattas", indexes={@ORM\Index(name="idRgtaVenCtryIdEndDate", columns={"RgtaVenCtryId", "RgtaEndDate"}), @ORM\Index(name="RgtaApvlStatus", columns={"RgtaApvlStatus"}), @ORM\Index(name="RgtaStartDate", columns={"RgtaStartDate"})})
 * @ORM\Entity
 */
class Regattas extends \worldsailing\Isaf\model\entity\Regattas  implements JsonSerializable, IsafOrmModelInterface
{

    /**
     * @return array
     */
    public static function getDefaultValues()
    {
        return [
            'RgtaTournId' => null,
            'RgtaStartDate' => function() {
                return date('Y-m-d');
            },
            'RgtaEndDate' => function() {
                return date('Y-m-d');
            },
            'RgtaVenCtryId' => null,
            'RgtaFoldId' => null,
            'RgtaLogoUpldId' => null
        ];
    }

    public static function getConstraints()
    {
        $constraints = new Assert\Collection([
            'fields' => [
                'RgtaIsafId' => [new Assert\Length(['max' => 15])],
                'RgtaSrcId' => [new Assert\Length(['max' => 45])],
                'RgtaTournId' => [new Assert\NotBlank()],
                'RgtaStartDate' => [new Assert\NotBlank()],
                'RgtaEndDate' => [new Assert\NotBlank()],
                'RgtaVenCtryId' => [new Assert\NotBlank()],
                'RgtaFoldId' => [new Assert\NotBlank()],
                'RgtaLogoUpldId' => [new Assert\NotBlank()],
                'RgtaApvlStatus' => [new Assert\Collection(['Y','N'])]
            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = self::getDefaultValues();
        if (isset($defaults[$key])) {
            if (is_callable($defaults[$key])) {
                return $defaults[$key]();
            } else {
                return $defaults[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param bool $useDefaults
     * @return IsafOrmModelInterface
     */
    public function setData($data, $useDefaults = false)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    if ($useDefaults === true) {
                        $this->{$key} = $this->getDefaultValue($key);
                    }
                } else {
                    $this->{$key} = $data[$key];
                }
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
