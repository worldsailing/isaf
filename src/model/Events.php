<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JsonSerializable;

/**
 * Events
 *
 * @ORM\Table(name="Events", indexes={@ORM\Index(name="EvntRgtaId", columns={"EvntRgtaId"}), @ORM\Index(name="EvntStatus", columns={"EvntStatus"})})
 * @ORM\Entity
 */
class Events extends \worldsailing\Isaf\model\entity\Events  implements JsonSerializable, IsafOrmModelInterface
{
    /**
     * @return array
     */
    public static function getDefaultValues()
    {
        return [

        ];
    }

    /**
     * @return Assert\Collection
     */
    public static function getConstraints()
    {

        $constraints = new Assert\Collection([
            'fields' => [
                'EvntOrigClass' => [new Assert\Length(['max' => 10])],
                'EvntSrcId' => [new Assert\Length(['max' => 45])],
                'EvntOrigMoe' => [new Assert\NotBlank()],
                'EvntRgtaId' => [new Assert\NotBlank()],
                'EvntMultiplier' => [new Assert\NotBlank()],
                'EvntBonus' => [new Assert\NotBlank()],
                'EvntQuality' => [new Assert\NotBlank()],
                'EvntStatus' => [new Assert\NotBlank(), new Assert\Collection(['planned','scheduled','startseq','inprogress','recall','finished','result','delayed','cancelled','protested','postponed','abandoned','rescheduled'])],
                'EvntRank' => [new Assert\NotBlank()]

            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = self::getDefaultValues();
        if (isset($defaults[$key])) {
            if (is_callable($defaults[$key])) {
                return $defaults[$key]();
            } else {
                return $defaults[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param bool $useDefaults
     * @return IsafOrmModelInterface
     */
    public function setData($data, $useDefaults = false)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    if ($useDefaults === true) {
                        $this->{$key} = $this->getDefaultValue($key);
                    }
                } else {
                    $this->{$key} = $data[$key];
                }
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}