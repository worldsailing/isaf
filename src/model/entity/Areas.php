<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Areas
 *
 * @ORM\Table(name="Areas")
 * @ORM\Entity
 */
class Areas
{
    /**
     * @var string
     *
     * @ORM\Column(name="AreaCode", type="string", length=255, nullable=true)
     */
    protected $AreaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="AreaName", type="string", length=255, nullable=true)
     */
    protected $AreaName;

    /**
     * @var integer
     *
     * @ORM\Column(name="AreaId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $AreaId;



    /**
     * Set AreaCode
     *
     * @param string $areaCode
     * @return Areas
     */
    public function setAreaCode($areaCode)
    {
        $this->AreaCode = $areaCode;

        return $this;
    }

    /**
     * Get AreaCode
     *
     * @return string 
     */
    public function getAreaCode()
    {
        return $this->AreaCode;
    }

    /**
     * Set AreaName
     *
     * @param string $areaName
     * @return Areas
     */
    public function setAreaName($areaName)
    {
        $this->AreaName = $areaName;

        return $this;
    }

    /**
     * Get AreaName
     *
     * @return string 
     */
    public function getAreaName()
    {
        return $this->AreaName;
    }

    /**
     * Get AreaId
     *
     * @return integer 
     */
    public function getAreaId()
    {
        return $this->AreaId;
    }
}
