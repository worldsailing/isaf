<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rankingeventsdisabled
 *
 * @ORM\Table(name="RankingEventsDisabled")
 * @ORM\Entity
 */
class Rankingeventsdisabled
{
    /**
     * @var integer
     *
     * @ORM\Column(name="DisbldRankId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $DisbldRankId;

    /**
     * @var integer
     *
     * @ORM\Column(name="DisbldEvntId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $DisbldEvntId;



    /**
     * Set DisbldRankId
     *
     * @param integer $disbldRankId
     * @return Rankingeventsdisabled
     */
    public function setDisbldRankId($disbldRankId)
    {
        $this->DisbldRankId = $disbldRankId;

        return $this;
    }

    /**
     * Get DisbldRankId
     *
     * @return integer 
     */
    public function getDisbldRankId()
    {
        return $this->DisbldRankId;
    }

    /**
     * Set DisbldEvntId
     *
     * @param integer $disbldEvntId
     * @return Rankingeventsdisabled
     */
    public function setDisbldEvntId($disbldEvntId)
    {
        $this->DisbldEvntId = $disbldEvntId;

        return $this;
    }

    /**
     * Get DisbldEvntId
     *
     * @return integer 
     */
    public function getDisbldEvntId()
    {
        return $this->DisbldEvntId;
    }
}
