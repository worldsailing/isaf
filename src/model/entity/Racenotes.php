<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Racenotes
 *
 * @ORM\Table(name="RaceNotes")
 * @ORM\Entity
 */
class Racenotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RnteRaceId", type="integer", nullable=false)
     */
    protected $RnteRaceId;

    /**
     * @var string
     *
     * @ORM\Column(name="RnteNote", type="text", length=65535, nullable=true)
     */
    protected $RnteNote;

    /**
     * @var integer
     *
     * @ORM\Column(name="RnteId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RnteId;



    /**
     * Set RnteRaceId
     *
     * @param integer $rnteRaceId
     * @return Racenotes
     */
    public function setRnteRaceId($rnteRaceId)
    {
        $this->RnteRaceId = $rnteRaceId;

        return $this;
    }

    /**
     * Get RnteRaceId
     *
     * @return integer 
     */
    public function getRnteRaceId()
    {
        return $this->RnteRaceId;
    }

    /**
     * Set RnteNote
     *
     * @param string $rnteNote
     * @return Racenotes
     */
    public function setRnteNote($rnteNote)
    {
        $this->RnteNote = $rnteNote;

        return $this;
    }

    /**
     * Get RnteNote
     *
     * @return string 
     */
    public function getRnteNote()
    {
        return $this->RnteNote;
    }

    /**
     * Get RnteId
     *
     * @return integer 
     */
    public function getRnteId()
    {
        return $this->RnteId;
    }
}
