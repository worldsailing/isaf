<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Title
 *
 * @ORM\Table(name="Title")
 * @ORM\Entity
 */
class Title
{
    /**
     * @var integer
     *
     * @ORM\Column(name="TtlOrigId", type="integer", nullable=true)
     */
    protected $TtlOrigId;

    /**
     * @var string
     *
     * @ORM\Column(name="TtlName", type="string", length=128, nullable=true)
     */
    protected $TtlName;

    /**
     * @var integer
     *
     * @ORM\Column(name="TtlOrder", type="integer", nullable=true)
     */
    protected $TtlOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="TtlId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $TtlId;



    /**
     * Set TtlOrigId
     *
     * @param integer $ttlOrigId
     * @return Title
     */
    public function setTtlOrigId($ttlOrigId)
    {
        $this->TtlOrigId = $ttlOrigId;

        return $this;
    }

    /**
     * Get TtlOrigId
     *
     * @return integer 
     */
    public function getTtlOrigId()
    {
        return $this->TtlOrigId;
    }

    /**
     * Set TtlName
     *
     * @param string $ttlName
     * @return Title
     */
    public function setTtlName($ttlName)
    {
        $this->TtlName = $ttlName;

        return $this;
    }

    /**
     * Get TtlName
     *
     * @return string 
     */
    public function getTtlName()
    {
        return $this->TtlName;
    }

    /**
     * Set TtlOrder
     *
     * @param integer $ttlOrder
     * @return Title
     */
    public function setTtlOrder($ttlOrder)
    {
        $this->TtlOrder = $ttlOrder;

        return $this;
    }

    /**
     * Get TtlOrder
     *
     * @return integer 
     */
    public function getTtlOrder()
    {
        return $this->TtlOrder;
    }

    /**
     * Get TtlId
     *
     * @return integer 
     */
    public function getTtlId()
    {
        return $this->TtlId;
    }
}
