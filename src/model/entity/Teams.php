<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Teams
 *
 * @ORM\Table(name="Teams", indexes={@ORM\Index(name="TeamSource", columns={"TeamSource", "TeamSrcId"})})
 * @ORM\Entity
 */
class Teams
{
    /**
     * @var string
     *
     * @ORM\Column(name="TeamSource", type="string", length=10, nullable=true)
     */
    protected $TeamSource;

    /**
     * @var string
     *
     * @ORM\Column(name="TeamSrcId", type="string", length=50, nullable=true)
     */
    protected $TeamSrcId;

    /**
     * @var string
     *
     * @ORM\Column(name="TeamName", type="string", length=255, nullable=true)
     */
    protected $TeamName;

    /**
     * @var integer
     *
     * @ORM\Column(name="TeamEntryId", type="integer", nullable=false)
     */
    protected $TeamEntryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TeamCtryId", type="integer", nullable=false)
     */
    protected $TeamCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TeamRgtaId", type="integer", nullable=false)
     */
    protected $TeamRgtaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TeamId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $TeamId;



    /**
     * Set TeamSource
     *
     * @param string $teamSource
     * @return Teams
     */
    public function setTeamSource($teamSource)
    {
        $this->TeamSource = $teamSource;

        return $this;
    }

    /**
     * Get TeamSource
     *
     * @return string 
     */
    public function getTeamSource()
    {
        return $this->TeamSource;
    }

    /**
     * Set TeamSrcId
     *
     * @param string $teamSrcId
     * @return Teams
     */
    public function setTeamSrcId($teamSrcId)
    {
        $this->TeamSrcId = $teamSrcId;

        return $this;
    }

    /**
     * Get TeamSrcId
     *
     * @return string 
     */
    public function getTeamSrcId()
    {
        return $this->TeamSrcId;
    }

    /**
     * Set TeamName
     *
     * @param string $teamName
     * @return Teams
     */
    public function setTeamName($teamName)
    {
        $this->TeamName = $teamName;

        return $this;
    }

    /**
     * Get TeamName
     *
     * @return string 
     */
    public function getTeamName()
    {
        return $this->TeamName;
    }

    /**
     * Set TeamEntryId
     *
     * @param integer $teamEntryId
     * @return Teams
     */
    public function setTeamEntryId($teamEntryId)
    {
        $this->TeamEntryId = $teamEntryId;

        return $this;
    }

    /**
     * Get TeamEntryId
     *
     * @return integer 
     */
    public function getTeamEntryId()
    {
        return $this->TeamEntryId;
    }

    /**
     * Set TeamCtryId
     *
     * @param integer $teamCtryId
     * @return Teams
     */
    public function setTeamCtryId($teamCtryId)
    {
        $this->TeamCtryId = $teamCtryId;

        return $this;
    }

    /**
     * Get TeamCtryId
     *
     * @return integer 
     */
    public function getTeamCtryId()
    {
        return $this->TeamCtryId;
    }

    /**
     * Set TeamRgtaId
     *
     * @param integer $teamRgtaId
     * @return Teams
     */
    public function setTeamRgtaId($teamRgtaId)
    {
        $this->TeamRgtaId = $teamRgtaId;

        return $this;
    }

    /**
     * Get TeamRgtaId
     *
     * @return integer 
     */
    public function getTeamRgtaId()
    {
        return $this->TeamRgtaId;
    }

    /**
     * Get TeamId
     *
     * @return integer 
     */
    public function getTeamId()
    {
        return $this->TeamId;
    }
}
