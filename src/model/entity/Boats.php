<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boats
 *
 * @ORM\Table(name="Boats", indexes={@ORM\Index(name="idBoatTeamEvntId", columns={"BoatTeamId", "BoatEvntId"}), @ORM\Index(name="idBoatPosition", columns={"BoatPosition"}), @ORM\Index(name="idBoatEvntId", columns={"BoatEvntId"}), @ORM\Index(name="fk_BoatGroup", columns={"BoatBtGrpId"}), @ORM\Index(name="BoatSource", columns={"BoatSource", "BoatSrcId"})})
 * @ORM\Entity
 */
class Boats
{
    /**
     * @var string
     *
     * @ORM\Column(name="BoatSource", type="string", length=10, nullable=true)
     */
    protected $BoatSource;

    /**
     * @var string
     *
     * @ORM\Column(name="BoatSrcId", type="string", length=50, nullable=true)
     */
    protected $BoatSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BoatEvntId", type="integer", nullable=false)
     */
    protected $BoatEvntId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BoatTeamId", type="integer", nullable=false)
     */
    protected $BoatTeamId;

    /**
     * @var string
     *
     * @ORM\Column(name="BoatName", type="string", length=255, nullable=true)
     */
    protected $BoatName;

    /**
     * @var string
     *
     * @ORM\Column(name="BoatBowNum", type="string", length=20, nullable=true)
     */
    protected $BoatBowNum;

    /**
     * @var string
     *
     * @ORM\Column(name="BoatSailNum", type="string", length=20, nullable=true)
     */
    protected $BoatSailNum;

    /**
     * @var string
     *
     * @ORM\Column(name="BoatGender", type="string", nullable=true)
     */
    protected $BoatGender;

    /**
     * @var integer
     *
     * @ORM\Column(name="BoatPosition", type="integer", nullable=false)
     */
    protected $BoatPosition;

    /**
     * @var float
     *
     * @ORM\Column(name="BoatPoints", type="float", precision=10, scale=2, nullable=false)
     */
    protected $BoatPoints;

    /**
     * @var float
     *
     * @ORM\Column(name="BoatBonus", type="float", precision=10, scale=2, nullable=false)
     */
    protected $BoatBonus;

    /**
     * @var integer
     *
     * @ORM\Column(name="BoatPointsOverride", type="integer", nullable=true)
     */
    protected $BoatPointsOverride;

    /**
     * @var float
     *
     * @ORM\Column(name="BoatPercentWon", type="float", precision=10, scale=0, nullable=false)
     */
    protected $BoatPercentWon;

    /**
     * @var float
     *
     * @ORM\Column(name="BoatTotalPoints", type="float", precision=10, scale=2, nullable=true)
     */
    protected $BoatTotalPoints;

    /**
     * @var float
     *
     * @ORM\Column(name="BoatNetPoints", type="float", precision=10, scale=2, nullable=true)
     */
    protected $BoatNetPoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="BoatId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $BoatId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Boatgroup
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Boatgroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="BoatBtGrpId", referencedColumnName="BtGrpId")
     * })
     */
    protected $boatBtGrpId;



    /**
     * Set BoatSource
     *
     * @param string $boatSource
     * @return Boats
     */
    public function setBoatSource($boatSource)
    {
        $this->BoatSource = $boatSource;

        return $this;
    }

    /**
     * Get BoatSource
     *
     * @return string 
     */
    public function getBoatSource()
    {
        return $this->BoatSource;
    }

    /**
     * Set BoatSrcId
     *
     * @param string $boatSrcId
     * @return Boats
     */
    public function setBoatSrcId($boatSrcId)
    {
        $this->BoatSrcId = $boatSrcId;

        return $this;
    }

    /**
     * Get BoatSrcId
     *
     * @return string 
     */
    public function getBoatSrcId()
    {
        return $this->BoatSrcId;
    }

    /**
     * Set BoatEvntId
     *
     * @param integer $boatEvntId
     * @return Boats
     */
    public function setBoatEvntId($boatEvntId)
    {
        $this->BoatEvntId = $boatEvntId;

        return $this;
    }

    /**
     * Get BoatEvntId
     *
     * @return integer 
     */
    public function getBoatEvntId()
    {
        return $this->BoatEvntId;
    }

    /**
     * Set BoatTeamId
     *
     * @param integer $boatTeamId
     * @return Boats
     */
    public function setBoatTeamId($boatTeamId)
    {
        $this->BoatTeamId = $boatTeamId;

        return $this;
    }

    /**
     * Get BoatTeamId
     *
     * @return integer 
     */
    public function getBoatTeamId()
    {
        return $this->BoatTeamId;
    }

    /**
     * Set BoatName
     *
     * @param string $boatName
     * @return Boats
     */
    public function setBoatName($boatName)
    {
        $this->BoatName = $boatName;

        return $this;
    }

    /**
     * Get BoatName
     *
     * @return string 
     */
    public function getBoatName()
    {
        return $this->BoatName;
    }

    /**
     * Set BoatBowNum
     *
     * @param string $boatBowNum
     * @return Boats
     */
    public function setBoatBowNum($boatBowNum)
    {
        $this->BoatBowNum = $boatBowNum;

        return $this;
    }

    /**
     * Get BoatBowNum
     *
     * @return string 
     */
    public function getBoatBowNum()
    {
        return $this->BoatBowNum;
    }

    /**
     * Set BoatSailNum
     *
     * @param string $boatSailNum
     * @return Boats
     */
    public function setBoatSailNum($boatSailNum)
    {
        $this->BoatSailNum = $boatSailNum;

        return $this;
    }

    /**
     * Get BoatSailNum
     *
     * @return string 
     */
    public function getBoatSailNum()
    {
        return $this->BoatSailNum;
    }

    /**
     * Set BoatGender
     *
     * @param string $boatGender
     * @return Boats
     */
    public function setBoatGender($boatGender)
    {
        $this->BoatGender = $boatGender;

        return $this;
    }

    /**
     * Get BoatGender
     *
     * @return string 
     */
    public function getBoatGender()
    {
        return $this->BoatGender;
    }

    /**
     * Set BoatPosition
     *
     * @param integer $boatPosition
     * @return Boats
     */
    public function setBoatPosition($boatPosition)
    {
        $this->BoatPosition = $boatPosition;

        return $this;
    }

    /**
     * Get BoatPosition
     *
     * @return integer 
     */
    public function getBoatPosition()
    {
        return $this->BoatPosition;
    }

    /**
     * Set BoatPoints
     *
     * @param float $boatPoints
     * @return Boats
     */
    public function setBoatPoints($boatPoints)
    {
        $this->BoatPoints = $boatPoints;

        return $this;
    }

    /**
     * Get BoatPoints
     *
     * @return float 
     */
    public function getBoatPoints()
    {
        return $this->BoatPoints;
    }

    /**
     * Set BoatBonus
     *
     * @param float $boatBonus
     * @return Boats
     */
    public function setBoatBonus($boatBonus)
    {
        $this->BoatBonus = $boatBonus;

        return $this;
    }

    /**
     * Get BoatBonus
     *
     * @return float 
     */
    public function getBoatBonus()
    {
        return $this->BoatBonus;
    }

    /**
     * Set BoatPointsOverride
     *
     * @param integer $boatPointsOverride
     * @return Boats
     */
    public function setBoatPointsOverride($boatPointsOverride)
    {
        $this->BoatPointsOverride = $boatPointsOverride;

        return $this;
    }

    /**
     * Get BoatPointsOverride
     *
     * @return integer 
     */
    public function getBoatPointsOverride()
    {
        return $this->BoatPointsOverride;
    }

    /**
     * Set BoatPercentWon
     *
     * @param float $boatPercentWon
     * @return Boats
     */
    public function setBoatPercentWon($boatPercentWon)
    {
        $this->BoatPercentWon = $boatPercentWon;

        return $this;
    }

    /**
     * Get BoatPercentWon
     *
     * @return float 
     */
    public function getBoatPercentWon()
    {
        return $this->BoatPercentWon;
    }

    /**
     * Set BoatTotalPoints
     *
     * @param float $boatTotalPoints
     * @return Boats
     */
    public function setBoatTotalPoints($boatTotalPoints)
    {
        $this->BoatTotalPoints = $boatTotalPoints;

        return $this;
    }

    /**
     * Get BoatTotalPoints
     *
     * @return float 
     */
    public function getBoatTotalPoints()
    {
        return $this->BoatTotalPoints;
    }

    /**
     * Set BoatNetPoints
     *
     * @param float $boatNetPoints
     * @return Boats
     */
    public function setBoatNetPoints($boatNetPoints)
    {
        $this->BoatNetPoints = $boatNetPoints;

        return $this;
    }

    /**
     * Get BoatNetPoints
     *
     * @return float 
     */
    public function getBoatNetPoints()
    {
        return $this->BoatNetPoints;
    }

    /**
     * Get BoatId
     *
     * @return integer 
     */
    public function getBoatId()
    {
        return $this->BoatId;
    }

    /**
     * Set boatBtGrpId
     *
     * @param \worldsailing\Isaf\model\Entity\Boatgroup $boatBtGrpId
     * @return Boats
     */
    public function setBoatBtGrpId(\worldsailing\Isaf\model\Entity\Boatgroup $boatBtGrpId = null)
    {
        $this->boatBtGrpId = $boatBtGrpId;

        return $this;
    }

    /**
     * Get boatBtGrpId
     *
     * @return \worldsailing\Isaf\model\Entity\Boatgroup
     */
    public function getBoatBtGrpId()
    {
        return $this->boatBtGrpId;
    }
}
