<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Auditlog
 *
 * @ORM\Table(name="AuditLog")
 * @ORM\Entity
 */
class Auditlog
{
    /**
     * @var string
     *
     * @ORM\Column(name="LogUserId", type="string", length=45, nullable=false)
     */
    protected $LogUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LogDate", type="datetime", nullable=false)
     */
    protected $LogDate;

    /**
     * @var string
     *
     * @ORM\Column(name="LogParameters", type="blob", length=65535, nullable=true)
     */
    protected $LogParameters;

    /**
     * @var string
     *
     * @ORM\Column(name="LogCategory", type="string", length=100, nullable=true)
     */
    protected $LogCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="LogPath", type="string", length=256, nullable=true)
     */
    protected $LogPath;

    /**
     * @var string
     *
     * @ORM\Column(name="LogKey", type="string", length=256, nullable=true)
     */
    protected $LogKey;

    /**
     * @var string
     *
     * @ORM\Column(name="LogTitle", type="string", length=45, nullable=true)
     */
    protected $LogTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="LogId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $LogId;



    /**
     * Set LogUserId
     *
     * @param string $logUserId
     * @return Auditlog
     */
    public function setLogUserId($logUserId)
    {
        $this->LogUserId = $logUserId;

        return $this;
    }

    /**
     * Get LogUserId
     *
     * @return string 
     */
    public function getLogUserId()
    {
        return $this->LogUserId;
    }

    /**
     * Set LogDate
     *
     * @param \DateTime $logDate
     * @return Auditlog
     */
    public function setLogDate($logDate)
    {
        $this->LogDate = $logDate;

        return $this;
    }

    /**
     * Get LogDate
     *
     * @return \DateTime 
     */
    public function getLogDate()
    {
        return $this->LogDate;
    }

    /**
     * Set LogParameters
     *
     * @param string $logParameters
     * @return Auditlog
     */
    public function setLogParameters($logParameters)
    {
        $this->LogParameters = $logParameters;

        return $this;
    }

    /**
     * Get LogParameters
     *
     * @return string 
     */
    public function getLogParameters()
    {
        return $this->LogParameters;
    }

    /**
     * Set LogCategory
     *
     * @param string $logCategory
     * @return Auditlog
     */
    public function setLogCategory($logCategory)
    {
        $this->LogCategory = $logCategory;

        return $this;
    }

    /**
     * Get LogCategory
     *
     * @return string 
     */
    public function getLogCategory()
    {
        return $this->LogCategory;
    }

    /**
     * Set LogPath
     *
     * @param string $logPath
     * @return Auditlog
     */
    public function setLogPath($logPath)
    {
        $this->LogPath = $logPath;

        return $this;
    }

    /**
     * Get LogPath
     *
     * @return string 
     */
    public function getLogPath()
    {
        return $this->LogPath;
    }

    /**
     * Set LogKey
     *
     * @param string $logKey
     * @return Auditlog
     */
    public function setLogKey($logKey)
    {
        $this->LogKey = $logKey;

        return $this;
    }

    /**
     * Get LogKey
     *
     * @return string 
     */
    public function getLogKey()
    {
        return $this->LogKey;
    }

    /**
     * Set LogTitle
     *
     * @param string $logTitle
     * @return Auditlog
     */
    public function setLogTitle($logTitle)
    {
        $this->LogTitle = $logTitle;

        return $this;
    }

    /**
     * Get LogTitle
     *
     * @return string 
     */
    public function getLogTitle()
    {
        return $this->LogTitle;
    }

    /**
     * Get LogId
     *
     * @return integer 
     */
    public function getLogId()
    {
        return $this->LogId;
    }
}
