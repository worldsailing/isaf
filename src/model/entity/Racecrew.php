<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Racecrew
 *
 * @ORM\Table(name="RaceCrew", indexes={@ORM\Index(name="fk_rcrew_member", columns={"RcMembId"}), @ORM\Index(name="fk_rcrew_race", columns={"RcRaceId"}), @ORM\Index(name="fk_rcrew_boat", columns={"RcBoatId"})})
 * @ORM\Entity
 */
class Racecrew
{
    /**
     * @var \worldsailing\Isaf\model\Entity\Races
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="worldsailing\Isaf\model\Entity\Races")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RcRaceId", referencedColumnName="RaceId")
     * })
     */
    protected $rcRaceId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Memberbiogs
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="worldsailing\Isaf\model\Entity\Memberbiogs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RcMembId", referencedColumnName="BiogMembId")
     * })
     */
    protected $rcMembId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Boats
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="worldsailing\Isaf\model\Entity\Boats")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RcBoatId", referencedColumnName="BoatId")
     * })
     */
    protected $rcBoatId;



    /**
     * Set rcRaceId
     *
     * @param \worldsailing\Isaf\model\Entity\Races $rcRaceId
     * @return Racecrew
     */
    public function setRcRaceId(\worldsailing\Isaf\model\Entity\Races $rcRaceId)
    {
        $this->rcRaceId = $rcRaceId;

        return $this;
    }

    /**
     * Get rcRaceId
     *
     * @return \worldsailing\Isaf\model\Entity\Races
     */
    public function getRcRaceId()
    {
        return $this->rcRaceId;
    }

    /**
     * Set rcMembId
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $rcMembId
     * @return Racecrew
     */
    public function setRcMembId(\worldsailing\Isaf\model\Entity\Memberbiogs $rcMembId)
    {
        $this->rcMembId = $rcMembId;

        return $this;
    }

    /**
     * Get rcMembId
     *
     * @return \worldsailing\Isaf\model\Entity\Memberbiogs
     */
    public function getRcMembId()
    {
        return $this->rcMembId;
    }

    /**
     * Set rcBoatId
     *
     * @param \worldsailing\Isaf\model\Entity\Boats $rcBoatId
     * @return Racecrew
     */
    public function setRcBoatId(\worldsailing\Isaf\model\Entity\Boats $rcBoatId)
    {
        $this->rcBoatId = $rcBoatId;

        return $this;
    }

    /**
     * Get rcBoatId
     *
     * @return \worldsailing\Isaf\model\Entity\Boats
     */
    public function getRcBoatId()
    {
        return $this->rcBoatId;
    }
}
