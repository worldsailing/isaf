<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rankings
 *
 * @ORM\Table(name="Rankings", indexes={@ORM\Index(name="idRankIdStatus", columns={"RankId", "RankStatus"})})
 * @ORM\Entity
 */
class Rankings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RankDisId", type="integer", nullable=false)
     */
    protected $RankDisId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankTypeId", type="integer", nullable=false)
     */
    protected $RankTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankClassId", type="integer", nullable=false)
     */
    protected $RankClassId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RankDate", type="date", nullable=false)
     */
    protected $RankDate;

    /**
     * @var string
     *
     * @ORM\Column(name="RankStatus", type="string", nullable=false)
     */
    protected $RankStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RankId;



    /**
     * Set RankDisId
     *
     * @param integer $rankDisId
     * @return Rankings
     */
    public function setRankDisId($rankDisId)
    {
        $this->RankDisId = $rankDisId;

        return $this;
    }

    /**
     * Get RankDisId
     *
     * @return integer 
     */
    public function getRankDisId()
    {
        return $this->RankDisId;
    }

    /**
     * Set RankTypeId
     *
     * @param integer $rankTypeId
     * @return Rankings
     */
    public function setRankTypeId($rankTypeId)
    {
        $this->RankTypeId = $rankTypeId;

        return $this;
    }

    /**
     * Get RankTypeId
     *
     * @return integer 
     */
    public function getRankTypeId()
    {
        return $this->RankTypeId;
    }

    /**
     * Set RankClassId
     *
     * @param integer $rankClassId
     * @return Rankings
     */
    public function setRankClassId($rankClassId)
    {
        $this->RankClassId = $rankClassId;

        return $this;
    }

    /**
     * Get RankClassId
     *
     * @return integer 
     */
    public function getRankClassId()
    {
        return $this->RankClassId;
    }

    /**
     * Set RankDate
     *
     * @param \DateTime $rankDate
     * @return Rankings
     */
    public function setRankDate($rankDate)
    {
        $this->RankDate = $rankDate;

        return $this;
    }

    /**
     * Get RankDate
     *
     * @return \DateTime 
     */
    public function getRankDate()
    {
        return $this->RankDate;
    }

    /**
     * Set RankStatus
     *
     * @param string $rankStatus
     * @return Rankings
     */
    public function setRankStatus($rankStatus)
    {
        $this->RankStatus = $rankStatus;

        return $this;
    }

    /**
     * Get RankStatus
     *
     * @return string 
     */
    public function getRankStatus()
    {
        return $this->RankStatus;
    }

    /**
     * Get RankId
     *
     * @return integer 
     */
    public function getRankId()
    {
        return $this->RankId;
    }
}
