<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logaudit
 *
 * @ORM\Table(name="LogAudit")
 * @ORM\Entity
 */
class Logaudit
{
    /**
     * @var string
     *
     * @ORM\Column(name="LogKey", type="string", length=50, nullable=false)
     */
    protected $LogKey;

    /**
     * @var string
     *
     * @ORM\Column(name="LogData", type="text", length=65535, nullable=false)
     */
    protected $LogData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LogTime", type="datetime", nullable=false)
     */
    protected $LogTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="LogId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $LogId;



    /**
     * Set LogKey
     *
     * @param string $logKey
     * @return Logaudit
     */
    public function setLogKey($logKey)
    {
        $this->LogKey = $logKey;

        return $this;
    }

    /**
     * Get LogKey
     *
     * @return string 
     */
    public function getLogKey()
    {
        return $this->LogKey;
    }

    /**
     * Set LogData
     *
     * @param string $logData
     * @return Logaudit
     */
    public function setLogData($logData)
    {
        $this->LogData = $logData;

        return $this;
    }

    /**
     * Get LogData
     *
     * @return string 
     */
    public function getLogData()
    {
        return $this->LogData;
    }

    /**
     * Set LogTime
     *
     * @param \DateTime $logTime
     * @return Logaudit
     */
    public function setLogTime($logTime)
    {
        $this->LogTime = $logTime;

        return $this;
    }

    /**
     * Get LogTime
     *
     * @return \DateTime 
     */
    public function getLogTime()
    {
        return $this->LogTime;
    }

    /**
     * Get LogId
     *
     * @return integer 
     */
    public function getLogId()
    {
        return $this->LogId;
    }
}
