<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Registrations
 *
 * @ORM\Table(name="Registrations")
 * @ORM\Entity
 */
class Registrations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RegCtryId", type="integer", nullable=false)
     */
    protected $RegCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RegRgtaId", type="integer", nullable=false)
     */
    protected $RegRgtaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RegEvntId", type="integer", nullable=false)
     */
    protected $RegEvntId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RegMembId", type="integer", nullable=false)
     */
    protected $RegMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RegRoleId", type="integer", nullable=false)
     */
    protected $RegRoleId;

    /**
     * @var string
     *
     * @ORM\Column(name="RegStatus", type="string", nullable=false)
     */
    protected $RegStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="RegStage", type="string", nullable=false)
     */
    protected $RegStage;

    /**
     * @var integer
     *
     * @ORM\Column(name="RegTransId", type="integer", nullable=true)
     */
    protected $RegTransId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RegPayment", type="integer", nullable=false)
     */
    protected $RegPayment;

    /**
     * @var string
     *
     * @ORM\Column(name="RegSections", type="text", length=65535, nullable=false)
     */
    protected $RegSections;

    /**
     * @var integer
     *
     * @ORM\Column(name="RegCreateMembId", type="integer", nullable=true)
     */
    protected $RegCreateMembId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RegCreated", type="datetime", nullable=false)
     */
    protected $RegCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RegModified", type="datetime", nullable=false)
     */
    protected $RegModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RegInvitationDeadline", type="datetime", nullable=true)
     */
    protected $RegInvitationDeadline;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RegEarlyPaymentDeadline", type="datetime", nullable=true)
     */
    protected $RegEarlyPaymentDeadline;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RegPaymentDeadline", type="datetime", nullable=true)
     */
    protected $RegPaymentDeadline;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RegRegattaDeadline", type="datetime", nullable=true)
     */
    protected $RegRegattaDeadline;

    /**
     * @var integer
     *
     * @ORM\Column(name="RegId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RegId;



    /**
     * Set RegCtryId
     *
     * @param integer $regCtryId
     * @return Registrations
     */
    public function setRegCtryId($regCtryId)
    {
        $this->RegCtryId = $regCtryId;

        return $this;
    }

    /**
     * Get RegCtryId
     *
     * @return integer 
     */
    public function getRegCtryId()
    {
        return $this->RegCtryId;
    }

    /**
     * Set RegRgtaId
     *
     * @param integer $regRgtaId
     * @return Registrations
     */
    public function setRegRgtaId($regRgtaId)
    {
        $this->RegRgtaId = $regRgtaId;

        return $this;
    }

    /**
     * Get RegRgtaId
     *
     * @return integer 
     */
    public function getRegRgtaId()
    {
        return $this->RegRgtaId;
    }

    /**
     * Set RegEvntId
     *
     * @param integer $regEvntId
     * @return Registrations
     */
    public function setRegEvntId($regEvntId)
    {
        $this->RegEvntId = $regEvntId;

        return $this;
    }

    /**
     * Get RegEvntId
     *
     * @return integer 
     */
    public function getRegEvntId()
    {
        return $this->RegEvntId;
    }

    /**
     * Set RegMembId
     *
     * @param integer $regMembId
     * @return Registrations
     */
    public function setRegMembId($regMembId)
    {
        $this->RegMembId = $regMembId;

        return $this;
    }

    /**
     * Get RegMembId
     *
     * @return integer 
     */
    public function getRegMembId()
    {
        return $this->RegMembId;
    }

    /**
     * Set RegRoleId
     *
     * @param integer $regRoleId
     * @return Registrations
     */
    public function setRegRoleId($regRoleId)
    {
        $this->RegRoleId = $regRoleId;

        return $this;
    }

    /**
     * Get RegRoleId
     *
     * @return integer 
     */
    public function getRegRoleId()
    {
        return $this->RegRoleId;
    }

    /**
     * Set RegStatus
     *
     * @param string $regStatus
     * @return Registrations
     */
    public function setRegStatus($regStatus)
    {
        $this->RegStatus = $regStatus;

        return $this;
    }

    /**
     * Get RegStatus
     *
     * @return string 
     */
    public function getRegStatus()
    {
        return $this->RegStatus;
    }

    /**
     * Set RegStage
     *
     * @param string $regStage
     * @return Registrations
     */
    public function setRegStage($regStage)
    {
        $this->RegStage = $regStage;

        return $this;
    }

    /**
     * Get RegStage
     *
     * @return string 
     */
    public function getRegStage()
    {
        return $this->RegStage;
    }

    /**
     * Set RegTransId
     *
     * @param integer $regTransId
     * @return Registrations
     */
    public function setRegTransId($regTransId)
    {
        $this->RegTransId = $regTransId;

        return $this;
    }

    /**
     * Get RegTransId
     *
     * @return integer 
     */
    public function getRegTransId()
    {
        return $this->RegTransId;
    }

    /**
     * Set RegPayment
     *
     * @param integer $regPayment
     * @return Registrations
     */
    public function setRegPayment($regPayment)
    {
        $this->RegPayment = $regPayment;

        return $this;
    }

    /**
     * Get RegPayment
     *
     * @return integer 
     */
    public function getRegPayment()
    {
        return $this->RegPayment;
    }

    /**
     * Set RegSections
     *
     * @param string $regSections
     * @return Registrations
     */
    public function setRegSections($regSections)
    {
        $this->RegSections = $regSections;

        return $this;
    }

    /**
     * Get RegSections
     *
     * @return string 
     */
    public function getRegSections()
    {
        return $this->RegSections;
    }

    /**
     * Set RegCreateMembId
     *
     * @param integer $regCreateMembId
     * @return Registrations
     */
    public function setRegCreateMembId($regCreateMembId)
    {
        $this->RegCreateMembId = $regCreateMembId;

        return $this;
    }

    /**
     * Get RegCreateMembId
     *
     * @return integer 
     */
    public function getRegCreateMembId()
    {
        return $this->RegCreateMembId;
    }

    /**
     * Set RegCreated
     *
     * @param \DateTime $regCreated
     * @return Registrations
     */
    public function setRegCreated($regCreated)
    {
        $this->RegCreated = $regCreated;

        return $this;
    }

    /**
     * Get RegCreated
     *
     * @return \DateTime 
     */
    public function getRegCreated()
    {
        return $this->RegCreated;
    }

    /**
     * Set RegModified
     *
     * @param \DateTime $regModified
     * @return Registrations
     */
    public function setRegModified($regModified)
    {
        $this->RegModified = $regModified;

        return $this;
    }

    /**
     * Get RegModified
     *
     * @return \DateTime 
     */
    public function getRegModified()
    {
        return $this->RegModified;
    }

    /**
     * Set RegInvitationDeadline
     *
     * @param \DateTime $regInvitationDeadline
     * @return Registrations
     */
    public function setRegInvitationDeadline($regInvitationDeadline)
    {
        $this->RegInvitationDeadline = $regInvitationDeadline;

        return $this;
    }

    /**
     * Get RegInvitationDeadline
     *
     * @return \DateTime 
     */
    public function getRegInvitationDeadline()
    {
        return $this->RegInvitationDeadline;
    }

    /**
     * Set RegEarlyPaymentDeadline
     *
     * @param \DateTime $regEarlyPaymentDeadline
     * @return Registrations
     */
    public function setRegEarlyPaymentDeadline($regEarlyPaymentDeadline)
    {
        $this->RegEarlyPaymentDeadline = $regEarlyPaymentDeadline;

        return $this;
    }

    /**
     * Get RegEarlyPaymentDeadline
     *
     * @return \DateTime 
     */
    public function getRegEarlyPaymentDeadline()
    {
        return $this->RegEarlyPaymentDeadline;
    }

    /**
     * Set RegPaymentDeadline
     *
     * @param \DateTime $regPaymentDeadline
     * @return Registrations
     */
    public function setRegPaymentDeadline($regPaymentDeadline)
    {
        $this->RegPaymentDeadline = $regPaymentDeadline;

        return $this;
    }

    /**
     * Get RegPaymentDeadline
     *
     * @return \DateTime 
     */
    public function getRegPaymentDeadline()
    {
        return $this->RegPaymentDeadline;
    }

    /**
     * Set RegRegattaDeadline
     *
     * @param \DateTime $regRegattaDeadline
     * @return Registrations
     */
    public function setRegRegattaDeadline($regRegattaDeadline)
    {
        $this->RegRegattaDeadline = $regRegattaDeadline;

        return $this;
    }

    /**
     * Get RegRegattaDeadline
     *
     * @return \DateTime 
     */
    public function getRegRegattaDeadline()
    {
        return $this->RegRegattaDeadline;
    }

    /**
     * Get RegId
     *
     * @return integer 
     */
    public function getRegId()
    {
        return $this->RegId;
    }
}
