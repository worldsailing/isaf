<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timedemailhash
 *
 * @ORM\Table(name="TimedEmailHash", uniqueConstraints={@ORM\UniqueConstraint(name="uq_hash", columns={"HashType", "HashCode"})})
 * @ORM\Entity
 */
class Timedemailhash
{
    /**
     * @var string
     *
     * @ORM\Column(name="HashCode", type="string", length=45, nullable=false)
     */
    protected $HashCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="HashExpiry", type="datetime", nullable=true)
     */
    protected $HashExpiry;

    /**
     * @var boolean
     *
     * @ORM\Column(name="HashType", type="boolean")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $HashType;

    /**
     * @var integer
     *
     * @ORM\Column(name="HashPk", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $HashPk;



    /**
     * Set HashCode
     *
     * @param string $hashCode
     * @return Timedemailhash
     */
    public function setHashCode($hashCode)
    {
        $this->HashCode = $hashCode;

        return $this;
    }

    /**
     * Get HashCode
     *
     * @return string 
     */
    public function getHashCode()
    {
        return $this->HashCode;
    }

    /**
     * Set HashExpiry
     *
     * @param \DateTime $hashExpiry
     * @return Timedemailhash
     */
    public function setHashExpiry($hashExpiry)
    {
        $this->HashExpiry = $hashExpiry;

        return $this;
    }

    /**
     * Get HashExpiry
     *
     * @return \DateTime 
     */
    public function getHashExpiry()
    {
        return $this->HashExpiry;
    }

    /**
     * Set HashType
     *
     * @param boolean $hashType
     * @return Timedemailhash
     */
    public function setHashType($hashType)
    {
        $this->HashType = $hashType;

        return $this;
    }

    /**
     * Get HashType
     *
     * @return boolean 
     */
    public function getHashType()
    {
        return $this->HashType;
    }

    /**
     * Set HashPk
     *
     * @param integer $hashPk
     * @return Timedemailhash
     */
    public function setHashPk($hashPk)
    {
        $this->HashPk = $hashPk;

        return $this;
    }

    /**
     * Get HashPk
     *
     * @return integer 
     */
    public function getHashPk()
    {
        return $this->HashPk;
    }
}
