<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="Events", indexes={@ORM\Index(name="EvntRgtaId", columns={"EvntRgtaId"}), @ORM\Index(name="EvntStatus", columns={"EvntStatus"})})
 * @ORM\Entity
 */
class Events
{
    /**
     * @var integer
     *
     * @ORM\Column(name="EvntOrigId", type="integer", nullable=true)
     */
    protected $EvntOrigId;

    /**
     * @var string
     *
     * @ORM\Column(name="EvntOrigClass", type="string", length=10, nullable=true)
     */
    protected $EvntOrigClass;

    /**
     * @var boolean
     *
     * @ORM\Column(name="EvntOrigMoe", type="boolean", nullable=false)
     */
    protected $EvntOrigMoe;

    /**
     * @var integer
     *
     * @ORM\Column(name="EvntRgtaId", type="integer", nullable=false)
     */
    protected $EvntRgtaId;

    /**
     * @var string
     *
     * @ORM\Column(name="EvntName", type="string", length=255, nullable=true)
     */
    protected $EvntName;

    /**
     * @var integer
     *
     * @ORM\Column(name="EvntDisId", type="integer", nullable=true)
     */
    protected $EvntDisId;

    /**
     * @var integer
     *
     * @ORM\Column(name="EvntTypeId", type="integer", nullable=true)
     */
    protected $EvntTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="EvntClassId", type="integer", nullable=true)
     */
    protected $EvntClassId;

    /**
     * @var integer
     *
     * @ORM\Column(name="EvntGradeId", type="integer", nullable=true)
     */
    protected $EvntGradeId;

    /**
     * @var string
     *
     * @ORM\Column(name="EvntSrcId", type="string", length=45, nullable=true)
     */
    protected $EvntSrcId;

    /**
     * @var float
     *
     * @ORM\Column(name="EvntMultiplier", type="float", precision=10, scale=0, nullable=false)
     */
    protected $EvntMultiplier;

    /**
     * @var float
     *
     * @ORM\Column(name="EvntBonus", type="float", precision=10, scale=0, nullable=false)
     */
    protected $EvntBonus;

    /**
     * @var float
     *
     * @ORM\Column(name="EvntQuality", type="float", precision=10, scale=0, nullable=false)
     */
    protected $EvntQuality;

    /**
     * @var integer
     *
     * @ORM\Column(name="EvntCompetitors", type="integer", nullable=true)
     */
    protected $EvntCompetitors;

    /**
     * @var string
     *
     * @ORM\Column(name="EvntStatus", type="string", nullable=false)
     */
    protected $EvntStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="EvntRank", type="integer", nullable=false)
     */
    protected $EvntRank;

    /**
     * @var boolean
     *
     * @ORM\Column(name="EvntSingleHander", type="boolean", nullable=true)
     */
    protected $EvntSingleHander;

    /**
     * @var string
     *
     * @ORM\Column(name="EvntNatsCtEst", type="string", length=100, nullable=true)
     */
    protected $EvntNatsCtEst;

    /**
     * @var string
     *
     * @ORM\Column(name="EvntComptsCtEst", type="string", length=100, nullable=true)
     */
    protected $EvntComptsCtEst;

    /**
     * @var integer
     *
     * @ORM\Column(name="EvntId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $EvntId;



    /**
     * Set EvntOrigId
     *
     * @param integer $evntOrigId
     * @return Events
     */
    public function setEvntOrigId($evntOrigId)
    {
        $this->EvntOrigId = $evntOrigId;

        return $this;
    }

    /**
     * Get EvntOrigId
     *
     * @return integer 
     */
    public function getEvntOrigId()
    {
        return $this->EvntOrigId;
    }

    /**
     * Set EvntOrigClass
     *
     * @param string $evntOrigClass
     * @return Events
     */
    public function setEvntOrigClass($evntOrigClass)
    {
        $this->EvntOrigClass = $evntOrigClass;

        return $this;
    }

    /**
     * Get EvntOrigClass
     *
     * @return string 
     */
    public function getEvntOrigClass()
    {
        return $this->EvntOrigClass;
    }

    /**
     * Set EvntOrigMoe
     *
     * @param boolean $evntOrigMoe
     * @return Events
     */
    public function setEvntOrigMoe($evntOrigMoe)
    {
        $this->EvntOrigMoe = $evntOrigMoe;

        return $this;
    }

    /**
     * Get EvntOrigMoe
     *
     * @return boolean 
     */
    public function getEvntOrigMoe()
    {
        return $this->EvntOrigMoe;
    }

    /**
     * Set EvntRgtaId
     *
     * @param integer $evntRgtaId
     * @return Events
     */
    public function setEvntRgtaId($evntRgtaId)
    {
        $this->EvntRgtaId = $evntRgtaId;

        return $this;
    }

    /**
     * Get EvntRgtaId
     *
     * @return integer 
     */
    public function getEvntRgtaId()
    {
        return $this->EvntRgtaId;
    }

    /**
     * Set EvntName
     *
     * @param string $evntName
     * @return Events
     */
    public function setEvntName($evntName)
    {
        $this->EvntName = $evntName;

        return $this;
    }

    /**
     * Get EvntName
     *
     * @return string 
     */
    public function getEvntName()
    {
        return $this->EvntName;
    }

    /**
     * Set EvntDisId
     *
     * @param integer $evntDisId
     * @return Events
     */
    public function setEvntDisId($evntDisId)
    {
        $this->EvntDisId = $evntDisId;

        return $this;
    }

    /**
     * Get EvntDisId
     *
     * @return integer 
     */
    public function getEvntDisId()
    {
        return $this->EvntDisId;
    }

    /**
     * Set EvntTypeId
     *
     * @param integer $evntTypeId
     * @return Events
     */
    public function setEvntTypeId($evntTypeId)
    {
        $this->EvntTypeId = $evntTypeId;

        return $this;
    }

    /**
     * Get EvntTypeId
     *
     * @return integer 
     */
    public function getEvntTypeId()
    {
        return $this->EvntTypeId;
    }

    /**
     * Set EvntClassId
     *
     * @param integer $evntClassId
     * @return Events
     */
    public function setEvntClassId($evntClassId)
    {
        $this->EvntClassId = $evntClassId;

        return $this;
    }

    /**
     * Get EvntClassId
     *
     * @return integer 
     */
    public function getEvntClassId()
    {
        return $this->EvntClassId;
    }

    /**
     * Set EvntGradeId
     *
     * @param integer $evntGradeId
     * @return Events
     */
    public function setEvntGradeId($evntGradeId)
    {
        $this->EvntGradeId = $evntGradeId;

        return $this;
    }

    /**
     * Get EvntGradeId
     *
     * @return integer 
     */
    public function getEvntGradeId()
    {
        return $this->EvntGradeId;
    }

    /**
     * Set EvntSrcId
     *
     * @param string $evntSrcId
     * @return Events
     */
    public function setEvntSrcId($evntSrcId)
    {
        $this->EvntSrcId = $evntSrcId;

        return $this;
    }

    /**
     * Get EvntSrcId
     *
     * @return string 
     */
    public function getEvntSrcId()
    {
        return $this->EvntSrcId;
    }

    /**
     * Set EvntMultiplier
     *
     * @param float $evntMultiplier
     * @return Events
     */
    public function setEvntMultiplier($evntMultiplier)
    {
        $this->EvntMultiplier = $evntMultiplier;

        return $this;
    }

    /**
     * Get EvntMultiplier
     *
     * @return float 
     */
    public function getEvntMultiplier()
    {
        return $this->EvntMultiplier;
    }

    /**
     * Set EvntBonus
     *
     * @param float $evntBonus
     * @return Events
     */
    public function setEvntBonus($evntBonus)
    {
        $this->EvntBonus = $evntBonus;

        return $this;
    }

    /**
     * Get EvntBonus
     *
     * @return float 
     */
    public function getEvntBonus()
    {
        return $this->EvntBonus;
    }

    /**
     * Set EvntQuality
     *
     * @param float $evntQuality
     * @return Events
     */
    public function setEvntQuality($evntQuality)
    {
        $this->EvntQuality = $evntQuality;

        return $this;
    }

    /**
     * Get EvntQuality
     *
     * @return float 
     */
    public function getEvntQuality()
    {
        return $this->EvntQuality;
    }

    /**
     * Set EvntCompetitors
     *
     * @param integer $evntCompetitors
     * @return Events
     */
    public function setEvntCompetitors($evntCompetitors)
    {
        $this->EvntCompetitors = $evntCompetitors;

        return $this;
    }

    /**
     * Get EvntCompetitors
     *
     * @return integer 
     */
    public function getEvntCompetitors()
    {
        return $this->EvntCompetitors;
    }

    /**
     * Set EvntStatus
     *
     * @param string $evntStatus
     * @return Events
     */
    public function setEvntStatus($evntStatus)
    {
        $this->EvntStatus = $evntStatus;

        return $this;
    }

    /**
     * Get EvntStatus
     *
     * @return string 
     */
    public function getEvntStatus()
    {
        return $this->EvntStatus;
    }

    /**
     * Set EvntRank
     *
     * @param integer $evntRank
     * @return Events
     */
    public function setEvntRank($evntRank)
    {
        $this->EvntRank = $evntRank;

        return $this;
    }

    /**
     * Get EvntRank
     *
     * @return integer 
     */
    public function getEvntRank()
    {
        return $this->EvntRank;
    }

    /**
     * Set EvntSingleHander
     *
     * @param boolean $evntSingleHander
     * @return Events
     */
    public function setEvntSingleHander($evntSingleHander)
    {
        $this->EvntSingleHander = $evntSingleHander;

        return $this;
    }

    /**
     * Get EvntSingleHander
     *
     * @return boolean 
     */
    public function getEvntSingleHander()
    {
        return $this->EvntSingleHander;
    }

    /**
     * Set EvntNatsCtEst
     *
     * @param string $evntNatsCtEst
     * @return Events
     */
    public function setEvntNatsCtEst($evntNatsCtEst)
    {
        $this->EvntNatsCtEst = $evntNatsCtEst;

        return $this;
    }

    /**
     * Get EvntNatsCtEst
     *
     * @return string 
     */
    public function getEvntNatsCtEst()
    {
        return $this->EvntNatsCtEst;
    }

    /**
     * Set EvntComptsCtEst
     *
     * @param string $evntComptsCtEst
     * @return Events
     */
    public function setEvntComptsCtEst($evntComptsCtEst)
    {
        $this->EvntComptsCtEst = $evntComptsCtEst;

        return $this;
    }

    /**
     * Get EvntComptsCtEst
     *
     * @return string 
     */
    public function getEvntComptsCtEst()
    {
        return $this->EvntComptsCtEst;
    }

    /**
     * Get EvntId
     *
     * @return integer 
     */
    public function getEvntId()
    {
        return $this->EvntId;
    }
}
