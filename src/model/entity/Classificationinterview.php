<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationinterview
 *
 * @ORM\Table(name="ClassificationInterview", indexes={@ORM\Index(name="fk_ivw_action", columns={"CfcnIvwActionId"})})
 * @ORM\Entity
 */
class Classificationinterview
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnIvwDate", type="date", nullable=true)
     */
    protected $CfcnIvwDate;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnIvwActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnIvwActionId;



    /**
     * Set CfcnIvwDate
     *
     * @param \DateTime $cfcnIvwDate
     * @return Classificationinterview
     */
    public function setCfcnIvwDate($cfcnIvwDate)
    {
        $this->CfcnIvwDate = $cfcnIvwDate;

        return $this;
    }

    /**
     * Get CfcnIvwDate
     *
     * @return \DateTime 
     */
    public function getCfcnIvwDate()
    {
        return $this->CfcnIvwDate;
    }

    /**
     * Set cfcnIvwActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnIvwActionId
     * @return Classificationinterview
     */
    public function setCfcnIvwActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnIvwActionId)
    {
        $this->cfcnIvwActionId = $cfcnIvwActionId;

        return $this;
    }

    /**
     * Get cfcnIvwActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnIvwActionId()
    {
        return $this->cfcnIvwActionId;
    }
}
