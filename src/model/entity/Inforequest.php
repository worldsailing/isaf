<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inforequest
 *
 * @ORM\Table(name="InfoRequest")
 * @ORM\Entity
 */
class Inforequest
{
    /**
     * @var string
     *
     * @ORM\Column(name="InfSource", type="string", length=20, nullable=true)
     */
    protected $InfSource;

    /**
     * @var integer
     *
     * @ORM\Column(name="InfSrcId", type="integer", nullable=false)
     */
    protected $InfSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="InfRgtaId", type="integer", nullable=false)
     */
    protected $InfRgtaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="InfTeamId", type="integer", nullable=false)
     */
    protected $InfTeamId;

    /**
     * @var integer
     *
     * @ORM\Column(name="InfEvntId", type="integer", nullable=false)
     */
    protected $InfEvntId;

    /**
     * @var string
     *
     * @ORM\Column(name="InfRequest", type="text", length=65535, nullable=true)
     */
    protected $InfRequest;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="InfRequestDate", type="date", nullable=true)
     */
    protected $InfRequestDate;

    /**
     * @var string
     *
     * @ORM\Column(name="InfReply", type="text", length=65535, nullable=true)
     */
    protected $InfReply;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="InfReplyDate", type="date", nullable=true)
     */
    protected $InfReplyDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="InfId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $InfId;



    /**
     * Set InfSource
     *
     * @param string $infSource
     * @return Inforequest
     */
    public function setInfSource($infSource)
    {
        $this->InfSource = $infSource;

        return $this;
    }

    /**
     * Get InfSource
     *
     * @return string 
     */
    public function getInfSource()
    {
        return $this->InfSource;
    }

    /**
     * Set InfSrcId
     *
     * @param integer $infSrcId
     * @return Inforequest
     */
    public function setInfSrcId($infSrcId)
    {
        $this->InfSrcId = $infSrcId;

        return $this;
    }

    /**
     * Get InfSrcId
     *
     * @return integer 
     */
    public function getInfSrcId()
    {
        return $this->InfSrcId;
    }

    /**
     * Set InfRgtaId
     *
     * @param integer $infRgtaId
     * @return Inforequest
     */
    public function setInfRgtaId($infRgtaId)
    {
        $this->InfRgtaId = $infRgtaId;

        return $this;
    }

    /**
     * Get InfRgtaId
     *
     * @return integer 
     */
    public function getInfRgtaId()
    {
        return $this->InfRgtaId;
    }

    /**
     * Set InfTeamId
     *
     * @param integer $infTeamId
     * @return Inforequest
     */
    public function setInfTeamId($infTeamId)
    {
        $this->InfTeamId = $infTeamId;

        return $this;
    }

    /**
     * Get InfTeamId
     *
     * @return integer 
     */
    public function getInfTeamId()
    {
        return $this->InfTeamId;
    }

    /**
     * Set InfEvntId
     *
     * @param integer $infEvntId
     * @return Inforequest
     */
    public function setInfEvntId($infEvntId)
    {
        $this->InfEvntId = $infEvntId;

        return $this;
    }

    /**
     * Get InfEvntId
     *
     * @return integer 
     */
    public function getInfEvntId()
    {
        return $this->InfEvntId;
    }

    /**
     * Set InfRequest
     *
     * @param string $infRequest
     * @return Inforequest
     */
    public function setInfRequest($infRequest)
    {
        $this->InfRequest = $infRequest;

        return $this;
    }

    /**
     * Get InfRequest
     *
     * @return string 
     */
    public function getInfRequest()
    {
        return $this->InfRequest;
    }

    /**
     * Set InfRequestDate
     *
     * @param \DateTime $infRequestDate
     * @return Inforequest
     */
    public function setInfRequestDate($infRequestDate)
    {
        $this->InfRequestDate = $infRequestDate;

        return $this;
    }

    /**
     * Get InfRequestDate
     *
     * @return \DateTime 
     */
    public function getInfRequestDate()
    {
        return $this->InfRequestDate;
    }

    /**
     * Set InfReply
     *
     * @param string $infReply
     * @return Inforequest
     */
    public function setInfReply($infReply)
    {
        $this->InfReply = $infReply;

        return $this;
    }

    /**
     * Get InfReply
     *
     * @return string 
     */
    public function getInfReply()
    {
        return $this->InfReply;
    }

    /**
     * Set InfReplyDate
     *
     * @param \DateTime $infReplyDate
     * @return Inforequest
     */
    public function setInfReplyDate($infReplyDate)
    {
        $this->InfReplyDate = $infReplyDate;

        return $this;
    }

    /**
     * Get InfReplyDate
     *
     * @return \DateTime 
     */
    public function getInfReplyDate()
    {
        return $this->InfReplyDate;
    }

    /**
     * Get InfId
     *
     * @return integer 
     */
    public function getInfId()
    {
        return $this->InfId;
    }
}
