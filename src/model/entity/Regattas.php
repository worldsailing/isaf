<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Regattas
 *
 * @ORM\Table(name="Regattas", indexes={@ORM\Index(name="idRgtaVenCtryIdEndDate", columns={"RgtaVenCtryId", "RgtaEndDate"}), @ORM\Index(name="RgtaApvlStatus", columns={"RgtaApvlStatus"}), @ORM\Index(name="RgtaStartDate", columns={"RgtaStartDate"})})
 * @ORM\Entity
 */
class Regattas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaOrigId", type="integer", nullable=true)
     */
    protected $RgtaOrigId;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOrigCode", type="string", length=200, nullable=true)
     */
    protected $RgtaOrigCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaLevId", type="integer", nullable=true)
     */
    protected $RgtaLevId;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaName", type="string", length=255, nullable=true)
     */
    protected $RgtaName;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaIsafId", type="string", length=15, nullable=true)
     */
    protected $RgtaIsafId;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaSrcId", type="string", length=45, nullable=true)
     */
    protected $RgtaSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaTournId", type="integer", nullable=false)
     */
    protected $RgtaTournId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RgtaStartDate", type="date", nullable=false)
     */
    protected $RgtaStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RgtaEndDate", type="date", nullable=false)
     */
    protected $RgtaEndDate;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaURL", type="string", length=255, nullable=true)
     */
    protected $RgtaURL;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaVenAddress", type="text", length=65535, nullable=true)
     */
    protected $RgtaVenAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaVenCtryId", type="integer", nullable=false)
     */
    protected $RgtaVenCtryId;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOrgName", type="string", length=100, nullable=true)
     */
    protected $RgtaOrgName;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOrgAddress", type="text", length=65535, nullable=true)
     */
    protected $RgtaOrgAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOrgAddressLine2", type="text", length=65535, nullable=true)
     */
    protected $RgtaOrgAddressLine2;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOrgAddressLine3", type="text", length=65535, nullable=true)
     */
    protected $RgtaOrgAddressLine3;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaOrgCtryId", type="integer", nullable=true)
     */
    protected $RgtaOrgCtryId;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOrgPhone", type="string", length=100, nullable=true)
     */
    protected $RgtaOrgPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOrgFax", type="string", length=100, nullable=true)
     */
    protected $RgtaOrgFax;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOrgEmail", type="string", length=100, nullable=true)
     */
    protected $RgtaOrgEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaEntName", type="string", length=100, nullable=true)
     */
    protected $RgtaEntName;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaEntAddress", type="text", length=65535, nullable=true)
     */
    protected $RgtaEntAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaEntAddressLine2", type="text", length=65535, nullable=true)
     */
    protected $RgtaEntAddressLine2;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaEntAddressLine3", type="text", length=65535, nullable=true)
     */
    protected $RgtaEntAddressLine3;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaEntCtryId", type="integer", nullable=true)
     */
    protected $RgtaEntCtryId;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaEntPhone", type="string", length=100, nullable=true)
     */
    protected $RgtaEntPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaEntFax", type="string", length=100, nullable=true)
     */
    protected $RgtaEntFax;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaEntEmail", type="string", length=100, nullable=true)
     */
    protected $RgtaEntEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaPageUrl", type="string", length=50, nullable=true)
     */
    protected $RgtaPageUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaFoldId", type="integer", nullable=false)
     */
    protected $RgtaFoldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaLogoUpldId", type="integer", nullable=false)
     */
    protected $RgtaLogoUpldId;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaRegLink", type="string", length=255, nullable=true)
     */
    protected $RgtaRegLink;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaXRRUploadCode", type="string", length=20, nullable=true)
     */
    protected $RgtaXRRUploadCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RgtaXRRDateCutoff", type="date", nullable=true)
     */
    protected $RgtaXRRDateCutoff;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaOffDocMeta", type="string", length=512, nullable=true)
     */
    protected $RgtaOffDocMeta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RgtaPrizeMoney", type="boolean", nullable=true)
     */
    protected $RgtaPrizeMoney;

    /**
     * @var string
     *
     * @ORM\Column(name="RgtaApvlStatus", type="string", nullable=true)
     */
    protected $RgtaApvlStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaCreateMembId", type="integer", nullable=true)
     */
    protected $RgtaCreateMembId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RgtaHideRegstProg", type="boolean", nullable=true)
     */
    protected $RgtaHideRegstProg;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RgtaShowRegstNoInfo", type="boolean", nullable=true)
     */
    protected $RgtaShowRegstNoInfo;

    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RgtaId;



    /**
     * Set RgtaOrigId
     *
     * @param integer $rgtaOrigId
     * @return Regattas
     */
    public function setRgtaOrigId($rgtaOrigId)
    {
        $this->RgtaOrigId = $rgtaOrigId;

        return $this;
    }

    /**
     * Get RgtaOrigId
     *
     * @return integer 
     */
    public function getRgtaOrigId()
    {
        return $this->RgtaOrigId;
    }

    /**
     * Set RgtaOrigCode
     *
     * @param string $rgtaOrigCode
     * @return Regattas
     */
    public function setRgtaOrigCode($rgtaOrigCode)
    {
        $this->RgtaOrigCode = $rgtaOrigCode;

        return $this;
    }

    /**
     * Get RgtaOrigCode
     *
     * @return string 
     */
    public function getRgtaOrigCode()
    {
        return $this->RgtaOrigCode;
    }

    /**
     * Set RgtaLevId
     *
     * @param integer $rgtaLevId
     * @return Regattas
     */
    public function setRgtaLevId($rgtaLevId)
    {
        $this->RgtaLevId = $rgtaLevId;

        return $this;
    }

    /**
     * Get RgtaLevId
     *
     * @return integer 
     */
    public function getRgtaLevId()
    {
        return $this->RgtaLevId;
    }

    /**
     * Set RgtaName
     *
     * @param string $rgtaName
     * @return Regattas
     */
    public function setRgtaName($rgtaName)
    {
        $this->RgtaName = $rgtaName;

        return $this;
    }

    /**
     * Get RgtaName
     *
     * @return string 
     */
    public function getRgtaName()
    {
        return $this->RgtaName;
    }

    /**
     * Set RgtaIsafId
     *
     * @param string $rgtaIsafId
     * @return Regattas
     */
    public function setRgtaIsafId($rgtaIsafId)
    {
        $this->RgtaIsafId = $rgtaIsafId;

        return $this;
    }

    /**
     * Get RgtaIsafId
     *
     * @return string 
     */
    public function getRgtaIsafId()
    {
        return $this->RgtaIsafId;
    }

    /**
     * Set RgtaSrcId
     *
     * @param string $rgtaSrcId
     * @return Regattas
     */
    public function setRgtaSrcId($rgtaSrcId)
    {
        $this->RgtaSrcId = $rgtaSrcId;

        return $this;
    }

    /**
     * Get RgtaSrcId
     *
     * @return string 
     */
    public function getRgtaSrcId()
    {
        return $this->RgtaSrcId;
    }

    /**
     * Set RgtaTournId
     *
     * @param integer $rgtaTournId
     * @return Regattas
     */
    public function setRgtaTournId($rgtaTournId)
    {
        $this->RgtaTournId = $rgtaTournId;

        return $this;
    }

    /**
     * Get RgtaTournId
     *
     * @return integer 
     */
    public function getRgtaTournId()
    {
        return $this->RgtaTournId;
    }

    /**
     * Set RgtaStartDate
     *
     * @param \DateTime $rgtaStartDate
     * @return Regattas
     */
    public function setRgtaStartDate($rgtaStartDate)
    {
        $this->RgtaStartDate = $rgtaStartDate;

        return $this;
    }

    /**
     * Get RgtaStartDate
     *
     * @return \DateTime 
     */
    public function getRgtaStartDate()
    {
        return $this->RgtaStartDate;
    }

    /**
     * Set RgtaEndDate
     *
     * @param \DateTime $rgtaEndDate
     * @return Regattas
     */
    public function setRgtaEndDate($rgtaEndDate)
    {
        $this->RgtaEndDate = $rgtaEndDate;

        return $this;
    }

    /**
     * Get RgtaEndDate
     *
     * @return \DateTime 
     */
    public function getRgtaEndDate()
    {
        return $this->RgtaEndDate;
    }

    /**
     * Set RgtaURL
     *
     * @param string $rgtaURL
     * @return Regattas
     */
    public function setRgtaURL($rgtaURL)
    {
        $this->RgtaURL = $rgtaURL;

        return $this;
    }

    /**
     * Get RgtaURL
     *
     * @return string 
     */
    public function getRgtaURL()
    {
        return $this->RgtaURL;
    }

    /**
     * Set RgtaVenAddress
     *
     * @param string $rgtaVenAddress
     * @return Regattas
     */
    public function setRgtaVenAddress($rgtaVenAddress)
    {
        $this->RgtaVenAddress = $rgtaVenAddress;

        return $this;
    }

    /**
     * Get RgtaVenAddress
     *
     * @return string 
     */
    public function getRgtaVenAddress()
    {
        return $this->RgtaVenAddress;
    }

    /**
     * Set RgtaVenCtryId
     *
     * @param integer $rgtaVenCtryId
     * @return Regattas
     */
    public function setRgtaVenCtryId($rgtaVenCtryId)
    {
        $this->RgtaVenCtryId = $rgtaVenCtryId;

        return $this;
    }

    /**
     * Get RgtaVenCtryId
     *
     * @return integer 
     */
    public function getRgtaVenCtryId()
    {
        return $this->RgtaVenCtryId;
    }

    /**
     * Set RgtaOrgName
     *
     * @param string $rgtaOrgName
     * @return Regattas
     */
    public function setRgtaOrgName($rgtaOrgName)
    {
        $this->RgtaOrgName = $rgtaOrgName;

        return $this;
    }

    /**
     * Get RgtaOrgName
     *
     * @return string 
     */
    public function getRgtaOrgName()
    {
        return $this->RgtaOrgName;
    }

    /**
     * Set RgtaOrgAddress
     *
     * @param string $rgtaOrgAddress
     * @return Regattas
     */
    public function setRgtaOrgAddress($rgtaOrgAddress)
    {
        $this->RgtaOrgAddress = $rgtaOrgAddress;

        return $this;
    }

    /**
     * Get RgtaOrgAddress
     *
     * @return string 
     */
    public function getRgtaOrgAddress()
    {
        return $this->RgtaOrgAddress;
    }

    /**
     * Set RgtaOrgAddressLine2
     *
     * @param string $rgtaOrgAddressLine2
     * @return Regattas
     */
    public function setRgtaOrgAddressLine2($rgtaOrgAddressLine2)
    {
        $this->RgtaOrgAddressLine2 = $rgtaOrgAddressLine2;

        return $this;
    }

    /**
     * Get RgtaOrgAddressLine2
     *
     * @return string 
     */
    public function getRgtaOrgAddressLine2()
    {
        return $this->RgtaOrgAddressLine2;
    }

    /**
     * Set RgtaOrgAddressLine3
     *
     * @param string $rgtaOrgAddressLine3
     * @return Regattas
     */
    public function setRgtaOrgAddressLine3($rgtaOrgAddressLine3)
    {
        $this->RgtaOrgAddressLine3 = $rgtaOrgAddressLine3;

        return $this;
    }

    /**
     * Get RgtaOrgAddressLine3
     *
     * @return string 
     */
    public function getRgtaOrgAddressLine3()
    {
        return $this->RgtaOrgAddressLine3;
    }

    /**
     * Set RgtaOrgCtryId
     *
     * @param integer $rgtaOrgCtryId
     * @return Regattas
     */
    public function setRgtaOrgCtryId($rgtaOrgCtryId)
    {
        $this->RgtaOrgCtryId = $rgtaOrgCtryId;

        return $this;
    }

    /**
     * Get RgtaOrgCtryId
     *
     * @return integer 
     */
    public function getRgtaOrgCtryId()
    {
        return $this->RgtaOrgCtryId;
    }

    /**
     * Set RgtaOrgPhone
     *
     * @param string $rgtaOrgPhone
     * @return Regattas
     */
    public function setRgtaOrgPhone($rgtaOrgPhone)
    {
        $this->RgtaOrgPhone = $rgtaOrgPhone;

        return $this;
    }

    /**
     * Get RgtaOrgPhone
     *
     * @return string 
     */
    public function getRgtaOrgPhone()
    {
        return $this->RgtaOrgPhone;
    }

    /**
     * Set RgtaOrgFax
     *
     * @param string $rgtaOrgFax
     * @return Regattas
     */
    public function setRgtaOrgFax($rgtaOrgFax)
    {
        $this->RgtaOrgFax = $rgtaOrgFax;

        return $this;
    }

    /**
     * Get RgtaOrgFax
     *
     * @return string 
     */
    public function getRgtaOrgFax()
    {
        return $this->RgtaOrgFax;
    }

    /**
     * Set RgtaOrgEmail
     *
     * @param string $rgtaOrgEmail
     * @return Regattas
     */
    public function setRgtaOrgEmail($rgtaOrgEmail)
    {
        $this->RgtaOrgEmail = $rgtaOrgEmail;

        return $this;
    }

    /**
     * Get RgtaOrgEmail
     *
     * @return string 
     */
    public function getRgtaOrgEmail()
    {
        return $this->RgtaOrgEmail;
    }

    /**
     * Set RgtaEntName
     *
     * @param string $rgtaEntName
     * @return Regattas
     */
    public function setRgtaEntName($rgtaEntName)
    {
        $this->RgtaEntName = $rgtaEntName;

        return $this;
    }

    /**
     * Get RgtaEntName
     *
     * @return string 
     */
    public function getRgtaEntName()
    {
        return $this->RgtaEntName;
    }

    /**
     * Set RgtaEntAddress
     *
     * @param string $rgtaEntAddress
     * @return Regattas
     */
    public function setRgtaEntAddress($rgtaEntAddress)
    {
        $this->RgtaEntAddress = $rgtaEntAddress;

        return $this;
    }

    /**
     * Get RgtaEntAddress
     *
     * @return string 
     */
    public function getRgtaEntAddress()
    {
        return $this->RgtaEntAddress;
    }

    /**
     * Set RgtaEntAddressLine2
     *
     * @param string $rgtaEntAddressLine2
     * @return Regattas
     */
    public function setRgtaEntAddressLine2($rgtaEntAddressLine2)
    {
        $this->RgtaEntAddressLine2 = $rgtaEntAddressLine2;

        return $this;
    }

    /**
     * Get RgtaEntAddressLine2
     *
     * @return string 
     */
    public function getRgtaEntAddressLine2()
    {
        return $this->RgtaEntAddressLine2;
    }

    /**
     * Set RgtaEntAddressLine3
     *
     * @param string $rgtaEntAddressLine3
     * @return Regattas
     */
    public function setRgtaEntAddressLine3($rgtaEntAddressLine3)
    {
        $this->RgtaEntAddressLine3 = $rgtaEntAddressLine3;

        return $this;
    }

    /**
     * Get RgtaEntAddressLine3
     *
     * @return string 
     */
    public function getRgtaEntAddressLine3()
    {
        return $this->RgtaEntAddressLine3;
    }

    /**
     * Set RgtaEntCtryId
     *
     * @param integer $rgtaEntCtryId
     * @return Regattas
     */
    public function setRgtaEntCtryId($rgtaEntCtryId)
    {
        $this->RgtaEntCtryId = $rgtaEntCtryId;

        return $this;
    }

    /**
     * Get RgtaEntCtryId
     *
     * @return integer 
     */
    public function getRgtaEntCtryId()
    {
        return $this->RgtaEntCtryId;
    }

    /**
     * Set RgtaEntPhone
     *
     * @param string $rgtaEntPhone
     * @return Regattas
     */
    public function setRgtaEntPhone($rgtaEntPhone)
    {
        $this->RgtaEntPhone = $rgtaEntPhone;

        return $this;
    }

    /**
     * Get RgtaEntPhone
     *
     * @return string 
     */
    public function getRgtaEntPhone()
    {
        return $this->RgtaEntPhone;
    }

    /**
     * Set RgtaEntFax
     *
     * @param string $rgtaEntFax
     * @return Regattas
     */
    public function setRgtaEntFax($rgtaEntFax)
    {
        $this->RgtaEntFax = $rgtaEntFax;

        return $this;
    }

    /**
     * Get RgtaEntFax
     *
     * @return string 
     */
    public function getRgtaEntFax()
    {
        return $this->RgtaEntFax;
    }

    /**
     * Set RgtaEntEmail
     *
     * @param string $rgtaEntEmail
     * @return Regattas
     */
    public function setRgtaEntEmail($rgtaEntEmail)
    {
        $this->RgtaEntEmail = $rgtaEntEmail;

        return $this;
    }

    /**
     * Get RgtaEntEmail
     *
     * @return string 
     */
    public function getRgtaEntEmail()
    {
        return $this->RgtaEntEmail;
    }

    /**
     * Set RgtaPageUrl
     *
     * @param string $rgtaPageUrl
     * @return Regattas
     */
    public function setRgtaPageUrl($rgtaPageUrl)
    {
        $this->RgtaPageUrl = $rgtaPageUrl;

        return $this;
    }

    /**
     * Get RgtaPageUrl
     *
     * @return string 
     */
    public function getRgtaPageUrl()
    {
        return $this->RgtaPageUrl;
    }

    /**
     * Set RgtaFoldId
     *
     * @param integer $rgtaFoldId
     * @return Regattas
     */
    public function setRgtaFoldId($rgtaFoldId)
    {
        $this->RgtaFoldId = $rgtaFoldId;

        return $this;
    }

    /**
     * Get RgtaFoldId
     *
     * @return integer 
     */
    public function getRgtaFoldId()
    {
        return $this->RgtaFoldId;
    }

    /**
     * Set RgtaLogoUpldId
     *
     * @param integer $rgtaLogoUpldId
     * @return Regattas
     */
    public function setRgtaLogoUpldId($rgtaLogoUpldId)
    {
        $this->RgtaLogoUpldId = $rgtaLogoUpldId;

        return $this;
    }

    /**
     * Get RgtaLogoUpldId
     *
     * @return integer 
     */
    public function getRgtaLogoUpldId()
    {
        return $this->RgtaLogoUpldId;
    }

    /**
     * Set RgtaRegLink
     *
     * @param string $rgtaRegLink
     * @return Regattas
     */
    public function setRgtaRegLink($rgtaRegLink)
    {
        $this->RgtaRegLink = $rgtaRegLink;

        return $this;
    }

    /**
     * Get RgtaRegLink
     *
     * @return string 
     */
    public function getRgtaRegLink()
    {
        return $this->RgtaRegLink;
    }

    /**
     * Set RgtaXRRUploadCode
     *
     * @param string $rgtaXRRUploadCode
     * @return Regattas
     */
    public function setRgtaXRRUploadCode($rgtaXRRUploadCode)
    {
        $this->RgtaXRRUploadCode = $rgtaXRRUploadCode;

        return $this;
    }

    /**
     * Get RgtaXRRUploadCode
     *
     * @return string 
     */
    public function getRgtaXRRUploadCode()
    {
        return $this->RgtaXRRUploadCode;
    }

    /**
     * Set RgtaXRRDateCutoff
     *
     * @param \DateTime $rgtaXRRDateCutoff
     * @return Regattas
     */
    public function setRgtaXRRDateCutoff($rgtaXRRDateCutoff)
    {
        $this->RgtaXRRDateCutoff = $rgtaXRRDateCutoff;

        return $this;
    }

    /**
     * Get RgtaXRRDateCutoff
     *
     * @return \DateTime 
     */
    public function getRgtaXRRDateCutoff()
    {
        return $this->RgtaXRRDateCutoff;
    }

    /**
     * Set RgtaOffDocMeta
     *
     * @param string $rgtaOffDocMeta
     * @return Regattas
     */
    public function setRgtaOffDocMeta($rgtaOffDocMeta)
    {
        $this->RgtaOffDocMeta = $rgtaOffDocMeta;

        return $this;
    }

    /**
     * Get RgtaOffDocMeta
     *
     * @return string 
     */
    public function getRgtaOffDocMeta()
    {
        return $this->RgtaOffDocMeta;
    }

    /**
     * Set RgtaPrizeMoney
     *
     * @param boolean $rgtaPrizeMoney
     * @return Regattas
     */
    public function setRgtaPrizeMoney($rgtaPrizeMoney)
    {
        $this->RgtaPrizeMoney = $rgtaPrizeMoney;

        return $this;
    }

    /**
     * Get RgtaPrizeMoney
     *
     * @return boolean 
     */
    public function getRgtaPrizeMoney()
    {
        return $this->RgtaPrizeMoney;
    }

    /**
     * Set RgtaApvlStatus
     *
     * @param string $rgtaApvlStatus
     * @return Regattas
     */
    public function setRgtaApvlStatus($rgtaApvlStatus)
    {
        $this->RgtaApvlStatus = $rgtaApvlStatus;

        return $this;
    }

    /**
     * Get RgtaApvlStatus
     *
     * @return string 
     */
    public function getRgtaApvlStatus()
    {
        return $this->RgtaApvlStatus;
    }

    /**
     * Set RgtaCreateMembId
     *
     * @param integer $rgtaCreateMembId
     * @return Regattas
     */
    public function setRgtaCreateMembId($rgtaCreateMembId)
    {
        $this->RgtaCreateMembId = $rgtaCreateMembId;

        return $this;
    }

    /**
     * Get RgtaCreateMembId
     *
     * @return integer 
     */
    public function getRgtaCreateMembId()
    {
        return $this->RgtaCreateMembId;
    }

    /**
     * Set RgtaHideRegstProg
     *
     * @param boolean $rgtaHideRegstProg
     * @return Regattas
     */
    public function setRgtaHideRegstProg($rgtaHideRegstProg)
    {
        $this->RgtaHideRegstProg = $rgtaHideRegstProg;

        return $this;
    }

    /**
     * Get RgtaHideRegstProg
     *
     * @return boolean 
     */
    public function getRgtaHideRegstProg()
    {
        return $this->RgtaHideRegstProg;
    }

    /**
     * Set RgtaShowRegstNoInfo
     *
     * @param boolean $rgtaShowRegstNoInfo
     * @return Regattas
     */
    public function setRgtaShowRegstNoInfo($rgtaShowRegstNoInfo)
    {
        $this->RgtaShowRegstNoInfo = $rgtaShowRegstNoInfo;

        return $this;
    }

    /**
     * Get RgtaShowRegstNoInfo
     *
     * @return boolean 
     */
    public function getRgtaShowRegstNoInfo()
    {
        return $this->RgtaShowRegstNoInfo;
    }

    /**
     * Get RgtaId
     *
     * @return integer 
     */
    public function getRgtaId()
    {
        return $this->RgtaId;
    }
}
