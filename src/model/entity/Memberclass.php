<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Memberclass
 *
 * @ORM\Table(name="MemberClass")
 * @ORM\Entity
 */
class Memberclass
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="MbclPrevious", type="boolean", nullable=false)
     */
    protected $MbclPrevious;

    /**
     * @var boolean
     *
     * @ORM\Column(name="MbclCurrent", type="boolean", nullable=false)
     */
    protected $MbclCurrent;

    /**
     * @var integer
     *
     * @ORM\Column(name="MbclMembId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $MbclMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MbclClassId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $MbclClassId;



    /**
     * Set MbclPrevious
     *
     * @param boolean $mbclPrevious
     * @return Memberclass
     */
    public function setMbclPrevious($mbclPrevious)
    {
        $this->MbclPrevious = $mbclPrevious;

        return $this;
    }

    /**
     * Get MbclPrevious
     *
     * @return boolean 
     */
    public function getMbclPrevious()
    {
        return $this->MbclPrevious;
    }

    /**
     * Set MbclCurrent
     *
     * @param boolean $mbclCurrent
     * @return Memberclass
     */
    public function setMbclCurrent($mbclCurrent)
    {
        $this->MbclCurrent = $mbclCurrent;

        return $this;
    }

    /**
     * Get MbclCurrent
     *
     * @return boolean 
     */
    public function getMbclCurrent()
    {
        return $this->MbclCurrent;
    }

    /**
     * Set MbclMembId
     *
     * @param integer $mbclMembId
     * @return Memberclass
     */
    public function setMbclMembId($mbclMembId)
    {
        $this->MbclMembId = $mbclMembId;

        return $this;
    }

    /**
     * Get MbclMembId
     *
     * @return integer 
     */
    public function getMbclMembId()
    {
        return $this->MbclMembId;
    }

    /**
     * Set MbclClassId
     *
     * @param integer $mbclClassId
     * @return Memberclass
     */
    public function setMbclClassId($mbclClassId)
    {
        $this->MbclClassId = $mbclClassId;

        return $this;
    }

    /**
     * Get MbclClassId
     *
     * @return integer 
     */
    public function getMbclClassId()
    {
        return $this->MbclClassId;
    }
}
