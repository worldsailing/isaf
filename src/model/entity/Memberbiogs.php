<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Memberbiogs
 *
 * @ORM\Table(name="MemberBiogs", indexes={@ORM\Index(name="MembId_Deleted", columns={"BiogMembId", "BiogDeleted"}), @ORM\Index(name="idBiogIsafId", columns={"BiogIsafId"}), @ORM\Index(name="cfcnuserlevel", columns={"BiogClassificationsUserLevel"}), @ORM\Index(name="biogioc", columns={"BiogIOCNum"}), @ORM\Index(name="soticusermap", columns={"BiogSoticIdMap"})})
 * @ORM\Entity
 */
class Memberbiogs
{
    /**
     * @var string
     *
     * @ORM\Column(name="BiogOrigSkipCode", type="string", length=10, nullable=true)
     */
    protected $BiogOrigSkipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogOrigRankSkipCode", type="string", length=10, nullable=true)
     */
    protected $BiogOrigRankSkipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogOrigCurrentClasses", type="string", length=255, nullable=true)
     */
    protected $BiogOrigCurrentClasses;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogOrigPreviousClasses", type="string", length=255, nullable=true)
     */
    protected $BiogOrigPreviousClasses;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogIsafId", type="string", length=10, nullable=true)
     */
    protected $BiogIsafId;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogFirstName", type="string", length=255, nullable=true)
     */
    protected $BiogFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogSurname", type="string", length=255, nullable=true)
     */
    protected $BiogSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogEmail", type="string", length=100, nullable=true)
     */
    protected $BiogEmail;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogCtryId", type="integer", nullable=true)
     */
    protected $BiogCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogCtryId2", type="integer", nullable=true)
     */
    protected $BiogCtryId2;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogClassification", type="string", length=20, nullable=true)
     */
    protected $BiogClassification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BiogClassExpire", type="date", nullable=true)
     */
    protected $BiogClassExpire;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogNickname", type="string", length=255, nullable=true)
     */
    protected $BiogNickname;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogNatAuthNum", type="string", length=255, nullable=true)
     */
    protected $BiogNatAuthNum;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogIOCNum", type="string", length=16, nullable=true)
     */
    protected $BiogIOCNum;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogBirthPlace", type="string", length=255, nullable=true)
     */
    protected $BiogBirthPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogHomeTown", type="string", length=255, nullable=true)
     */
    protected $BiogHomeTown;

    /**
     * @var float
     *
     * @ORM\Column(name="BiogHeight", type="float", precision=10, scale=0, nullable=true)
     */
    protected $BiogHeight;

    /**
     * @var float
     *
     * @ORM\Column(name="BiogWeight", type="float", precision=10, scale=0, nullable=true)
     */
    protected $BiogWeight;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogMaritalStatus", type="string", length=20, nullable=true)
     */
    protected $BiogMaritalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogChildren", type="integer", nullable=true)
     */
    protected $BiogChildren;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogOccupation", type="string", length=255, nullable=true)
     */
    protected $BiogOccupation;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogEducation", type="string", length=255, nullable=true)
     */
    protected $BiogEducation;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogLanguages", type="text", length=16777215, nullable=true)
     */
    protected $BiogLanguages;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogClubName", type="string", length=255, nullable=true)
     */
    protected $BiogClubName;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogClubLocation", type="text", length=16777215, nullable=true)
     */
    protected $BiogClubLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogCoachName", type="string", length=255, nullable=true)
     */
    protected $BiogCoachName;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogCoachStart", type="text", length=16777215, nullable=true)
     */
    protected $BiogCoachStart;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogWebsite", type="string", length=255, nullable=true)
     */
    protected $BiogWebsite;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogFirstBoat", type="text", length=16777215, nullable=true)
     */
    protected $BiogFirstBoat;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogSailAge", type="integer", nullable=true)
     */
    protected $BiogSailAge;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogRaceAge", type="integer", nullable=true)
     */
    protected $BiogRaceAge;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogFirstEvent", type="text", length=16777215, nullable=true)
     */
    protected $BiogFirstEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogFirstWin", type="text", length=16777215, nullable=true)
     */
    protected $BiogFirstWin;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogHobbies", type="text", length=16777215, nullable=true)
     */
    protected $BiogHobbies;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogAmuSportEp", type="text", length=16777215, nullable=true)
     */
    protected $BiogAmuSportEp;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogInjuries", type="text", length=16777215, nullable=true)
     */
    protected $BiogInjuries;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogSuperstitions", type="text", length=16777215, nullable=true)
     */
    protected $BiogSuperstitions;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogSpAchievement", type="text", length=16777215, nullable=true)
     */
    protected $BiogSpAchievement;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogSpRelatives", type="text", length=16777215, nullable=true)
     */
    protected $BiogSpRelatives;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogWhy", type="text", length=16777215, nullable=true)
     */
    protected $BiogWhy;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogInfluential", type="text", length=16777215, nullable=true)
     */
    protected $BiogInfluential;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogAdmired", type="text", length=16777215, nullable=true)
     */
    protected $BiogAdmired;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogAmbitions", type="text", length=16777215, nullable=true)
     */
    protected $BiogAmbitions;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogAwards", type="text", length=16777215, nullable=true)
     */
    protected $BiogAwards;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogNonSailingAch", type="text", length=16777215, nullable=true)
     */
    protected $BiogNonSailingAch;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogShow", type="boolean", nullable=false)
     */
    protected $BiogShow;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogHideInSearch", type="boolean", nullable=false)
     */
    protected $BiogHideInSearch;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogEmailList", type="string", nullable=false)
     */
    protected $BiogEmailList;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogShowClass", type="boolean", nullable=false)
     */
    protected $BiogShowClass;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogShowGender", type="boolean", nullable=false)
     */
    protected $BiogShowGender;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogShowDob", type="boolean", nullable=false)
     */
    protected $BiogShowDob;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogShowEmail", type="boolean", nullable=false)
     */
    protected $BiogShowEmail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogShowResidence", type="boolean", nullable=false)
     */
    protected $BiogShowResidence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BiogUpdated", type="datetime", nullable=true)
     */
    protected $BiogUpdated;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogOffLanguages", type="string", length=100, nullable=true)
     */
    protected $BiogOffLanguages;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogNotes", type="text", length=65535, nullable=true)
     */
    protected $BiogNotes;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogDeleted", type="string", nullable=true)
     */
    protected $BiogDeleted;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogClassificationsUserLevel", type="string", length=2, nullable=true)
     */
    protected $BiogClassificationsUserLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogSkypeContact", type="string", length=100, nullable=true)
     */
    protected $BiogSkypeContact;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogMnaDocId", type="string", length=100, nullable=true)
     */
    protected $BiogMnaDocId;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogFormerFamilyName", type="string", length=100, nullable=true)
     */
    protected $BiogFormerFamilyName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogCfcnAlwaysReview", type="boolean", nullable=true)
     */
    protected $BiogCfcnAlwaysReview;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogLgcySQuest", type="string", length=100, nullable=true)
     */
    protected $BiogLgcySQuest;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogLgcySAns", type="string", length=100, nullable=true)
     */
    protected $BiogLgcySAns;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogExtUrl1", type="string", length=500, nullable=true)
     */
    protected $BiogExtUrl1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BiogMergePending", type="boolean", nullable=true)
     */
    protected $BiogMergePending;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogMotherLangId", type="integer", nullable=true)
     */
    protected $BiogMotherLangId;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogIUAppUserLevel", type="string", length=2, nullable=true)
     */
    protected $BiogIUAppUserLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogSoticIdMap", type="string", length=20, nullable=true)
     */
    protected $BiogSoticIdMap;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogSQuestId", type="integer", nullable=true)
     */
    protected $BiogSQuestId;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogSansText", type="string", length=256, nullable=true)
     */
    protected $BiogSansText;

    /**
     * @var string
     *
     * @ORM\Column(name="BiogPasswordSwap", type="string", length=32, nullable=true)
     */
    protected $BiogPasswordSwap;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogMnaCtryId", type="integer", nullable=true)
     */
    protected $BiogMnaCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogDialHomeCtryId", type="integer", nullable=true)
     */
    protected $BiogDialHomeCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogDialWorkCtryId", type="integer", nullable=true)
     */
    protected $BiogDialWorkCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogDialMobCtryId", type="integer", nullable=true)
     */
    protected $BiogDialMobCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BiogMembId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $BiogMembId;


    /**
     * Set BiogOrigSkipCode
     *
     * @param string $biogOrigSkipCode
     * @return Memberbiogs
     */
    public function setBiogOrigSkipCode($biogOrigSkipCode)
    {
        $this->BiogOrigSkipCode = $biogOrigSkipCode;

        return $this;
    }

    /**
     * Get BiogOrigSkipCode
     *
     * @return string 
     */
    public function getBiogOrigSkipCode()
    {
        return $this->BiogOrigSkipCode;
    }

    /**
     * Set BiogOrigRankSkipCode
     *
     * @param string $biogOrigRankSkipCode
     * @return Memberbiogs
     */
    public function setBiogOrigRankSkipCode($biogOrigRankSkipCode)
    {
        $this->BiogOrigRankSkipCode = $biogOrigRankSkipCode;

        return $this;
    }

    /**
     * Get BiogOrigRankSkipCode
     *
     * @return string 
     */
    public function getBiogOrigRankSkipCode()
    {
        return $this->BiogOrigRankSkipCode;
    }

    /**
     * Set BiogOrigCurrentClasses
     *
     * @param string $biogOrigCurrentClasses
     * @return Memberbiogs
     */
    public function setBiogOrigCurrentClasses($biogOrigCurrentClasses)
    {
        $this->BiogOrigCurrentClasses = $biogOrigCurrentClasses;

        return $this;
    }

    /**
     * Get BiogOrigCurrentClasses
     *
     * @return string 
     */
    public function getBiogOrigCurrentClasses()
    {
        return $this->BiogOrigCurrentClasses;
    }

    /**
     * Set BiogOrigPreviousClasses
     *
     * @param string $biogOrigPreviousClasses
     * @return Memberbiogs
     */
    public function setBiogOrigPreviousClasses($biogOrigPreviousClasses)
    {
        $this->BiogOrigPreviousClasses = $biogOrigPreviousClasses;

        return $this;
    }

    /**
     * Get BiogOrigPreviousClasses
     *
     * @return string 
     */
    public function getBiogOrigPreviousClasses()
    {
        return $this->BiogOrigPreviousClasses;
    }

    /**
     * Set BiogIsafId
     *
     * @param string $biogIsafId
     * @return Memberbiogs
     */
    public function setBiogIsafId($biogIsafId)
    {
        $this->BiogIsafId = $biogIsafId;

        return $this;
    }

    /**
     * Get BiogIsafId
     *
     * @return string 
     */
    public function getBiogIsafId()
    {
        return $this->BiogIsafId;
    }

    /**
     * Set BiogFirstName
     *
     * @param string $biogFirstName
     * @return Memberbiogs
     */
    public function setBiogFirstName($biogFirstName)
    {
        $this->BiogFirstName = $biogFirstName;

        return $this;
    }

    /**
     * Get BiogFirstName
     *
     * @return string 
     */
    public function getBiogFirstName()
    {
        return $this->BiogFirstName;
    }

    /**
     * Set BiogSurname
     *
     * @param string $biogSurname
     * @return Memberbiogs
     */
    public function setBiogSurname($biogSurname)
    {
        $this->BiogSurname = $biogSurname;

        return $this;
    }

    /**
     * Get BiogSurname
     *
     * @return string 
     */
    public function getBiogSurname()
    {
        return $this->BiogSurname;
    }

    /**
     * Set BiogEmail
     *
     * @param string $biogEmail
     * @return Memberbiogs
     */
    public function setBiogEmail($biogEmail)
    {
        $this->BiogEmail = $biogEmail;

        return $this;
    }

    /**
     * Get BiogEmail
     *
     * @return string 
     */
    public function getBiogEmail()
    {
        return $this->BiogEmail;
    }

    /**
     * Set BiogCtryId
     *
     * @param integer $biogCtryId
     * @return Memberbiogs
     */
    public function setBiogCtryId($biogCtryId)
    {
        $this->BiogCtryId = $biogCtryId;

        return $this;
    }

    /**
     * Get BiogCtryId
     *
     * @return integer 
     */
    public function getBiogCtryId()
    {
        return $this->BiogCtryId;
    }

    /**
     * Set BiogCtryId2
     *
     * @param integer $biogCtryId2
     * @return Memberbiogs
     */
    public function setBiogCtryId2($biogCtryId2)
    {
        $this->BiogCtryId2 = $biogCtryId2;

        return $this;
    }

    /**
     * Get BiogCtryId2
     *
     * @return integer 
     */
    public function getBiogCtryId2()
    {
        return $this->BiogCtryId2;
    }

    /**
     * Set BiogClassification
     *
     * @param string $biogClassification
     * @return Memberbiogs
     */
    public function setBiogClassification($biogClassification)
    {
        $this->BiogClassification = $biogClassification;

        return $this;
    }

    /**
     * Get BiogClassification
     *
     * @return string 
     */
    public function getBiogClassification()
    {
        return $this->BiogClassification;
    }

    /**
     * Set BiogClassExpire
     *
     * @param \DateTime $biogClassExpire
     * @return Memberbiogs
     */
    public function setBiogClassExpire($biogClassExpire)
    {
        $this->BiogClassExpire = $biogClassExpire;

        return $this;
    }

    /**
     * Get BiogClassExpire
     *
     * @return \DateTime 
     */
    public function getBiogClassExpire()
    {
        return $this->BiogClassExpire;
    }

    /**
     * Set BiogNickname
     *
     * @param string $biogNickname
     * @return Memberbiogs
     */
    public function setBiogNickname($biogNickname)
    {
        $this->BiogNickname = $biogNickname;

        return $this;
    }

    /**
     * Get BiogNickname
     *
     * @return string 
     */
    public function getBiogNickname()
    {
        return $this->BiogNickname;
    }

    /**
     * Set BiogNatAuthNum
     *
     * @param string $biogNatAuthNum
     * @return Memberbiogs
     */
    public function setBiogNatAuthNum($biogNatAuthNum)
    {
        $this->BiogNatAuthNum = $biogNatAuthNum;

        return $this;
    }

    /**
     * Get BiogNatAuthNum
     *
     * @return string 
     */
    public function getBiogNatAuthNum()
    {
        return $this->BiogNatAuthNum;
    }

    /**
     * Set BiogIOCNum
     *
     * @param string $biogIOCNum
     * @return Memberbiogs
     */
    public function setBiogIOCNum($biogIOCNum)
    {
        $this->BiogIOCNum = $biogIOCNum;

        return $this;
    }

    /**
     * Get BiogIOCNum
     *
     * @return string 
     */
    public function getBiogIOCNum()
    {
        return $this->BiogIOCNum;
    }

    /**
     * Set BiogBirthPlace
     *
     * @param string $biogBirthPlace
     * @return Memberbiogs
     */
    public function setBiogBirthPlace($biogBirthPlace)
    {
        $this->BiogBirthPlace = $biogBirthPlace;

        return $this;
    }

    /**
     * Get BiogBirthPlace
     *
     * @return string 
     */
    public function getBiogBirthPlace()
    {
        return $this->BiogBirthPlace;
    }

    /**
     * Set BiogHomeTown
     *
     * @param string $biogHomeTown
     * @return Memberbiogs
     */
    public function setBiogHomeTown($biogHomeTown)
    {
        $this->BiogHomeTown = $biogHomeTown;

        return $this;
    }

    /**
     * Get BiogHomeTown
     *
     * @return string 
     */
    public function getBiogHomeTown()
    {
        return $this->BiogHomeTown;
    }

    /**
     * Set BiogHeight
     *
     * @param float $biogHeight
     * @return Memberbiogs
     */
    public function setBiogHeight($biogHeight)
    {
        $this->BiogHeight = $biogHeight;

        return $this;
    }

    /**
     * Get BiogHeight
     *
     * @return float 
     */
    public function getBiogHeight()
    {
        return $this->BiogHeight;
    }

    /**
     * Set BiogWeight
     *
     * @param float $biogWeight
     * @return Memberbiogs
     */
    public function setBiogWeight($biogWeight)
    {
        $this->BiogWeight = $biogWeight;

        return $this;
    }

    /**
     * Get BiogWeight
     *
     * @return float 
     */
    public function getBiogWeight()
    {
        return $this->BiogWeight;
    }

    /**
     * Set BiogMaritalStatus
     *
     * @param string $biogMaritalStatus
     * @return Memberbiogs
     */
    public function setBiogMaritalStatus($biogMaritalStatus)
    {
        $this->BiogMaritalStatus = $biogMaritalStatus;

        return $this;
    }

    /**
     * Get BiogMaritalStatus
     *
     * @return string 
     */
    public function getBiogMaritalStatus()
    {
        return $this->BiogMaritalStatus;
    }

    /**
     * Set BiogChildren
     *
     * @param integer $biogChildren
     * @return Memberbiogs
     */
    public function setBiogChildren($biogChildren)
    {
        $this->BiogChildren = $biogChildren;

        return $this;
    }

    /**
     * Get BiogChildren
     *
     * @return integer 
     */
    public function getBiogChildren()
    {
        return $this->BiogChildren;
    }

    /**
     * Set BiogOccupation
     *
     * @param string $biogOccupation
     * @return Memberbiogs
     */
    public function setBiogOccupation($biogOccupation)
    {
        $this->BiogOccupation = $biogOccupation;

        return $this;
    }

    /**
     * Get BiogOccupation
     *
     * @return string 
     */
    public function getBiogOccupation()
    {
        return $this->BiogOccupation;
    }

    /**
     * Set BiogEducation
     *
     * @param string $biogEducation
     * @return Memberbiogs
     */
    public function setBiogEducation($biogEducation)
    {
        $this->BiogEducation = $biogEducation;

        return $this;
    }

    /**
     * Get BiogEducation
     *
     * @return string 
     */
    public function getBiogEducation()
    {
        return $this->BiogEducation;
    }

    /**
     * Set BiogLanguages
     *
     * @param string $biogLanguages
     * @return Memberbiogs
     */
    public function setBiogLanguages($biogLanguages)
    {
        $this->BiogLanguages = $biogLanguages;

        return $this;
    }

    /**
     * Get BiogLanguages
     *
     * @return string 
     */
    public function getBiogLanguages()
    {
        return $this->BiogLanguages;
    }

    /**
     * Set BiogClubName
     *
     * @param string $biogClubName
     * @return Memberbiogs
     */
    public function setBiogClubName($biogClubName)
    {
        $this->BiogClubName = $biogClubName;

        return $this;
    }

    /**
     * Get BiogClubName
     *
     * @return string 
     */
    public function getBiogClubName()
    {
        return $this->BiogClubName;
    }

    /**
     * Set BiogClubLocation
     *
     * @param string $biogClubLocation
     * @return Memberbiogs
     */
    public function setBiogClubLocation($biogClubLocation)
    {
        $this->BiogClubLocation = $biogClubLocation;

        return $this;
    }

    /**
     * Get BiogClubLocation
     *
     * @return string 
     */
    public function getBiogClubLocation()
    {
        return $this->BiogClubLocation;
    }

    /**
     * Set BiogCoachName
     *
     * @param string $biogCoachName
     * @return Memberbiogs
     */
    public function setBiogCoachName($biogCoachName)
    {
        $this->BiogCoachName = $biogCoachName;

        return $this;
    }

    /**
     * Get BiogCoachName
     *
     * @return string 
     */
    public function getBiogCoachName()
    {
        return $this->BiogCoachName;
    }

    /**
     * Set BiogCoachStart
     *
     * @param string $biogCoachStart
     * @return Memberbiogs
     */
    public function setBiogCoachStart($biogCoachStart)
    {
        $this->BiogCoachStart = $biogCoachStart;

        return $this;
    }

    /**
     * Get BiogCoachStart
     *
     * @return string 
     */
    public function getBiogCoachStart()
    {
        return $this->BiogCoachStart;
    }

    /**
     * Set BiogWebsite
     *
     * @param string $biogWebsite
     * @return Memberbiogs
     */
    public function setBiogWebsite($biogWebsite)
    {
        $this->BiogWebsite = $biogWebsite;

        return $this;
    }

    /**
     * Get BiogWebsite
     *
     * @return string 
     */
    public function getBiogWebsite()
    {
        return $this->BiogWebsite;
    }

    /**
     * Set BiogFirstBoat
     *
     * @param string $biogFirstBoat
     * @return Memberbiogs
     */
    public function setBiogFirstBoat($biogFirstBoat)
    {
        $this->BiogFirstBoat = $biogFirstBoat;

        return $this;
    }

    /**
     * Get BiogFirstBoat
     *
     * @return string 
     */
    public function getBiogFirstBoat()
    {
        return $this->BiogFirstBoat;
    }

    /**
     * Set BiogSailAge
     *
     * @param integer $biogSailAge
     * @return Memberbiogs
     */
    public function setBiogSailAge($biogSailAge)
    {
        $this->BiogSailAge = $biogSailAge;

        return $this;
    }

    /**
     * Get BiogSailAge
     *
     * @return integer 
     */
    public function getBiogSailAge()
    {
        return $this->BiogSailAge;
    }

    /**
     * Set BiogRaceAge
     *
     * @param integer $biogRaceAge
     * @return Memberbiogs
     */
    public function setBiogRaceAge($biogRaceAge)
    {
        $this->BiogRaceAge = $biogRaceAge;

        return $this;
    }

    /**
     * Get BiogRaceAge
     *
     * @return integer 
     */
    public function getBiogRaceAge()
    {
        return $this->BiogRaceAge;
    }

    /**
     * Set BiogFirstEvent
     *
     * @param string $biogFirstEvent
     * @return Memberbiogs
     */
    public function setBiogFirstEvent($biogFirstEvent)
    {
        $this->BiogFirstEvent = $biogFirstEvent;

        return $this;
    }

    /**
     * Get BiogFirstEvent
     *
     * @return string 
     */
    public function getBiogFirstEvent()
    {
        return $this->BiogFirstEvent;
    }

    /**
     * Set BiogFirstWin
     *
     * @param string $biogFirstWin
     * @return Memberbiogs
     */
    public function setBiogFirstWin($biogFirstWin)
    {
        $this->BiogFirstWin = $biogFirstWin;

        return $this;
    }

    /**
     * Get BiogFirstWin
     *
     * @return string 
     */
    public function getBiogFirstWin()
    {
        return $this->BiogFirstWin;
    }

    /**
     * Set BiogHobbies
     *
     * @param string $biogHobbies
     * @return Memberbiogs
     */
    public function setBiogHobbies($biogHobbies)
    {
        $this->BiogHobbies = $biogHobbies;

        return $this;
    }

    /**
     * Get BiogHobbies
     *
     * @return string 
     */
    public function getBiogHobbies()
    {
        return $this->BiogHobbies;
    }

    /**
     * Set BiogAmuSportEp
     *
     * @param string $biogAmuSportEp
     * @return Memberbiogs
     */
    public function setBiogAmuSportEp($biogAmuSportEp)
    {
        $this->BiogAmuSportEp = $biogAmuSportEp;

        return $this;
    }

    /**
     * Get BiogAmuSportEp
     *
     * @return string 
     */
    public function getBiogAmuSportEp()
    {
        return $this->BiogAmuSportEp;
    }

    /**
     * Set BiogInjuries
     *
     * @param string $biogInjuries
     * @return Memberbiogs
     */
    public function setBiogInjuries($biogInjuries)
    {
        $this->BiogInjuries = $biogInjuries;

        return $this;
    }

    /**
     * Get BiogInjuries
     *
     * @return string 
     */
    public function getBiogInjuries()
    {
        return $this->BiogInjuries;
    }

    /**
     * Set BiogSuperstitions
     *
     * @param string $biogSuperstitions
     * @return Memberbiogs
     */
    public function setBiogSuperstitions($biogSuperstitions)
    {
        $this->BiogSuperstitions = $biogSuperstitions;

        return $this;
    }

    /**
     * Get BiogSuperstitions
     *
     * @return string 
     */
    public function getBiogSuperstitions()
    {
        return $this->BiogSuperstitions;
    }

    /**
     * Set BiogSpAchievement
     *
     * @param string $biogSpAchievement
     * @return Memberbiogs
     */
    public function setBiogSpAchievement($biogSpAchievement)
    {
        $this->BiogSpAchievement = $biogSpAchievement;

        return $this;
    }

    /**
     * Get BiogSpAchievement
     *
     * @return string 
     */
    public function getBiogSpAchievement()
    {
        return $this->BiogSpAchievement;
    }

    /**
     * Set BiogSpRelatives
     *
     * @param string $biogSpRelatives
     * @return Memberbiogs
     */
    public function setBiogSpRelatives($biogSpRelatives)
    {
        $this->BiogSpRelatives = $biogSpRelatives;

        return $this;
    }

    /**
     * Get BiogSpRelatives
     *
     * @return string 
     */
    public function getBiogSpRelatives()
    {
        return $this->BiogSpRelatives;
    }

    /**
     * Set BiogWhy
     *
     * @param string $biogWhy
     * @return Memberbiogs
     */
    public function setBiogWhy($biogWhy)
    {
        $this->BiogWhy = $biogWhy;

        return $this;
    }

    /**
     * Get BiogWhy
     *
     * @return string 
     */
    public function getBiogWhy()
    {
        return $this->BiogWhy;
    }

    /**
     * Set BiogInfluential
     *
     * @param string $biogInfluential
     * @return Memberbiogs
     */
    public function setBiogInfluential($biogInfluential)
    {
        $this->BiogInfluential = $biogInfluential;

        return $this;
    }

    /**
     * Get BiogInfluential
     *
     * @return string 
     */
    public function getBiogInfluential()
    {
        return $this->BiogInfluential;
    }

    /**
     * Set BiogAdmired
     *
     * @param string $biogAdmired
     * @return Memberbiogs
     */
    public function setBiogAdmired($biogAdmired)
    {
        $this->BiogAdmired = $biogAdmired;

        return $this;
    }

    /**
     * Get BiogAdmired
     *
     * @return string 
     */
    public function getBiogAdmired()
    {
        return $this->BiogAdmired;
    }

    /**
     * Set BiogAmbitions
     *
     * @param string $biogAmbitions
     * @return Memberbiogs
     */
    public function setBiogAmbitions($biogAmbitions)
    {
        $this->BiogAmbitions = $biogAmbitions;

        return $this;
    }

    /**
     * Get BiogAmbitions
     *
     * @return string 
     */
    public function getBiogAmbitions()
    {
        return $this->BiogAmbitions;
    }

    /**
     * Set BiogAwards
     *
     * @param string $biogAwards
     * @return Memberbiogs
     */
    public function setBiogAwards($biogAwards)
    {
        $this->BiogAwards = $biogAwards;

        return $this;
    }

    /**
     * Get BiogAwards
     *
     * @return string 
     */
    public function getBiogAwards()
    {
        return $this->BiogAwards;
    }

    /**
     * Set BiogNonSailingAch
     *
     * @param string $biogNonSailingAch
     * @return Memberbiogs
     */
    public function setBiogNonSailingAch($biogNonSailingAch)
    {
        $this->BiogNonSailingAch = $biogNonSailingAch;

        return $this;
    }

    /**
     * Get BiogNonSailingAch
     *
     * @return string 
     */
    public function getBiogNonSailingAch()
    {
        return $this->BiogNonSailingAch;
    }

    /**
     * Set BiogShow
     *
     * @param boolean $biogShow
     * @return Memberbiogs
     */
    public function setBiogShow($biogShow)
    {
        $this->BiogShow = $biogShow;

        return $this;
    }

    /**
     * Get BiogShow
     *
     * @return boolean 
     */
    public function getBiogShow()
    {
        return $this->BiogShow;
    }

    /**
     * Set BiogHideInSearch
     *
     * @param boolean $biogHideInSearch
     * @return Memberbiogs
     */
    public function setBiogHideInSearch($biogHideInSearch)
    {
        $this->BiogHideInSearch = $biogHideInSearch;

        return $this;
    }

    /**
     * Get BiogHideInSearch
     *
     * @return boolean 
     */
    public function getBiogHideInSearch()
    {
        return $this->BiogHideInSearch;
    }

    /**
     * Set BiogEmailList
     *
     * @param string $biogEmailList
     * @return Memberbiogs
     */
    public function setBiogEmailList($biogEmailList)
    {
        $this->BiogEmailList = $biogEmailList;

        return $this;
    }

    /**
     * Get BiogEmailList
     *
     * @return string 
     */
    public function getBiogEmailList()
    {
        return $this->BiogEmailList;
    }

    /**
     * Set BiogShowClass
     *
     * @param boolean $biogShowClass
     * @return Memberbiogs
     */
    public function setBiogShowClass($biogShowClass)
    {
        $this->BiogShowClass = $biogShowClass;

        return $this;
    }

    /**
     * Get BiogShowClass
     *
     * @return boolean 
     */
    public function getBiogShowClass()
    {
        return $this->BiogShowClass;
    }

    /**
     * Set BiogShowGender
     *
     * @param boolean $biogShowGender
     * @return Memberbiogs
     */
    public function setBiogShowGender($biogShowGender)
    {
        $this->BiogShowGender = $biogShowGender;

        return $this;
    }

    /**
     * Get BiogShowGender
     *
     * @return boolean 
     */
    public function getBiogShowGender()
    {
        return $this->BiogShowGender;
    }

    /**
     * Set BiogShowDob
     *
     * @param boolean $biogShowDob
     * @return Memberbiogs
     */
    public function setBiogShowDob($biogShowDob)
    {
        $this->BiogShowDob = $biogShowDob;

        return $this;
    }

    /**
     * Get BiogShowDob
     *
     * @return boolean 
     */
    public function getBiogShowDob()
    {
        return $this->BiogShowDob;
    }

    /**
     * Set BiogShowEmail
     *
     * @param boolean $biogShowEmail
     * @return Memberbiogs
     */
    public function setBiogShowEmail($biogShowEmail)
    {
        $this->BiogShowEmail = $biogShowEmail;

        return $this;
    }

    /**
     * Get BiogShowEmail
     *
     * @return boolean 
     */
    public function getBiogShowEmail()
    {
        return $this->BiogShowEmail;
    }

    /**
     * Set BiogShowResidence
     *
     * @param boolean $biogShowResidence
     * @return Memberbiogs
     */
    public function setBiogShowResidence($biogShowResidence)
    {
        $this->BiogShowResidence = $biogShowResidence;

        return $this;
    }

    /**
     * Get BiogShowResidence
     *
     * @return boolean 
     */
    public function getBiogShowResidence()
    {
        return $this->BiogShowResidence;
    }

    /**
     * Set BiogUpdated
     *
     * @param \DateTime $biogUpdated
     * @return Memberbiogs
     */
    public function setBiogUpdated($biogUpdated)
    {
        $this->BiogUpdated = $biogUpdated;

        return $this;
    }

    /**
     * Get BiogUpdated
     *
     * @return \DateTime 
     */
    public function getBiogUpdated()
    {
        return $this->BiogUpdated;
    }

    /**
     * Set BiogOffLanguages
     *
     * @param string $biogOffLanguages
     * @return Memberbiogs
     */
    public function setBiogOffLanguages($biogOffLanguages)
    {
        $this->BiogOffLanguages = $biogOffLanguages;

        return $this;
    }

    /**
     * Get BiogOffLanguages
     *
     * @return string 
     */
    public function getBiogOffLanguages()
    {
        return $this->BiogOffLanguages;
    }

    /**
     * Set BiogNotes
     *
     * @param string $biogNotes
     * @return Memberbiogs
     */
    public function setBiogNotes($biogNotes)
    {
        $this->BiogNotes = $biogNotes;

        return $this;
    }

    /**
     * Get BiogNotes
     *
     * @return string 
     */
    public function getBiogNotes()
    {
        return $this->BiogNotes;
    }

    /**
     * Set BiogDeleted
     *
     * @param string $biogDeleted
     * @return Memberbiogs
     */
    public function setBiogDeleted($biogDeleted)
    {
        $this->BiogDeleted = $biogDeleted;

        return $this;
    }

    /**
     * Get BiogDeleted
     *
     * @return string 
     */
    public function getBiogDeleted()
    {
        return $this->BiogDeleted;
    }

    /**
     * Set BiogClassificationsUserLevel
     *
     * @param string $biogClassificationsUserLevel
     * @return Memberbiogs
     */
    public function setBiogClassificationsUserLevel($biogClassificationsUserLevel)
    {
        $this->BiogClassificationsUserLevel = $biogClassificationsUserLevel;

        return $this;
    }

    /**
     * Get BiogClassificationsUserLevel
     *
     * @return string 
     */
    public function getBiogClassificationsUserLevel()
    {
        return $this->BiogClassificationsUserLevel;
    }

    /**
     * Set BiogSkypeContact
     *
     * @param string $biogSkypeContact
     * @return Memberbiogs
     */
    public function setBiogSkypeContact($biogSkypeContact)
    {
        $this->BiogSkypeContact = $biogSkypeContact;

        return $this;
    }

    /**
     * Get BiogSkypeContact
     *
     * @return string 
     */
    public function getBiogSkypeContact()
    {
        return $this->BiogSkypeContact;
    }

    /**
     * Set BiogMnaDocId
     *
     * @param string $biogMnaDocId
     * @return Memberbiogs
     */
    public function setBiogMnaDocId($biogMnaDocId)
    {
        $this->BiogMnaDocId = $biogMnaDocId;

        return $this;
    }

    /**
     * Get BiogMnaDocId
     *
     * @return string 
     */
    public function getBiogMnaDocId()
    {
        return $this->BiogMnaDocId;
    }

    /**
     * Set BiogFormerFamilyName
     *
     * @param string $biogFormerFamilyName
     * @return Memberbiogs
     */
    public function setBiogFormerFamilyName($biogFormerFamilyName)
    {
        $this->BiogFormerFamilyName = $biogFormerFamilyName;

        return $this;
    }

    /**
     * Get BiogFormerFamilyName
     *
     * @return string 
     */
    public function getBiogFormerFamilyName()
    {
        return $this->BiogFormerFamilyName;
    }

    /**
     * Set BiogCfcnAlwaysReview
     *
     * @param boolean $biogCfcnAlwaysReview
     * @return Memberbiogs
     */
    public function setBiogCfcnAlwaysReview($biogCfcnAlwaysReview)
    {
        $this->BiogCfcnAlwaysReview = $biogCfcnAlwaysReview;

        return $this;
    }

    /**
     * Get BiogCfcnAlwaysReview
     *
     * @return boolean 
     */
    public function getBiogCfcnAlwaysReview()
    {
        return $this->BiogCfcnAlwaysReview;
    }

    /**
     * Set BiogLgcySQuest
     *
     * @param string $biogLgcySQuest
     * @return Memberbiogs
     */
    public function setBiogLgcySQuest($biogLgcySQuest)
    {
        $this->BiogLgcySQuest = $biogLgcySQuest;

        return $this;
    }

    /**
     * Get BiogLgcySQuest
     *
     * @return string 
     */
    public function getBiogLgcySQuest()
    {
        return $this->BiogLgcySQuest;
    }

    /**
     * Set BiogLgcySAns
     *
     * @param string $biogLgcySAns
     * @return Memberbiogs
     */
    public function setBiogLgcySAns($biogLgcySAns)
    {
        $this->BiogLgcySAns = $biogLgcySAns;

        return $this;
    }

    /**
     * Get BiogLgcySAns
     *
     * @return string 
     */
    public function getBiogLgcySAns()
    {
        return $this->BiogLgcySAns;
    }

    /**
     * Set BiogExtUrl1
     *
     * @param string $biogExtUrl1
     * @return Memberbiogs
     */
    public function setBiogExtUrl1($biogExtUrl1)
    {
        $this->BiogExtUrl1 = $biogExtUrl1;

        return $this;
    }

    /**
     * Get BiogExtUrl1
     *
     * @return string 
     */
    public function getBiogExtUrl1()
    {
        return $this->BiogExtUrl1;
    }

    /**
     * Set BiogMergePending
     *
     * @param boolean $biogMergePending
     * @return Memberbiogs
     */
    public function setBiogMergePending($biogMergePending)
    {
        $this->BiogMergePending = $biogMergePending;

        return $this;
    }

    /**
     * Get BiogMergePending
     *
     * @return boolean 
     */
    public function getBiogMergePending()
    {
        return $this->BiogMergePending;
    }

    /**
     * Set BiogMotherLangId
     *
     * @param integer $biogMotherLangId
     * @return Memberbiogs
     */
    public function setBiogMotherLangId($biogMotherLangId)
    {
        $this->BiogMotherLangId = $biogMotherLangId;

        return $this;
    }

    /**
     * Get BiogMotherLangId
     *
     * @return integer 
     */
    public function getBiogMotherLangId()
    {
        return $this->BiogMotherLangId;
    }

    /**
     * Set BiogIUAppUserLevel
     *
     * @param string $biogIUAppUserLevel
     * @return Memberbiogs
     */
    public function setBiogIUAppUserLevel($biogIUAppUserLevel)
    {
        $this->BiogIUAppUserLevel = $biogIUAppUserLevel;

        return $this;
    }

    /**
     * Get BiogIUAppUserLevel
     *
     * @return string 
     */
    public function getBiogIUAppUserLevel()
    {
        return $this->BiogIUAppUserLevel;
    }

    /**
     * Set BiogSoticIdMap
     *
     * @param string $biogSoticIdMap
     * @return Memberbiogs
     */
    public function setBiogSoticIdMap($biogSoticIdMap)
    {
        $this->BiogSoticIdMap = $biogSoticIdMap;

        return $this;
    }

    /**
     * Get BiogSoticIdMap
     *
     * @return string 
     */
    public function getBiogSoticIdMap()
    {
        return $this->BiogSoticIdMap;
    }

    /**
     * Set BiogSQuestId
     *
     * @param integer $biogSQuestId
     * @return Memberbiogs
     */
    public function setBiogSQuestId($biogSQuestId)
    {
        $this->BiogSQuestId = $biogSQuestId;

        return $this;
    }

    /**
     * Get BiogSQuestId
     *
     * @return integer 
     */
    public function getBiogSQuestId()
    {
        return $this->BiogSQuestId;
    }

    /**
     * Set BiogSansText
     *
     * @param string $biogSansText
     * @return Memberbiogs
     */
    public function setBiogSansText($biogSansText)
    {
        $this->BiogSansText = $biogSansText;

        return $this;
    }

    /**
     * Get BiogSansText
     *
     * @return string 
     */
    public function getBiogSansText()
    {
        return $this->BiogSansText;
    }

    /**
     * Set BiogPasswordSwap
     *
     * @param string $biogPasswordSwap
     * @return Memberbiogs
     */
    public function setBiogPasswordSwap($biogPasswordSwap)
    {
        $this->BiogPasswordSwap = $biogPasswordSwap;

        return $this;
    }

    /**
     * Get BiogPasswordSwap
     *
     * @return string 
     */
    public function getBiogPasswordSwap()
    {
        return $this->BiogPasswordSwap;
    }

    /**
     * Set BiogMnaCtryId
     *
     * @param integer $biogMnaCtryId
     * @return Memberbiogs
     */
    public function setBiogMnaCtryId($biogMnaCtryId)
    {
        $this->BiogMnaCtryId = $biogMnaCtryId;

        return $this;
    }

    /**
     * Get BiogMnaCtryId
     *
     * @return integer 
     */
    public function getBiogMnaCtryId()
    {
        return $this->BiogMnaCtryId;
    }

    /**
     * Set BiogDialHomeCtryId
     *
     * @param integer $biogDialHomeCtryId
     * @return Memberbiogs
     */
    public function setBiogDialHomeCtryId($biogDialHomeCtryId)
    {
        $this->BiogDialHomeCtryId = $biogDialHomeCtryId;

        return $this;
    }

    /**
     * Get BiogDialHomeCtryId
     *
     * @return integer 
     */
    public function getBiogDialHomeCtryId()
    {
        return $this->BiogDialHomeCtryId;
    }

    /**
     * Set BiogDialWorkCtryId
     *
     * @param integer $biogDialWorkCtryId
     * @return Memberbiogs
     */
    public function setBiogDialWorkCtryId($biogDialWorkCtryId)
    {
        $this->BiogDialWorkCtryId = $biogDialWorkCtryId;

        return $this;
    }

    /**
     * Get BiogDialWorkCtryId
     *
     * @return integer 
     */
    public function getBiogDialWorkCtryId()
    {
        return $this->BiogDialWorkCtryId;
    }

    /**
     * Set BiogDialMobCtryId
     *
     * @param integer $biogDialMobCtryId
     * @return Memberbiogs
     */
    public function setBiogDialMobCtryId($biogDialMobCtryId)
    {
        $this->BiogDialMobCtryId = $biogDialMobCtryId;

        return $this;
    }

    /**
     * Get BiogDialMobCtryId
     *
     * @return integer 
     */
    public function getBiogDialMobCtryId()
    {
        return $this->BiogDialMobCtryId;
    }

    /**
     * Get BiogMembId
     *
     * @return integer 
     */
    public function getBiogMembId()
    {
        return $this->BiogMembId;
    }


}
