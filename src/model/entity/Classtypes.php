<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classtypes
 *
 * @ORM\Table(name="ClassTypes")
 * @ORM\Entity
 */
class Classtypes
{
    /**
     * @var string
     *
     * @ORM\Column(name="TypeName", type="string", length=50, nullable=true)
     */
    protected $TypeName;

    /**
     * @var integer
     *
     * @ORM\Column(name="TypeId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $TypeId;



    /**
     * Set TypeName
     *
     * @param string $typeName
     * @return Classtypes
     */
    public function setTypeName($typeName)
    {
        $this->TypeName = $typeName;

        return $this;
    }

    /**
     * Get TypeName
     *
     * @return string 
     */
    public function getTypeName()
    {
        return $this->TypeName;
    }

    /**
     * Get TypeId
     *
     * @return integer 
     */
    public function getTypeId()
    {
        return $this->TypeId;
    }
}
