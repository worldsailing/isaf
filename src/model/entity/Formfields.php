<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formfields
 *
 * @ORM\Table(name="formfields")
 * @ORM\Entity
 */
class Formfields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="FmfdFmgpId", type="integer", nullable=true)
     */
    protected $FmfdFmgpId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmfdOrder", type="integer", nullable=false)
     */
    protected $FmfdOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmfdOrderBackup", type="integer", nullable=false)
     */
    protected $FmfdOrderBackup;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfdName", type="string", length=255, nullable=true)
     */
    protected $FmfdName;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfdHelpText", type="text", length=65535, nullable=true)
     */
    protected $FmfdHelpText;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfdDataType", type="string", nullable=true)
     */
    protected $FmfdDataType;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfdDisplayType", type="string", length=50, nullable=true)
     */
    protected $FmfdDisplayType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FmfdKey", type="boolean", nullable=true)
     */
    protected $FmfdKey;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FmfdRequired", type="boolean", nullable=true)
     */
    protected $FmfdRequired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FmfdVersion", type="boolean", nullable=true)
     */
    protected $FmfdVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfdOptions", type="text", length=65535, nullable=true)
     */
    protected $FmfdOptions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FmfdVisible", type="boolean", nullable=true)
     */
    protected $FmfdVisible;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfdDescription", type="text", length=65535, nullable=true)
     */
    protected $FmfdDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfdEntityOverrides", type="text", length=65535, nullable=true)
     */
    protected $FmfdEntityOverrides;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmfdFmfsId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $FmfdFmfsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmfdFldId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $FmfdFldId;



    /**
     * Set FmfdFmgpId
     *
     * @param integer $fmfdFmgpId
     * @return Formfields
     */
    public function setFmfdFmgpId($fmfdFmgpId)
    {
        $this->FmfdFmgpId = $fmfdFmgpId;

        return $this;
    }

    /**
     * Get FmfdFmgpId
     *
     * @return integer 
     */
    public function getFmfdFmgpId()
    {
        return $this->FmfdFmgpId;
    }

    /**
     * Set FmfdOrder
     *
     * @param integer $fmfdOrder
     * @return Formfields
     */
    public function setFmfdOrder($fmfdOrder)
    {
        $this->FmfdOrder = $fmfdOrder;

        return $this;
    }

    /**
     * Get FmfdOrder
     *
     * @return integer 
     */
    public function getFmfdOrder()
    {
        return $this->FmfdOrder;
    }

    /**
     * Set FmfdOrderBackup
     *
     * @param integer $fmfdOrderBackup
     * @return Formfields
     */
    public function setFmfdOrderBackup($fmfdOrderBackup)
    {
        $this->FmfdOrderBackup = $fmfdOrderBackup;

        return $this;
    }

    /**
     * Get FmfdOrderBackup
     *
     * @return integer 
     */
    public function getFmfdOrderBackup()
    {
        return $this->FmfdOrderBackup;
    }

    /**
     * Set FmfdName
     *
     * @param string $fmfdName
     * @return Formfields
     */
    public function setFmfdName($fmfdName)
    {
        $this->FmfdName = $fmfdName;

        return $this;
    }

    /**
     * Get FmfdName
     *
     * @return string 
     */
    public function getFmfdName()
    {
        return $this->FmfdName;
    }

    /**
     * Set FmfdHelpText
     *
     * @param string $fmfdHelpText
     * @return Formfields
     */
    public function setFmfdHelpText($fmfdHelpText)
    {
        $this->FmfdHelpText = $fmfdHelpText;

        return $this;
    }

    /**
     * Get FmfdHelpText
     *
     * @return string 
     */
    public function getFmfdHelpText()
    {
        return $this->FmfdHelpText;
    }

    /**
     * Set FmfdDataType
     *
     * @param string $fmfdDataType
     * @return Formfields
     */
    public function setFmfdDataType($fmfdDataType)
    {
        $this->FmfdDataType = $fmfdDataType;

        return $this;
    }

    /**
     * Get FmfdDataType
     *
     * @return string 
     */
    public function getFmfdDataType()
    {
        return $this->FmfdDataType;
    }

    /**
     * Set FmfdDisplayType
     *
     * @param string $fmfdDisplayType
     * @return Formfields
     */
    public function setFmfdDisplayType($fmfdDisplayType)
    {
        $this->FmfdDisplayType = $fmfdDisplayType;

        return $this;
    }

    /**
     * Get FmfdDisplayType
     *
     * @return string 
     */
    public function getFmfdDisplayType()
    {
        return $this->FmfdDisplayType;
    }

    /**
     * Set FmfdKey
     *
     * @param boolean $fmfdKey
     * @return Formfields
     */
    public function setFmfdKey($fmfdKey)
    {
        $this->FmfdKey = $fmfdKey;

        return $this;
    }

    /**
     * Get FmfdKey
     *
     * @return boolean 
     */
    public function getFmfdKey()
    {
        return $this->FmfdKey;
    }

    /**
     * Set FmfdRequired
     *
     * @param boolean $fmfdRequired
     * @return Formfields
     */
    public function setFmfdRequired($fmfdRequired)
    {
        $this->FmfdRequired = $fmfdRequired;

        return $this;
    }

    /**
     * Get FmfdRequired
     *
     * @return boolean 
     */
    public function getFmfdRequired()
    {
        return $this->FmfdRequired;
    }

    /**
     * Set FmfdVersion
     *
     * @param boolean $fmfdVersion
     * @return Formfields
     */
    public function setFmfdVersion($fmfdVersion)
    {
        $this->FmfdVersion = $fmfdVersion;

        return $this;
    }

    /**
     * Get FmfdVersion
     *
     * @return boolean 
     */
    public function getFmfdVersion()
    {
        return $this->FmfdVersion;
    }

    /**
     * Set FmfdOptions
     *
     * @param string $fmfdOptions
     * @return Formfields
     */
    public function setFmfdOptions($fmfdOptions)
    {
        $this->FmfdOptions = $fmfdOptions;

        return $this;
    }

    /**
     * Get FmfdOptions
     *
     * @return string 
     */
    public function getFmfdOptions()
    {
        return $this->FmfdOptions;
    }

    /**
     * Set FmfdVisible
     *
     * @param boolean $fmfdVisible
     * @return Formfields
     */
    public function setFmfdVisible($fmfdVisible)
    {
        $this->FmfdVisible = $fmfdVisible;

        return $this;
    }

    /**
     * Get FmfdVisible
     *
     * @return boolean 
     */
    public function getFmfdVisible()
    {
        return $this->FmfdVisible;
    }

    /**
     * Set FmfdDescription
     *
     * @param string $fmfdDescription
     * @return Formfields
     */
    public function setFmfdDescription($fmfdDescription)
    {
        $this->FmfdDescription = $fmfdDescription;

        return $this;
    }

    /**
     * Get FmfdDescription
     *
     * @return string 
     */
    public function getFmfdDescription()
    {
        return $this->FmfdDescription;
    }

    /**
     * Set FmfdEntityOverrides
     *
     * @param string $fmfdEntityOverrides
     * @return Formfields
     */
    public function setFmfdEntityOverrides($fmfdEntityOverrides)
    {
        $this->FmfdEntityOverrides = $fmfdEntityOverrides;

        return $this;
    }

    /**
     * Get FmfdEntityOverrides
     *
     * @return string 
     */
    public function getFmfdEntityOverrides()
    {
        return $this->FmfdEntityOverrides;
    }

    /**
     * Set FmfdFmfsId
     *
     * @param integer $fmfdFmfsId
     * @return Formfields
     */
    public function setFmfdFmfsId($fmfdFmfsId)
    {
        $this->FmfdFmfsId = $fmfdFmfsId;

        return $this;
    }

    /**
     * Get FmfdFmfsId
     *
     * @return integer 
     */
    public function getFmfdFmfsId()
    {
        return $this->FmfdFmfsId;
    }

    /**
     * Set FmfdFldId
     *
     * @param integer $fmfdFldId
     * @return Formfields
     */
    public function setFmfdFldId($fmfdFldId)
    {
        $this->FmfdFldId = $fmfdFldId;

        return $this;
    }

    /**
     * Get FmfdFldId
     *
     * @return integer 
     */
    public function getFmfdFldId()
    {
        return $this->FmfdFldId;
    }
}
