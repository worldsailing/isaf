<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Adminpermission
 *
 * @ORM\Table(name="AdminPermission", uniqueConstraints={@ORM\UniqueConstraint(name="admperm_uq_code", columns={"AdmPermCode"})})
 * @ORM\Entity
 */
class Adminpermission
{
    /**
     * @var string
     *
     * @ORM\Column(name="AdmPermCode", type="string", length=45, nullable=false)
     */
    protected $AdmPermCode;

    /**
     * @var string
     *
     * @ORM\Column(name="AdmPermEnglish", type="string", length=100, nullable=false)
     */
    protected $AdmPermEnglish;

    /**
     * @var integer
     *
     * @ORM\Column(name="AdmPermId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $AdmPermId;



    /**
     * Set AdmPermCode
     *
     * @param string $admPermCode
     * @return Adminpermission
     */
    public function setAdmPermCode($admPermCode)
    {
        $this->AdmPermCode = $admPermCode;

        return $this;
    }

    /**
     * Get AdmPermCode
     *
     * @return string 
     */
    public function getAdmPermCode()
    {
        return $this->AdmPermCode;
    }

    /**
     * Set AdmPermEnglish
     *
     * @param string $admPermEnglish
     * @return Adminpermission
     */
    public function setAdmPermEnglish($admPermEnglish)
    {
        $this->AdmPermEnglish = $admPermEnglish;

        return $this;
    }

    /**
     * Get AdmPermEnglish
     *
     * @return string 
     */
    public function getAdmPermEnglish()
    {
        return $this->AdmPermEnglish;
    }

    /**
     * Get AdmPermId
     *
     * @return integer 
     */
    public function getAdmPermId()
    {
        return $this->AdmPermId;
    }
}
