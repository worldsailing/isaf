<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Officials
 *
 * @ORM\Table(name="Officials")
 * @ORM\Entity
 */
class Officials
{
    /**
     * @var integer
     *
     * @ORM\Column(name="OffMembId", type="integer", nullable=false)
     */
    protected $OffMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="OffOrigMembId", type="integer", nullable=false)
     */
    protected $OffOrigMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="OffOfstId", type="integer", nullable=false)
     */
    protected $OffOfstId;

    /**
     * @var integer
     *
     * @ORM\Column(name="OffOrigOfstId", type="integer", nullable=false)
     */
    protected $OffOrigOfstId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="OffStart", type="date", nullable=false)
     */
    protected $OffStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="OffReapply", type="date", nullable=false)
     */
    protected $OffReapply;

    /**
     * @var integer
     *
     * @ORM\Column(name="OffClassId", type="integer", nullable=false)
     */
    protected $OffClassId;

    /**
     * @var integer
     *
     * @ORM\Column(name="OffOrigClassId", type="integer", nullable=false)
     */
    protected $OffOrigClassId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="OffPaid", type="boolean", nullable=false)
     */
    protected $OffPaid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="OffChair", type="boolean", nullable=false)
     */
    protected $OffChair;

    /**
     * @var integer
     *
     * @ORM\Column(name="OffId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $OffId;



    /**
     * Set OffMembId
     *
     * @param integer $offMembId
     * @return Officials
     */
    public function setOffMembId($offMembId)
    {
        $this->OffMembId = $offMembId;

        return $this;
    }

    /**
     * Get OffMembId
     *
     * @return integer 
     */
    public function getOffMembId()
    {
        return $this->OffMembId;
    }

    /**
     * Set OffOrigMembId
     *
     * @param integer $offOrigMembId
     * @return Officials
     */
    public function setOffOrigMembId($offOrigMembId)
    {
        $this->OffOrigMembId = $offOrigMembId;

        return $this;
    }

    /**
     * Get OffOrigMembId
     *
     * @return integer 
     */
    public function getOffOrigMembId()
    {
        return $this->OffOrigMembId;
    }

    /**
     * Set OffOfstId
     *
     * @param integer $offOfstId
     * @return Officials
     */
    public function setOffOfstId($offOfstId)
    {
        $this->OffOfstId = $offOfstId;

        return $this;
    }

    /**
     * Get OffOfstId
     *
     * @return integer 
     */
    public function getOffOfstId()
    {
        return $this->OffOfstId;
    }

    /**
     * Set OffOrigOfstId
     *
     * @param integer $offOrigOfstId
     * @return Officials
     */
    public function setOffOrigOfstId($offOrigOfstId)
    {
        $this->OffOrigOfstId = $offOrigOfstId;

        return $this;
    }

    /**
     * Get OffOrigOfstId
     *
     * @return integer 
     */
    public function getOffOrigOfstId()
    {
        return $this->OffOrigOfstId;
    }

    /**
     * Set OffStart
     *
     * @param \DateTime $offStart
     * @return Officials
     */
    public function setOffStart($offStart)
    {
        $this->OffStart = $offStart;

        return $this;
    }

    /**
     * Get OffStart
     *
     * @return \DateTime 
     */
    public function getOffStart()
    {
        return $this->OffStart;
    }

    /**
     * Set OffReapply
     *
     * @param \DateTime $offReapply
     * @return Officials
     */
    public function setOffReapply($offReapply)
    {
        $this->OffReapply = $offReapply;

        return $this;
    }

    /**
     * Get OffReapply
     *
     * @return \DateTime 
     */
    public function getOffReapply()
    {
        return $this->OffReapply;
    }

    /**
     * Set OffClassId
     *
     * @param integer $offClassId
     * @return Officials
     */
    public function setOffClassId($offClassId)
    {
        $this->OffClassId = $offClassId;

        return $this;
    }

    /**
     * Get OffClassId
     *
     * @return integer 
     */
    public function getOffClassId()
    {
        return $this->OffClassId;
    }

    /**
     * Set OffOrigClassId
     *
     * @param integer $offOrigClassId
     * @return Officials
     */
    public function setOffOrigClassId($offOrigClassId)
    {
        $this->OffOrigClassId = $offOrigClassId;

        return $this;
    }

    /**
     * Get OffOrigClassId
     *
     * @return integer 
     */
    public function getOffOrigClassId()
    {
        return $this->OffOrigClassId;
    }

    /**
     * Set OffPaid
     *
     * @param boolean $offPaid
     * @return Officials
     */
    public function setOffPaid($offPaid)
    {
        $this->OffPaid = $offPaid;

        return $this;
    }

    /**
     * Get OffPaid
     *
     * @return boolean 
     */
    public function getOffPaid()
    {
        return $this->OffPaid;
    }

    /**
     * Set OffChair
     *
     * @param boolean $offChair
     * @return Officials
     */
    public function setOffChair($offChair)
    {
        $this->OffChair = $offChair;

        return $this;
    }

    /**
     * Get OffChair
     *
     * @return boolean 
     */
    public function getOffChair()
    {
        return $this->OffChair;
    }

    /**
     * Get OffId
     *
     * @return integer 
     */
    public function getOffId()
    {
        return $this->OffId;
    }
}
