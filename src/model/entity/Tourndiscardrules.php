<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tourndiscardrules
 *
 * @ORM\Table(name="TournDiscardRules")
 * @ORM\Entity
 */
class Tourndiscardrules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="TdisTournId", type="integer", nullable=false)
     */
    protected $TdisTournId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TdisStart", type="integer", nullable=false)
     */
    protected $TdisStart;

    /**
     * @var integer
     *
     * @ORM\Column(name="TdisEnd", type="integer", nullable=false)
     */
    protected $TdisEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="TdisTies", type="string", length=100, nullable=true)
     */
    protected $TdisTies;

    /**
     * @var integer
     *
     * @ORM\Column(name="TdisId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $TdisId;



    /**
     * Set TdisTournId
     *
     * @param integer $tdisTournId
     * @return Tourndiscardrules
     */
    public function setTdisTournId($tdisTournId)
    {
        $this->TdisTournId = $tdisTournId;

        return $this;
    }

    /**
     * Get TdisTournId
     *
     * @return integer 
     */
    public function getTdisTournId()
    {
        return $this->TdisTournId;
    }

    /**
     * Set TdisStart
     *
     * @param integer $tdisStart
     * @return Tourndiscardrules
     */
    public function setTdisStart($tdisStart)
    {
        $this->TdisStart = $tdisStart;

        return $this;
    }

    /**
     * Get TdisStart
     *
     * @return integer 
     */
    public function getTdisStart()
    {
        return $this->TdisStart;
    }

    /**
     * Set TdisEnd
     *
     * @param integer $tdisEnd
     * @return Tourndiscardrules
     */
    public function setTdisEnd($tdisEnd)
    {
        $this->TdisEnd = $tdisEnd;

        return $this;
    }

    /**
     * Get TdisEnd
     *
     * @return integer 
     */
    public function getTdisEnd()
    {
        return $this->TdisEnd;
    }

    /**
     * Set TdisTies
     *
     * @param string $tdisTies
     * @return Tourndiscardrules
     */
    public function setTdisTies($tdisTies)
    {
        $this->TdisTies = $tdisTies;

        return $this;
    }

    /**
     * Get TdisTies
     *
     * @return string 
     */
    public function getTdisTies()
    {
        return $this->TdisTies;
    }

    /**
     * Get TdisId
     *
     * @return integer 
     */
    public function getTdisId()
    {
        return $this->TdisId;
    }
}
