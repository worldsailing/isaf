<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Configuration
 *
 * @ORM\Table(name="Configuration")
 * @ORM\Entity
 */
class Configuration
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfgValue", type="text", length=65535, nullable=true)
     */
    protected $CfgValue;

    /**
     * @var string
     *
     * @ORM\Column(name="CfgName", type="string", length=50)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CfgName;

    /**
     * @var string
     *
     * @ORM\Column(name="CfgContext", type="string", length=50)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CfgContext;



    /**
     * Set CfgValue
     *
     * @param string $cfgValue
     * @return Configuration
     */
    public function setCfgValue($cfgValue)
    {
        $this->CfgValue = $cfgValue;

        return $this;
    }

    /**
     * Get CfgValue
     *
     * @return string 
     */
    public function getCfgValue()
    {
        return $this->CfgValue;
    }

    /**
     * Set CfgName
     *
     * @param string $cfgName
     * @return Configuration
     */
    public function setCfgName($cfgName)
    {
        $this->CfgName = $cfgName;

        return $this;
    }

    /**
     * Get CfgName
     *
     * @return string 
     */
    public function getCfgName()
    {
        return $this->CfgName;
    }

    /**
     * Set CfgContext
     *
     * @param string $cfgContext
     * @return Configuration
     */
    public function setCfgContext($cfgContext)
    {
        $this->CfgContext = $cfgContext;

        return $this;
    }

    /**
     * Get CfgContext
     *
     * @return string 
     */
    public function getCfgContext()
    {
        return $this->CfgContext;
    }
}
