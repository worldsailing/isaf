<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationanswer
 *
 * @ORM\Table(name="ClassificationAnswer", indexes={@ORM\Index(name="fk_application_1", columns={"CfcnAnsAppId"}), @ORM\Index(name="fk_question_1", columns={"CfcnAnsQuestId", "CfcnAnsQuestVers"})})
 * @ORM\Entity
 */
class Classificationanswer
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfcnAnsContent", type="text", length=65535, nullable=true)
     */
    protected $CfcnAnsContent;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnAnsAdditional", type="text", length=65535, nullable=true)
     */
    protected $CfcnAnsAdditional;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnAnsId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CfcnAnsId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationquestion
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationquestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnAnsQuestId", referencedColumnName="QuestionId"),
     *   @ORM\JoinColumn(name="CfcnAnsQuestVers", referencedColumnName="QuestionVersion")
     * })
     */
    protected $cfcnAnsQuestId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationapplication
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationapplication")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnAnsAppId", referencedColumnName="CfcnAppId")
     * })
     */
    protected $cfcnAnsAppId;



    /**
     * Set CfcnAnsContent
     *
     * @param string $cfcnAnsContent
     * @return Classificationanswer
     */
    public function setCfcnAnsContent($cfcnAnsContent)
    {
        $this->CfcnAnsContent = $cfcnAnsContent;

        return $this;
    }

    /**
     * Get CfcnAnsContent
     *
     * @return string 
     */
    public function getCfcnAnsContent()
    {
        return $this->CfcnAnsContent;
    }

    /**
     * Set CfcnAnsAdditional
     *
     * @param string $cfcnAnsAdditional
     * @return Classificationanswer
     */
    public function setCfcnAnsAdditional($cfcnAnsAdditional)
    {
        $this->CfcnAnsAdditional = $cfcnAnsAdditional;

        return $this;
    }

    /**
     * Get CfcnAnsAdditional
     *
     * @return string 
     */
    public function getCfcnAnsAdditional()
    {
        return $this->CfcnAnsAdditional;
    }

    /**
     * Get CfcnAnsId
     *
     * @return integer 
     */
    public function getCfcnAnsId()
    {
        return $this->CfcnAnsId;
    }

    /**
     * Set cfcnAnsQuestId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationquestion $cfcnAnsQuestId
     * @return Classificationanswer
     */
    public function setCfcnAnsQuestId(\worldsailing\Isaf\model\Entity\Classificationquestion $cfcnAnsQuestId = null)
    {
        $this->cfcnAnsQuestId = $cfcnAnsQuestId;

        return $this;
    }

    /**
     * Get cfcnAnsQuestId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationquestion
     */
    public function getCfcnAnsQuestId()
    {
        return $this->cfcnAnsQuestId;
    }

    /**
     * Set cfcnAnsAppId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationapplication $cfcnAnsAppId
     * @return Classificationanswer
     */
    public function setCfcnAnsAppId(\worldsailing\Isaf\model\Entity\Classificationapplication $cfcnAnsAppId = null)
    {
        $this->cfcnAnsAppId = $cfcnAnsAppId;

        return $this;
    }

    /**
     * Get cfcnAnsAppId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationapplication
     */
    public function getCfcnAnsAppId()
    {
        return $this->cfcnAnsAppId;
    }
}
