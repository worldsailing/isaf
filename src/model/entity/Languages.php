<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Languages
 *
 * @ORM\Table(name="Languages", uniqueConstraints={@ORM\UniqueConstraint(name="LangCode", columns={"LangCode"})})
 * @ORM\Entity
 */
class Languages
{
    /**
     * @var string
     *
     * @ORM\Column(name="LangName", type="string", length=100, nullable=true)
     */
    protected $LangName;

    /**
     * @var string
     *
     * @ORM\Column(name="LangCode", type="string", length=3, nullable=true)
     */
    protected $LangCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="LangId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $LangId;



    /**
     * Set LangName
     *
     * @param string $langName
     * @return Languages
     */
    public function setLangName($langName)
    {
        $this->LangName = $langName;

        return $this;
    }

    /**
     * Get LangName
     *
     * @return string 
     */
    public function getLangName()
    {
        return $this->LangName;
    }

    /**
     * Set LangCode
     *
     * @param string $langCode
     * @return Languages
     */
    public function setLangCode($langCode)
    {
        $this->LangCode = $langCode;

        return $this;
    }

    /**
     * Get LangCode
     *
     * @return string 
     */
    public function getLangCode()
    {
        return $this->LangCode;
    }

    /**
     * Get LangId
     *
     * @return integer 
     */
    public function getLangId()
    {
        return $this->LangId;
    }
}
