<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Osradditionals
 *
 * @ORM\Table(name="OSRAdditionals", indexes={@ORM\Index(name="IdxOsrAdditionals", columns={"OSRAddName"})})
 * @ORM\Entity
 */
class Osradditionals
{
    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddName", type="string", length=20, nullable=true)
     */
    protected $OSRAddName;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddFontColorScheme", type="string", length=20, nullable=true)
     */
    protected $OSRAddFontColorScheme;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddFontItalic", type="string", length=2, nullable=true)
     */
    protected $OSRAddFontItalic;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddFontBold", type="string", length=2, nullable=true)
     */
    protected $OSRAddFontBold;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddFormat", type="text", length=65535, nullable=true)
     */
    protected $OSRAddFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol01", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol01;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol02", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol02;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol03", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol03;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol04", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol04;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol05", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol05;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol06", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol06;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol07", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol07;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol08", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol08;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol09", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol09;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRAddCol10", type="text", length=65535, nullable=true)
     */
    protected $OSRAddCol10;

    /**
     * @var integer
     *
     * @ORM\Column(name="OSRAddId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $OSRAddId;



    /**
     * Set OSRAddName
     *
     * @param string $oSRAddName
     * @return Osradditionals
     */
    public function setOSRAddName($oSRAddName)
    {
        $this->OSRAddName = $oSRAddName;

        return $this;
    }

    /**
     * Get OSRAddName
     *
     * @return string 
     */
    public function getOSRAddName()
    {
        return $this->OSRAddName;
    }

    /**
     * Set OSRAddFontColorScheme
     *
     * @param string $oSRAddFontColorScheme
     * @return Osradditionals
     */
    public function setOSRAddFontColorScheme($oSRAddFontColorScheme)
    {
        $this->OSRAddFontColorScheme = $oSRAddFontColorScheme;

        return $this;
    }

    /**
     * Get OSRAddFontColorScheme
     *
     * @return string 
     */
    public function getOSRAddFontColorScheme()
    {
        return $this->OSRAddFontColorScheme;
    }

    /**
     * Set OSRAddFontItalic
     *
     * @param string $oSRAddFontItalic
     * @return Osradditionals
     */
    public function setOSRAddFontItalic($oSRAddFontItalic)
    {
        $this->OSRAddFontItalic = $oSRAddFontItalic;

        return $this;
    }

    /**
     * Get OSRAddFontItalic
     *
     * @return string 
     */
    public function getOSRAddFontItalic()
    {
        return $this->OSRAddFontItalic;
    }

    /**
     * Set OSRAddFontBold
     *
     * @param string $oSRAddFontBold
     * @return Osradditionals
     */
    public function setOSRAddFontBold($oSRAddFontBold)
    {
        $this->OSRAddFontBold = $oSRAddFontBold;

        return $this;
    }

    /**
     * Get OSRAddFontBold
     *
     * @return string 
     */
    public function getOSRAddFontBold()
    {
        return $this->OSRAddFontBold;
    }

    /**
     * Set OSRAddFormat
     *
     * @param string $oSRAddFormat
     * @return Osradditionals
     */
    public function setOSRAddFormat($oSRAddFormat)
    {
        $this->OSRAddFormat = $oSRAddFormat;

        return $this;
    }

    /**
     * Get OSRAddFormat
     *
     * @return string 
     */
    public function getOSRAddFormat()
    {
        return $this->OSRAddFormat;
    }

    /**
     * Set OSRAddCol01
     *
     * @param string $oSRAddCol01
     * @return Osradditionals
     */
    public function setOSRAddCol01($oSRAddCol01)
    {
        $this->OSRAddCol01 = $oSRAddCol01;

        return $this;
    }

    /**
     * Get OSRAddCol01
     *
     * @return string 
     */
    public function getOSRAddCol01()
    {
        return $this->OSRAddCol01;
    }

    /**
     * Set OSRAddCol02
     *
     * @param string $oSRAddCol02
     * @return Osradditionals
     */
    public function setOSRAddCol02($oSRAddCol02)
    {
        $this->OSRAddCol02 = $oSRAddCol02;

        return $this;
    }

    /**
     * Get OSRAddCol02
     *
     * @return string 
     */
    public function getOSRAddCol02()
    {
        return $this->OSRAddCol02;
    }

    /**
     * Set OSRAddCol03
     *
     * @param string $oSRAddCol03
     * @return Osradditionals
     */
    public function setOSRAddCol03($oSRAddCol03)
    {
        $this->OSRAddCol03 = $oSRAddCol03;

        return $this;
    }

    /**
     * Get OSRAddCol03
     *
     * @return string 
     */
    public function getOSRAddCol03()
    {
        return $this->OSRAddCol03;
    }

    /**
     * Set OSRAddCol04
     *
     * @param string $oSRAddCol04
     * @return Osradditionals
     */
    public function setOSRAddCol04($oSRAddCol04)
    {
        $this->OSRAddCol04 = $oSRAddCol04;

        return $this;
    }

    /**
     * Get OSRAddCol04
     *
     * @return string 
     */
    public function getOSRAddCol04()
    {
        return $this->OSRAddCol04;
    }

    /**
     * Set OSRAddCol05
     *
     * @param string $oSRAddCol05
     * @return Osradditionals
     */
    public function setOSRAddCol05($oSRAddCol05)
    {
        $this->OSRAddCol05 = $oSRAddCol05;

        return $this;
    }

    /**
     * Get OSRAddCol05
     *
     * @return string 
     */
    public function getOSRAddCol05()
    {
        return $this->OSRAddCol05;
    }

    /**
     * Set OSRAddCol06
     *
     * @param string $oSRAddCol06
     * @return Osradditionals
     */
    public function setOSRAddCol06($oSRAddCol06)
    {
        $this->OSRAddCol06 = $oSRAddCol06;

        return $this;
    }

    /**
     * Get OSRAddCol06
     *
     * @return string 
     */
    public function getOSRAddCol06()
    {
        return $this->OSRAddCol06;
    }

    /**
     * Set OSRAddCol07
     *
     * @param string $oSRAddCol07
     * @return Osradditionals
     */
    public function setOSRAddCol07($oSRAddCol07)
    {
        $this->OSRAddCol07 = $oSRAddCol07;

        return $this;
    }

    /**
     * Get OSRAddCol07
     *
     * @return string 
     */
    public function getOSRAddCol07()
    {
        return $this->OSRAddCol07;
    }

    /**
     * Set OSRAddCol08
     *
     * @param string $oSRAddCol08
     * @return Osradditionals
     */
    public function setOSRAddCol08($oSRAddCol08)
    {
        $this->OSRAddCol08 = $oSRAddCol08;

        return $this;
    }

    /**
     * Get OSRAddCol08
     *
     * @return string 
     */
    public function getOSRAddCol08()
    {
        return $this->OSRAddCol08;
    }

    /**
     * Set OSRAddCol09
     *
     * @param string $oSRAddCol09
     * @return Osradditionals
     */
    public function setOSRAddCol09($oSRAddCol09)
    {
        $this->OSRAddCol09 = $oSRAddCol09;

        return $this;
    }

    /**
     * Get OSRAddCol09
     *
     * @return string 
     */
    public function getOSRAddCol09()
    {
        return $this->OSRAddCol09;
    }

    /**
     * Set OSRAddCol10
     *
     * @param string $oSRAddCol10
     * @return Osradditionals
     */
    public function setOSRAddCol10($oSRAddCol10)
    {
        $this->OSRAddCol10 = $oSRAddCol10;

        return $this;
    }

    /**
     * Get OSRAddCol10
     *
     * @return string 
     */
    public function getOSRAddCol10()
    {
        return $this->OSRAddCol10;
    }

    /**
     * Get OSRAddId
     *
     * @return integer 
     */
    public function getOSRAddId()
    {
        return $this->OSRAddId;
    }
}
