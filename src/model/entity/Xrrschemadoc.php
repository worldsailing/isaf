<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Xrrschemadoc
 *
 * @ORM\Table(name="XRRSchemaDoc")
 * @ORM\Entity
 */
class Xrrschemadoc
{
    /**
     * @var string
     *
     * @ORM\Column(name="XRRSchDocContent", type="text", nullable=false)
     */
    protected $XRRSchDocContent;

    /**
     * @var string
     *
     * @ORM\Column(name="XRRSchDocType", type="string", length=45, nullable=false)
     */
    protected $XRRSchDocType;

    /**
     * @var string
     *
     * @ORM\Column(name="XRRSchDocVersion", type="string", length=45, nullable=false)
     */
    protected $XRRSchDocVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="XRRSchDocFilename", type="string", length=500, nullable=false)
     */
    protected $XRRSchDocFilename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="XRRSchDocCreateDate", type="datetime", nullable=false)
     */
    protected $XRRSchDocCreateDate;

    /**
     * @var string
     *
     * @ORM\Column(name="XRRSchDocCreatedBy", type="string", length=45, nullable=false)
     */
    protected $XRRSchDocCreatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="XRRSchDocTitle", type="string", length=500, nullable=false)
     */
    protected $XRRSchDocTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="XRRSchDocId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $XRRSchDocId;



    /**
     * Set XRRSchDocContent
     *
     * @param string $xRRSchDocContent
     * @return Xrrschemadoc
     */
    public function setXRRSchDocContent($xRRSchDocContent)
    {
        $this->XRRSchDocContent = $xRRSchDocContent;

        return $this;
    }

    /**
     * Get XRRSchDocContent
     *
     * @return string 
     */
    public function getXRRSchDocContent()
    {
        return $this->XRRSchDocContent;
    }

    /**
     * Set XRRSchDocType
     *
     * @param string $xRRSchDocType
     * @return Xrrschemadoc
     */
    public function setXRRSchDocType($xRRSchDocType)
    {
        $this->XRRSchDocType = $xRRSchDocType;

        return $this;
    }

    /**
     * Get XRRSchDocType
     *
     * @return string 
     */
    public function getXRRSchDocType()
    {
        return $this->XRRSchDocType;
    }

    /**
     * Set XRRSchDocVersion
     *
     * @param string $xRRSchDocVersion
     * @return Xrrschemadoc
     */
    public function setXRRSchDocVersion($xRRSchDocVersion)
    {
        $this->XRRSchDocVersion = $xRRSchDocVersion;

        return $this;
    }

    /**
     * Get XRRSchDocVersion
     *
     * @return string 
     */
    public function getXRRSchDocVersion()
    {
        return $this->XRRSchDocVersion;
    }

    /**
     * Set XRRSchDocFilename
     *
     * @param string $xRRSchDocFilename
     * @return Xrrschemadoc
     */
    public function setXRRSchDocFilename($xRRSchDocFilename)
    {
        $this->XRRSchDocFilename = $xRRSchDocFilename;

        return $this;
    }

    /**
     * Get XRRSchDocFilename
     *
     * @return string 
     */
    public function getXRRSchDocFilename()
    {
        return $this->XRRSchDocFilename;
    }

    /**
     * Set XRRSchDocCreateDate
     *
     * @param \DateTime $xRRSchDocCreateDate
     * @return Xrrschemadoc
     */
    public function setXRRSchDocCreateDate($xRRSchDocCreateDate)
    {
        $this->XRRSchDocCreateDate = $xRRSchDocCreateDate;

        return $this;
    }

    /**
     * Get XRRSchDocCreateDate
     *
     * @return \DateTime 
     */
    public function getXRRSchDocCreateDate()
    {
        return $this->XRRSchDocCreateDate;
    }

    /**
     * Set XRRSchDocCreatedBy
     *
     * @param string $xRRSchDocCreatedBy
     * @return Xrrschemadoc
     */
    public function setXRRSchDocCreatedBy($xRRSchDocCreatedBy)
    {
        $this->XRRSchDocCreatedBy = $xRRSchDocCreatedBy;

        return $this;
    }

    /**
     * Get XRRSchDocCreatedBy
     *
     * @return string 
     */
    public function getXRRSchDocCreatedBy()
    {
        return $this->XRRSchDocCreatedBy;
    }

    /**
     * Set XRRSchDocTitle
     *
     * @param string $xRRSchDocTitle
     * @return Xrrschemadoc
     */
    public function setXRRSchDocTitle($xRRSchDocTitle)
    {
        $this->XRRSchDocTitle = $xRRSchDocTitle;

        return $this;
    }

    /**
     * Get XRRSchDocTitle
     *
     * @return string 
     */
    public function getXRRSchDocTitle()
    {
        return $this->XRRSchDocTitle;
    }

    /**
     * Get XRRSchDocId
     *
     * @return integer 
     */
    public function getXRRSchDocId()
    {
        return $this->XRRSchDocId;
    }
}
