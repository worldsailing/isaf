<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classstatus
 *
 * @ORM\Table(name="ClassStatus")
 * @ORM\Entity
 */
class Classstatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="StatName", type="string", length=100, nullable=true)
     */
    protected $StatName;

    /**
     * @var integer
     *
     * @ORM\Column(name="StatId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $StatId;



    /**
     * Set StatName
     *
     * @param string $statName
     * @return Classstatus
     */
    public function setStatName($statName)
    {
        $this->StatName = $statName;

        return $this;
    }

    /**
     * Get StatName
     *
     * @return string 
     */
    public function getStatName()
    {
        return $this->StatName;
    }

    /**
     * Get StatId
     *
     * @return integer 
     */
    public function getStatId()
    {
        return $this->StatId;
    }
}
