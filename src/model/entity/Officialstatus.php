<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Officialstatus
 *
 * @ORM\Table(name="OfficialStatus")
 * @ORM\Entity
 */
class Officialstatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="OfstOrigOfstId", type="integer", nullable=true)
     */
    protected $OfstOrigOfstId;

    /**
     * @var string
     *
     * @ORM\Column(name="OfstType", type="string", nullable=false)
     */
    protected $OfstType;

    /**
     * @var string
     *
     * @ORM\Column(name="OfstName", type="string", length=255, nullable=true)
     */
    protected $OfstName;

    /**
     * @var string
     *
     * @ORM\Column(name="OfstAbbrev", type="string", length=50, nullable=true)
     */
    protected $OfstAbbrev;

    /**
     * @var boolean
     *
     * @ORM\Column(name="OfstDisplay", type="boolean", nullable=false)
     */
    protected $OfstDisplay;

    /**
     * @var integer
     *
     * @ORM\Column(name="OfstId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $OfstId;



    /**
     * Set OfstOrigOfstId
     *
     * @param integer $ofstOrigOfstId
     * @return Officialstatus
     */
    public function setOfstOrigOfstId($ofstOrigOfstId)
    {
        $this->OfstOrigOfstId = $ofstOrigOfstId;

        return $this;
    }

    /**
     * Get OfstOrigOfstId
     *
     * @return integer 
     */
    public function getOfstOrigOfstId()
    {
        return $this->OfstOrigOfstId;
    }

    /**
     * Set OfstType
     *
     * @param string $ofstType
     * @return Officialstatus
     */
    public function setOfstType($ofstType)
    {
        $this->OfstType = $ofstType;

        return $this;
    }

    /**
     * Get OfstType
     *
     * @return string 
     */
    public function getOfstType()
    {
        return $this->OfstType;
    }

    /**
     * Set OfstName
     *
     * @param string $ofstName
     * @return Officialstatus
     */
    public function setOfstName($ofstName)
    {
        $this->OfstName = $ofstName;

        return $this;
    }

    /**
     * Get OfstName
     *
     * @return string 
     */
    public function getOfstName()
    {
        return $this->OfstName;
    }

    /**
     * Set OfstAbbrev
     *
     * @param string $ofstAbbrev
     * @return Officialstatus
     */
    public function setOfstAbbrev($ofstAbbrev)
    {
        $this->OfstAbbrev = $ofstAbbrev;

        return $this;
    }

    /**
     * Get OfstAbbrev
     *
     * @return string 
     */
    public function getOfstAbbrev()
    {
        return $this->OfstAbbrev;
    }

    /**
     * Set OfstDisplay
     *
     * @param boolean $ofstDisplay
     * @return Officialstatus
     */
    public function setOfstDisplay($ofstDisplay)
    {
        $this->OfstDisplay = $ofstDisplay;

        return $this;
    }

    /**
     * Get OfstDisplay
     *
     * @return boolean 
     */
    public function getOfstDisplay()
    {
        return $this->OfstDisplay;
    }

    /**
     * Get OfstId
     *
     * @return integer 
     */
    public function getOfstId()
    {
        return $this->OfstId;
    }
}
