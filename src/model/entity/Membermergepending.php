<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Membermergepending
 *
 * @ORM\Table(name="MemberMergePending", indexes={@ORM\Index(name="fk_mrgpnd_biog", columns={"MrgPndMembId"}), @ORM\Index(name="fk_mrgpnd_tbiog", columns={"MrgPndTargetId"})})
 * @ORM\Entity
 */
class Membermergepending
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MrgPndDate", type="datetime", nullable=false)
     */
    protected $MrgPndDate;

    /**
     * @var string
     *
     * @ORM\Column(name="MrgPndSession", type="text", nullable=false)
     */
    protected $MrgPndSession;

    /**
     * @var integer
     *
     * @ORM\Column(name="MrgPndId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $MrgPndId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Memberbiogs
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Memberbiogs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="MrgPndTargetId", referencedColumnName="BiogMembId")
     * })
     */
    protected $mrgPndTargetId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Memberbiogs
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Memberbiogs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="MrgPndMembId", referencedColumnName="BiogMembId")
     * })
     */
    protected $mrgPndMembId;



    /**
     * Set MrgPndDate
     *
     * @param \DateTime $mrgPndDate
     * @return Membermergepending
     */
    public function setMrgPndDate($mrgPndDate)
    {
        $this->MrgPndDate = $mrgPndDate;

        return $this;
    }

    /**
     * Get MrgPndDate
     *
     * @return \DateTime 
     */
    public function getMrgPndDate()
    {
        return $this->MrgPndDate;
    }

    /**
     * Set MrgPndSession
     *
     * @param string $mrgPndSession
     * @return Membermergepending
     */
    public function setMrgPndSession($mrgPndSession)
    {
        $this->MrgPndSession = $mrgPndSession;

        return $this;
    }

    /**
     * Get MrgPndSession
     *
     * @return string 
     */
    public function getMrgPndSession()
    {
        return $this->MrgPndSession;
    }

    /**
     * Get MrgPndId
     *
     * @return integer 
     */
    public function getMrgPndId()
    {
        return $this->MrgPndId;
    }

    /**
     * Set mrgPndTargetId
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $mrgPndTargetId
     * @return Membermergepending
     */
    public function setMrgPndTargetId(\worldsailing\Isaf\model\Entity\Memberbiogs $mrgPndTargetId = null)
    {
        $this->mrgPndTargetId = $mrgPndTargetId;

        return $this;
    }

    /**
     * Get mrgPndTargetId
     *
     * @return \worldsailing\Isaf\model\Entity\Memberbiogs
     */
    public function getMrgPndTargetId()
    {
        return $this->mrgPndTargetId;
    }

    /**
     * Set mrgPndMembId
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $mrgPndMembId
     * @return Membermergepending
     */
    public function setMrgPndMembId(\worldsailing\Isaf\model\Entity\Memberbiogs $mrgPndMembId = null)
    {
        $this->mrgPndMembId = $mrgPndMembId;

        return $this;
    }

    /**
     * Get mrgPndMembId
     *
     * @return \worldsailing\Isaf\model\Entity\Memberbiogs
     */
    public function getMrgPndMembId()
    {
        return $this->mrgPndMembId;
    }
}
