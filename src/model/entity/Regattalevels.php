<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Regattalevels
 *
 * @ORM\Table(name="RegattaLevels")
 * @ORM\Entity
 */
class Regattalevels
{
    /**
     * @var string
     *
     * @ORM\Column(name="LevName", type="string", length=50, nullable=true)
     */
    protected $LevName;

    /**
     * @var integer
     *
     * @ORM\Column(name="LevId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $LevId;



    /**
     * Set LevName
     *
     * @param string $levName
     * @return Regattalevels
     */
    public function setLevName($levName)
    {
        $this->LevName = $levName;

        return $this;
    }

    /**
     * Get LevName
     *
     * @return string 
     */
    public function getLevName()
    {
        return $this->LevName;
    }

    /**
     * Get LevId
     *
     * @return integer 
     */
    public function getLevId()
    {
        return $this->LevId;
    }
}
