<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nonmembersailor
 *
 * @ORM\Table(name="NonMemberSailor")
 * @ORM\Entity
 */
class Nonmembersailor
{
    /**
     * @var string
     *
     * @ORM\Column(name="NonMembSailFirstName", type="string", length=50, nullable=true)
     */
    protected $NonMembSailFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="NonMembSailSurname", type="string", length=50, nullable=true)
     */
    protected $NonMembSailSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="NonMembSailCtryCode", type="string", length=5, nullable=true)
     */
    protected $NonMembSailCtryCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="NonMembId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $NonMembId;



    /**
     * Set NonMembSailFirstName
     *
     * @param string $nonMembSailFirstName
     * @return Nonmembersailor
     */
    public function setNonMembSailFirstName($nonMembSailFirstName)
    {
        $this->NonMembSailFirstName = $nonMembSailFirstName;

        return $this;
    }

    /**
     * Get NonMembSailFirstName
     *
     * @return string 
     */
    public function getNonMembSailFirstName()
    {
        return $this->NonMembSailFirstName;
    }

    /**
     * Set NonMembSailSurname
     *
     * @param string $nonMembSailSurname
     * @return Nonmembersailor
     */
    public function setNonMembSailSurname($nonMembSailSurname)
    {
        $this->NonMembSailSurname = $nonMembSailSurname;

        return $this;
    }

    /**
     * Get NonMembSailSurname
     *
     * @return string 
     */
    public function getNonMembSailSurname()
    {
        return $this->NonMembSailSurname;
    }

    /**
     * Set NonMembSailCtryCode
     *
     * @param string $nonMembSailCtryCode
     * @return Nonmembersailor
     */
    public function setNonMembSailCtryCode($nonMembSailCtryCode)
    {
        $this->NonMembSailCtryCode = $nonMembSailCtryCode;

        return $this;
    }

    /**
     * Get NonMembSailCtryCode
     *
     * @return string 
     */
    public function getNonMembSailCtryCode()
    {
        return $this->NonMembSailCtryCode;
    }

    /**
     * Get NonMembId
     *
     * @return integer 
     */
    public function getNonMembId()
    {
        return $this->NonMembId;
    }
}
