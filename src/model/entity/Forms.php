<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Forms
 *
 * @ORM\Table(name="forms")
 * @ORM\Entity
 */
class Forms
{
    /**
     * @var string
     *
     * @ORM\Column(name="FrmName", type="string", length=50, nullable=false)
     */
    protected $FrmName;

    /**
     * @var string
     *
     * @ORM\Column(name="FrmDisplayName", type="string", length=100, nullable=true)
     */
    protected $FrmDisplayName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FrmSystem", type="boolean", nullable=false)
     */
    protected $FrmSystem;

    /**
     * @var integer
     *
     * @ORM\Column(name="FrmId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $FrmId;



    /**
     * Set FrmName
     *
     * @param string $frmName
     * @return Forms
     */
    public function setFrmName($frmName)
    {
        $this->FrmName = $frmName;

        return $this;
    }

    /**
     * Get FrmName
     *
     * @return string 
     */
    public function getFrmName()
    {
        return $this->FrmName;
    }

    /**
     * Set FrmDisplayName
     *
     * @param string $frmDisplayName
     * @return Forms
     */
    public function setFrmDisplayName($frmDisplayName)
    {
        $this->FrmDisplayName = $frmDisplayName;

        return $this;
    }

    /**
     * Get FrmDisplayName
     *
     * @return string 
     */
    public function getFrmDisplayName()
    {
        return $this->FrmDisplayName;
    }

    /**
     * Set FrmSystem
     *
     * @param boolean $frmSystem
     * @return Forms
     */
    public function setFrmSystem($frmSystem)
    {
        $this->FrmSystem = $frmSystem;

        return $this;
    }

    /**
     * Get FrmSystem
     *
     * @return boolean 
     */
    public function getFrmSystem()
    {
        return $this->FrmSystem;
    }

    /**
     * Get FrmId
     *
     * @return integer 
     */
    public function getFrmId()
    {
        return $this->FrmId;
    }
}
