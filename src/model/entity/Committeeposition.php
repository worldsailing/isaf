<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Committeeposition
 *
 * @ORM\Table(name="CommitteePosition")
 * @ORM\Entity
 */
class Committeeposition
{
    /**
     * @var string
     *
     * @ORM\Column(name="CposName", type="string", length=100, nullable=true)
     */
    protected $CposName;

    /**
     * @var integer
     *
     * @ORM\Column(name="CposId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CposId;



    /**
     * Set CposName
     *
     * @param string $cposName
     * @return Committeeposition
     */
    public function setCposName($cposName)
    {
        $this->CposName = $cposName;

        return $this;
    }

    /**
     * Get CposName
     *
     * @return string 
     */
    public function getCposName()
    {
        return $this->CposName;
    }

    /**
     * Get CposId
     *
     * @return integer 
     */
    public function getCposId()
    {
        return $this->CposId;
    }
}
