<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Regattaroletemplates
 *
 * @ORM\Table(name="RegattaRoleTemplates")
 * @ORM\Entity
 */
class Regattaroletemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RgtaId", type="integer", nullable=false)
     */
    protected $RgtaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RlId", type="integer", nullable=false)
     */
    protected $RlId;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleAdminInvitation", type="text", length=65535, nullable=false)
     */
    protected $RoleAdminInvitation;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleOrgInvitation", type="text", length=65535, nullable=false)
     */
    protected $RoleOrgInvitation;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleAdminYouthInvitation", type="text", length=65535, nullable=false)
     */
    protected $RoleAdminYouthInvitation;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $Id;



    /**
     * Set RgtaId
     *
     * @param integer $rgtaId
     * @return Regattaroletemplates
     */
    public function setRgtaId($rgtaId)
    {
        $this->RgtaId = $rgtaId;

        return $this;
    }

    /**
     * Get RgtaId
     *
     * @return integer 
     */
    public function getRgtaId()
    {
        return $this->RgtaId;
    }

    /**
     * Set RlId
     *
     * @param integer $rlId
     * @return Regattaroletemplates
     */
    public function setRlId($rlId)
    {
        $this->RlId = $rlId;

        return $this;
    }

    /**
     * Get RlId
     *
     * @return integer 
     */
    public function getRlId()
    {
        return $this->RlId;
    }

    /**
     * Set RoleAdminInvitation
     *
     * @param string $roleAdminInvitation
     * @return Regattaroletemplates
     */
    public function setRoleAdminInvitation($roleAdminInvitation)
    {
        $this->RoleAdminInvitation = $roleAdminInvitation;

        return $this;
    }

    /**
     * Get RoleAdminInvitation
     *
     * @return string 
     */
    public function getRoleAdminInvitation()
    {
        return $this->RoleAdminInvitation;
    }

    /**
     * Set RoleOrgInvitation
     *
     * @param string $roleOrgInvitation
     * @return Regattaroletemplates
     */
    public function setRoleOrgInvitation($roleOrgInvitation)
    {
        $this->RoleOrgInvitation = $roleOrgInvitation;

        return $this;
    }

    /**
     * Get RoleOrgInvitation
     *
     * @return string 
     */
    public function getRoleOrgInvitation()
    {
        return $this->RoleOrgInvitation;
    }

    /**
     * Set RoleAdminYouthInvitation
     *
     * @param string $roleAdminYouthInvitation
     * @return Regattaroletemplates
     */
    public function setRoleAdminYouthInvitation($roleAdminYouthInvitation)
    {
        $this->RoleAdminYouthInvitation = $roleAdminYouthInvitation;

        return $this;
    }

    /**
     * Get RoleAdminYouthInvitation
     *
     * @return string 
     */
    public function getRoleAdminYouthInvitation()
    {
        return $this->RoleAdminYouthInvitation;
    }

    /**
     * Get Id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->Id;
    }
}
