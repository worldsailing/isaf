<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationaction
 *
 * @ORM\Table(name="ClassificationAction", indexes={@ORM\Index(name="fk_action_relaction", columns={"CfcnActionRelActionId"}), @ORM\Index(name="fk_action_biog_by", columns={"CfcnActionByMembId"}), @ORM\Index(name="fk_action_biog_for", columns={"CfcnActionForMembId"})})
 * @ORM\Entity
 */
class Classificationaction
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfcnActionType", type="string", nullable=true)
     */
    protected $CfcnActionType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnActionDate", type="datetime", nullable=true)
     */
    protected $CfcnActionDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnActionByMembId", type="integer", nullable=false)
     */
    protected $CfcnActionByMembId;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnActionStatus", type="string", nullable=false)
     */
    protected $CfcnActionStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CfcnLegacyImport", type="boolean", nullable=true)
     */
    protected $CfcnLegacyImport;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnActionId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CfcnActionId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnActionRelActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnActionRelActionId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Memberbiogs
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Memberbiogs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnActionForMembId", referencedColumnName="BiogMembId")
     * })
     */
    protected $cfcnActionForMembId;



    /**
     * Set CfcnActionType
     *
     * @param string $cfcnActionType
     * @return Classificationaction
     */
    public function setCfcnActionType($cfcnActionType)
    {
        $this->CfcnActionType = $cfcnActionType;

        return $this;
    }

    /**
     * Get CfcnActionType
     *
     * @return string 
     */
    public function getCfcnActionType()
    {
        return $this->CfcnActionType;
    }

    /**
     * Set CfcnActionDate
     *
     * @param \DateTime $cfcnActionDate
     * @return Classificationaction
     */
    public function setCfcnActionDate($cfcnActionDate)
    {
        $this->CfcnActionDate = $cfcnActionDate;

        return $this;
    }

    /**
     * Get CfcnActionDate
     *
     * @return \DateTime 
     */
    public function getCfcnActionDate()
    {
        return $this->CfcnActionDate;
    }

    /**
     * Set CfcnActionByMembId
     *
     * @param integer $cfcnActionByMembId
     * @return Classificationaction
     */
    public function setCfcnActionByMembId($cfcnActionByMembId)
    {
        $this->CfcnActionByMembId = $cfcnActionByMembId;

        return $this;
    }

    /**
     * Get CfcnActionByMembId
     *
     * @return integer 
     */
    public function getCfcnActionByMembId()
    {
        return $this->CfcnActionByMembId;
    }

    /**
     * Set CfcnActionStatus
     *
     * @param string $cfcnActionStatus
     * @return Classificationaction
     */
    public function setCfcnActionStatus($cfcnActionStatus)
    {
        $this->CfcnActionStatus = $cfcnActionStatus;

        return $this;
    }

    /**
     * Get CfcnActionStatus
     *
     * @return string 
     */
    public function getCfcnActionStatus()
    {
        return $this->CfcnActionStatus;
    }

    /**
     * Set CfcnLegacyImport
     *
     * @param boolean $cfcnLegacyImport
     * @return Classificationaction
     */
    public function setCfcnLegacyImport($cfcnLegacyImport)
    {
        $this->CfcnLegacyImport = $cfcnLegacyImport;

        return $this;
    }

    /**
     * Get CfcnLegacyImport
     *
     * @return boolean 
     */
    public function getCfcnLegacyImport()
    {
        return $this->CfcnLegacyImport;
    }

    /**
     * Get CfcnActionId
     *
     * @return integer 
     */
    public function getCfcnActionId()
    {
        return $this->CfcnActionId;
    }

    /**
     * Set cfcnActionRelActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnActionRelActionId
     * @return Classificationaction
     */
    public function setCfcnActionRelActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnActionRelActionId = null)
    {
        $this->cfcnActionRelActionId = $cfcnActionRelActionId;

        return $this;
    }

    /**
     * Get cfcnActionRelActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnActionRelActionId()
    {
        return $this->cfcnActionRelActionId;
    }

    /**
     * Set cfcnActionForMembId
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $cfcnActionForMembId
     * @return Classificationaction
     */
    public function setCfcnActionForMembId(\worldsailing\Isaf\model\Entity\Memberbiogs $cfcnActionForMembId = null)
    {
        $this->cfcnActionForMembId = $cfcnActionForMembId;

        return $this;
    }

    /**
     * Get cfcnActionForMembId
     *
     * @return \worldsailing\Isaf\model\Entity\Memberbiogs
     */
    public function getCfcnActionForMembId()
    {
        return $this->cfcnActionForMembId;
    }
}
