<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organisations
 *
 * @ORM\Table(name="Organisations")
 * @ORM\Entity
 */
class Organisations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="OrgOtypId", type="integer", nullable=false)
     */
    protected $OrgOtypId;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgName", type="string", length=256, nullable=true)
     */
    protected $OrgName;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgLevel", type="string", nullable=true)
     */
    protected $OrgLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgAddress1", type="string", length=100, nullable=true)
     */
    protected $OrgAddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgAddress2", type="string", length=100, nullable=true)
     */
    protected $OrgAddress2;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgAddress3", type="string", length=100, nullable=true)
     */
    protected $OrgAddress3;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgCity", type="string", length=100, nullable=true)
     */
    protected $OrgCity;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgCountyState", type="string", length=100, nullable=true)
     */
    protected $OrgCountyState;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgPostCode", type="string", length=20, nullable=true)
     */
    protected $OrgPostCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="OrgCtryId", type="integer", nullable=false)
     */
    protected $OrgCtryId;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgAddressAdditional", type="string", length=255, nullable=true)
     */
    protected $OrgAddressAdditional;

    /**
     * @var integer
     *
     * @ORM\Column(name="OrgNationId", type="integer", nullable=false)
     */
    protected $OrgNationId;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgArea", type="string", length=100, nullable=true)
     */
    protected $OrgArea;

    /**
     * @var integer
     *
     * @ORM\Column(name="OrgClassId", type="integer", nullable=false)
     */
    protected $OrgClassId;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgClassType", type="string", length=100, nullable=true)
     */
    protected $OrgClassType;

    /**
     * @var integer
     *
     * @ORM\Column(name="OrgGrpId", type="integer", nullable=false)
     */
    protected $OrgGrpId;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgClassStatus", type="string", length=100, nullable=true)
     */
    protected $OrgClassStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgDialCode", type="string", length=6, nullable=true)
     */
    protected $OrgDialCode;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgSubCat", type="string", length=100, nullable=true)
     */
    protected $OrgSubCat;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgPhone1", type="string", length=30, nullable=true)
     */
    protected $OrgPhone1;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgPhone2", type="string", length=30, nullable=true)
     */
    protected $OrgPhone2;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgFax", type="string", length=30, nullable=true)
     */
    protected $OrgFax;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgEmail", type="string", length=50, nullable=true)
     */
    protected $OrgEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgUrl", type="string", length=255, nullable=true)
     */
    protected $OrgUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="OrgPaidUntil", type="date", nullable=false)
     */
    protected $OrgPaidUntil;

    /**
     * @var integer
     *
     * @ORM\Column(name="OrgVatNum", type="integer", nullable=false)
     */
    protected $OrgVatNum;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgEzine", type="string", nullable=false)
     */
    protected $OrgEzine;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgNotes", type="text", length=65535, nullable=true)
     */
    protected $OrgNotes;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgPhone3", type="string", length=30, nullable=true)
     */
    protected $OrgPhone3;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgEmail2", type="string", length=50, nullable=true)
     */
    protected $OrgEmail2;

    /**
     * @var string
     *
     * @ORM\Column(name="OrgSkypeContact", type="string", length=50, nullable=true)
     */
    protected $OrgSkypeContact;

    /**
     * @var integer
     *
     * @ORM\Column(name="OrgId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $OrgId;



    /**
     * Set OrgOtypId
     *
     * @param integer $orgOtypId
     * @return Organisations
     */
    public function setOrgOtypId($orgOtypId)
    {
        $this->OrgOtypId = $orgOtypId;

        return $this;
    }

    /**
     * Get OrgOtypId
     *
     * @return integer 
     */
    public function getOrgOtypId()
    {
        return $this->OrgOtypId;
    }

    /**
     * Set OrgName
     *
     * @param string $orgName
     * @return Organisations
     */
    public function setOrgName($orgName)
    {
        $this->OrgName = $orgName;

        return $this;
    }

    /**
     * Get OrgName
     *
     * @return string 
     */
    public function getOrgName()
    {
        return $this->OrgName;
    }

    /**
     * Set OrgLevel
     *
     * @param string $orgLevel
     * @return Organisations
     */
    public function setOrgLevel($orgLevel)
    {
        $this->OrgLevel = $orgLevel;

        return $this;
    }

    /**
     * Get OrgLevel
     *
     * @return string 
     */
    public function getOrgLevel()
    {
        return $this->OrgLevel;
    }

    /**
     * Set OrgAddress1
     *
     * @param string $orgAddress1
     * @return Organisations
     */
    public function setOrgAddress1($orgAddress1)
    {
        $this->OrgAddress1 = $orgAddress1;

        return $this;
    }

    /**
     * Get OrgAddress1
     *
     * @return string 
     */
    public function getOrgAddress1()
    {
        return $this->OrgAddress1;
    }

    /**
     * Set OrgAddress2
     *
     * @param string $orgAddress2
     * @return Organisations
     */
    public function setOrgAddress2($orgAddress2)
    {
        $this->OrgAddress2 = $orgAddress2;

        return $this;
    }

    /**
     * Get OrgAddress2
     *
     * @return string 
     */
    public function getOrgAddress2()
    {
        return $this->OrgAddress2;
    }

    /**
     * Set OrgAddress3
     *
     * @param string $orgAddress3
     * @return Organisations
     */
    public function setOrgAddress3($orgAddress3)
    {
        $this->OrgAddress3 = $orgAddress3;

        return $this;
    }

    /**
     * Get OrgAddress3
     *
     * @return string 
     */
    public function getOrgAddress3()
    {
        return $this->OrgAddress3;
    }

    /**
     * Set OrgCity
     *
     * @param string $orgCity
     * @return Organisations
     */
    public function setOrgCity($orgCity)
    {
        $this->OrgCity = $orgCity;

        return $this;
    }

    /**
     * Get OrgCity
     *
     * @return string 
     */
    public function getOrgCity()
    {
        return $this->OrgCity;
    }

    /**
     * Set OrgCountyState
     *
     * @param string $orgCountyState
     * @return Organisations
     */
    public function setOrgCountyState($orgCountyState)
    {
        $this->OrgCountyState = $orgCountyState;

        return $this;
    }

    /**
     * Get OrgCountyState
     *
     * @return string 
     */
    public function getOrgCountyState()
    {
        return $this->OrgCountyState;
    }

    /**
     * Set OrgPostCode
     *
     * @param string $orgPostCode
     * @return Organisations
     */
    public function setOrgPostCode($orgPostCode)
    {
        $this->OrgPostCode = $orgPostCode;

        return $this;
    }

    /**
     * Get OrgPostCode
     *
     * @return string 
     */
    public function getOrgPostCode()
    {
        return $this->OrgPostCode;
    }

    /**
     * Set OrgCtryId
     *
     * @param integer $orgCtryId
     * @return Organisations
     */
    public function setOrgCtryId($orgCtryId)
    {
        $this->OrgCtryId = $orgCtryId;

        return $this;
    }

    /**
     * Get OrgCtryId
     *
     * @return integer 
     */
    public function getOrgCtryId()
    {
        return $this->OrgCtryId;
    }

    /**
     * Set OrgAddressAdditional
     *
     * @param string $orgAddressAdditional
     * @return Organisations
     */
    public function setOrgAddressAdditional($orgAddressAdditional)
    {
        $this->OrgAddressAdditional = $orgAddressAdditional;

        return $this;
    }

    /**
     * Get OrgAddressAdditional
     *
     * @return string 
     */
    public function getOrgAddressAdditional()
    {
        return $this->OrgAddressAdditional;
    }

    /**
     * Set OrgNationId
     *
     * @param integer $orgNationId
     * @return Organisations
     */
    public function setOrgNationId($orgNationId)
    {
        $this->OrgNationId = $orgNationId;

        return $this;
    }

    /**
     * Get OrgNationId
     *
     * @return integer 
     */
    public function getOrgNationId()
    {
        return $this->OrgNationId;
    }

    /**
     * Set OrgArea
     *
     * @param string $orgArea
     * @return Organisations
     */
    public function setOrgArea($orgArea)
    {
        $this->OrgArea = $orgArea;

        return $this;
    }

    /**
     * Get OrgArea
     *
     * @return string 
     */
    public function getOrgArea()
    {
        return $this->OrgArea;
    }

    /**
     * Set OrgClassId
     *
     * @param integer $orgClassId
     * @return Organisations
     */
    public function setOrgClassId($orgClassId)
    {
        $this->OrgClassId = $orgClassId;

        return $this;
    }

    /**
     * Get OrgClassId
     *
     * @return integer 
     */
    public function getOrgClassId()
    {
        return $this->OrgClassId;
    }

    /**
     * Set OrgClassType
     *
     * @param string $orgClassType
     * @return Organisations
     */
    public function setOrgClassType($orgClassType)
    {
        $this->OrgClassType = $orgClassType;

        return $this;
    }

    /**
     * Get OrgClassType
     *
     * @return string 
     */
    public function getOrgClassType()
    {
        return $this->OrgClassType;
    }

    /**
     * Set OrgGrpId
     *
     * @param integer $orgGrpId
     * @return Organisations
     */
    public function setOrgGrpId($orgGrpId)
    {
        $this->OrgGrpId = $orgGrpId;

        return $this;
    }

    /**
     * Get OrgGrpId
     *
     * @return integer 
     */
    public function getOrgGrpId()
    {
        return $this->OrgGrpId;
    }

    /**
     * Set OrgClassStatus
     *
     * @param string $orgClassStatus
     * @return Organisations
     */
    public function setOrgClassStatus($orgClassStatus)
    {
        $this->OrgClassStatus = $orgClassStatus;

        return $this;
    }

    /**
     * Get OrgClassStatus
     *
     * @return string 
     */
    public function getOrgClassStatus()
    {
        return $this->OrgClassStatus;
    }

    /**
     * Set OrgDialCode
     *
     * @param string $orgDialCode
     * @return Organisations
     */
    public function setOrgDialCode($orgDialCode)
    {
        $this->OrgDialCode = $orgDialCode;

        return $this;
    }

    /**
     * Get OrgDialCode
     *
     * @return string 
     */
    public function getOrgDialCode()
    {
        return $this->OrgDialCode;
    }

    /**
     * Set OrgSubCat
     *
     * @param string $orgSubCat
     * @return Organisations
     */
    public function setOrgSubCat($orgSubCat)
    {
        $this->OrgSubCat = $orgSubCat;

        return $this;
    }

    /**
     * Get OrgSubCat
     *
     * @return string 
     */
    public function getOrgSubCat()
    {
        return $this->OrgSubCat;
    }

    /**
     * Set OrgPhone1
     *
     * @param string $orgPhone1
     * @return Organisations
     */
    public function setOrgPhone1($orgPhone1)
    {
        $this->OrgPhone1 = $orgPhone1;

        return $this;
    }

    /**
     * Get OrgPhone1
     *
     * @return string 
     */
    public function getOrgPhone1()
    {
        return $this->OrgPhone1;
    }

    /**
     * Set OrgPhone2
     *
     * @param string $orgPhone2
     * @return Organisations
     */
    public function setOrgPhone2($orgPhone2)
    {
        $this->OrgPhone2 = $orgPhone2;

        return $this;
    }

    /**
     * Get OrgPhone2
     *
     * @return string 
     */
    public function getOrgPhone2()
    {
        return $this->OrgPhone2;
    }

    /**
     * Set OrgFax
     *
     * @param string $orgFax
     * @return Organisations
     */
    public function setOrgFax($orgFax)
    {
        $this->OrgFax = $orgFax;

        return $this;
    }

    /**
     * Get OrgFax
     *
     * @return string 
     */
    public function getOrgFax()
    {
        return $this->OrgFax;
    }

    /**
     * Set OrgEmail
     *
     * @param string $orgEmail
     * @return Organisations
     */
    public function setOrgEmail($orgEmail)
    {
        $this->OrgEmail = $orgEmail;

        return $this;
    }

    /**
     * Get OrgEmail
     *
     * @return string 
     */
    public function getOrgEmail()
    {
        return $this->OrgEmail;
    }

    /**
     * Set OrgUrl
     *
     * @param string $orgUrl
     * @return Organisations
     */
    public function setOrgUrl($orgUrl)
    {
        $this->OrgUrl = $orgUrl;

        return $this;
    }

    /**
     * Get OrgUrl
     *
     * @return string 
     */
    public function getOrgUrl()
    {
        return $this->OrgUrl;
    }

    /**
     * Set OrgPaidUntil
     *
     * @param \DateTime $orgPaidUntil
     * @return Organisations
     */
    public function setOrgPaidUntil($orgPaidUntil)
    {
        $this->OrgPaidUntil = $orgPaidUntil;

        return $this;
    }

    /**
     * Get OrgPaidUntil
     *
     * @return \DateTime 
     */
    public function getOrgPaidUntil()
    {
        return $this->OrgPaidUntil;
    }

    /**
     * Set OrgVatNum
     *
     * @param integer $orgVatNum
     * @return Organisations
     */
    public function setOrgVatNum($orgVatNum)
    {
        $this->OrgVatNum = $orgVatNum;

        return $this;
    }

    /**
     * Get OrgVatNum
     *
     * @return integer 
     */
    public function getOrgVatNum()
    {
        return $this->OrgVatNum;
    }

    /**
     * Set OrgEzine
     *
     * @param string $orgEzine
     * @return Organisations
     */
    public function setOrgEzine($orgEzine)
    {
        $this->OrgEzine = $orgEzine;

        return $this;
    }

    /**
     * Get OrgEzine
     *
     * @return string 
     */
    public function getOrgEzine()
    {
        return $this->OrgEzine;
    }

    /**
     * Set OrgNotes
     *
     * @param string $orgNotes
     * @return Organisations
     */
    public function setOrgNotes($orgNotes)
    {
        $this->OrgNotes = $orgNotes;

        return $this;
    }

    /**
     * Get OrgNotes
     *
     * @return string 
     */
    public function getOrgNotes()
    {
        return $this->OrgNotes;
    }

    /**
     * Set OrgPhone3
     *
     * @param string $orgPhone3
     * @return Organisations
     */
    public function setOrgPhone3($orgPhone3)
    {
        $this->OrgPhone3 = $orgPhone3;

        return $this;
    }

    /**
     * Get OrgPhone3
     *
     * @return string 
     */
    public function getOrgPhone3()
    {
        return $this->OrgPhone3;
    }

    /**
     * Set OrgEmail2
     *
     * @param string $orgEmail2
     * @return Organisations
     */
    public function setOrgEmail2($orgEmail2)
    {
        $this->OrgEmail2 = $orgEmail2;

        return $this;
    }

    /**
     * Get OrgEmail2
     *
     * @return string 
     */
    public function getOrgEmail2()
    {
        return $this->OrgEmail2;
    }

    /**
     * Set OrgSkypeContact
     *
     * @param string $orgSkypeContact
     * @return Organisations
     */
    public function setOrgSkypeContact($orgSkypeContact)
    {
        $this->OrgSkypeContact = $orgSkypeContact;

        return $this;
    }

    /**
     * Get OrgSkypeContact
     *
     * @return string 
     */
    public function getOrgSkypeContact()
    {
        return $this->OrgSkypeContact;
    }

    /**
     * Get OrgId
     *
     * @return integer 
     */
    public function getOrgId()
    {
        return $this->OrgId;
    }
}
