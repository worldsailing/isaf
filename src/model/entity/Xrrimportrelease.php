<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Xrrimportrelease
 *
 * @ORM\Table(name="XRRImportRelease", indexes={@ORM\Index(name="fk_rls_tourn", columns={"RlsTournId"}), @ORM\Index(name="fk_rls_rgta", columns={"RlsRgtaId"})})
 * @ORM\Entity
 */
class Xrrimportrelease
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RlsStartDate", type="datetime", nullable=true)
     */
    protected $RlsStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RlsEndDate", type="datetime", nullable=true)
     */
    protected $RlsEndDate;

    /**
     * @var string
     *
     * @ORM\Column(name="RlsLevels", type="string", length=500, nullable=true)
     */
    protected $RlsLevels;

    /**
     * @var integer
     *
     * @ORM\Column(name="RlsId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RlsId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Tournaments
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Tournaments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RlsTournId", referencedColumnName="TournId")
     * })
     */
    protected $rlsTournId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Regattas
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Regattas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RlsRgtaId", referencedColumnName="RgtaId")
     * })
     */
    protected $rlsRgtaId;



    /**
     * Set RlsStartDate
     *
     * @param \DateTime $rlsStartDate
     * @return Xrrimportrelease
     */
    public function setRlsStartDate($rlsStartDate)
    {
        $this->RlsStartDate = $rlsStartDate;

        return $this;
    }

    /**
     * Get RlsStartDate
     *
     * @return \DateTime 
     */
    public function getRlsStartDate()
    {
        return $this->RlsStartDate;
    }

    /**
     * Set RlsEndDate
     *
     * @param \DateTime $rlsEndDate
     * @return Xrrimportrelease
     */
    public function setRlsEndDate($rlsEndDate)
    {
        $this->RlsEndDate = $rlsEndDate;

        return $this;
    }

    /**
     * Get RlsEndDate
     *
     * @return \DateTime 
     */
    public function getRlsEndDate()
    {
        return $this->RlsEndDate;
    }

    /**
     * Set RlsLevels
     *
     * @param string $rlsLevels
     * @return Xrrimportrelease
     */
    public function setRlsLevels($rlsLevels)
    {
        $this->RlsLevels = $rlsLevels;

        return $this;
    }

    /**
     * Get RlsLevels
     *
     * @return string 
     */
    public function getRlsLevels()
    {
        return $this->RlsLevels;
    }

    /**
     * Get RlsId
     *
     * @return integer 
     */
    public function getRlsId()
    {
        return $this->RlsId;
    }

    /**
     * Set rlsTournId
     *
     * @param \worldsailing\Isaf\model\Entity\Tournaments $rlsTournId
     * @return Xrrimportrelease
     */
    public function setRlsTournId(\worldsailing\Isaf\model\Entity\Tournaments $rlsTournId = null)
    {
        $this->rlsTournId = $rlsTournId;

        return $this;
    }

    /**
     * Get rlsTournId
     *
     * @return \worldsailing\Isaf\model\Entity\Tournaments
     */
    public function getRlsTournId()
    {
        return $this->rlsTournId;
    }

    /**
     * Set rlsRgtaId
     *
     * @param \worldsailing\Isaf\model\Entity\Regattas $rlsRgtaId
     * @return Xrrimportrelease
     */
    public function setRlsRgtaId(\worldsailing\Isaf\model\Entity\Regattas $rlsRgtaId = null)
    {
        $this->rlsRgtaId = $rlsRgtaId;

        return $this;
    }

    /**
     * Get rlsRgtaId
     *
     * @return \worldsailing\Isaf\model\Entity\Regattas
     */
    public function getRlsRgtaId()
    {
        return $this->rlsRgtaId;
    }
}
