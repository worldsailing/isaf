<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boatgroup
 *
 * @ORM\Table(name="BoatGroup")
 * @ORM\Entity
 */
class Boatgroup
{
    /**
     * @var string
     *
     * @ORM\Column(name="BtGrpName", type="string", length=500, nullable=true)
     */
    protected $BtGrpName;

    /**
     * @var integer
     *
     * @ORM\Column(name="BtGrpEvntId", type="integer", nullable=false)
     */
    protected $BtGrpEvntId;

    /**
     * @var float
     *
     * @ORM\Column(name="BtGrpTotalPoints", type="float", precision=10, scale=2, nullable=true)
     */
    protected $BtGrpTotalPoints;

    /**
     * @var float
     *
     * @ORM\Column(name="BtGrpNetPoints", type="float", precision=10, scale=2, nullable=true)
     */
    protected $BtGrpNetPoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="BtGrpPosition", type="integer", nullable=true)
     */
    protected $BtGrpPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="BtGrpSrcId", type="string", length=45, nullable=true)
     */
    protected $BtGrpSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BtGrpId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $BtGrpId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="worldsailing\Isaf\model\Entity\Races", mappedBy="rresBtGrpId")
     */
    protected $rresBtGrpRaceId;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rresBtGrpRaceId = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set BtGrpName
     *
     * @param string $btGrpName
     * @return Boatgroup
     */
    public function setBtGrpName($btGrpName)
    {
        $this->BtGrpName = $btGrpName;

        return $this;
    }

    /**
     * Get BtGrpName
     *
     * @return string 
     */
    public function getBtGrpName()
    {
        return $this->BtGrpName;
    }

    /**
     * Set BtGrpEvntId
     *
     * @param integer $btGrpEvntId
     * @return Boatgroup
     */
    public function setBtGrpEvntId($btGrpEvntId)
    {
        $this->BtGrpEvntId = $btGrpEvntId;

        return $this;
    }

    /**
     * Get BtGrpEvntId
     *
     * @return integer 
     */
    public function getBtGrpEvntId()
    {
        return $this->BtGrpEvntId;
    }

    /**
     * Set BtGrpTotalPoints
     *
     * @param float $btGrpTotalPoints
     * @return Boatgroup
     */
    public function setBtGrpTotalPoints($btGrpTotalPoints)
    {
        $this->BtGrpTotalPoints = $btGrpTotalPoints;

        return $this;
    }

    /**
     * Get BtGrpTotalPoints
     *
     * @return float 
     */
    public function getBtGrpTotalPoints()
    {
        return $this->BtGrpTotalPoints;
    }

    /**
     * Set BtGrpNetPoints
     *
     * @param float $btGrpNetPoints
     * @return Boatgroup
     */
    public function setBtGrpNetPoints($btGrpNetPoints)
    {
        $this->BtGrpNetPoints = $btGrpNetPoints;

        return $this;
    }

    /**
     * Get BtGrpNetPoints
     *
     * @return float 
     */
    public function getBtGrpNetPoints()
    {
        return $this->BtGrpNetPoints;
    }

    /**
     * Set BtGrpPosition
     *
     * @param integer $btGrpPosition
     * @return Boatgroup
     */
    public function setBtGrpPosition($btGrpPosition)
    {
        $this->BtGrpPosition = $btGrpPosition;

        return $this;
    }

    /**
     * Get BtGrpPosition
     *
     * @return integer 
     */
    public function getBtGrpPosition()
    {
        return $this->BtGrpPosition;
    }

    /**
     * Set BtGrpSrcId
     *
     * @param string $btGrpSrcId
     * @return Boatgroup
     */
    public function setBtGrpSrcId($btGrpSrcId)
    {
        $this->BtGrpSrcId = $btGrpSrcId;

        return $this;
    }

    /**
     * Get BtGrpSrcId
     *
     * @return string 
     */
    public function getBtGrpSrcId()
    {
        return $this->BtGrpSrcId;
    }

    /**
     * Get BtGrpId
     *
     * @return integer 
     */
    public function getBtGrpId()
    {
        return $this->BtGrpId;
    }

    /**
     * Add rresBtGrpRaceId
     *
     * @param \worldsailing\Isaf\model\Entity\Races $rresBtGrpRaceId
     * @return Boatgroup
     */
    public function addRresBtGrpRaceId(\worldsailing\Isaf\model\Entity\Races $rresBtGrpRaceId)
    {
        $this->rresBtGrpRaceId[] = $rresBtGrpRaceId;

        return $this;
    }

    /**
     * Remove rresBtGrpRaceId
     *
     * @param \worldsailing\Isaf\model\Entity\Races $rresBtGrpRaceId
     */
    public function removeRresBtGrpRaceId(\worldsailing\Isaf\model\Entity\Races $rresBtGrpRaceId)
    {
        $this->rresBtGrpRaceId->removeElement($rresBtGrpRaceId);
    }

    /**
     * Get rresBtGrpRaceId
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRresBtGrpRaceId()
    {
        return $this->rresBtGrpRaceId;
    }
}
