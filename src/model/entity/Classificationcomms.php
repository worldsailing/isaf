<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationcomms
 *
 * @ORM\Table(name="ClassificationComms", indexes={@ORM\Index(name="comm_action", columns={"CfcnCommActionId"})})
 * @ORM\Entity
 */
class Classificationcomms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnCommSubmittedBy", type="integer", nullable=true)
     */
    protected $CfcnCommSubmittedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnCommContent", type="text", length=65535, nullable=true)
     */
    protected $CfcnCommContent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnCommCreationDate", type="datetime", nullable=true)
     */
    protected $CfcnCommCreationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnCommReminderSent", type="datetime", nullable=true)
     */
    protected $CfcnCommReminderSent;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnCommType", type="string", nullable=true)
     */
    protected $CfcnCommType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CfcnCommShowToSailor", type="boolean", nullable=true)
     */
    protected $CfcnCommShowToSailor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnCommRespReqdBy", type="date", nullable=true)
     */
    protected $CfcnCommRespReqdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnCommRespReminderSent", type="date", nullable=true)
     */
    protected $CfcnCommRespReminderSent;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnCommId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CfcnCommId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnCommActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnCommActionId;



    /**
     * Set CfcnCommSubmittedBy
     *
     * @param integer $cfcnCommSubmittedBy
     * @return Classificationcomms
     */
    public function setCfcnCommSubmittedBy($cfcnCommSubmittedBy)
    {
        $this->CfcnCommSubmittedBy = $cfcnCommSubmittedBy;

        return $this;
    }

    /**
     * Get CfcnCommSubmittedBy
     *
     * @return integer 
     */
    public function getCfcnCommSubmittedBy()
    {
        return $this->CfcnCommSubmittedBy;
    }

    /**
     * Set CfcnCommContent
     *
     * @param string $cfcnCommContent
     * @return Classificationcomms
     */
    public function setCfcnCommContent($cfcnCommContent)
    {
        $this->CfcnCommContent = $cfcnCommContent;

        return $this;
    }

    /**
     * Get CfcnCommContent
     *
     * @return string 
     */
    public function getCfcnCommContent()
    {
        return $this->CfcnCommContent;
    }

    /**
     * Set CfcnCommCreationDate
     *
     * @param \DateTime $cfcnCommCreationDate
     * @return Classificationcomms
     */
    public function setCfcnCommCreationDate($cfcnCommCreationDate)
    {
        $this->CfcnCommCreationDate = $cfcnCommCreationDate;

        return $this;
    }

    /**
     * Get CfcnCommCreationDate
     *
     * @return \DateTime 
     */
    public function getCfcnCommCreationDate()
    {
        return $this->CfcnCommCreationDate;
    }

    /**
     * Set CfcnCommReminderSent
     *
     * @param \DateTime $cfcnCommReminderSent
     * @return Classificationcomms
     */
    public function setCfcnCommReminderSent($cfcnCommReminderSent)
    {
        $this->CfcnCommReminderSent = $cfcnCommReminderSent;

        return $this;
    }

    /**
     * Get CfcnCommReminderSent
     *
     * @return \DateTime 
     */
    public function getCfcnCommReminderSent()
    {
        return $this->CfcnCommReminderSent;
    }

    /**
     * Set CfcnCommType
     *
     * @param string $cfcnCommType
     * @return Classificationcomms
     */
    public function setCfcnCommType($cfcnCommType)
    {
        $this->CfcnCommType = $cfcnCommType;

        return $this;
    }

    /**
     * Get CfcnCommType
     *
     * @return string 
     */
    public function getCfcnCommType()
    {
        return $this->CfcnCommType;
    }

    /**
     * Set CfcnCommShowToSailor
     *
     * @param boolean $cfcnCommShowToSailor
     * @return Classificationcomms
     */
    public function setCfcnCommShowToSailor($cfcnCommShowToSailor)
    {
        $this->CfcnCommShowToSailor = $cfcnCommShowToSailor;

        return $this;
    }

    /**
     * Get CfcnCommShowToSailor
     *
     * @return boolean 
     */
    public function getCfcnCommShowToSailor()
    {
        return $this->CfcnCommShowToSailor;
    }

    /**
     * Set CfcnCommRespReqdBy
     *
     * @param \DateTime $cfcnCommRespReqdBy
     * @return Classificationcomms
     */
    public function setCfcnCommRespReqdBy($cfcnCommRespReqdBy)
    {
        $this->CfcnCommRespReqdBy = $cfcnCommRespReqdBy;

        return $this;
    }

    /**
     * Get CfcnCommRespReqdBy
     *
     * @return \DateTime 
     */
    public function getCfcnCommRespReqdBy()
    {
        return $this->CfcnCommRespReqdBy;
    }

    /**
     * Set CfcnCommRespReminderSent
     *
     * @param \DateTime $cfcnCommRespReminderSent
     * @return Classificationcomms
     */
    public function setCfcnCommRespReminderSent($cfcnCommRespReminderSent)
    {
        $this->CfcnCommRespReminderSent = $cfcnCommRespReminderSent;

        return $this;
    }

    /**
     * Get CfcnCommRespReminderSent
     *
     * @return \DateTime 
     */
    public function getCfcnCommRespReminderSent()
    {
        return $this->CfcnCommRespReminderSent;
    }

    /**
     * Get CfcnCommId
     *
     * @return integer 
     */
    public function getCfcnCommId()
    {
        return $this->CfcnCommId;
    }

    /**
     * Set cfcnCommActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnCommActionId
     * @return Classificationcomms
     */
    public function setCfcnCommActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnCommActionId = null)
    {
        $this->cfcnCommActionId = $cfcnCommActionId;

        return $this;
    }

    /**
     * Get cfcnCommActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnCommActionId()
    {
        return $this->cfcnCommActionId;
    }
}
