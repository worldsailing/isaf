<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fleetracepoints
 *
 * @ORM\Table(name="FleetRacePoints")
 * @ORM\Entity
 */
class Fleetracepoints
{
    /**
     * @var integer
     *
     * @ORM\Column(name="FleetPoints", type="integer", nullable=false)
     */
    protected $FleetPoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="FleetPosition", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $FleetPosition;



    /**
     * Set FleetPoints
     *
     * @param integer $fleetPoints
     * @return Fleetracepoints
     */
    public function setFleetPoints($fleetPoints)
    {
        $this->FleetPoints = $fleetPoints;

        return $this;
    }

    /**
     * Get FleetPoints
     *
     * @return integer 
     */
    public function getFleetPoints()
    {
        return $this->FleetPoints;
    }

    /**
     * Get FleetPosition
     *
     * @return integer 
     */
    public function getFleetPosition()
    {
        return $this->FleetPosition;
    }
}
