<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationprotest
 *
 * @ORM\Table(name="ClassificationProtest", indexes={@ORM\Index(name="fk_protest_action", columns={"CfcnPtstActionId"})})
 * @ORM\Entity
 */
class Classificationprotest
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfcnPtstProtestor", type="string", length=1024, nullable=true)
     */
    protected $CfcnPtstProtestor;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnPtstEventName", type="string", length=1024, nullable=true)
     */
    protected $CfcnPtstEventName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnPtstDate", type="datetime", nullable=true)
     */
    protected $CfcnPtstDate;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnPtstFactsFound", type="text", length=65535, nullable=true)
     */
    protected $CfcnPtstFactsFound;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnPtstConclusion", type="text", length=65535, nullable=true)
     */
    protected $CfcnPtstConclusion;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnPtstJuryChairman", type="string", length=512, nullable=true)
     */
    protected $CfcnPtstJuryChairman;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnPtstJuryMembers", type="string", length=1024, nullable=true)
     */
    protected $CfcnPtstJuryMembers;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnPtstDecision", type="text", length=65535, nullable=true)
     */
    protected $CfcnPtstDecision;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnPtstActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnPtstActionId;



    /**
     * Set CfcnPtstProtestor
     *
     * @param string $cfcnPtstProtestor
     * @return Classificationprotest
     */
    public function setCfcnPtstProtestor($cfcnPtstProtestor)
    {
        $this->CfcnPtstProtestor = $cfcnPtstProtestor;

        return $this;
    }

    /**
     * Get CfcnPtstProtestor
     *
     * @return string 
     */
    public function getCfcnPtstProtestor()
    {
        return $this->CfcnPtstProtestor;
    }

    /**
     * Set CfcnPtstEventName
     *
     * @param string $cfcnPtstEventName
     * @return Classificationprotest
     */
    public function setCfcnPtstEventName($cfcnPtstEventName)
    {
        $this->CfcnPtstEventName = $cfcnPtstEventName;

        return $this;
    }

    /**
     * Get CfcnPtstEventName
     *
     * @return string 
     */
    public function getCfcnPtstEventName()
    {
        return $this->CfcnPtstEventName;
    }

    /**
     * Set CfcnPtstDate
     *
     * @param \DateTime $cfcnPtstDate
     * @return Classificationprotest
     */
    public function setCfcnPtstDate($cfcnPtstDate)
    {
        $this->CfcnPtstDate = $cfcnPtstDate;

        return $this;
    }

    /**
     * Get CfcnPtstDate
     *
     * @return \DateTime 
     */
    public function getCfcnPtstDate()
    {
        return $this->CfcnPtstDate;
    }

    /**
     * Set CfcnPtstFactsFound
     *
     * @param string $cfcnPtstFactsFound
     * @return Classificationprotest
     */
    public function setCfcnPtstFactsFound($cfcnPtstFactsFound)
    {
        $this->CfcnPtstFactsFound = $cfcnPtstFactsFound;

        return $this;
    }

    /**
     * Get CfcnPtstFactsFound
     *
     * @return string 
     */
    public function getCfcnPtstFactsFound()
    {
        return $this->CfcnPtstFactsFound;
    }

    /**
     * Set CfcnPtstConclusion
     *
     * @param string $cfcnPtstConclusion
     * @return Classificationprotest
     */
    public function setCfcnPtstConclusion($cfcnPtstConclusion)
    {
        $this->CfcnPtstConclusion = $cfcnPtstConclusion;

        return $this;
    }

    /**
     * Get CfcnPtstConclusion
     *
     * @return string 
     */
    public function getCfcnPtstConclusion()
    {
        return $this->CfcnPtstConclusion;
    }

    /**
     * Set CfcnPtstJuryChairman
     *
     * @param string $cfcnPtstJuryChairman
     * @return Classificationprotest
     */
    public function setCfcnPtstJuryChairman($cfcnPtstJuryChairman)
    {
        $this->CfcnPtstJuryChairman = $cfcnPtstJuryChairman;

        return $this;
    }

    /**
     * Get CfcnPtstJuryChairman
     *
     * @return string 
     */
    public function getCfcnPtstJuryChairman()
    {
        return $this->CfcnPtstJuryChairman;
    }

    /**
     * Set CfcnPtstJuryMembers
     *
     * @param string $cfcnPtstJuryMembers
     * @return Classificationprotest
     */
    public function setCfcnPtstJuryMembers($cfcnPtstJuryMembers)
    {
        $this->CfcnPtstJuryMembers = $cfcnPtstJuryMembers;

        return $this;
    }

    /**
     * Get CfcnPtstJuryMembers
     *
     * @return string 
     */
    public function getCfcnPtstJuryMembers()
    {
        return $this->CfcnPtstJuryMembers;
    }

    /**
     * Set CfcnPtstDecision
     *
     * @param string $cfcnPtstDecision
     * @return Classificationprotest
     */
    public function setCfcnPtstDecision($cfcnPtstDecision)
    {
        $this->CfcnPtstDecision = $cfcnPtstDecision;

        return $this;
    }

    /**
     * Get CfcnPtstDecision
     *
     * @return string 
     */
    public function getCfcnPtstDecision()
    {
        return $this->CfcnPtstDecision;
    }

    /**
     * Set cfcnPtstActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnPtstActionId
     * @return Classificationprotest
     */
    public function setCfcnPtstActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnPtstActionId)
    {
        $this->cfcnPtstActionId = $cfcnPtstActionId;

        return $this;
    }

    /**
     * Get cfcnPtstActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnPtstActionId()
    {
        return $this->cfcnPtstActionId;
    }
}
