<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Races
 *
 * @ORM\Table(name="Races", indexes={@ORM\Index(name="RaceSource", columns={"RaceSource", "RaceSrcId"}), @ORM\Index(name="RaceEvntId", columns={"RaceEvntId"})})
 * @ORM\Entity
 */
class Races
{
    /**
     * @var string
     *
     * @ORM\Column(name="RaceSource", type="string", length=10, nullable=true)
     */
    protected $RaceSource;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceSrcId", type="string", length=50, nullable=true)
     */
    protected $RaceSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RaceEvntId", type="integer", nullable=false)
     */
    protected $RaceEvntId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RaceNumber", type="integer", nullable=false)
     */
    protected $RaceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceName", type="string", length=150, nullable=true)
     */
    protected $RaceName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RaceMedalRace", type="boolean", nullable=false)
     */
    protected $RaceMedalRace;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RaceStartDate", type="date", nullable=true)
     */
    protected $RaceStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RaceStartTime", type="time", nullable=true)
     */
    protected $RaceStartTime;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceArea", type="string", length=50, nullable=true)
     */
    protected $RaceArea;

    /**
     * @var integer
     *
     * @ORM\Column(name="RaceCourseId", type="integer", nullable=false)
     */
    protected $RaceCourseId;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceStatus", type="string", nullable=false)
     */
    protected $RaceStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RaceShowResults", type="boolean", nullable=false)
     */
    protected $RaceShowResults;

    /**
     * @var integer
     *
     * @ORM\Column(name="RaceMultiplier", type="integer", nullable=false)
     */
    protected $RaceMultiplier;

    /**
     * @var float
     *
     * @ORM\Column(name="RaceTemp", type="float", precision=7, scale=2, nullable=true)
     */
    protected $RaceTemp;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceTempUnit", type="string", length=20, nullable=true)
     */
    protected $RaceTempUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="RaceHumidity", type="float", precision=7, scale=2, nullable=true)
     */
    protected $RaceHumidity;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceWeather", type="string", length=255, nullable=true)
     */
    protected $RaceWeather;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceStage", type="string", length=100, nullable=true)
     */
    protected $RaceStage;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceFlight", type="string", length=100, nullable=true)
     */
    protected $RaceFlight;

    /**
     * @var string
     *
     * @ORM\Column(name="RaceMatch", type="string", length=100, nullable=true)
     */
    protected $RaceMatch;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RaceResultUpdated", type="datetime", nullable=true)
     */
    protected $RaceResultUpdated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RaceScheduled", type="datetime", nullable=true)
     */
    protected $RaceScheduled;

    /**
     * @var integer
     *
     * @ORM\Column(name="RaceId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RaceId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="worldsailing\Isaf\model\Entity\Boatgroup", inversedBy="rresBtGrpRaceId")
     * @ORM\JoinTable(name="boatgroupraceresult",
     *   joinColumns={
     *     @ORM\JoinColumn(name="RresBtGrpRaceId", referencedColumnName="RaceId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="RresBtGrpId", referencedColumnName="BtGrpId")
     *   }
     * )
     */
    protected $rresBtGrpId;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rresBtGrpId = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set RaceSource
     *
     * @param string $raceSource
     * @return Races
     */
    public function setRaceSource($raceSource)
    {
        $this->RaceSource = $raceSource;

        return $this;
    }

    /**
     * Get RaceSource
     *
     * @return string 
     */
    public function getRaceSource()
    {
        return $this->RaceSource;
    }

    /**
     * Set RaceSrcId
     *
     * @param string $raceSrcId
     * @return Races
     */
    public function setRaceSrcId($raceSrcId)
    {
        $this->RaceSrcId = $raceSrcId;

        return $this;
    }

    /**
     * Get RaceSrcId
     *
     * @return string 
     */
    public function getRaceSrcId()
    {
        return $this->RaceSrcId;
    }

    /**
     * Set RaceEvntId
     *
     * @param integer $raceEvntId
     * @return Races
     */
    public function setRaceEvntId($raceEvntId)
    {
        $this->RaceEvntId = $raceEvntId;

        return $this;
    }

    /**
     * Get RaceEvntId
     *
     * @return integer 
     */
    public function getRaceEvntId()
    {
        return $this->RaceEvntId;
    }

    /**
     * Set RaceNumber
     *
     * @param integer $raceNumber
     * @return Races
     */
    public function setRaceNumber($raceNumber)
    {
        $this->RaceNumber = $raceNumber;

        return $this;
    }

    /**
     * Get RaceNumber
     *
     * @return integer 
     */
    public function getRaceNumber()
    {
        return $this->RaceNumber;
    }

    /**
     * Set RaceName
     *
     * @param string $raceName
     * @return Races
     */
    public function setRaceName($raceName)
    {
        $this->RaceName = $raceName;

        return $this;
    }

    /**
     * Get RaceName
     *
     * @return string 
     */
    public function getRaceName()
    {
        return $this->RaceName;
    }

    /**
     * Set RaceMedalRace
     *
     * @param boolean $raceMedalRace
     * @return Races
     */
    public function setRaceMedalRace($raceMedalRace)
    {
        $this->RaceMedalRace = $raceMedalRace;

        return $this;
    }

    /**
     * Get RaceMedalRace
     *
     * @return boolean 
     */
    public function getRaceMedalRace()
    {
        return $this->RaceMedalRace;
    }

    /**
     * Set RaceStartDate
     *
     * @param \DateTime $raceStartDate
     * @return Races
     */
    public function setRaceStartDate($raceStartDate)
    {
        $this->RaceStartDate = $raceStartDate;

        return $this;
    }

    /**
     * Get RaceStartDate
     *
     * @return \DateTime 
     */
    public function getRaceStartDate()
    {
        return $this->RaceStartDate;
    }

    /**
     * Set RaceStartTime
     *
     * @param \DateTime $raceStartTime
     * @return Races
     */
    public function setRaceStartTime($raceStartTime)
    {
        $this->RaceStartTime = $raceStartTime;

        return $this;
    }

    /**
     * Get RaceStartTime
     *
     * @return \DateTime 
     */
    public function getRaceStartTime()
    {
        return $this->RaceStartTime;
    }

    /**
     * Set RaceArea
     *
     * @param string $raceArea
     * @return Races
     */
    public function setRaceArea($raceArea)
    {
        $this->RaceArea = $raceArea;

        return $this;
    }

    /**
     * Get RaceArea
     *
     * @return string 
     */
    public function getRaceArea()
    {
        return $this->RaceArea;
    }

    /**
     * Set RaceCourseId
     *
     * @param integer $raceCourseId
     * @return Races
     */
    public function setRaceCourseId($raceCourseId)
    {
        $this->RaceCourseId = $raceCourseId;

        return $this;
    }

    /**
     * Get RaceCourseId
     *
     * @return integer 
     */
    public function getRaceCourseId()
    {
        return $this->RaceCourseId;
    }

    /**
     * Set RaceStatus
     *
     * @param string $raceStatus
     * @return Races
     */
    public function setRaceStatus($raceStatus)
    {
        $this->RaceStatus = $raceStatus;

        return $this;
    }

    /**
     * Get RaceStatus
     *
     * @return string 
     */
    public function getRaceStatus()
    {
        return $this->RaceStatus;
    }

    /**
     * Set RaceShowResults
     *
     * @param boolean $raceShowResults
     * @return Races
     */
    public function setRaceShowResults($raceShowResults)
    {
        $this->RaceShowResults = $raceShowResults;

        return $this;
    }

    /**
     * Get RaceShowResults
     *
     * @return boolean 
     */
    public function getRaceShowResults()
    {
        return $this->RaceShowResults;
    }

    /**
     * Set RaceMultiplier
     *
     * @param integer $raceMultiplier
     * @return Races
     */
    public function setRaceMultiplier($raceMultiplier)
    {
        $this->RaceMultiplier = $raceMultiplier;

        return $this;
    }

    /**
     * Get RaceMultiplier
     *
     * @return integer 
     */
    public function getRaceMultiplier()
    {
        return $this->RaceMultiplier;
    }

    /**
     * Set RaceTemp
     *
     * @param float $raceTemp
     * @return Races
     */
    public function setRaceTemp($raceTemp)
    {
        $this->RaceTemp = $raceTemp;

        return $this;
    }

    /**
     * Get RaceTemp
     *
     * @return float 
     */
    public function getRaceTemp()
    {
        return $this->RaceTemp;
    }

    /**
     * Set RaceTempUnit
     *
     * @param string $raceTempUnit
     * @return Races
     */
    public function setRaceTempUnit($raceTempUnit)
    {
        $this->RaceTempUnit = $raceTempUnit;

        return $this;
    }

    /**
     * Get RaceTempUnit
     *
     * @return string 
     */
    public function getRaceTempUnit()
    {
        return $this->RaceTempUnit;
    }

    /**
     * Set RaceHumidity
     *
     * @param float $raceHumidity
     * @return Races
     */
    public function setRaceHumidity($raceHumidity)
    {
        $this->RaceHumidity = $raceHumidity;

        return $this;
    }

    /**
     * Get RaceHumidity
     *
     * @return float 
     */
    public function getRaceHumidity()
    {
        return $this->RaceHumidity;
    }

    /**
     * Set RaceWeather
     *
     * @param string $raceWeather
     * @return Races
     */
    public function setRaceWeather($raceWeather)
    {
        $this->RaceWeather = $raceWeather;

        return $this;
    }

    /**
     * Get RaceWeather
     *
     * @return string 
     */
    public function getRaceWeather()
    {
        return $this->RaceWeather;
    }

    /**
     * Set RaceStage
     *
     * @param string $raceStage
     * @return Races
     */
    public function setRaceStage($raceStage)
    {
        $this->RaceStage = $raceStage;

        return $this;
    }

    /**
     * Get RaceStage
     *
     * @return string 
     */
    public function getRaceStage()
    {
        return $this->RaceStage;
    }

    /**
     * Set RaceFlight
     *
     * @param string $raceFlight
     * @return Races
     */
    public function setRaceFlight($raceFlight)
    {
        $this->RaceFlight = $raceFlight;

        return $this;
    }

    /**
     * Get RaceFlight
     *
     * @return string 
     */
    public function getRaceFlight()
    {
        return $this->RaceFlight;
    }

    /**
     * Set RaceMatch
     *
     * @param string $raceMatch
     * @return Races
     */
    public function setRaceMatch($raceMatch)
    {
        $this->RaceMatch = $raceMatch;

        return $this;
    }

    /**
     * Get RaceMatch
     *
     * @return string 
     */
    public function getRaceMatch()
    {
        return $this->RaceMatch;
    }

    /**
     * Set RaceResultUpdated
     *
     * @param \DateTime $raceResultUpdated
     * @return Races
     */
    public function setRaceResultUpdated($raceResultUpdated)
    {
        $this->RaceResultUpdated = $raceResultUpdated;

        return $this;
    }

    /**
     * Get RaceResultUpdated
     *
     * @return \DateTime 
     */
    public function getRaceResultUpdated()
    {
        return $this->RaceResultUpdated;
    }

    /**
     * Set RaceScheduled
     *
     * @param \DateTime $raceScheduled
     * @return Races
     */
    public function setRaceScheduled($raceScheduled)
    {
        $this->RaceScheduled = $raceScheduled;

        return $this;
    }

    /**
     * Get RaceScheduled
     *
     * @return \DateTime 
     */
    public function getRaceScheduled()
    {
        return $this->RaceScheduled;
    }

    /**
     * Get RaceId
     *
     * @return integer 
     */
    public function getRaceId()
    {
        return $this->RaceId;
    }

    /**
     * Add rresBtGrpId
     *
     * @param \worldsailing\Isaf\model\Entity\Boatgroup $rresBtGrpId
     * @return Races
     */
    public function addRresBtGrpId(\worldsailing\Isaf\model\Entity\Boatgroup $rresBtGrpId)
    {
        $this->rresBtGrpId[] = $rresBtGrpId;

        return $this;
    }

    /**
     * Remove rresBtGrpId
     *
     * @param \worldsailing\Isaf\model\Entity\Boatgroup $rresBtGrpId
     */
    public function removeRresBtGrpId(\worldsailing\Isaf\model\Entity\Boatgroup $rresBtGrpId)
    {
        $this->rresBtGrpId->removeElement($rresBtGrpId);
    }

    /**
     * Get rresBtGrpId
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRresBtGrpId()
    {
        return $this->rresBtGrpId;
    }
}
