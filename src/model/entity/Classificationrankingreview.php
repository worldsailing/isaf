<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationrankingreview
 *
 * @ORM\Table(name="ClassificationRankingReview", indexes={@ORM\Index(name="fk_rk_action_idx", columns={"CfcnRRActionId"})})
 * @ORM\Entity
 */
class Classificationrankingreview
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnRRMembId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CfcnRRMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnRRRankId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CfcnRRRankId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnRRActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnRRActionId;



    /**
     * Set CfcnRRMembId
     *
     * @param integer $cfcnRRMembId
     * @return Classificationrankingreview
     */
    public function setCfcnRRMembId($cfcnRRMembId)
    {
        $this->CfcnRRMembId = $cfcnRRMembId;

        return $this;
    }

    /**
     * Get CfcnRRMembId
     *
     * @return integer 
     */
    public function getCfcnRRMembId()
    {
        return $this->CfcnRRMembId;
    }

    /**
     * Set CfcnRRRankId
     *
     * @param integer $cfcnRRRankId
     * @return Classificationrankingreview
     */
    public function setCfcnRRRankId($cfcnRRRankId)
    {
        $this->CfcnRRRankId = $cfcnRRRankId;

        return $this;
    }

    /**
     * Get CfcnRRRankId
     *
     * @return integer 
     */
    public function getCfcnRRRankId()
    {
        return $this->CfcnRRRankId;
    }

    /**
     * Set cfcnRRActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnRRActionId
     * @return Classificationrankingreview
     */
    public function setCfcnRRActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnRRActionId = null)
    {
        $this->cfcnRRActionId = $cfcnRRActionId;

        return $this;
    }

    /**
     * Get cfcnRRActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnRRActionId()
    {
        return $this->cfcnRRActionId;
    }
}
