<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Courses
 *
 * @ORM\Table(name="Courses", indexes={@ORM\Index(name="CourseSource", columns={"CourseSource", "CourseCode"})})
 * @ORM\Entity
 */
class Courses
{
    /**
     * @var string
     *
     * @ORM\Column(name="CourseSource", type="string", length=20, nullable=true)
     */
    protected $CourseSource;

    /**
     * @var string
     *
     * @ORM\Column(name="CourseCode", type="string", length=10, nullable=true)
     */
    protected $CourseCode;

    /**
     * @var string
     *
     * @ORM\Column(name="CourseName", type="string", length=255, nullable=true)
     */
    protected $CourseName;

    /**
     * @var integer
     *
     * @ORM\Column(name="CourseId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CourseId;



    /**
     * Set CourseSource
     *
     * @param string $courseSource
     * @return Courses
     */
    public function setCourseSource($courseSource)
    {
        $this->CourseSource = $courseSource;

        return $this;
    }

    /**
     * Get CourseSource
     *
     * @return string 
     */
    public function getCourseSource()
    {
        return $this->CourseSource;
    }

    /**
     * Set CourseCode
     *
     * @param string $courseCode
     * @return Courses
     */
    public function setCourseCode($courseCode)
    {
        $this->CourseCode = $courseCode;

        return $this;
    }

    /**
     * Get CourseCode
     *
     * @return string 
     */
    public function getCourseCode()
    {
        return $this->CourseCode;
    }

    /**
     * Set CourseName
     *
     * @param string $courseName
     * @return Courses
     */
    public function setCourseName($courseName)
    {
        $this->CourseName = $courseName;

        return $this;
    }

    /**
     * Get CourseName
     *
     * @return string 
     */
    public function getCourseName()
    {
        return $this->CourseName;
    }

    /**
     * Get CourseId
     *
     * @return integer 
     */
    public function getCourseId()
    {
        return $this->CourseId;
    }
}
