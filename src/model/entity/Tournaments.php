<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tournaments
 *
 * @ORM\Table(name="Tournaments")
 * @ORM\Entity
 */
class Tournaments
{
    /**
     * @var string
     *
     * @ORM\Column(name="TournName", type="string", length=255, nullable=true)
     */
    protected $TournName;

    /**
     * @var string
     *
     * @ORM\Column(name="TournXRRUploadCode", type="string", length=20, nullable=true)
     */
    protected $TournXRRUploadCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="TournId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $TournId;



    /**
     * Set TournName
     *
     * @param string $tournName
     * @return Tournaments
     */
    public function setTournName($tournName)
    {
        $this->TournName = $tournName;

        return $this;
    }

    /**
     * Get TournName
     *
     * @return string 
     */
    public function getTournName()
    {
        return $this->TournName;
    }

    /**
     * Set TournXRRUploadCode
     *
     * @param string $tournXRRUploadCode
     * @return Tournaments
     */
    public function setTournXRRUploadCode($tournXRRUploadCode)
    {
        $this->TournXRRUploadCode = $tournXRRUploadCode;

        return $this;
    }

    /**
     * Get TournXRRUploadCode
     *
     * @return string 
     */
    public function getTournXRRUploadCode()
    {
        return $this->TournXRRUploadCode;
    }

    /**
     * Get TournId
     *
     * @return integer 
     */
    public function getTournId()
    {
        return $this->TournId;
    }
}
