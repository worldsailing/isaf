<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationdecision
 *
 * @ORM\Table(name="ClassificationDecision", uniqueConstraints={@ORM\UniqueConstraint(name="idClassificationDecision_UNIQUE", columns={"CfcnDcsnActionId"})}, indexes={@ORM\Index(name="dcsn_act_fk_1", columns={"CfcnDcsnActionId"})})
 * @ORM\Entity
 */
class Classificationdecision
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfcnDcsnGrp", type="string", length=5, nullable=true)
     */
    protected $CfcnDcsnGrp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnDcsnEffectiveDate", type="date", nullable=true)
     */
    protected $CfcnDcsnEffectiveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnDcsnExpiryDate", type="date", nullable=true)
     */
    protected $CfcnDcsnExpiryDate;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnDcsnActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnDcsnActionId;



    /**
     * Set CfcnDcsnGrp
     *
     * @param string $cfcnDcsnGrp
     * @return Classificationdecision
     */
    public function setCfcnDcsnGrp($cfcnDcsnGrp)
    {
        $this->CfcnDcsnGrp = $cfcnDcsnGrp;

        return $this;
    }

    /**
     * Get CfcnDcsnGrp
     *
     * @return string 
     */
    public function getCfcnDcsnGrp()
    {
        return $this->CfcnDcsnGrp;
    }

    /**
     * Set CfcnDcsnEffectiveDate
     *
     * @param \DateTime $cfcnDcsnEffectiveDate
     * @return Classificationdecision
     */
    public function setCfcnDcsnEffectiveDate($cfcnDcsnEffectiveDate)
    {
        $this->CfcnDcsnEffectiveDate = $cfcnDcsnEffectiveDate;

        return $this;
    }

    /**
     * Get CfcnDcsnEffectiveDate
     *
     * @return \DateTime 
     */
    public function getCfcnDcsnEffectiveDate()
    {
        return $this->CfcnDcsnEffectiveDate;
    }

    /**
     * Set CfcnDcsnExpiryDate
     *
     * @param \DateTime $cfcnDcsnExpiryDate
     * @return Classificationdecision
     */
    public function setCfcnDcsnExpiryDate($cfcnDcsnExpiryDate)
    {
        $this->CfcnDcsnExpiryDate = $cfcnDcsnExpiryDate;

        return $this;
    }

    /**
     * Get CfcnDcsnExpiryDate
     *
     * @return \DateTime 
     */
    public function getCfcnDcsnExpiryDate()
    {
        return $this->CfcnDcsnExpiryDate;
    }

    /**
     * Set cfcnDcsnActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnDcsnActionId
     * @return Classificationdecision
     */
    public function setCfcnDcsnActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnDcsnActionId)
    {
        $this->cfcnDcsnActionId = $cfcnDcsnActionId;

        return $this;
    }

    /**
     * Get cfcnDcsnActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnDcsnActionId()
    {
        return $this->cfcnDcsnActionId;
    }
}
