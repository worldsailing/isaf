<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Datacache
 *
 * @ORM\Table(name="DataCache")
 * @ORM\Entity
 */
class Datacache
{
    /**
     * @var string
     *
     * @ORM\Column(name="CchValue", type="text", nullable=false)
     */
    protected $CchValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CchTimeout", type="datetime", nullable=true)
     */
    protected $CchTimeout;

    /**
     * @var string
     *
     * @ORM\Column(name="CchSource", type="string", length=45)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CchSource;

    /**
     * @var string
     *
     * @ORM\Column(name="CchName", type="string", length=45)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CchName;

    /**
     * @var integer
     *
     * @ORM\Column(name="CchIndex", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CchIndex;



    /**
     * Set CchValue
     *
     * @param string $cchValue
     * @return Datacache
     */
    public function setCchValue($cchValue)
    {
        $this->CchValue = $cchValue;

        return $this;
    }

    /**
     * Get CchValue
     *
     * @return string 
     */
    public function getCchValue()
    {
        return $this->CchValue;
    }

    /**
     * Set CchTimeout
     *
     * @param \DateTime $cchTimeout
     * @return Datacache
     */
    public function setCchTimeout($cchTimeout)
    {
        $this->CchTimeout = $cchTimeout;

        return $this;
    }

    /**
     * Get CchTimeout
     *
     * @return \DateTime 
     */
    public function getCchTimeout()
    {
        return $this->CchTimeout;
    }

    /**
     * Set CchSource
     *
     * @param string $cchSource
     * @return Datacache
     */
    public function setCchSource($cchSource)
    {
        $this->CchSource = $cchSource;

        return $this;
    }

    /**
     * Get CchSource
     *
     * @return string 
     */
    public function getCchSource()
    {
        return $this->CchSource;
    }

    /**
     * Set CchName
     *
     * @param string $cchName
     * @return Datacache
     */
    public function setCchName($cchName)
    {
        $this->CchName = $cchName;

        return $this;
    }

    /**
     * Get CchName
     *
     * @return string 
     */
    public function getCchName()
    {
        return $this->CchName;
    }

    /**
     * Set CchIndex
     *
     * @param integer $cchIndex
     * @return Datacache
     */
    public function setCchIndex($cchIndex)
    {
        $this->CchIndex = $cchIndex;

        return $this;
    }

    /**
     * Get CchIndex
     *
     * @return integer 
     */
    public function getCchIndex()
    {
        return $this->CchIndex;
    }
}
