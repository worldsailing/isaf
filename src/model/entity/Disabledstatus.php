<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Disabledstatus
 *
 * @ORM\Table(name="DisabledStatus")
 * @ORM\Entity
 */
class Disabledstatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="DstMembId", type="integer", nullable=false)
     */
    protected $DstMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="DstOfstId", type="integer", nullable=false)
     */
    protected $DstOfstId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DstStart", type="date", nullable=true)
     */
    protected $DstStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DstEnd", type="date", nullable=true)
     */
    protected $DstEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="DstNotes", type="text", length=65535, nullable=true)
     */
    protected $DstNotes;

    /**
     * @var integer
     *
     * @ORM\Column(name="DstId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $DstId;



    /**
     * Set DstMembId
     *
     * @param integer $dstMembId
     * @return Disabledstatus
     */
    public function setDstMembId($dstMembId)
    {
        $this->DstMembId = $dstMembId;

        return $this;
    }

    /**
     * Get DstMembId
     *
     * @return integer 
     */
    public function getDstMembId()
    {
        return $this->DstMembId;
    }

    /**
     * Set DstOfstId
     *
     * @param integer $dstOfstId
     * @return Disabledstatus
     */
    public function setDstOfstId($dstOfstId)
    {
        $this->DstOfstId = $dstOfstId;

        return $this;
    }

    /**
     * Get DstOfstId
     *
     * @return integer 
     */
    public function getDstOfstId()
    {
        return $this->DstOfstId;
    }

    /**
     * Set DstStart
     *
     * @param \DateTime $dstStart
     * @return Disabledstatus
     */
    public function setDstStart($dstStart)
    {
        $this->DstStart = $dstStart;

        return $this;
    }

    /**
     * Get DstStart
     *
     * @return \DateTime 
     */
    public function getDstStart()
    {
        return $this->DstStart;
    }

    /**
     * Set DstEnd
     *
     * @param \DateTime $dstEnd
     * @return Disabledstatus
     */
    public function setDstEnd($dstEnd)
    {
        $this->DstEnd = $dstEnd;

        return $this;
    }

    /**
     * Get DstEnd
     *
     * @return \DateTime 
     */
    public function getDstEnd()
    {
        return $this->DstEnd;
    }

    /**
     * Set DstNotes
     *
     * @param string $dstNotes
     * @return Disabledstatus
     */
    public function setDstNotes($dstNotes)
    {
        $this->DstNotes = $dstNotes;

        return $this;
    }

    /**
     * Get DstNotes
     *
     * @return string 
     */
    public function getDstNotes()
    {
        return $this->DstNotes;
    }

    /**
     * Get DstId
     *
     * @return integer 
     */
    public function getDstId()
    {
        return $this->DstId;
    }
}
