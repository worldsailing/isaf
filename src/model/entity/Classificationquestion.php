<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationquestion
 *
 * @ORM\Table(name="ClassificationQuestion")
 * @ORM\Entity
 */
class Classificationquestion
{
    /**
     * @var string
     *
     * @ORM\Column(name="QuestionType", type="string", length=100, nullable=true)
     */
    protected $QuestionType;

    /**
     * @var string
     *
     * @ORM\Column(name="QuestionText", type="text", length=65535, nullable=true)
     */
    protected $QuestionText;

    /**
     * @var string
     *
     * @ORM\Column(name="QuestionData", type="text", length=65535, nullable=true)
     */
    protected $QuestionData;

    /**
     * @var string
     *
     * @ORM\Column(name="QuestionConfirmation", type="text", length=65535, nullable=true)
     */
    protected $QuestionConfirmation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="QuestionAllowInfo", type="boolean", nullable=true)
     */
    protected $QuestionAllowInfo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="QuestionMoreInfoOptional", type="boolean", nullable=true)
     */
    protected $QuestionMoreInfoOptional;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="QuestionCreationDate", type="datetime", nullable=false)
     */
    protected $QuestionCreationDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="QuestionCreator", type="integer", nullable=false)
     */
    protected $QuestionCreator;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="QuestionVersStart", type="date", nullable=true)
     */
    protected $QuestionVersStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="QuestionVersEnd", type="date", nullable=true)
     */
    protected $QuestionVersEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="QuestionOrder", type="integer", nullable=false)
     */
    protected $QuestionOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="QuestionIsMandatory", type="boolean", nullable=false)
     */
    protected $QuestionIsMandatory;

    /**
     * @var string
     *
     * @ORM\Column(name="QuestionMoreInfoLabel", type="text", length=65535, nullable=true)
     */
    protected $QuestionMoreInfoLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="QuestionMoreInfoError", type="text", length=65535, nullable=true)
     */
    protected $QuestionMoreInfoError;

    /**
     * @var string
     *
     * @ORM\Column(name="QuestionHelpContent", type="text", length=65535, nullable=true)
     */
    protected $QuestionHelpContent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="QuestionIsLegacy", type="boolean", nullable=true)
     */
    protected $QuestionIsLegacy;

    /**
     * @var integer
     *
     * @ORM\Column(name="QuestionId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $QuestionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="QuestionVersion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $QuestionVersion;



    /**
     * Set QuestionType
     *
     * @param string $questionType
     * @return Classificationquestion
     */
    public function setQuestionType($questionType)
    {
        $this->QuestionType = $questionType;

        return $this;
    }

    /**
     * Get QuestionType
     *
     * @return string 
     */
    public function getQuestionType()
    {
        return $this->QuestionType;
    }

    /**
     * Set QuestionText
     *
     * @param string $questionText
     * @return Classificationquestion
     */
    public function setQuestionText($questionText)
    {
        $this->QuestionText = $questionText;

        return $this;
    }

    /**
     * Get QuestionText
     *
     * @return string 
     */
    public function getQuestionText()
    {
        return $this->QuestionText;
    }

    /**
     * Set QuestionData
     *
     * @param string $questionData
     * @return Classificationquestion
     */
    public function setQuestionData($questionData)
    {
        $this->QuestionData = $questionData;

        return $this;
    }

    /**
     * Get QuestionData
     *
     * @return string 
     */
    public function getQuestionData()
    {
        return $this->QuestionData;
    }

    /**
     * Set QuestionConfirmation
     *
     * @param string $questionConfirmation
     * @return Classificationquestion
     */
    public function setQuestionConfirmation($questionConfirmation)
    {
        $this->QuestionConfirmation = $questionConfirmation;

        return $this;
    }

    /**
     * Get QuestionConfirmation
     *
     * @return string 
     */
    public function getQuestionConfirmation()
    {
        return $this->QuestionConfirmation;
    }

    /**
     * Set QuestionAllowInfo
     *
     * @param boolean $questionAllowInfo
     * @return Classificationquestion
     */
    public function setQuestionAllowInfo($questionAllowInfo)
    {
        $this->QuestionAllowInfo = $questionAllowInfo;

        return $this;
    }

    /**
     * Get QuestionAllowInfo
     *
     * @return boolean 
     */
    public function getQuestionAllowInfo()
    {
        return $this->QuestionAllowInfo;
    }

    /**
     * Set QuestionMoreInfoOptional
     *
     * @param boolean $questionMoreInfoOptional
     * @return Classificationquestion
     */
    public function setQuestionMoreInfoOptional($questionMoreInfoOptional)
    {
        $this->QuestionMoreInfoOptional = $questionMoreInfoOptional;

        return $this;
    }

    /**
     * Get QuestionMoreInfoOptional
     *
     * @return boolean 
     */
    public function getQuestionMoreInfoOptional()
    {
        return $this->QuestionMoreInfoOptional;
    }

    /**
     * Set QuestionCreationDate
     *
     * @param \DateTime $questionCreationDate
     * @return Classificationquestion
     */
    public function setQuestionCreationDate($questionCreationDate)
    {
        $this->QuestionCreationDate = $questionCreationDate;

        return $this;
    }

    /**
     * Get QuestionCreationDate
     *
     * @return \DateTime 
     */
    public function getQuestionCreationDate()
    {
        return $this->QuestionCreationDate;
    }

    /**
     * Set QuestionCreator
     *
     * @param integer $questionCreator
     * @return Classificationquestion
     */
    public function setQuestionCreator($questionCreator)
    {
        $this->QuestionCreator = $questionCreator;

        return $this;
    }

    /**
     * Get QuestionCreator
     *
     * @return integer 
     */
    public function getQuestionCreator()
    {
        return $this->QuestionCreator;
    }

    /**
     * Set QuestionVersStart
     *
     * @param \DateTime $questionVersStart
     * @return Classificationquestion
     */
    public function setQuestionVersStart($questionVersStart)
    {
        $this->QuestionVersStart = $questionVersStart;

        return $this;
    }

    /**
     * Get QuestionVersStart
     *
     * @return \DateTime 
     */
    public function getQuestionVersStart()
    {
        return $this->QuestionVersStart;
    }

    /**
     * Set QuestionVersEnd
     *
     * @param \DateTime $questionVersEnd
     * @return Classificationquestion
     */
    public function setQuestionVersEnd($questionVersEnd)
    {
        $this->QuestionVersEnd = $questionVersEnd;

        return $this;
    }

    /**
     * Get QuestionVersEnd
     *
     * @return \DateTime 
     */
    public function getQuestionVersEnd()
    {
        return $this->QuestionVersEnd;
    }

    /**
     * Set QuestionOrder
     *
     * @param integer $questionOrder
     * @return Classificationquestion
     */
    public function setQuestionOrder($questionOrder)
    {
        $this->QuestionOrder = $questionOrder;

        return $this;
    }

    /**
     * Get QuestionOrder
     *
     * @return integer 
     */
    public function getQuestionOrder()
    {
        return $this->QuestionOrder;
    }

    /**
     * Set QuestionIsMandatory
     *
     * @param boolean $questionIsMandatory
     * @return Classificationquestion
     */
    public function setQuestionIsMandatory($questionIsMandatory)
    {
        $this->QuestionIsMandatory = $questionIsMandatory;

        return $this;
    }

    /**
     * Get QuestionIsMandatory
     *
     * @return boolean 
     */
    public function getQuestionIsMandatory()
    {
        return $this->QuestionIsMandatory;
    }

    /**
     * Set QuestionMoreInfoLabel
     *
     * @param string $questionMoreInfoLabel
     * @return Classificationquestion
     */
    public function setQuestionMoreInfoLabel($questionMoreInfoLabel)
    {
        $this->QuestionMoreInfoLabel = $questionMoreInfoLabel;

        return $this;
    }

    /**
     * Get QuestionMoreInfoLabel
     *
     * @return string 
     */
    public function getQuestionMoreInfoLabel()
    {
        return $this->QuestionMoreInfoLabel;
    }

    /**
     * Set QuestionMoreInfoError
     *
     * @param string $questionMoreInfoError
     * @return Classificationquestion
     */
    public function setQuestionMoreInfoError($questionMoreInfoError)
    {
        $this->QuestionMoreInfoError = $questionMoreInfoError;

        return $this;
    }

    /**
     * Get QuestionMoreInfoError
     *
     * @return string 
     */
    public function getQuestionMoreInfoError()
    {
        return $this->QuestionMoreInfoError;
    }

    /**
     * Set QuestionHelpContent
     *
     * @param string $questionHelpContent
     * @return Classificationquestion
     */
    public function setQuestionHelpContent($questionHelpContent)
    {
        $this->QuestionHelpContent = $questionHelpContent;

        return $this;
    }

    /**
     * Get QuestionHelpContent
     *
     * @return string 
     */
    public function getQuestionHelpContent()
    {
        return $this->QuestionHelpContent;
    }

    /**
     * Set QuestionIsLegacy
     *
     * @param boolean $questionIsLegacy
     * @return Classificationquestion
     */
    public function setQuestionIsLegacy($questionIsLegacy)
    {
        $this->QuestionIsLegacy = $questionIsLegacy;

        return $this;
    }

    /**
     * Get QuestionIsLegacy
     *
     * @return boolean 
     */
    public function getQuestionIsLegacy()
    {
        return $this->QuestionIsLegacy;
    }

    /**
     * Set QuestionId
     *
     * @param integer $questionId
     * @return Classificationquestion
     */
    public function setQuestionId($questionId)
    {
        $this->QuestionId = $questionId;

        return $this;
    }

    /**
     * Get QuestionId
     *
     * @return integer 
     */
    public function getQuestionId()
    {
        return $this->QuestionId;
    }

    /**
     * Set QuestionVersion
     *
     * @param integer $questionVersion
     * @return Classificationquestion
     */
    public function setQuestionVersion($questionVersion)
    {
        $this->QuestionVersion = $questionVersion;

        return $this;
    }

    /**
     * Get QuestionVersion
     *
     * @return integer 
     */
    public function getQuestionVersion()
    {
        return $this->QuestionVersion;
    }
}
