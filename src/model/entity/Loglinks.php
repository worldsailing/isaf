<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Loglinks
 *
 * @ORM\Table(name="LogLinks", indexes={@ORM\Index(name="LglnLogKey", columns={"LglnLogKey"}), @ORM\Index(name="LglnEntityType", columns={"LglnEntityType", "LglnEntityId"})})
 * @ORM\Entity
 */
class Loglinks
{
    /**
     * @var string
     *
     * @ORM\Column(name="LglnLogKey", type="string", length=50, nullable=false)
     */
    protected $LglnLogKey;

    /**
     * @var string
     *
     * @ORM\Column(name="LglnEntityType", type="string", length=20, nullable=false)
     */
    protected $LglnEntityType;

    /**
     * @var integer
     *
     * @ORM\Column(name="LglnEntityId", type="integer", nullable=false)
     */
    protected $LglnEntityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $Id;



    /**
     * Set LglnLogKey
     *
     * @param string $lglnLogKey
     * @return Loglinks
     */
    public function setLglnLogKey($lglnLogKey)
    {
        $this->LglnLogKey = $lglnLogKey;

        return $this;
    }

    /**
     * Get LglnLogKey
     *
     * @return string 
     */
    public function getLglnLogKey()
    {
        return $this->LglnLogKey;
    }

    /**
     * Set LglnEntityType
     *
     * @param string $lglnEntityType
     * @return Loglinks
     */
    public function setLglnEntityType($lglnEntityType)
    {
        $this->LglnEntityType = $lglnEntityType;

        return $this;
    }

    /**
     * Get LglnEntityType
     *
     * @return string 
     */
    public function getLglnEntityType()
    {
        return $this->LglnEntityType;
    }

    /**
     * Set LglnEntityId
     *
     * @param integer $lglnEntityId
     * @return Loglinks
     */
    public function setLglnEntityId($lglnEntityId)
    {
        $this->LglnEntityId = $lglnEntityId;

        return $this;
    }

    /**
     * Get LglnEntityId
     *
     * @return integer 
     */
    public function getLglnEntityId()
    {
        return $this->LglnEntityId;
    }

    /**
     * Get Id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->Id;
    }
}
