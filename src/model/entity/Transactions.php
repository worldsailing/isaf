<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transactions
 *
 * @ORM\Table(name="Transactions")
 * @ORM\Entity
 */
class Transactions
{
    /**
     * @var string
     *
     * @ORM\Column(name="TransMethod", type="string", length=20, nullable=false)
     */
    protected $TransMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="TransStatus", type="string", nullable=false)
     */
    protected $TransStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="TransType", type="string", length=100, nullable=false)
     */
    protected $TransType;

    /**
     * @var string
     *
     * @ORM\Column(name="TransDescription", type="string", length=200, nullable=false)
     */
    protected $TransDescription;

    /**
     * @var float
     *
     * @ORM\Column(name="TransTotal", type="float", precision=10, scale=0, nullable=false)
     */
    protected $TransTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="TransCurrency", type="string", length=20, nullable=false)
     */
    protected $TransCurrency;

    /**
     * @var string
     *
     * @ORM\Column(name="TransEmail", type="string", length=200, nullable=false)
     */
    protected $TransEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="TransBillFirstName", type="string", length=200, nullable=false)
     */
    protected $TransBillFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="TransBillSurname", type="string", length=200, nullable=false)
     */
    protected $TransBillSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="TransBillAddress1", type="string", length=200, nullable=false)
     */
    protected $TransBillAddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="TransBillTownCity", type="string", length=200, nullable=false)
     */
    protected $TransBillTownCity;

    /**
     * @var string
     *
     * @ORM\Column(name="TransBillCounty", type="string", length=200, nullable=false)
     */
    protected $TransBillCounty;

    /**
     * @var string
     *
     * @ORM\Column(name="TransBillPostcode", type="string", length=200, nullable=false)
     */
    protected $TransBillPostcode;

    /**
     * @var string
     *
     * @ORM\Column(name="TransBillCountry", type="string", length=200, nullable=false)
     */
    protected $TransBillCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="TransShipFirstName", type="string", length=200, nullable=false)
     */
    protected $TransShipFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="TransShipSurname", type="string", length=200, nullable=false)
     */
    protected $TransShipSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="TransShipAddress1", type="string", length=200, nullable=false)
     */
    protected $TransShipAddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="TransShipTownCity", type="string", length=200, nullable=false)
     */
    protected $TransShipTownCity;

    /**
     * @var string
     *
     * @ORM\Column(name="TransShipCounty", type="string", length=200, nullable=false)
     */
    protected $TransShipCounty;

    /**
     * @var string
     *
     * @ORM\Column(name="TransShipPostcode", type="string", length=200, nullable=false)
     */
    protected $TransShipPostcode;

    /**
     * @var string
     *
     * @ORM\Column(name="TransShipCountry", type="string", length=200, nullable=false)
     */
    protected $TransShipCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="TransReturnUrl", type="string", length=200, nullable=false)
     */
    protected $TransReturnUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TransCreated", type="datetime", nullable=false)
     */
    protected $TransCreated;

    /**
     * @var integer
     *
     * @ORM\Column(name="TransId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $TransId;



    /**
     * Set TransMethod
     *
     * @param string $transMethod
     * @return Transactions
     */
    public function setTransMethod($transMethod)
    {
        $this->TransMethod = $transMethod;

        return $this;
    }

    /**
     * Get TransMethod
     *
     * @return string 
     */
    public function getTransMethod()
    {
        return $this->TransMethod;
    }

    /**
     * Set TransStatus
     *
     * @param string $transStatus
     * @return Transactions
     */
    public function setTransStatus($transStatus)
    {
        $this->TransStatus = $transStatus;

        return $this;
    }

    /**
     * Get TransStatus
     *
     * @return string 
     */
    public function getTransStatus()
    {
        return $this->TransStatus;
    }

    /**
     * Set TransType
     *
     * @param string $transType
     * @return Transactions
     */
    public function setTransType($transType)
    {
        $this->TransType = $transType;

        return $this;
    }

    /**
     * Get TransType
     *
     * @return string 
     */
    public function getTransType()
    {
        return $this->TransType;
    }

    /**
     * Set TransDescription
     *
     * @param string $transDescription
     * @return Transactions
     */
    public function setTransDescription($transDescription)
    {
        $this->TransDescription = $transDescription;

        return $this;
    }

    /**
     * Get TransDescription
     *
     * @return string 
     */
    public function getTransDescription()
    {
        return $this->TransDescription;
    }

    /**
     * Set TransTotal
     *
     * @param float $transTotal
     * @return Transactions
     */
    public function setTransTotal($transTotal)
    {
        $this->TransTotal = $transTotal;

        return $this;
    }

    /**
     * Get TransTotal
     *
     * @return float 
     */
    public function getTransTotal()
    {
        return $this->TransTotal;
    }

    /**
     * Set TransCurrency
     *
     * @param string $transCurrency
     * @return Transactions
     */
    public function setTransCurrency($transCurrency)
    {
        $this->TransCurrency = $transCurrency;

        return $this;
    }

    /**
     * Get TransCurrency
     *
     * @return string 
     */
    public function getTransCurrency()
    {
        return $this->TransCurrency;
    }

    /**
     * Set TransEmail
     *
     * @param string $transEmail
     * @return Transactions
     */
    public function setTransEmail($transEmail)
    {
        $this->TransEmail = $transEmail;

        return $this;
    }

    /**
     * Get TransEmail
     *
     * @return string 
     */
    public function getTransEmail()
    {
        return $this->TransEmail;
    }

    /**
     * Set TransBillFirstName
     *
     * @param string $transBillFirstName
     * @return Transactions
     */
    public function setTransBillFirstName($transBillFirstName)
    {
        $this->TransBillFirstName = $transBillFirstName;

        return $this;
    }

    /**
     * Get TransBillFirstName
     *
     * @return string 
     */
    public function getTransBillFirstName()
    {
        return $this->TransBillFirstName;
    }

    /**
     * Set TransBillSurname
     *
     * @param string $transBillSurname
     * @return Transactions
     */
    public function setTransBillSurname($transBillSurname)
    {
        $this->TransBillSurname = $transBillSurname;

        return $this;
    }

    /**
     * Get TransBillSurname
     *
     * @return string 
     */
    public function getTransBillSurname()
    {
        return $this->TransBillSurname;
    }

    /**
     * Set TransBillAddress1
     *
     * @param string $transBillAddress1
     * @return Transactions
     */
    public function setTransBillAddress1($transBillAddress1)
    {
        $this->TransBillAddress1 = $transBillAddress1;

        return $this;
    }

    /**
     * Get TransBillAddress1
     *
     * @return string 
     */
    public function getTransBillAddress1()
    {
        return $this->TransBillAddress1;
    }

    /**
     * Set TransBillTownCity
     *
     * @param string $transBillTownCity
     * @return Transactions
     */
    public function setTransBillTownCity($transBillTownCity)
    {
        $this->TransBillTownCity = $transBillTownCity;

        return $this;
    }

    /**
     * Get TransBillTownCity
     *
     * @return string 
     */
    public function getTransBillTownCity()
    {
        return $this->TransBillTownCity;
    }

    /**
     * Set TransBillCounty
     *
     * @param string $transBillCounty
     * @return Transactions
     */
    public function setTransBillCounty($transBillCounty)
    {
        $this->TransBillCounty = $transBillCounty;

        return $this;
    }

    /**
     * Get TransBillCounty
     *
     * @return string 
     */
    public function getTransBillCounty()
    {
        return $this->TransBillCounty;
    }

    /**
     * Set TransBillPostcode
     *
     * @param string $transBillPostcode
     * @return Transactions
     */
    public function setTransBillPostcode($transBillPostcode)
    {
        $this->TransBillPostcode = $transBillPostcode;

        return $this;
    }

    /**
     * Get TransBillPostcode
     *
     * @return string 
     */
    public function getTransBillPostcode()
    {
        return $this->TransBillPostcode;
    }

    /**
     * Set TransBillCountry
     *
     * @param string $transBillCountry
     * @return Transactions
     */
    public function setTransBillCountry($transBillCountry)
    {
        $this->TransBillCountry = $transBillCountry;

        return $this;
    }

    /**
     * Get TransBillCountry
     *
     * @return string 
     */
    public function getTransBillCountry()
    {
        return $this->TransBillCountry;
    }

    /**
     * Set TransShipFirstName
     *
     * @param string $transShipFirstName
     * @return Transactions
     */
    public function setTransShipFirstName($transShipFirstName)
    {
        $this->TransShipFirstName = $transShipFirstName;

        return $this;
    }

    /**
     * Get TransShipFirstName
     *
     * @return string 
     */
    public function getTransShipFirstName()
    {
        return $this->TransShipFirstName;
    }

    /**
     * Set TransShipSurname
     *
     * @param string $transShipSurname
     * @return Transactions
     */
    public function setTransShipSurname($transShipSurname)
    {
        $this->TransShipSurname = $transShipSurname;

        return $this;
    }

    /**
     * Get TransShipSurname
     *
     * @return string 
     */
    public function getTransShipSurname()
    {
        return $this->TransShipSurname;
    }

    /**
     * Set TransShipAddress1
     *
     * @param string $transShipAddress1
     * @return Transactions
     */
    public function setTransShipAddress1($transShipAddress1)
    {
        $this->TransShipAddress1 = $transShipAddress1;

        return $this;
    }

    /**
     * Get TransShipAddress1
     *
     * @return string 
     */
    public function getTransShipAddress1()
    {
        return $this->TransShipAddress1;
    }

    /**
     * Set TransShipTownCity
     *
     * @param string $transShipTownCity
     * @return Transactions
     */
    public function setTransShipTownCity($transShipTownCity)
    {
        $this->TransShipTownCity = $transShipTownCity;

        return $this;
    }

    /**
     * Get TransShipTownCity
     *
     * @return string 
     */
    public function getTransShipTownCity()
    {
        return $this->TransShipTownCity;
    }

    /**
     * Set TransShipCounty
     *
     * @param string $transShipCounty
     * @return Transactions
     */
    public function setTransShipCounty($transShipCounty)
    {
        $this->TransShipCounty = $transShipCounty;

        return $this;
    }

    /**
     * Get TransShipCounty
     *
     * @return string 
     */
    public function getTransShipCounty()
    {
        return $this->TransShipCounty;
    }

    /**
     * Set TransShipPostcode
     *
     * @param string $transShipPostcode
     * @return Transactions
     */
    public function setTransShipPostcode($transShipPostcode)
    {
        $this->TransShipPostcode = $transShipPostcode;

        return $this;
    }

    /**
     * Get TransShipPostcode
     *
     * @return string 
     */
    public function getTransShipPostcode()
    {
        return $this->TransShipPostcode;
    }

    /**
     * Set TransShipCountry
     *
     * @param string $transShipCountry
     * @return Transactions
     */
    public function setTransShipCountry($transShipCountry)
    {
        $this->TransShipCountry = $transShipCountry;

        return $this;
    }

    /**
     * Get TransShipCountry
     *
     * @return string 
     */
    public function getTransShipCountry()
    {
        return $this->TransShipCountry;
    }

    /**
     * Set TransReturnUrl
     *
     * @param string $transReturnUrl
     * @return Transactions
     */
    public function setTransReturnUrl($transReturnUrl)
    {
        $this->TransReturnUrl = $transReturnUrl;

        return $this;
    }

    /**
     * Get TransReturnUrl
     *
     * @return string 
     */
    public function getTransReturnUrl()
    {
        return $this->TransReturnUrl;
    }

    /**
     * Set TransCreated
     *
     * @param \DateTime $transCreated
     * @return Transactions
     */
    public function setTransCreated($transCreated)
    {
        $this->TransCreated = $transCreated;

        return $this;
    }

    /**
     * Get TransCreated
     *
     * @return \DateTime 
     */
    public function getTransCreated()
    {
        return $this->TransCreated;
    }

    /**
     * Get TransId
     *
     * @return integer 
     */
    public function getTransId()
    {
        return $this->TransId;
    }
}
