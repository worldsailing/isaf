<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Userroles
 *
 * @ORM\Table(name="userroles")
 * @ORM\Entity
 */
class Userroles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="UsrlUserId", type="integer", nullable=false)
     */
    protected $UsrlUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="UsrlRoleId", type="integer", nullable=false)
     */
    protected $UsrlRoleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="UsrlEntityId", type="integer", nullable=true)
     */
    protected $UsrlEntityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="UsrlId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $UsrlId;



    /**
     * Set UsrlUserId
     *
     * @param integer $usrlUserId
     * @return Userroles
     */
    public function setUsrlUserId($usrlUserId)
    {
        $this->UsrlUserId = $usrlUserId;

        return $this;
    }

    /**
     * Get UsrlUserId
     *
     * @return integer 
     */
    public function getUsrlUserId()
    {
        return $this->UsrlUserId;
    }

    /**
     * Set UsrlRoleId
     *
     * @param integer $usrlRoleId
     * @return Userroles
     */
    public function setUsrlRoleId($usrlRoleId)
    {
        $this->UsrlRoleId = $usrlRoleId;

        return $this;
    }

    /**
     * Get UsrlRoleId
     *
     * @return integer 
     */
    public function getUsrlRoleId()
    {
        return $this->UsrlRoleId;
    }

    /**
     * Set UsrlEntityId
     *
     * @param integer $usrlEntityId
     * @return Userroles
     */
    public function setUsrlEntityId($usrlEntityId)
    {
        $this->UsrlEntityId = $usrlEntityId;

        return $this;
    }

    /**
     * Get UsrlEntityId
     *
     * @return integer 
     */
    public function getUsrlEntityId()
    {
        return $this->UsrlEntityId;
    }

    /**
     * Get UsrlId
     *
     * @return integer 
     */
    public function getUsrlId()
    {
        return $this->UsrlId;
    }
}
