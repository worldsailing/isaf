<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Buildersplaques
 *
 * @ORM\Table(name="BuildersPlaques")
 * @ORM\Entity
 */
class Buildersplaques
{
    /**
     * @var integer
     *
     * @ORM\Column(name="PlaqBuildOrgId", type="integer", nullable=false)
     */
    protected $PlaqBuildOrgId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PlaqClassId", type="integer", nullable=false)
     */
    protected $PlaqClassId;

    /**
     * @var string
     *
     * @ORM\Column(name="PlaqNum", type="string", length=100, nullable=true)
     */
    protected $PlaqNum;

    /**
     * @var string
     *
     * @ORM\Column(name="PlaqDate", type="string", length=100, nullable=true)
     */
    protected $PlaqDate;

    /**
     * @var string
     *
     * @ORM\Column(name="PlaqComponentType", type="string", nullable=true)
     */
    protected $PlaqComponentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="PlaqId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $PlaqId;



    /**
     * Set PlaqBuildOrgId
     *
     * @param integer $plaqBuildOrgId
     * @return Buildersplaques
     */
    public function setPlaqBuildOrgId($plaqBuildOrgId)
    {
        $this->PlaqBuildOrgId = $plaqBuildOrgId;

        return $this;
    }

    /**
     * Get PlaqBuildOrgId
     *
     * @return integer 
     */
    public function getPlaqBuildOrgId()
    {
        return $this->PlaqBuildOrgId;
    }

    /**
     * Set PlaqClassId
     *
     * @param integer $plaqClassId
     * @return Buildersplaques
     */
    public function setPlaqClassId($plaqClassId)
    {
        $this->PlaqClassId = $plaqClassId;

        return $this;
    }

    /**
     * Get PlaqClassId
     *
     * @return integer 
     */
    public function getPlaqClassId()
    {
        return $this->PlaqClassId;
    }

    /**
     * Set PlaqNum
     *
     * @param string $plaqNum
     * @return Buildersplaques
     */
    public function setPlaqNum($plaqNum)
    {
        $this->PlaqNum = $plaqNum;

        return $this;
    }

    /**
     * Get PlaqNum
     *
     * @return string 
     */
    public function getPlaqNum()
    {
        return $this->PlaqNum;
    }

    /**
     * Set PlaqDate
     *
     * @param string $plaqDate
     * @return Buildersplaques
     */
    public function setPlaqDate($plaqDate)
    {
        $this->PlaqDate = $plaqDate;

        return $this;
    }

    /**
     * Get PlaqDate
     *
     * @return string 
     */
    public function getPlaqDate()
    {
        return $this->PlaqDate;
    }

    /**
     * Set PlaqComponentType
     *
     * @param string $plaqComponentType
     * @return Buildersplaques
     */
    public function setPlaqComponentType($plaqComponentType)
    {
        $this->PlaqComponentType = $plaqComponentType;

        return $this;
    }

    /**
     * Get PlaqComponentType
     *
     * @return string 
     */
    public function getPlaqComponentType()
    {
        return $this->PlaqComponentType;
    }

    /**
     * Get PlaqId
     *
     * @return integer 
     */
    public function getPlaqId()
    {
        return $this->PlaqId;
    }
}
