<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationwarnings
 *
 * @ORM\Table(name="ClassificationWarnings", indexes={@ORM\Index(name="warnidx", columns={"CfcnWarnAbout"})})
 * @ORM\Entity
 */
class Classificationwarnings
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnWarnEditDate", type="datetime", nullable=false)
     */
    protected $CfcnWarnEditDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnWarnEditedBy", type="integer", nullable=false)
     */
    protected $CfcnWarnEditedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnWarnContent", type="text", length=65535, nullable=false)
     */
    protected $CfcnWarnContent;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnWarnAbout", type="integer", nullable=false)
     */
    protected $CfcnWarnAbout;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnWarningId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CfcnWarningId;



    /**
     * Set CfcnWarnEditDate
     *
     * @param \DateTime $cfcnWarnEditDate
     * @return Classificationwarnings
     */
    public function setCfcnWarnEditDate($cfcnWarnEditDate)
    {
        $this->CfcnWarnEditDate = $cfcnWarnEditDate;

        return $this;
    }

    /**
     * Get CfcnWarnEditDate
     *
     * @return \DateTime 
     */
    public function getCfcnWarnEditDate()
    {
        return $this->CfcnWarnEditDate;
    }

    /**
     * Set CfcnWarnEditedBy
     *
     * @param integer $cfcnWarnEditedBy
     * @return Classificationwarnings
     */
    public function setCfcnWarnEditedBy($cfcnWarnEditedBy)
    {
        $this->CfcnWarnEditedBy = $cfcnWarnEditedBy;

        return $this;
    }

    /**
     * Get CfcnWarnEditedBy
     *
     * @return integer 
     */
    public function getCfcnWarnEditedBy()
    {
        return $this->CfcnWarnEditedBy;
    }

    /**
     * Set CfcnWarnContent
     *
     * @param string $cfcnWarnContent
     * @return Classificationwarnings
     */
    public function setCfcnWarnContent($cfcnWarnContent)
    {
        $this->CfcnWarnContent = $cfcnWarnContent;

        return $this;
    }

    /**
     * Get CfcnWarnContent
     *
     * @return string 
     */
    public function getCfcnWarnContent()
    {
        return $this->CfcnWarnContent;
    }

    /**
     * Set CfcnWarnAbout
     *
     * @param integer $cfcnWarnAbout
     * @return Classificationwarnings
     */
    public function setCfcnWarnAbout($cfcnWarnAbout)
    {
        $this->CfcnWarnAbout = $cfcnWarnAbout;

        return $this;
    }

    /**
     * Get CfcnWarnAbout
     *
     * @return integer 
     */
    public function getCfcnWarnAbout()
    {
        return $this->CfcnWarnAbout;
    }

    /**
     * Get CfcnWarningId
     *
     * @return integer 
     */
    public function getCfcnWarningId()
    {
        return $this->CfcnWarningId;
    }
}
