<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Countries
 *
 * @ORM\Table(name="Countries", uniqueConstraints={@ORM\UniqueConstraint(name="CtryCode", columns={"CtryCode"})})
 * @ORM\Entity
 */
class Countries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CtryOrigNationId", type="integer", nullable=true)
     */
    protected $CtryOrigNationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CtryOrigCountryId", type="integer", nullable=true)
     */
    protected $CtryOrigCountryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CtryAreaId", type="integer", nullable=false)
     */
    protected $CtryAreaId;

    /**
     * @var string
     *
     * @ORM\Column(name="CtryCode", type="string", length=255, nullable=true)
     */
    protected $CtryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="CtryName", type="string", length=255, nullable=true)
     */
    protected $CtryName;

    /**
     * @var string
     *
     * @ORM\Column(name="CtryEnglish", type="string", length=255, nullable=true)
     */
    protected $CtryEnglish;

    /**
     * @var string
     *
     * @ORM\Column(name="CtryDialCode", type="string", length=10, nullable=true)
     */
    protected $CtryDialCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CtryIsLegacy", type="boolean", nullable=false)
     */
    protected $CtryIsLegacy;

    /**
     * @var integer
     *
     * @ORM\Column(name="CtryId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CtryId;



    /**
     * Set CtryOrigNationId
     *
     * @param integer $ctryOrigNationId
     * @return Countries
     */
    public function setCtryOrigNationId($ctryOrigNationId)
    {
        $this->CtryOrigNationId = $ctryOrigNationId;

        return $this;
    }

    /**
     * Get CtryOrigNationId
     *
     * @return integer 
     */
    public function getCtryOrigNationId()
    {
        return $this->CtryOrigNationId;
    }

    /**
     * Set CtryOrigCountryId
     *
     * @param integer $ctryOrigCountryId
     * @return Countries
     */
    public function setCtryOrigCountryId($ctryOrigCountryId)
    {
        $this->CtryOrigCountryId = $ctryOrigCountryId;

        return $this;
    }

    /**
     * Get CtryOrigCountryId
     *
     * @return integer 
     */
    public function getCtryOrigCountryId()
    {
        return $this->CtryOrigCountryId;
    }

    /**
     * Set CtryAreaId
     *
     * @param integer $ctryAreaId
     * @return Countries
     */
    public function setCtryAreaId($ctryAreaId)
    {
        $this->CtryAreaId = $ctryAreaId;

        return $this;
    }

    /**
     * Get CtryAreaId
     *
     * @return integer 
     */
    public function getCtryAreaId()
    {
        return $this->CtryAreaId;
    }

    /**
     * Set CtryCode
     *
     * @param string $ctryCode
     * @return Countries
     */
    public function setCtryCode($ctryCode)
    {
        $this->CtryCode = $ctryCode;

        return $this;
    }

    /**
     * Get CtryCode
     *
     * @return string 
     */
    public function getCtryCode()
    {
        return $this->CtryCode;
    }

    /**
     * Set CtryName
     *
     * @param string $ctryName
     * @return Countries
     */
    public function setCtryName($ctryName)
    {
        $this->CtryName = $ctryName;

        return $this;
    }

    /**
     * Get CtryName
     *
     * @return string 
     */
    public function getCtryName()
    {
        return $this->CtryName;
    }

    /**
     * Set CtryEnglish
     *
     * @param string $ctryEnglish
     * @return Countries
     */
    public function setCtryEnglish($ctryEnglish)
    {
        $this->CtryEnglish = $ctryEnglish;

        return $this;
    }

    /**
     * Get CtryEnglish
     *
     * @return string 
     */
    public function getCtryEnglish()
    {
        return $this->CtryEnglish;
    }

    /**
     * Set CtryDialCode
     *
     * @param string $ctryDialCode
     * @return Countries
     */
    public function setCtryDialCode($ctryDialCode)
    {
        $this->CtryDialCode = $ctryDialCode;

        return $this;
    }

    /**
     * Get CtryDialCode
     *
     * @return string 
     */
    public function getCtryDialCode()
    {
        return $this->CtryDialCode;
    }

    /**
     * Set CtryIsLegacy
     *
     * @param boolean $ctryIsLegacy
     * @return Countries
     */
    public function setCtryIsLegacy($ctryIsLegacy)
    {
        $this->CtryIsLegacy = $ctryIsLegacy;

        return $this;
    }

    /**
     * Get CtryIsLegacy
     *
     * @return boolean 
     */
    public function getCtryIsLegacy()
    {
        return $this->CtryIsLegacy;
    }

    /**
     * Get CtryId
     *
     * @return integer 
     */
    public function getCtryId()
    {
        return $this->CtryId;
    }
}
