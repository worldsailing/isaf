<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organisationtypes
 *
 * @ORM\Table(name="OrganisationTypes")
 * @ORM\Entity
 */
class Organisationtypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="OtypOrigId", type="integer", nullable=true)
     */
    protected $OtypOrigId;

    /**
     * @var string
     *
     * @ORM\Column(name="OtypName", type="string", length=100, nullable=true)
     */
    protected $OtypName;

    /**
     * @var integer
     *
     * @ORM\Column(name="OtypId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $OtypId;



    /**
     * Set OtypOrigId
     *
     * @param integer $otypOrigId
     * @return Organisationtypes
     */
    public function setOtypOrigId($otypOrigId)
    {
        $this->OtypOrigId = $otypOrigId;

        return $this;
    }

    /**
     * Get OtypOrigId
     *
     * @return integer 
     */
    public function getOtypOrigId()
    {
        return $this->OtypOrigId;
    }

    /**
     * Set OtypName
     *
     * @param string $otypName
     * @return Organisationtypes
     */
    public function setOtypName($otypName)
    {
        $this->OtypName = $otypName;

        return $this;
    }

    /**
     * Get OtypName
     *
     * @return string 
     */
    public function getOtypName()
    {
        return $this->OtypName;
    }

    /**
     * Get OtypId
     *
     * @return integer 
     */
    public function getOtypId()
    {
        return $this->OtypId;
    }
}
