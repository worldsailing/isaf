<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boatstatus
 *
 * @ORM\Table(name="BoatStatus")
 * @ORM\Entity
 */
class Boatstatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="BstsSource", type="string", length=10, nullable=true)
     */
    protected $BstsSource;

    /**
     * @var string
     *
     * @ORM\Column(name="BstsSrcId", type="string", length=30, nullable=true)
     */
    protected $BstsSrcId;

    /**
     * @var string
     *
     * @ORM\Column(name="BstsDesc", type="string", length=255, nullable=true)
     */
    protected $BstsDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="BstsId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $BstsId;



    /**
     * Set BstsSource
     *
     * @param string $bstsSource
     * @return Boatstatus
     */
    public function setBstsSource($bstsSource)
    {
        $this->BstsSource = $bstsSource;

        return $this;
    }

    /**
     * Get BstsSource
     *
     * @return string 
     */
    public function getBstsSource()
    {
        return $this->BstsSource;
    }

    /**
     * Set BstsSrcId
     *
     * @param string $bstsSrcId
     * @return Boatstatus
     */
    public function setBstsSrcId($bstsSrcId)
    {
        $this->BstsSrcId = $bstsSrcId;

        return $this;
    }

    /**
     * Get BstsSrcId
     *
     * @return string 
     */
    public function getBstsSrcId()
    {
        return $this->BstsSrcId;
    }

    /**
     * Set BstsDesc
     *
     * @param string $bstsDesc
     * @return Boatstatus
     */
    public function setBstsDesc($bstsDesc)
    {
        $this->BstsDesc = $bstsDesc;

        return $this;
    }

    /**
     * Get BstsDesc
     *
     * @return string 
     */
    public function getBstsDesc()
    {
        return $this->BstsDesc;
    }

    /**
     * Get BstsId
     *
     * @return integer 
     */
    public function getBstsId()
    {
        return $this->BstsId;
    }
}
