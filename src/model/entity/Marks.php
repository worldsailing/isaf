<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Marks
 *
 * @ORM\Table(name="Marks", indexes={@ORM\Index(name="MarkRaceId", columns={"MarkRaceId"})})
 * @ORM\Entity
 */
class Marks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MarkRaceId", type="integer", nullable=false)
     */
    protected $MarkRaceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MarkOrd", type="integer", nullable=false)
     */
    protected $MarkOrd;

    /**
     * @var string
     *
     * @ORM\Column(name="MarkName", type="string", length=16, nullable=true)
     */
    protected $MarkName;

    /**
     * @var float
     *
     * @ORM\Column(name="MarkWindSpeed", type="float", precision=7, scale=2, nullable=true)
     */
    protected $MarkWindSpeed;

    /**
     * @var float
     *
     * @ORM\Column(name="MarkWindDirection", type="float", precision=7, scale=2, nullable=true)
     */
    protected $MarkWindDirection;

    /**
     * @var float
     *
     * @ORM\Column(name="MarkSeconds", type="float", precision=10, scale=0, nullable=true)
     */
    protected $MarkSeconds;

    /**
     * @var integer
     *
     * @ORM\Column(name="MarkId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $MarkId;



    /**
     * Set MarkRaceId
     *
     * @param integer $markRaceId
     * @return Marks
     */
    public function setMarkRaceId($markRaceId)
    {
        $this->MarkRaceId = $markRaceId;

        return $this;
    }

    /**
     * Get MarkRaceId
     *
     * @return integer 
     */
    public function getMarkRaceId()
    {
        return $this->MarkRaceId;
    }

    /**
     * Set MarkOrd
     *
     * @param integer $markOrd
     * @return Marks
     */
    public function setMarkOrd($markOrd)
    {
        $this->MarkOrd = $markOrd;

        return $this;
    }

    /**
     * Get MarkOrd
     *
     * @return integer 
     */
    public function getMarkOrd()
    {
        return $this->MarkOrd;
    }

    /**
     * Set MarkName
     *
     * @param string $markName
     * @return Marks
     */
    public function setMarkName($markName)
    {
        $this->MarkName = $markName;

        return $this;
    }

    /**
     * Get MarkName
     *
     * @return string 
     */
    public function getMarkName()
    {
        return $this->MarkName;
    }

    /**
     * Set MarkWindSpeed
     *
     * @param float $markWindSpeed
     * @return Marks
     */
    public function setMarkWindSpeed($markWindSpeed)
    {
        $this->MarkWindSpeed = $markWindSpeed;

        return $this;
    }

    /**
     * Get MarkWindSpeed
     *
     * @return float 
     */
    public function getMarkWindSpeed()
    {
        return $this->MarkWindSpeed;
    }

    /**
     * Set MarkWindDirection
     *
     * @param float $markWindDirection
     * @return Marks
     */
    public function setMarkWindDirection($markWindDirection)
    {
        $this->MarkWindDirection = $markWindDirection;

        return $this;
    }

    /**
     * Get MarkWindDirection
     *
     * @return float 
     */
    public function getMarkWindDirection()
    {
        return $this->MarkWindDirection;
    }

    /**
     * Set MarkSeconds
     *
     * @param float $markSeconds
     * @return Marks
     */
    public function setMarkSeconds($markSeconds)
    {
        $this->MarkSeconds = $markSeconds;

        return $this;
    }

    /**
     * Get MarkSeconds
     *
     * @return float 
     */
    public function getMarkSeconds()
    {
        return $this->MarkSeconds;
    }

    /**
     * Get MarkId
     *
     * @return integer 
     */
    public function getMarkId()
    {
        return $this->MarkId;
    }
}
