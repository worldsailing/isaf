<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rankingdata
 *
 * @ORM\Table(name="RankingData", indexes={@ORM\Index(name="idRankIdMemId", columns={"RankId", "RankMembId"})})
 * @ORM\Entity
 */
class Rankingdata
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RankId", type="integer", nullable=false)
     */
    protected $RankId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankMembId", type="integer", nullable=true)
     */
    protected $RankMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankNonMembId", type="integer", nullable=true)
     */
    protected $RankNonMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankPosition", type="integer", nullable=false)
     */
    protected $RankPosition;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RankEquals", type="boolean", nullable=false)
     */
    protected $RankEquals;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankEvents", type="integer", nullable=false)
     */
    protected $RankEvents;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankPoints", type="integer", nullable=false)
     */
    protected $RankPoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankPrevious", type="integer", nullable=true)
     */
    protected $RankPrevious;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankDataId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RankDataId;



    /**
     * Set RankId
     *
     * @param integer $rankId
     * @return Rankingdata
     */
    public function setRankId($rankId)
    {
        $this->RankId = $rankId;

        return $this;
    }

    /**
     * Get RankId
     *
     * @return integer 
     */
    public function getRankId()
    {
        return $this->RankId;
    }

    /**
     * Set RankMembId
     *
     * @param integer $rankMembId
     * @return Rankingdata
     */
    public function setRankMembId($rankMembId)
    {
        $this->RankMembId = $rankMembId;

        return $this;
    }

    /**
     * Get RankMembId
     *
     * @return integer 
     */
    public function getRankMembId()
    {
        return $this->RankMembId;
    }

    /**
     * Set RankNonMembId
     *
     * @param integer $rankNonMembId
     * @return Rankingdata
     */
    public function setRankNonMembId($rankNonMembId)
    {
        $this->RankNonMembId = $rankNonMembId;

        return $this;
    }

    /**
     * Get RankNonMembId
     *
     * @return integer 
     */
    public function getRankNonMembId()
    {
        return $this->RankNonMembId;
    }

    /**
     * Set RankPosition
     *
     * @param integer $rankPosition
     * @return Rankingdata
     */
    public function setRankPosition($rankPosition)
    {
        $this->RankPosition = $rankPosition;

        return $this;
    }

    /**
     * Get RankPosition
     *
     * @return integer 
     */
    public function getRankPosition()
    {
        return $this->RankPosition;
    }

    /**
     * Set RankEquals
     *
     * @param boolean $rankEquals
     * @return Rankingdata
     */
    public function setRankEquals($rankEquals)
    {
        $this->RankEquals = $rankEquals;

        return $this;
    }

    /**
     * Get RankEquals
     *
     * @return boolean 
     */
    public function getRankEquals()
    {
        return $this->RankEquals;
    }

    /**
     * Set RankEvents
     *
     * @param integer $rankEvents
     * @return Rankingdata
     */
    public function setRankEvents($rankEvents)
    {
        $this->RankEvents = $rankEvents;

        return $this;
    }

    /**
     * Get RankEvents
     *
     * @return integer 
     */
    public function getRankEvents()
    {
        return $this->RankEvents;
    }

    /**
     * Set RankPoints
     *
     * @param integer $rankPoints
     * @return Rankingdata
     */
    public function setRankPoints($rankPoints)
    {
        $this->RankPoints = $rankPoints;

        return $this;
    }

    /**
     * Get RankPoints
     *
     * @return integer 
     */
    public function getRankPoints()
    {
        return $this->RankPoints;
    }

    /**
     * Set RankPrevious
     *
     * @param integer $rankPrevious
     * @return Rankingdata
     */
    public function setRankPrevious($rankPrevious)
    {
        $this->RankPrevious = $rankPrevious;

        return $this;
    }

    /**
     * Get RankPrevious
     *
     * @return integer 
     */
    public function getRankPrevious()
    {
        return $this->RankPrevious;
    }

    /**
     * Get RankDataId
     *
     * @return integer 
     */
    public function getRankDataId()
    {
        return $this->RankDataId;
    }
}
