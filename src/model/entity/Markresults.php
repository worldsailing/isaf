<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Markresults
 *
 * @ORM\Table(name="MarkResults")
 * @ORM\Entity
 */
class Markresults
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MresId", type="integer", nullable=false)
     */
    protected $MresId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MresPos", type="integer", nullable=false)
     */
    protected $MresPos;

    /**
     * @var float
     *
     * @ORM\Column(name="MresSeconds", type="float", precision=10, scale=0, nullable=false)
     */
    protected $MresSeconds;

    /**
     * @var integer
     *
     * @ORM\Column(name="MresMarkId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $MresMarkId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MresBoatId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $MresBoatId;



    /**
     * Set MresId
     *
     * @param integer $mresId
     * @return Markresults
     */
    public function setMresId($mresId)
    {
        $this->MresId = $mresId;

        return $this;
    }

    /**
     * Get MresId
     *
     * @return integer 
     */
    public function getMresId()
    {
        return $this->MresId;
    }

    /**
     * Set MresPos
     *
     * @param integer $mresPos
     * @return Markresults
     */
    public function setMresPos($mresPos)
    {
        $this->MresPos = $mresPos;

        return $this;
    }

    /**
     * Get MresPos
     *
     * @return integer 
     */
    public function getMresPos()
    {
        return $this->MresPos;
    }

    /**
     * Set MresSeconds
     *
     * @param float $mresSeconds
     * @return Markresults
     */
    public function setMresSeconds($mresSeconds)
    {
        $this->MresSeconds = $mresSeconds;

        return $this;
    }

    /**
     * Get MresSeconds
     *
     * @return float 
     */
    public function getMresSeconds()
    {
        return $this->MresSeconds;
    }

    /**
     * Set MresMarkId
     *
     * @param integer $mresMarkId
     * @return Markresults
     */
    public function setMresMarkId($mresMarkId)
    {
        $this->MresMarkId = $mresMarkId;

        return $this;
    }

    /**
     * Get MresMarkId
     *
     * @return integer 
     */
    public function getMresMarkId()
    {
        return $this->MresMarkId;
    }

    /**
     * Set MresBoatId
     *
     * @param integer $mresBoatId
     * @return Markresults
     */
    public function setMresBoatId($mresBoatId)
    {
        $this->MresBoatId = $mresBoatId;

        return $this;
    }

    /**
     * Get MresBoatId
     *
     * @return integer 
     */
    public function getMresBoatId()
    {
        return $this->MresBoatId;
    }
}
