<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rolepermissions
 *
 * @ORM\Table(name="rolepermissions")
 * @ORM\Entity
 */
class Rolepermissions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RprmRoleId", type="integer", nullable=false)
     */
    protected $RprmRoleId;

    /**
     * @var string
     *
     * @ORM\Column(name="RprmResource", type="string", length=50, nullable=true)
     */
    protected $RprmResource;

    /**
     * @var string
     *
     * @ORM\Column(name="RprmPrivilege", type="string", length=50, nullable=true)
     */
    protected $RprmPrivilege;

    /**
     * @var string
     *
     * @ORM\Column(name="RprmAccess", type="string", nullable=false)
     */
    protected $RprmAccess;

    /**
     * @var integer
     *
     * @ORM\Column(name="RprmId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RprmId;



    /**
     * Set RprmRoleId
     *
     * @param integer $rprmRoleId
     * @return Rolepermissions
     */
    public function setRprmRoleId($rprmRoleId)
    {
        $this->RprmRoleId = $rprmRoleId;

        return $this;
    }

    /**
     * Get RprmRoleId
     *
     * @return integer 
     */
    public function getRprmRoleId()
    {
        return $this->RprmRoleId;
    }

    /**
     * Set RprmResource
     *
     * @param string $rprmResource
     * @return Rolepermissions
     */
    public function setRprmResource($rprmResource)
    {
        $this->RprmResource = $rprmResource;

        return $this;
    }

    /**
     * Get RprmResource
     *
     * @return string 
     */
    public function getRprmResource()
    {
        return $this->RprmResource;
    }

    /**
     * Set RprmPrivilege
     *
     * @param string $rprmPrivilege
     * @return Rolepermissions
     */
    public function setRprmPrivilege($rprmPrivilege)
    {
        $this->RprmPrivilege = $rprmPrivilege;

        return $this;
    }

    /**
     * Get RprmPrivilege
     *
     * @return string 
     */
    public function getRprmPrivilege()
    {
        return $this->RprmPrivilege;
    }

    /**
     * Set RprmAccess
     *
     * @param string $rprmAccess
     * @return Rolepermissions
     */
    public function setRprmAccess($rprmAccess)
    {
        $this->RprmAccess = $rprmAccess;

        return $this;
    }

    /**
     * Get RprmAccess
     *
     * @return string 
     */
    public function getRprmAccess()
    {
        return $this->RprmAccess;
    }

    /**
     * Get RprmId
     *
     * @return integer 
     */
    public function getRprmId()
    {
        return $this->RprmId;
    }
}
