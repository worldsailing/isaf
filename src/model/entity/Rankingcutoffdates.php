<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rankingcutoffdates
 *
 * @ORM\Table(name="RankingCutoffDates")
 * @ORM\Entity
 */
class Rankingcutoffdates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CutDisId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CutDisId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CutTypeId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CutTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CutClassId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CutClassId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CutDate", type="date")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CutDate;



    /**
     * Set CutDisId
     *
     * @param integer $cutDisId
     * @return Rankingcutoffdates
     */
    public function setCutDisId($cutDisId)
    {
        $this->CutDisId = $cutDisId;

        return $this;
    }

    /**
     * Get CutDisId
     *
     * @return integer 
     */
    public function getCutDisId()
    {
        return $this->CutDisId;
    }

    /**
     * Set CutTypeId
     *
     * @param integer $cutTypeId
     * @return Rankingcutoffdates
     */
    public function setCutTypeId($cutTypeId)
    {
        $this->CutTypeId = $cutTypeId;

        return $this;
    }

    /**
     * Get CutTypeId
     *
     * @return integer 
     */
    public function getCutTypeId()
    {
        return $this->CutTypeId;
    }

    /**
     * Set CutClassId
     *
     * @param integer $cutClassId
     * @return Rankingcutoffdates
     */
    public function setCutClassId($cutClassId)
    {
        $this->CutClassId = $cutClassId;

        return $this;
    }

    /**
     * Get CutClassId
     *
     * @return integer 
     */
    public function getCutClassId()
    {
        return $this->CutClassId;
    }

    /**
     * Set CutDate
     *
     * @param \DateTime $cutDate
     * @return Rankingcutoffdates
     */
    public function setCutDate($cutDate)
    {
        $this->CutDate = $cutDate;

        return $this;
    }

    /**
     * Get CutDate
     *
     * @return \DateTime 
     */
    public function getCutDate()
    {
        return $this->CutDate;
    }
}
