<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Matchracepoints
 *
 * @ORM\Table(name="MatchRacePoints")
 * @ORM\Entity
 */
class Matchracepoints
{
    /**
     * @var integer
     *
     * @ORM\Column(name="PtsPoints", type="integer", nullable=false)
     */
    protected $PtsPoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="PtsCompetitors", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $PtsCompetitors;

    /**
     * @var integer
     *
     * @ORM\Column(name="PtsPosition", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $PtsPosition;



    /**
     * Set PtsPoints
     *
     * @param integer $ptsPoints
     * @return Matchracepoints
     */
    public function setPtsPoints($ptsPoints)
    {
        $this->PtsPoints = $ptsPoints;

        return $this;
    }

    /**
     * Get PtsPoints
     *
     * @return integer 
     */
    public function getPtsPoints()
    {
        return $this->PtsPoints;
    }

    /**
     * Set PtsCompetitors
     *
     * @param integer $ptsCompetitors
     * @return Matchracepoints
     */
    public function setPtsCompetitors($ptsCompetitors)
    {
        $this->PtsCompetitors = $ptsCompetitors;

        return $this;
    }

    /**
     * Get PtsCompetitors
     *
     * @return integer 
     */
    public function getPtsCompetitors()
    {
        return $this->PtsCompetitors;
    }

    /**
     * Set PtsPosition
     *
     * @param integer $ptsPosition
     * @return Matchracepoints
     */
    public function setPtsPosition($ptsPosition)
    {
        $this->PtsPosition = $ptsPosition;

        return $this;
    }

    /**
     * Get PtsPosition
     *
     * @return integer 
     */
    public function getPtsPosition()
    {
        return $this->PtsPosition;
    }
}
