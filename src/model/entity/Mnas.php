<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mnas
 *
 * @ORM\Table(name="Mnas")
 * @ORM\Entity
 */
class Mnas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MnaOrigId", type="integer", nullable=true)
     */
    protected $MnaOrigId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MnaCtryId", type="integer", nullable=false)
     */
    protected $MnaCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MnaOrgId", type="integer", nullable=true)
     */
    protected $MnaOrgId;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaName", type="string", length=256, nullable=true)
     */
    protected $MnaName;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaPosition1", type="string", length=255, nullable=true)
     */
    protected $MnaPosition1;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaTitle1", type="string", length=255, nullable=true)
     */
    protected $MnaTitle1;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaFirstName1", type="string", length=255, nullable=true)
     */
    protected $MnaFirstName1;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaSurname1", type="string", length=255, nullable=true)
     */
    protected $MnaSurname1;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaPosition2", type="string", length=255, nullable=true)
     */
    protected $MnaPosition2;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaTitle2", type="string", length=255, nullable=true)
     */
    protected $MnaTitle2;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaFirstName2", type="string", length=255, nullable=true)
     */
    protected $MnaFirstName2;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaSurname2", type="string", length=255, nullable=true)
     */
    protected $MnaSurname2;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaAddress", type="text", length=65535, nullable=true)
     */
    protected $MnaAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaPhone1", type="string", length=100, nullable=true)
     */
    protected $MnaPhone1;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaPhone2", type="string", length=100, nullable=true)
     */
    protected $MnaPhone2;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaFax", type="string", length=100, nullable=true)
     */
    protected $MnaFax;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaEmail", type="string", length=255, nullable=true)
     */
    protected $MnaEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaWebsite", type="string", length=255, nullable=true)
     */
    protected $MnaWebsite;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaNotes", type="text", length=65535, nullable=true)
     */
    protected $MnaNotes;

    /**
     * @var string
     *
     * @ORM\Column(name="MnaType", type="string", nullable=false)
     */
    protected $MnaType;

    /**
     * @var integer
     *
     * @ORM\Column(name="MnaId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $MnaId;



    /**
     * Set MnaOrigId
     *
     * @param integer $mnaOrigId
     * @return Mnas
     */
    public function setMnaOrigId($mnaOrigId)
    {
        $this->MnaOrigId = $mnaOrigId;

        return $this;
    }

    /**
     * Get MnaOrigId
     *
     * @return integer 
     */
    public function getMnaOrigId()
    {
        return $this->MnaOrigId;
    }

    /**
     * Set MnaCtryId
     *
     * @param integer $mnaCtryId
     * @return Mnas
     */
    public function setMnaCtryId($mnaCtryId)
    {
        $this->MnaCtryId = $mnaCtryId;

        return $this;
    }

    /**
     * Get MnaCtryId
     *
     * @return integer 
     */
    public function getMnaCtryId()
    {
        return $this->MnaCtryId;
    }

    /**
     * Set MnaOrgId
     *
     * @param integer $mnaOrgId
     * @return Mnas
     */
    public function setMnaOrgId($mnaOrgId)
    {
        $this->MnaOrgId = $mnaOrgId;

        return $this;
    }

    /**
     * Get MnaOrgId
     *
     * @return integer 
     */
    public function getMnaOrgId()
    {
        return $this->MnaOrgId;
    }

    /**
     * Set MnaName
     *
     * @param string $mnaName
     * @return Mnas
     */
    public function setMnaName($mnaName)
    {
        $this->MnaName = $mnaName;

        return $this;
    }

    /**
     * Get MnaName
     *
     * @return string 
     */
    public function getMnaName()
    {
        return $this->MnaName;
    }

    /**
     * Set MnaPosition1
     *
     * @param string $mnaPosition1
     * @return Mnas
     */
    public function setMnaPosition1($mnaPosition1)
    {
        $this->MnaPosition1 = $mnaPosition1;

        return $this;
    }

    /**
     * Get MnaPosition1
     *
     * @return string 
     */
    public function getMnaPosition1()
    {
        return $this->MnaPosition1;
    }

    /**
     * Set MnaTitle1
     *
     * @param string $mnaTitle1
     * @return Mnas
     */
    public function setMnaTitle1($mnaTitle1)
    {
        $this->MnaTitle1 = $mnaTitle1;

        return $this;
    }

    /**
     * Get MnaTitle1
     *
     * @return string 
     */
    public function getMnaTitle1()
    {
        return $this->MnaTitle1;
    }

    /**
     * Set MnaFirstName1
     *
     * @param string $mnaFirstName1
     * @return Mnas
     */
    public function setMnaFirstName1($mnaFirstName1)
    {
        $this->MnaFirstName1 = $mnaFirstName1;

        return $this;
    }

    /**
     * Get MnaFirstName1
     *
     * @return string 
     */
    public function getMnaFirstName1()
    {
        return $this->MnaFirstName1;
    }

    /**
     * Set MnaSurname1
     *
     * @param string $mnaSurname1
     * @return Mnas
     */
    public function setMnaSurname1($mnaSurname1)
    {
        $this->MnaSurname1 = $mnaSurname1;

        return $this;
    }

    /**
     * Get MnaSurname1
     *
     * @return string 
     */
    public function getMnaSurname1()
    {
        return $this->MnaSurname1;
    }

    /**
     * Set MnaPosition2
     *
     * @param string $mnaPosition2
     * @return Mnas
     */
    public function setMnaPosition2($mnaPosition2)
    {
        $this->MnaPosition2 = $mnaPosition2;

        return $this;
    }

    /**
     * Get MnaPosition2
     *
     * @return string 
     */
    public function getMnaPosition2()
    {
        return $this->MnaPosition2;
    }

    /**
     * Set MnaTitle2
     *
     * @param string $mnaTitle2
     * @return Mnas
     */
    public function setMnaTitle2($mnaTitle2)
    {
        $this->MnaTitle2 = $mnaTitle2;

        return $this;
    }

    /**
     * Get MnaTitle2
     *
     * @return string 
     */
    public function getMnaTitle2()
    {
        return $this->MnaTitle2;
    }

    /**
     * Set MnaFirstName2
     *
     * @param string $mnaFirstName2
     * @return Mnas
     */
    public function setMnaFirstName2($mnaFirstName2)
    {
        $this->MnaFirstName2 = $mnaFirstName2;

        return $this;
    }

    /**
     * Get MnaFirstName2
     *
     * @return string 
     */
    public function getMnaFirstName2()
    {
        return $this->MnaFirstName2;
    }

    /**
     * Set MnaSurname2
     *
     * @param string $mnaSurname2
     * @return Mnas
     */
    public function setMnaSurname2($mnaSurname2)
    {
        $this->MnaSurname2 = $mnaSurname2;

        return $this;
    }

    /**
     * Get MnaSurname2
     *
     * @return string 
     */
    public function getMnaSurname2()
    {
        return $this->MnaSurname2;
    }

    /**
     * Set MnaAddress
     *
     * @param string $mnaAddress
     * @return Mnas
     */
    public function setMnaAddress($mnaAddress)
    {
        $this->MnaAddress = $mnaAddress;

        return $this;
    }

    /**
     * Get MnaAddress
     *
     * @return string 
     */
    public function getMnaAddress()
    {
        return $this->MnaAddress;
    }

    /**
     * Set MnaPhone1
     *
     * @param string $mnaPhone1
     * @return Mnas
     */
    public function setMnaPhone1($mnaPhone1)
    {
        $this->MnaPhone1 = $mnaPhone1;

        return $this;
    }

    /**
     * Get MnaPhone1
     *
     * @return string 
     */
    public function getMnaPhone1()
    {
        return $this->MnaPhone1;
    }

    /**
     * Set MnaPhone2
     *
     * @param string $mnaPhone2
     * @return Mnas
     */
    public function setMnaPhone2($mnaPhone2)
    {
        $this->MnaPhone2 = $mnaPhone2;

        return $this;
    }

    /**
     * Get MnaPhone2
     *
     * @return string 
     */
    public function getMnaPhone2()
    {
        return $this->MnaPhone2;
    }

    /**
     * Set MnaFax
     *
     * @param string $mnaFax
     * @return Mnas
     */
    public function setMnaFax($mnaFax)
    {
        $this->MnaFax = $mnaFax;

        return $this;
    }

    /**
     * Get MnaFax
     *
     * @return string 
     */
    public function getMnaFax()
    {
        return $this->MnaFax;
    }

    /**
     * Set MnaEmail
     *
     * @param string $mnaEmail
     * @return Mnas
     */
    public function setMnaEmail($mnaEmail)
    {
        $this->MnaEmail = $mnaEmail;

        return $this;
    }

    /**
     * Get MnaEmail
     *
     * @return string 
     */
    public function getMnaEmail()
    {
        return $this->MnaEmail;
    }

    /**
     * Set MnaWebsite
     *
     * @param string $mnaWebsite
     * @return Mnas
     */
    public function setMnaWebsite($mnaWebsite)
    {
        $this->MnaWebsite = $mnaWebsite;

        return $this;
    }

    /**
     * Get MnaWebsite
     *
     * @return string 
     */
    public function getMnaWebsite()
    {
        return $this->MnaWebsite;
    }

    /**
     * Set MnaNotes
     *
     * @param string $mnaNotes
     * @return Mnas
     */
    public function setMnaNotes($mnaNotes)
    {
        $this->MnaNotes = $mnaNotes;

        return $this;
    }

    /**
     * Get MnaNotes
     *
     * @return string 
     */
    public function getMnaNotes()
    {
        return $this->MnaNotes;
    }

    /**
     * Set MnaType
     *
     * @param string $mnaType
     * @return Mnas
     */
    public function setMnaType($mnaType)
    {
        $this->MnaType = $mnaType;

        return $this;
    }

    /**
     * Get MnaType
     *
     * @return string 
     */
    public function getMnaType()
    {
        return $this->MnaType;
    }

    /**
     * Get MnaId
     *
     * @return integer 
     */
    public function getMnaId()
    {
        return $this->MnaId;
    }
}
