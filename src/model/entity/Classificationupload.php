<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationupload
 *
 * @ORM\Table(name="ClassificationUpload", indexes={@ORM\Index(name="fk_upld_action_idx", columns={"CfcnUpldActionId"}), @ORM\Index(name="fk_upld_memb_idx", columns={"CfcnUpldMembById"})})
 * @ORM\Entity
 */
class Classificationupload
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfcnUpldName", type="string", length=500, nullable=false)
     */
    protected $CfcnUpldName;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnUpldContent", type="blob", nullable=false)
     */
    protected $CfcnUpldContent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnUpldDate", type="datetime", nullable=false)
     */
    protected $CfcnUpldDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnUpldFileSize", type="integer", nullable=false)
     */
    protected $CfcnUpldFileSize;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CfcnUpldDeleted", type="boolean", nullable=false)
     */
    protected $CfcnUpldDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnUpldId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CfcnUpldId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Memberbiogs
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Memberbiogs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnUpldMembById", referencedColumnName="BiogMembId")
     * })
     */
    protected $cfcnUpldMembById;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnUpldActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnUpldActionId;



    /**
     * Set CfcnUpldName
     *
     * @param string $cfcnUpldName
     * @return Classificationupload
     */
    public function setCfcnUpldName($cfcnUpldName)
    {
        $this->CfcnUpldName = $cfcnUpldName;

        return $this;
    }

    /**
     * Get CfcnUpldName
     *
     * @return string 
     */
    public function getCfcnUpldName()
    {
        return $this->CfcnUpldName;
    }

    /**
     * Set CfcnUpldContent
     *
     * @param string $cfcnUpldContent
     * @return Classificationupload
     */
    public function setCfcnUpldContent($cfcnUpldContent)
    {
        $this->CfcnUpldContent = $cfcnUpldContent;

        return $this;
    }

    /**
     * Get CfcnUpldContent
     *
     * @return string 
     */
    public function getCfcnUpldContent()
    {
        return $this->CfcnUpldContent;
    }

    /**
     * Set CfcnUpldDate
     *
     * @param \DateTime $cfcnUpldDate
     * @return Classificationupload
     */
    public function setCfcnUpldDate($cfcnUpldDate)
    {
        $this->CfcnUpldDate = $cfcnUpldDate;

        return $this;
    }

    /**
     * Get CfcnUpldDate
     *
     * @return \DateTime 
     */
    public function getCfcnUpldDate()
    {
        return $this->CfcnUpldDate;
    }

    /**
     * Set CfcnUpldFileSize
     *
     * @param integer $cfcnUpldFileSize
     * @return Classificationupload
     */
    public function setCfcnUpldFileSize($cfcnUpldFileSize)
    {
        $this->CfcnUpldFileSize = $cfcnUpldFileSize;

        return $this;
    }

    /**
     * Get CfcnUpldFileSize
     *
     * @return integer 
     */
    public function getCfcnUpldFileSize()
    {
        return $this->CfcnUpldFileSize;
    }

    /**
     * Set CfcnUpldDeleted
     *
     * @param boolean $cfcnUpldDeleted
     * @return Classificationupload
     */
    public function setCfcnUpldDeleted($cfcnUpldDeleted)
    {
        $this->CfcnUpldDeleted = $cfcnUpldDeleted;

        return $this;
    }

    /**
     * Get CfcnUpldDeleted
     *
     * @return boolean 
     */
    public function getCfcnUpldDeleted()
    {
        return $this->CfcnUpldDeleted;
    }

    /**
     * Get CfcnUpldId
     *
     * @return integer 
     */
    public function getCfcnUpldId()
    {
        return $this->CfcnUpldId;
    }

    /**
     * Set cfcnUpldMembById
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $cfcnUpldMembById
     * @return Classificationupload
     */
    public function setCfcnUpldMembById(\worldsailing\Isaf\model\Entity\Memberbiogs $cfcnUpldMembById = null)
    {
        $this->cfcnUpldMembById = $cfcnUpldMembById;

        return $this;
    }

    /**
     * Get cfcnUpldMembById
     *
     * @return \worldsailing\Isaf\model\Entity\Memberbiogs
     */
    public function getCfcnUpldMembById()
    {
        return $this->cfcnUpldMembById;
    }

    /**
     * Set cfcnUpldActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnUpldActionId
     * @return Classificationupload
     */
    public function setCfcnUpldActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnUpldActionId = null)
    {
        $this->cfcnUpldActionId = $cfcnUpldActionId;

        return $this;
    }

    /**
     * Get cfcnUpldActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnUpldActionId()
    {
        return $this->cfcnUpldActionId;
    }
}
