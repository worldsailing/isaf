<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Adminpermalloc
 *
 * @ORM\Table(name="AdminPermAlloc")
 * @ORM\Entity
 */
class Adminpermalloc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="PermAllocId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $PermAllocId;

    /**
     * @var string
     *
     * @ORM\Column(name="PermAllocLogin", type="string", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $PermAllocLogin;



    /**
     * Set PermAllocId
     *
     * @param integer $permAllocId
     * @return Adminpermalloc
     */
    public function setPermAllocId($permAllocId)
    {
        $this->PermAllocId = $permAllocId;

        return $this;
    }

    /**
     * Get PermAllocId
     *
     * @return integer 
     */
    public function getPermAllocId()
    {
        return $this->PermAllocId;
    }

    /**
     * Set PermAllocLogin
     *
     * @param string $permAllocLogin
     * @return Adminpermalloc
     */
    public function setPermAllocLogin($permAllocLogin)
    {
        $this->PermAllocLogin = $permAllocLogin;

        return $this;
    }

    /**
     * Get PermAllocLogin
     *
     * @return string 
     */
    public function getPermAllocLogin()
    {
        return $this->PermAllocLogin;
    }
}
