<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Xrrsftwrversion
 *
 * @ORM\Table(name="XRRSftwrVersion", indexes={@ORM\Index(name="IDX_A22C5AEDF6495415", columns={"SftwrVerSftwrId"})})
 * @ORM\Entity
 */
class Xrrsftwrversion
{
    /**
     * @var string
     *
     * @ORM\Column(name="SftwrVerNum", type="string", length=10)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $SftwrVerNum;

    /**
     * @var \worldsailing\Isaf\model\Entity\Xrrsoftware
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="worldsailing\Isaf\model\Entity\Xrrsoftware")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SftwrVerSftwrId", referencedColumnName="SftwrId")
     * })
     */
    protected $sftwrVerSftwrId;



    /**
     * Set SftwrVerNum
     *
     * @param string $sftwrVerNum
     * @return Xrrsftwrversion
     */
    public function setSftwrVerNum($sftwrVerNum)
    {
        $this->SftwrVerNum = $sftwrVerNum;

        return $this;
    }

    /**
     * Get SftwrVerNum
     *
     * @return string 
     */
    public function getSftwrVerNum()
    {
        return $this->SftwrVerNum;
    }

    /**
     * Set sftwrVerSftwrId
     *
     * @param \worldsailing\Isaf\model\Entity\Xrrsoftware $sftwrVerSftwrId
     * @return Xrrsftwrversion
     */
    public function setSftwrVerSftwrId(\worldsailing\Isaf\model\Entity\Xrrsoftware $sftwrVerSftwrId)
    {
        $this->sftwrVerSftwrId = $sftwrVerSftwrId;

        return $this;
    }

    /**
     * Get sftwrVerSftwrId
     *
     * @return \worldsailing\Isaf\model\Entity\Xrrsoftware
     */
    public function getSftwrVerSftwrId()
    {
        return $this->sftwrVerSftwrId;
    }
}
