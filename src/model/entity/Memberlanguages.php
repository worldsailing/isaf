<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Memberlanguages
 *
 * @ORM\Table(name="MemberLanguages")
 * @ORM\Entity
 */
class Memberlanguages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MlngMembId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $MlngMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MlngLangId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $MlngLangId;



    /**
     * Set MlngMembId
     *
     * @param integer $mlngMembId
     * @return Memberlanguages
     */
    public function setMlngMembId($mlngMembId)
    {
        $this->MlngMembId = $mlngMembId;

        return $this;
    }

    /**
     * Get MlngMembId
     *
     * @return integer 
     */
    public function getMlngMembId()
    {
        return $this->MlngMembId;
    }

    /**
     * Set MlngLangId
     *
     * @param integer $mlngLangId
     * @return Memberlanguages
     */
    public function setMlngLangId($mlngLangId)
    {
        $this->MlngLangId = $mlngLangId;

        return $this;
    }

    /**
     * Get MlngLangId
     *
     * @return integer 
     */
    public function getMlngLangId()
    {
        return $this->MlngLangId;
    }
}
