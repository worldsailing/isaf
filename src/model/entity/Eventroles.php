<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eventroles
 *
 * @ORM\Table(name="EventRoles")
 * @ORM\Entity
 */
class Eventroles
{
    /**
     * @var string
     *
     * @ORM\Column(name="RoleName", type="string", length=50, nullable=false)
     */
    protected $RoleName;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleRegistration", type="string", nullable=false)
     */
    protected $RoleRegistration;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RoleGenderRestricted", type="boolean", nullable=false)
     */
    protected $RoleGenderRestricted;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleOfficialStatus", type="text", length=65535, nullable=false)
     */
    protected $RoleOfficialStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleInvitation", type="text", length=65535, nullable=false)
     */
    protected $RoleInvitation;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleAdminInvitation", type="text", length=65535, nullable=false)
     */
    protected $RoleAdminInvitation;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleOrgInvitation", type="text", length=65535, nullable=false)
     */
    protected $RoleOrgInvitation;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleAdminYouthInvitation", type="text", length=65535, nullable=false)
     */
    protected $RoleAdminYouthInvitation;

    /**
     * @var integer
     *
     * @ORM\Column(name="RoleId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RoleId;



    /**
     * Set RoleName
     *
     * @param string $roleName
     * @return Eventroles
     */
    public function setRoleName($roleName)
    {
        $this->RoleName = $roleName;

        return $this;
    }

    /**
     * Get RoleName
     *
     * @return string 
     */
    public function getRoleName()
    {
        return $this->RoleName;
    }

    /**
     * Set RoleRegistration
     *
     * @param string $roleRegistration
     * @return Eventroles
     */
    public function setRoleRegistration($roleRegistration)
    {
        $this->RoleRegistration = $roleRegistration;

        return $this;
    }

    /**
     * Get RoleRegistration
     *
     * @return string 
     */
    public function getRoleRegistration()
    {
        return $this->RoleRegistration;
    }

    /**
     * Set RoleGenderRestricted
     *
     * @param boolean $roleGenderRestricted
     * @return Eventroles
     */
    public function setRoleGenderRestricted($roleGenderRestricted)
    {
        $this->RoleGenderRestricted = $roleGenderRestricted;

        return $this;
    }

    /**
     * Get RoleGenderRestricted
     *
     * @return boolean 
     */
    public function getRoleGenderRestricted()
    {
        return $this->RoleGenderRestricted;
    }

    /**
     * Set RoleOfficialStatus
     *
     * @param string $roleOfficialStatus
     * @return Eventroles
     */
    public function setRoleOfficialStatus($roleOfficialStatus)
    {
        $this->RoleOfficialStatus = $roleOfficialStatus;

        return $this;
    }

    /**
     * Get RoleOfficialStatus
     *
     * @return string 
     */
    public function getRoleOfficialStatus()
    {
        return $this->RoleOfficialStatus;
    }

    /**
     * Set RoleInvitation
     *
     * @param string $roleInvitation
     * @return Eventroles
     */
    public function setRoleInvitation($roleInvitation)
    {
        $this->RoleInvitation = $roleInvitation;

        return $this;
    }

    /**
     * Get RoleInvitation
     *
     * @return string 
     */
    public function getRoleInvitation()
    {
        return $this->RoleInvitation;
    }

    /**
     * Set RoleAdminInvitation
     *
     * @param string $roleAdminInvitation
     * @return Eventroles
     */
    public function setRoleAdminInvitation($roleAdminInvitation)
    {
        $this->RoleAdminInvitation = $roleAdminInvitation;

        return $this;
    }

    /**
     * Get RoleAdminInvitation
     *
     * @return string 
     */
    public function getRoleAdminInvitation()
    {
        return $this->RoleAdminInvitation;
    }

    /**
     * Set RoleOrgInvitation
     *
     * @param string $roleOrgInvitation
     * @return Eventroles
     */
    public function setRoleOrgInvitation($roleOrgInvitation)
    {
        $this->RoleOrgInvitation = $roleOrgInvitation;

        return $this;
    }

    /**
     * Get RoleOrgInvitation
     *
     * @return string 
     */
    public function getRoleOrgInvitation()
    {
        return $this->RoleOrgInvitation;
    }

    /**
     * Set RoleAdminYouthInvitation
     *
     * @param string $roleAdminYouthInvitation
     * @return Eventroles
     */
    public function setRoleAdminYouthInvitation($roleAdminYouthInvitation)
    {
        $this->RoleAdminYouthInvitation = $roleAdminYouthInvitation;

        return $this;
    }

    /**
     * Get RoleAdminYouthInvitation
     *
     * @return string 
     */
    public function getRoleAdminYouthInvitation()
    {
        return $this->RoleAdminYouthInvitation;
    }

    /**
     * Get RoleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->RoleId;
    }
}
