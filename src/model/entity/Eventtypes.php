<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eventtypes
 *
 * @ORM\Table(name="EventTypes")
 * @ORM\Entity
 */
class Eventtypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="TypeId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $TypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="TypeName", type="string", length=20)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $TypeName;



    /**
     * Set TypeId
     *
     * @param integer $typeId
     * @return Eventtypes
     */
    public function setTypeId($typeId)
    {
        $this->TypeId = $typeId;

        return $this;
    }

    /**
     * Get TypeId
     *
     * @return integer 
     */
    public function getTypeId()
    {
        return $this->TypeId;
    }

    /**
     * Set TypeName
     *
     * @param string $typeName
     * @return Eventtypes
     */
    public function setTypeName($typeName)
    {
        $this->TypeName = $typeName;

        return $this;
    }

    /**
     * Get TypeName
     *
     * @return string 
     */
    public function getTypeName()
    {
        return $this->TypeName;
    }
}
