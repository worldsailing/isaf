<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classes
 *
 * @ORM\Table(name="Classes")
 * @ORM\Entity
 */
class Classes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ClassOrigId", type="integer", nullable=true)
     */
    protected $ClassOrigId;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassCode", type="string", length=255, nullable=true)
     */
    protected $ClassCode;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassName", type="string", length=255, nullable=true)
     */
    protected $ClassName;

    /**
     * @var integer
     *
     * @ORM\Column(name="ClassStatId", type="integer", nullable=true)
     */
    protected $ClassStatId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ClassTypeId", type="integer", nullable=true)
     */
    protected $ClassTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassAssociation", type="string", length=255, nullable=true)
     */
    protected $ClassAssociation;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassPosition1", type="string", length=255, nullable=true)
     */
    protected $ClassPosition1;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassTitle1", type="string", length=255, nullable=true)
     */
    protected $ClassTitle1;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassFirstName1", type="string", length=255, nullable=true)
     */
    protected $ClassFirstName1;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassSurname1", type="string", length=255, nullable=true)
     */
    protected $ClassSurname1;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassPosition2", type="string", length=255, nullable=true)
     */
    protected $ClassPosition2;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassTitle2", type="string", length=255, nullable=true)
     */
    protected $ClassTitle2;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassFirstName2", type="string", length=255, nullable=true)
     */
    protected $ClassFirstName2;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassSurname2", type="string", length=255, nullable=true)
     */
    protected $ClassSurname2;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassAddress", type="text", length=65535, nullable=true)
     */
    protected $ClassAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="ClassCtryId", type="integer", nullable=true)
     */
    protected $ClassCtryId;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassPhone1", type="string", length=20, nullable=true)
     */
    protected $ClassPhone1;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassPhone2", type="string", length=20, nullable=true)
     */
    protected $ClassPhone2;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassFax", type="string", length=20, nullable=true)
     */
    protected $ClassFax;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassEmail", type="string", length=255, nullable=true)
     */
    protected $ClassEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassWebsite", type="string", length=255, nullable=true)
     */
    protected $ClassWebsite;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassNotes", type="text", length=65535, nullable=true)
     */
    protected $ClassNotes;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassHullMat", type="string", length=30, nullable=true)
     */
    protected $ClassHullMat;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassNumCrew", type="string", length=30, nullable=true)
     */
    protected $ClassNumCrew;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassOptCrewWeight", type="string", length=30, nullable=true)
     */
    protected $ClassOptCrewWeight;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassNumTrapeze", type="string", length=30, nullable=true)
     */
    protected $ClassNumTrapeze;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassDesigner", type="string", length=30, nullable=true)
     */
    protected $ClassDesigner;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassNationalOrigins", type="string", length=30, nullable=true)
     */
    protected $ClassNationalOrigins;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassYearDesigned", type="string", length=20, nullable=true)
     */
    protected $ClassYearDesigned;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassIsafStatus", type="string", length=20, nullable=true)
     */
    protected $ClassIsafStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassHullLen", type="string", length=30, nullable=true)
     */
    protected $ClassHullLen;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassBeamLen", type="string", length=30, nullable=true)
     */
    protected $ClassBeamLen;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassDraughtLen", type="string", length=30, nullable=true)
     */
    protected $ClassDraughtLen;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassMainsailArea", type="string", length=30, nullable=true)
     */
    protected $ClassMainsailArea;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassHeadsailArea", type="string", length=30, nullable=true)
     */
    protected $ClassHeadsailArea;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassSpinnakerArea", type="string", length=30, nullable=true)
     */
    protected $ClassSpinnakerArea;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassSailAreaUpwind", type="string", length=30, nullable=true)
     */
    protected $ClassSailAreaUpwind;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassWeightBoat", type="string", length=30, nullable=true)
     */
    protected $ClassWeightBoat;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassWeightHull", type="string", length=30, nullable=true)
     */
    protected $ClassWeightHull;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassWeightBallast", type="string", length=30, nullable=true)
     */
    protected $ClassWeightBallast;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassVolume", type="string", length=30, nullable=true)
     */
    protected $ClassVolume;

    /**
     * @var string
     *
     * @ORM\Column(name="ClassIsafAdCat", type="string", length=30, nullable=true)
     */
    protected $ClassIsafAdCat;

    /**
     * @var integer
     *
     * @ORM\Column(name="ClassId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $ClassId;



    /**
     * Set ClassOrigId
     *
     * @param integer $classOrigId
     * @return Classes
     */
    public function setClassOrigId($classOrigId)
    {
        $this->ClassOrigId = $classOrigId;

        return $this;
    }

    /**
     * Get ClassOrigId
     *
     * @return integer 
     */
    public function getClassOrigId()
    {
        return $this->ClassOrigId;
    }

    /**
     * Set ClassCode
     *
     * @param string $classCode
     * @return Classes
     */
    public function setClassCode($classCode)
    {
        $this->ClassCode = $classCode;

        return $this;
    }

    /**
     * Get ClassCode
     *
     * @return string 
     */
    public function getClassCode()
    {
        return $this->ClassCode;
    }

    /**
     * Set ClassName
     *
     * @param string $className
     * @return Classes
     */
    public function setClassName($className)
    {
        $this->ClassName = $className;

        return $this;
    }

    /**
     * Get ClassName
     *
     * @return string 
     */
    public function getClassName()
    {
        return $this->ClassName;
    }

    /**
     * Set ClassStatId
     *
     * @param integer $classStatId
     * @return Classes
     */
    public function setClassStatId($classStatId)
    {
        $this->ClassStatId = $classStatId;

        return $this;
    }

    /**
     * Get ClassStatId
     *
     * @return integer 
     */
    public function getClassStatId()
    {
        return $this->ClassStatId;
    }

    /**
     * Set ClassTypeId
     *
     * @param integer $classTypeId
     * @return Classes
     */
    public function setClassTypeId($classTypeId)
    {
        $this->ClassTypeId = $classTypeId;

        return $this;
    }

    /**
     * Get ClassTypeId
     *
     * @return integer 
     */
    public function getClassTypeId()
    {
        return $this->ClassTypeId;
    }

    /**
     * Set ClassAssociation
     *
     * @param string $classAssociation
     * @return Classes
     */
    public function setClassAssociation($classAssociation)
    {
        $this->ClassAssociation = $classAssociation;

        return $this;
    }

    /**
     * Get ClassAssociation
     *
     * @return string 
     */
    public function getClassAssociation()
    {
        return $this->ClassAssociation;
    }

    /**
     * Set ClassPosition1
     *
     * @param string $classPosition1
     * @return Classes
     */
    public function setClassPosition1($classPosition1)
    {
        $this->ClassPosition1 = $classPosition1;

        return $this;
    }

    /**
     * Get ClassPosition1
     *
     * @return string 
     */
    public function getClassPosition1()
    {
        return $this->ClassPosition1;
    }

    /**
     * Set ClassTitle1
     *
     * @param string $classTitle1
     * @return Classes
     */
    public function setClassTitle1($classTitle1)
    {
        $this->ClassTitle1 = $classTitle1;

        return $this;
    }

    /**
     * Get ClassTitle1
     *
     * @return string 
     */
    public function getClassTitle1()
    {
        return $this->ClassTitle1;
    }

    /**
     * Set ClassFirstName1
     *
     * @param string $classFirstName1
     * @return Classes
     */
    public function setClassFirstName1($classFirstName1)
    {
        $this->ClassFirstName1 = $classFirstName1;

        return $this;
    }

    /**
     * Get ClassFirstName1
     *
     * @return string 
     */
    public function getClassFirstName1()
    {
        return $this->ClassFirstName1;
    }

    /**
     * Set ClassSurname1
     *
     * @param string $classSurname1
     * @return Classes
     */
    public function setClassSurname1($classSurname1)
    {
        $this->ClassSurname1 = $classSurname1;

        return $this;
    }

    /**
     * Get ClassSurname1
     *
     * @return string 
     */
    public function getClassSurname1()
    {
        return $this->ClassSurname1;
    }

    /**
     * Set ClassPosition2
     *
     * @param string $classPosition2
     * @return Classes
     */
    public function setClassPosition2($classPosition2)
    {
        $this->ClassPosition2 = $classPosition2;

        return $this;
    }

    /**
     * Get ClassPosition2
     *
     * @return string 
     */
    public function getClassPosition2()
    {
        return $this->ClassPosition2;
    }

    /**
     * Set ClassTitle2
     *
     * @param string $classTitle2
     * @return Classes
     */
    public function setClassTitle2($classTitle2)
    {
        $this->ClassTitle2 = $classTitle2;

        return $this;
    }

    /**
     * Get ClassTitle2
     *
     * @return string 
     */
    public function getClassTitle2()
    {
        return $this->ClassTitle2;
    }

    /**
     * Set ClassFirstName2
     *
     * @param string $classFirstName2
     * @return Classes
     */
    public function setClassFirstName2($classFirstName2)
    {
        $this->ClassFirstName2 = $classFirstName2;

        return $this;
    }

    /**
     * Get ClassFirstName2
     *
     * @return string 
     */
    public function getClassFirstName2()
    {
        return $this->ClassFirstName2;
    }

    /**
     * Set ClassSurname2
     *
     * @param string $classSurname2
     * @return Classes
     */
    public function setClassSurname2($classSurname2)
    {
        $this->ClassSurname2 = $classSurname2;

        return $this;
    }

    /**
     * Get ClassSurname2
     *
     * @return string 
     */
    public function getClassSurname2()
    {
        return $this->ClassSurname2;
    }

    /**
     * Set ClassAddress
     *
     * @param string $classAddress
     * @return Classes
     */
    public function setClassAddress($classAddress)
    {
        $this->ClassAddress = $classAddress;

        return $this;
    }

    /**
     * Get ClassAddress
     *
     * @return string 
     */
    public function getClassAddress()
    {
        return $this->ClassAddress;
    }

    /**
     * Set ClassCtryId
     *
     * @param integer $classCtryId
     * @return Classes
     */
    public function setClassCtryId($classCtryId)
    {
        $this->ClassCtryId = $classCtryId;

        return $this;
    }

    /**
     * Get ClassCtryId
     *
     * @return integer 
     */
    public function getClassCtryId()
    {
        return $this->ClassCtryId;
    }

    /**
     * Set ClassPhone1
     *
     * @param string $classPhone1
     * @return Classes
     */
    public function setClassPhone1($classPhone1)
    {
        $this->ClassPhone1 = $classPhone1;

        return $this;
    }

    /**
     * Get ClassPhone1
     *
     * @return string 
     */
    public function getClassPhone1()
    {
        return $this->ClassPhone1;
    }

    /**
     * Set ClassPhone2
     *
     * @param string $classPhone2
     * @return Classes
     */
    public function setClassPhone2($classPhone2)
    {
        $this->ClassPhone2 = $classPhone2;

        return $this;
    }

    /**
     * Get ClassPhone2
     *
     * @return string 
     */
    public function getClassPhone2()
    {
        return $this->ClassPhone2;
    }

    /**
     * Set ClassFax
     *
     * @param string $classFax
     * @return Classes
     */
    public function setClassFax($classFax)
    {
        $this->ClassFax = $classFax;

        return $this;
    }

    /**
     * Get ClassFax
     *
     * @return string 
     */
    public function getClassFax()
    {
        return $this->ClassFax;
    }

    /**
     * Set ClassEmail
     *
     * @param string $classEmail
     * @return Classes
     */
    public function setClassEmail($classEmail)
    {
        $this->ClassEmail = $classEmail;

        return $this;
    }

    /**
     * Get ClassEmail
     *
     * @return string 
     */
    public function getClassEmail()
    {
        return $this->ClassEmail;
    }

    /**
     * Set ClassWebsite
     *
     * @param string $classWebsite
     * @return Classes
     */
    public function setClassWebsite($classWebsite)
    {
        $this->ClassWebsite = $classWebsite;

        return $this;
    }

    /**
     * Get ClassWebsite
     *
     * @return string 
     */
    public function getClassWebsite()
    {
        return $this->ClassWebsite;
    }

    /**
     * Set ClassNotes
     *
     * @param string $classNotes
     * @return Classes
     */
    public function setClassNotes($classNotes)
    {
        $this->ClassNotes = $classNotes;

        return $this;
    }

    /**
     * Get ClassNotes
     *
     * @return string 
     */
    public function getClassNotes()
    {
        return $this->ClassNotes;
    }

    /**
     * Set ClassHullMat
     *
     * @param string $classHullMat
     * @return Classes
     */
    public function setClassHullMat($classHullMat)
    {
        $this->ClassHullMat = $classHullMat;

        return $this;
    }

    /**
     * Get ClassHullMat
     *
     * @return string 
     */
    public function getClassHullMat()
    {
        return $this->ClassHullMat;
    }

    /**
     * Set ClassNumCrew
     *
     * @param string $classNumCrew
     * @return Classes
     */
    public function setClassNumCrew($classNumCrew)
    {
        $this->ClassNumCrew = $classNumCrew;

        return $this;
    }

    /**
     * Get ClassNumCrew
     *
     * @return string 
     */
    public function getClassNumCrew()
    {
        return $this->ClassNumCrew;
    }

    /**
     * Set ClassOptCrewWeight
     *
     * @param string $classOptCrewWeight
     * @return Classes
     */
    public function setClassOptCrewWeight($classOptCrewWeight)
    {
        $this->ClassOptCrewWeight = $classOptCrewWeight;

        return $this;
    }

    /**
     * Get ClassOptCrewWeight
     *
     * @return string 
     */
    public function getClassOptCrewWeight()
    {
        return $this->ClassOptCrewWeight;
    }

    /**
     * Set ClassNumTrapeze
     *
     * @param string $classNumTrapeze
     * @return Classes
     */
    public function setClassNumTrapeze($classNumTrapeze)
    {
        $this->ClassNumTrapeze = $classNumTrapeze;

        return $this;
    }

    /**
     * Get ClassNumTrapeze
     *
     * @return string 
     */
    public function getClassNumTrapeze()
    {
        return $this->ClassNumTrapeze;
    }

    /**
     * Set ClassDesigner
     *
     * @param string $classDesigner
     * @return Classes
     */
    public function setClassDesigner($classDesigner)
    {
        $this->ClassDesigner = $classDesigner;

        return $this;
    }

    /**
     * Get ClassDesigner
     *
     * @return string 
     */
    public function getClassDesigner()
    {
        return $this->ClassDesigner;
    }

    /**
     * Set ClassNationalOrigins
     *
     * @param string $classNationalOrigins
     * @return Classes
     */
    public function setClassNationalOrigins($classNationalOrigins)
    {
        $this->ClassNationalOrigins = $classNationalOrigins;

        return $this;
    }

    /**
     * Get ClassNationalOrigins
     *
     * @return string 
     */
    public function getClassNationalOrigins()
    {
        return $this->ClassNationalOrigins;
    }

    /**
     * Set ClassYearDesigned
     *
     * @param string $classYearDesigned
     * @return Classes
     */
    public function setClassYearDesigned($classYearDesigned)
    {
        $this->ClassYearDesigned = $classYearDesigned;

        return $this;
    }

    /**
     * Get ClassYearDesigned
     *
     * @return string 
     */
    public function getClassYearDesigned()
    {
        return $this->ClassYearDesigned;
    }

    /**
     * Set ClassIsafStatus
     *
     * @param string $classIsafStatus
     * @return Classes
     */
    public function setClassIsafStatus($classIsafStatus)
    {
        $this->ClassIsafStatus = $classIsafStatus;

        return $this;
    }

    /**
     * Get ClassIsafStatus
     *
     * @return string 
     */
    public function getClassIsafStatus()
    {
        return $this->ClassIsafStatus;
    }

    /**
     * Set ClassHullLen
     *
     * @param string $classHullLen
     * @return Classes
     */
    public function setClassHullLen($classHullLen)
    {
        $this->ClassHullLen = $classHullLen;

        return $this;
    }

    /**
     * Get ClassHullLen
     *
     * @return string 
     */
    public function getClassHullLen()
    {
        return $this->ClassHullLen;
    }

    /**
     * Set ClassBeamLen
     *
     * @param string $classBeamLen
     * @return Classes
     */
    public function setClassBeamLen($classBeamLen)
    {
        $this->ClassBeamLen = $classBeamLen;

        return $this;
    }

    /**
     * Get ClassBeamLen
     *
     * @return string 
     */
    public function getClassBeamLen()
    {
        return $this->ClassBeamLen;
    }

    /**
     * Set ClassDraughtLen
     *
     * @param string $classDraughtLen
     * @return Classes
     */
    public function setClassDraughtLen($classDraughtLen)
    {
        $this->ClassDraughtLen = $classDraughtLen;

        return $this;
    }

    /**
     * Get ClassDraughtLen
     *
     * @return string 
     */
    public function getClassDraughtLen()
    {
        return $this->ClassDraughtLen;
    }

    /**
     * Set ClassMainsailArea
     *
     * @param string $classMainsailArea
     * @return Classes
     */
    public function setClassMainsailArea($classMainsailArea)
    {
        $this->ClassMainsailArea = $classMainsailArea;

        return $this;
    }

    /**
     * Get ClassMainsailArea
     *
     * @return string 
     */
    public function getClassMainsailArea()
    {
        return $this->ClassMainsailArea;
    }

    /**
     * Set ClassHeadsailArea
     *
     * @param string $classHeadsailArea
     * @return Classes
     */
    public function setClassHeadsailArea($classHeadsailArea)
    {
        $this->ClassHeadsailArea = $classHeadsailArea;

        return $this;
    }

    /**
     * Get ClassHeadsailArea
     *
     * @return string 
     */
    public function getClassHeadsailArea()
    {
        return $this->ClassHeadsailArea;
    }

    /**
     * Set ClassSpinnakerArea
     *
     * @param string $classSpinnakerArea
     * @return Classes
     */
    public function setClassSpinnakerArea($classSpinnakerArea)
    {
        $this->ClassSpinnakerArea = $classSpinnakerArea;

        return $this;
    }

    /**
     * Get ClassSpinnakerArea
     *
     * @return string 
     */
    public function getClassSpinnakerArea()
    {
        return $this->ClassSpinnakerArea;
    }

    /**
     * Set ClassSailAreaUpwind
     *
     * @param string $classSailAreaUpwind
     * @return Classes
     */
    public function setClassSailAreaUpwind($classSailAreaUpwind)
    {
        $this->ClassSailAreaUpwind = $classSailAreaUpwind;

        return $this;
    }

    /**
     * Get ClassSailAreaUpwind
     *
     * @return string 
     */
    public function getClassSailAreaUpwind()
    {
        return $this->ClassSailAreaUpwind;
    }

    /**
     * Set ClassWeightBoat
     *
     * @param string $classWeightBoat
     * @return Classes
     */
    public function setClassWeightBoat($classWeightBoat)
    {
        $this->ClassWeightBoat = $classWeightBoat;

        return $this;
    }

    /**
     * Get ClassWeightBoat
     *
     * @return string 
     */
    public function getClassWeightBoat()
    {
        return $this->ClassWeightBoat;
    }

    /**
     * Set ClassWeightHull
     *
     * @param string $classWeightHull
     * @return Classes
     */
    public function setClassWeightHull($classWeightHull)
    {
        $this->ClassWeightHull = $classWeightHull;

        return $this;
    }

    /**
     * Get ClassWeightHull
     *
     * @return string 
     */
    public function getClassWeightHull()
    {
        return $this->ClassWeightHull;
    }

    /**
     * Set ClassWeightBallast
     *
     * @param string $classWeightBallast
     * @return Classes
     */
    public function setClassWeightBallast($classWeightBallast)
    {
        $this->ClassWeightBallast = $classWeightBallast;

        return $this;
    }

    /**
     * Get ClassWeightBallast
     *
     * @return string 
     */
    public function getClassWeightBallast()
    {
        return $this->ClassWeightBallast;
    }

    /**
     * Set ClassVolume
     *
     * @param string $classVolume
     * @return Classes
     */
    public function setClassVolume($classVolume)
    {
        $this->ClassVolume = $classVolume;

        return $this;
    }

    /**
     * Get ClassVolume
     *
     * @return string 
     */
    public function getClassVolume()
    {
        return $this->ClassVolume;
    }

    /**
     * Set ClassIsafAdCat
     *
     * @param string $classIsafAdCat
     * @return Classes
     */
    public function setClassIsafAdCat($classIsafAdCat)
    {
        $this->ClassIsafAdCat = $classIsafAdCat;

        return $this;
    }

    /**
     * Get ClassIsafAdCat
     *
     * @return string 
     */
    public function getClassIsafAdCat()
    {
        return $this->ClassIsafAdCat;
    }

    /**
     * Get ClassId
     *
     * @return integer 
     */
    public function getClassId()
    {
        return $this->ClassId;
    }
}
