<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fields
 *
 * @ORM\Table(name="fields")
 * @ORM\Entity
 */
class Fields
{
    /**
     * @var string
     *
     * @ORM\Column(name="FldTable", type="string", length=100, nullable=true)
     */
    protected $FldTable;

    /**
     * @var string
     *
     * @ORM\Column(name="FldColumn", type="string", length=100, nullable=true)
     */
    protected $FldColumn;

    /**
     * @var string
     *
     * @ORM\Column(name="FldName", type="string", length=255, nullable=false)
     */
    protected $FldName;

    /**
     * @var string
     *
     * @ORM\Column(name="FldDescription", type="text", length=65535, nullable=false)
     */
    protected $FldDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="FldSuffix", type="string", length=200, nullable=false)
     */
    protected $FldSuffix;

    /**
     * @var string
     *
     * @ORM\Column(name="FldHelpText", type="text", length=65535, nullable=true)
     */
    protected $FldHelpText;

    /**
     * @var string
     *
     * @ORM\Column(name="FldDataType", type="string", nullable=false)
     */
    protected $FldDataType;

    /**
     * @var string
     *
     * @ORM\Column(name="FldDisplayType", type="string", length=50, nullable=false)
     */
    protected $FldDisplayType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FldKey", type="boolean", nullable=false)
     */
    protected $FldKey;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FldVisible", type="boolean", nullable=false)
     */
    protected $FldVisible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FldRequired", type="boolean", nullable=false)
     */
    protected $FldRequired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FldVersion", type="boolean", nullable=false)
     */
    protected $FldVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FldCore", type="boolean", nullable=false)
     */
    protected $FldCore;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FldEditable", type="boolean", nullable=false)
     */
    protected $FldEditable;

    /**
     * @var integer
     *
     * @ORM\Column(name="FldGrpId", type="integer", nullable=true)
     */
    protected $FldGrpId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FldOrder", type="integer", nullable=false)
     */
    protected $FldOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="FldOptions", type="text", length=65535, nullable=true)
     */
    protected $FldOptions;

    /**
     * @var string
     *
     * @ORM\Column(name="FldValidation", type="string", length=200, nullable=false)
     */
    protected $FldValidation;

    /**
     * @var string
     *
     * @ORM\Column(name="FldRolesVisible", type="text", length=65535, nullable=false)
     */
    protected $FldRolesVisible;

    /**
     * @var string
     *
     * @ORM\Column(name="FldRolesRequired", type="text", length=65535, nullable=false)
     */
    protected $FldRolesRequired;

    /**
     * @var integer
     *
     * @ORM\Column(name="FldOwnerEntityId", type="integer", nullable=true)
     */
    protected $FldOwnerEntityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FldId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $FldId;



    /**
     * Set FldTable
     *
     * @param string $fldTable
     * @return Fields
     */
    public function setFldTable($fldTable)
    {
        $this->FldTable = $fldTable;

        return $this;
    }

    /**
     * Get FldTable
     *
     * @return string 
     */
    public function getFldTable()
    {
        return $this->FldTable;
    }

    /**
     * Set FldColumn
     *
     * @param string $fldColumn
     * @return Fields
     */
    public function setFldColumn($fldColumn)
    {
        $this->FldColumn = $fldColumn;

        return $this;
    }

    /**
     * Get FldColumn
     *
     * @return string 
     */
    public function getFldColumn()
    {
        return $this->FldColumn;
    }

    /**
     * Set FldName
     *
     * @param string $fldName
     * @return Fields
     */
    public function setFldName($fldName)
    {
        $this->FldName = $fldName;

        return $this;
    }

    /**
     * Get FldName
     *
     * @return string 
     */
    public function getFldName()
    {
        return $this->FldName;
    }

    /**
     * Set FldDescription
     *
     * @param string $fldDescription
     * @return Fields
     */
    public function setFldDescription($fldDescription)
    {
        $this->FldDescription = $fldDescription;

        return $this;
    }

    /**
     * Get FldDescription
     *
     * @return string 
     */
    public function getFldDescription()
    {
        return $this->FldDescription;
    }

    /**
     * Set FldSuffix
     *
     * @param string $fldSuffix
     * @return Fields
     */
    public function setFldSuffix($fldSuffix)
    {
        $this->FldSuffix = $fldSuffix;

        return $this;
    }

    /**
     * Get FldSuffix
     *
     * @return string 
     */
    public function getFldSuffix()
    {
        return $this->FldSuffix;
    }

    /**
     * Set FldHelpText
     *
     * @param string $fldHelpText
     * @return Fields
     */
    public function setFldHelpText($fldHelpText)
    {
        $this->FldHelpText = $fldHelpText;

        return $this;
    }

    /**
     * Get FldHelpText
     *
     * @return string 
     */
    public function getFldHelpText()
    {
        return $this->FldHelpText;
    }

    /**
     * Set FldDataType
     *
     * @param string $fldDataType
     * @return Fields
     */
    public function setFldDataType($fldDataType)
    {
        $this->FldDataType = $fldDataType;

        return $this;
    }

    /**
     * Get FldDataType
     *
     * @return string 
     */
    public function getFldDataType()
    {
        return $this->FldDataType;
    }

    /**
     * Set FldDisplayType
     *
     * @param string $fldDisplayType
     * @return Fields
     */
    public function setFldDisplayType($fldDisplayType)
    {
        $this->FldDisplayType = $fldDisplayType;

        return $this;
    }

    /**
     * Get FldDisplayType
     *
     * @return string 
     */
    public function getFldDisplayType()
    {
        return $this->FldDisplayType;
    }

    /**
     * Set FldKey
     *
     * @param boolean $fldKey
     * @return Fields
     */
    public function setFldKey($fldKey)
    {
        $this->FldKey = $fldKey;

        return $this;
    }

    /**
     * Get FldKey
     *
     * @return boolean 
     */
    public function getFldKey()
    {
        return $this->FldKey;
    }

    /**
     * Set FldVisible
     *
     * @param boolean $fldVisible
     * @return Fields
     */
    public function setFldVisible($fldVisible)
    {
        $this->FldVisible = $fldVisible;

        return $this;
    }

    /**
     * Get FldVisible
     *
     * @return boolean 
     */
    public function getFldVisible()
    {
        return $this->FldVisible;
    }

    /**
     * Set FldRequired
     *
     * @param boolean $fldRequired
     * @return Fields
     */
    public function setFldRequired($fldRequired)
    {
        $this->FldRequired = $fldRequired;

        return $this;
    }

    /**
     * Get FldRequired
     *
     * @return boolean 
     */
    public function getFldRequired()
    {
        return $this->FldRequired;
    }

    /**
     * Set FldVersion
     *
     * @param boolean $fldVersion
     * @return Fields
     */
    public function setFldVersion($fldVersion)
    {
        $this->FldVersion = $fldVersion;

        return $this;
    }

    /**
     * Get FldVersion
     *
     * @return boolean 
     */
    public function getFldVersion()
    {
        return $this->FldVersion;
    }

    /**
     * Set FldCore
     *
     * @param boolean $fldCore
     * @return Fields
     */
    public function setFldCore($fldCore)
    {
        $this->FldCore = $fldCore;

        return $this;
    }

    /**
     * Get FldCore
     *
     * @return boolean 
     */
    public function getFldCore()
    {
        return $this->FldCore;
    }

    /**
     * Set FldEditable
     *
     * @param boolean $fldEditable
     * @return Fields
     */
    public function setFldEditable($fldEditable)
    {
        $this->FldEditable = $fldEditable;

        return $this;
    }

    /**
     * Get FldEditable
     *
     * @return boolean 
     */
    public function getFldEditable()
    {
        return $this->FldEditable;
    }

    /**
     * Set FldGrpId
     *
     * @param integer $fldGrpId
     * @return Fields
     */
    public function setFldGrpId($fldGrpId)
    {
        $this->FldGrpId = $fldGrpId;

        return $this;
    }

    /**
     * Get FldGrpId
     *
     * @return integer 
     */
    public function getFldGrpId()
    {
        return $this->FldGrpId;
    }

    /**
     * Set FldOrder
     *
     * @param integer $fldOrder
     * @return Fields
     */
    public function setFldOrder($fldOrder)
    {
        $this->FldOrder = $fldOrder;

        return $this;
    }

    /**
     * Get FldOrder
     *
     * @return integer 
     */
    public function getFldOrder()
    {
        return $this->FldOrder;
    }

    /**
     * Set FldOptions
     *
     * @param string $fldOptions
     * @return Fields
     */
    public function setFldOptions($fldOptions)
    {
        $this->FldOptions = $fldOptions;

        return $this;
    }

    /**
     * Get FldOptions
     *
     * @return string 
     */
    public function getFldOptions()
    {
        return $this->FldOptions;
    }

    /**
     * Set FldValidation
     *
     * @param string $fldValidation
     * @return Fields
     */
    public function setFldValidation($fldValidation)
    {
        $this->FldValidation = $fldValidation;

        return $this;
    }

    /**
     * Get FldValidation
     *
     * @return string 
     */
    public function getFldValidation()
    {
        return $this->FldValidation;
    }

    /**
     * Set FldRolesVisible
     *
     * @param string $fldRolesVisible
     * @return Fields
     */
    public function setFldRolesVisible($fldRolesVisible)
    {
        $this->FldRolesVisible = $fldRolesVisible;

        return $this;
    }

    /**
     * Get FldRolesVisible
     *
     * @return string 
     */
    public function getFldRolesVisible()
    {
        return $this->FldRolesVisible;
    }

    /**
     * Set FldRolesRequired
     *
     * @param string $fldRolesRequired
     * @return Fields
     */
    public function setFldRolesRequired($fldRolesRequired)
    {
        $this->FldRolesRequired = $fldRolesRequired;

        return $this;
    }

    /**
     * Get FldRolesRequired
     *
     * @return string 
     */
    public function getFldRolesRequired()
    {
        return $this->FldRolesRequired;
    }

    /**
     * Set FldOwnerEntityId
     *
     * @param integer $fldOwnerEntityId
     * @return Fields
     */
    public function setFldOwnerEntityId($fldOwnerEntityId)
    {
        $this->FldOwnerEntityId = $fldOwnerEntityId;

        return $this;
    }

    /**
     * Get FldOwnerEntityId
     *
     * @return integer 
     */
    public function getFldOwnerEntityId()
    {
        return $this->FldOwnerEntityId;
    }

    /**
     * Get FldId
     *
     * @return integer 
     */
    public function getFldId()
    {
        return $this->FldId;
    }
}
