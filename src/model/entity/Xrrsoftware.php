<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Xrrsoftware
 *
 * @ORM\Table(name="XRRSoftware")
 * @ORM\Entity
 */
class Xrrsoftware
{
    /**
     * @var string
     *
     * @ORM\Column(name="SftwrName", type="text", length=65535, nullable=true)
     */
    protected $SftwrName;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrUrl", type="text", length=65535, nullable=true)
     */
    protected $SftwrUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrLinkText", type="text", length=65535, nullable=true)
     */
    protected $SftwrLinkText;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrDescSuppl", type="text", length=65535, nullable=true)
     */
    protected $SftwrDescSuppl;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrLogoExt", type="string", length=5, nullable=true)
     */
    protected $SftwrLogoExt;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrLogoContent", type="blob", nullable=true)
     */
    protected $SftwrLogoContent;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrContactName", type="string", length=100, nullable=true)
     */
    protected $SftwrContactName;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrContactNumber", type="string", length=50, nullable=true)
     */
    protected $SftwrContactNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrContactEmail", type="string", length=100, nullable=true)
     */
    protected $SftwrContactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="SftwrContactOther", type="text", length=65535, nullable=true)
     */
    protected $SftwrContactOther;

    /**
     * @var integer
     *
     * @ORM\Column(name="SftwrId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $SftwrId;



    /**
     * Set SftwrName
     *
     * @param string $sftwrName
     * @return Xrrsoftware
     */
    public function setSftwrName($sftwrName)
    {
        $this->SftwrName = $sftwrName;

        return $this;
    }

    /**
     * Get SftwrName
     *
     * @return string 
     */
    public function getSftwrName()
    {
        return $this->SftwrName;
    }

    /**
     * Set SftwrUrl
     *
     * @param string $sftwrUrl
     * @return Xrrsoftware
     */
    public function setSftwrUrl($sftwrUrl)
    {
        $this->SftwrUrl = $sftwrUrl;

        return $this;
    }

    /**
     * Get SftwrUrl
     *
     * @return string 
     */
    public function getSftwrUrl()
    {
        return $this->SftwrUrl;
    }

    /**
     * Set SftwrLinkText
     *
     * @param string $sftwrLinkText
     * @return Xrrsoftware
     */
    public function setSftwrLinkText($sftwrLinkText)
    {
        $this->SftwrLinkText = $sftwrLinkText;

        return $this;
    }

    /**
     * Get SftwrLinkText
     *
     * @return string 
     */
    public function getSftwrLinkText()
    {
        return $this->SftwrLinkText;
    }

    /**
     * Set SftwrDescSuppl
     *
     * @param string $sftwrDescSuppl
     * @return Xrrsoftware
     */
    public function setSftwrDescSuppl($sftwrDescSuppl)
    {
        $this->SftwrDescSuppl = $sftwrDescSuppl;

        return $this;
    }

    /**
     * Get SftwrDescSuppl
     *
     * @return string 
     */
    public function getSftwrDescSuppl()
    {
        return $this->SftwrDescSuppl;
    }

    /**
     * Set SftwrLogoExt
     *
     * @param string $sftwrLogoExt
     * @return Xrrsoftware
     */
    public function setSftwrLogoExt($sftwrLogoExt)
    {
        $this->SftwrLogoExt = $sftwrLogoExt;

        return $this;
    }

    /**
     * Get SftwrLogoExt
     *
     * @return string 
     */
    public function getSftwrLogoExt()
    {
        return $this->SftwrLogoExt;
    }

    /**
     * Set SftwrLogoContent
     *
     * @param string $sftwrLogoContent
     * @return Xrrsoftware
     */
    public function setSftwrLogoContent($sftwrLogoContent)
    {
        $this->SftwrLogoContent = $sftwrLogoContent;

        return $this;
    }

    /**
     * Get SftwrLogoContent
     *
     * @return string 
     */
    public function getSftwrLogoContent()
    {
        return $this->SftwrLogoContent;
    }

    /**
     * Set SftwrContactName
     *
     * @param string $sftwrContactName
     * @return Xrrsoftware
     */
    public function setSftwrContactName($sftwrContactName)
    {
        $this->SftwrContactName = $sftwrContactName;

        return $this;
    }

    /**
     * Get SftwrContactName
     *
     * @return string 
     */
    public function getSftwrContactName()
    {
        return $this->SftwrContactName;
    }

    /**
     * Set SftwrContactNumber
     *
     * @param string $sftwrContactNumber
     * @return Xrrsoftware
     */
    public function setSftwrContactNumber($sftwrContactNumber)
    {
        $this->SftwrContactNumber = $sftwrContactNumber;

        return $this;
    }

    /**
     * Get SftwrContactNumber
     *
     * @return string 
     */
    public function getSftwrContactNumber()
    {
        return $this->SftwrContactNumber;
    }

    /**
     * Set SftwrContactEmail
     *
     * @param string $sftwrContactEmail
     * @return Xrrsoftware
     */
    public function setSftwrContactEmail($sftwrContactEmail)
    {
        $this->SftwrContactEmail = $sftwrContactEmail;

        return $this;
    }

    /**
     * Get SftwrContactEmail
     *
     * @return string 
     */
    public function getSftwrContactEmail()
    {
        return $this->SftwrContactEmail;
    }

    /**
     * Set SftwrContactOther
     *
     * @param string $sftwrContactOther
     * @return Xrrsoftware
     */
    public function setSftwrContactOther($sftwrContactOther)
    {
        $this->SftwrContactOther = $sftwrContactOther;

        return $this;
    }

    /**
     * Get SftwrContactOther
     *
     * @return string 
     */
    public function getSftwrContactOther()
    {
        return $this->SftwrContactOther;
    }

    /**
     * Get SftwrId
     *
     * @return integer 
     */
    public function getSftwrId()
    {
        return $this->SftwrId;
    }
}
