<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formfieldsets
 *
 * @ORM\Table(name="formfieldsets")
 * @ORM\Entity
 */
class Formfieldsets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="FmfsFrmId", type="integer", nullable=false)
     */
    protected $FmfsFrmId;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfsTable", type="string", length=50, nullable=false)
     */
    protected $FmfsTable;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfsName", type="string", length=50, nullable=false)
     */
    protected $FmfsName;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfsIntroduction", type="text", length=65535, nullable=false)
     */
    protected $FmfsIntroduction;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmfsOrder", type="integer", nullable=false)
     */
    protected $FmfsOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfsRoles", type="text", length=65535, nullable=false)
     */
    protected $FmfsRoles;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfsRegattas", type="text", length=65535, nullable=false)
     */
    protected $FmfsRegattas;

    /**
     * @var string
     *
     * @ORM\Column(name="FmfsClasses", type="text", length=65535, nullable=false)
     */
    protected $FmfsClasses;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmfsId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $FmfsId;



    /**
     * Set FmfsFrmId
     *
     * @param integer $fmfsFrmId
     * @return Formfieldsets
     */
    public function setFmfsFrmId($fmfsFrmId)
    {
        $this->FmfsFrmId = $fmfsFrmId;

        return $this;
    }

    /**
     * Get FmfsFrmId
     *
     * @return integer 
     */
    public function getFmfsFrmId()
    {
        return $this->FmfsFrmId;
    }

    /**
     * Set FmfsTable
     *
     * @param string $fmfsTable
     * @return Formfieldsets
     */
    public function setFmfsTable($fmfsTable)
    {
        $this->FmfsTable = $fmfsTable;

        return $this;
    }

    /**
     * Get FmfsTable
     *
     * @return string 
     */
    public function getFmfsTable()
    {
        return $this->FmfsTable;
    }

    /**
     * Set FmfsName
     *
     * @param string $fmfsName
     * @return Formfieldsets
     */
    public function setFmfsName($fmfsName)
    {
        $this->FmfsName = $fmfsName;

        return $this;
    }

    /**
     * Get FmfsName
     *
     * @return string 
     */
    public function getFmfsName()
    {
        return $this->FmfsName;
    }

    /**
     * Set FmfsIntroduction
     *
     * @param string $fmfsIntroduction
     * @return Formfieldsets
     */
    public function setFmfsIntroduction($fmfsIntroduction)
    {
        $this->FmfsIntroduction = $fmfsIntroduction;

        return $this;
    }

    /**
     * Get FmfsIntroduction
     *
     * @return string 
     */
    public function getFmfsIntroduction()
    {
        return $this->FmfsIntroduction;
    }

    /**
     * Set FmfsOrder
     *
     * @param integer $fmfsOrder
     * @return Formfieldsets
     */
    public function setFmfsOrder($fmfsOrder)
    {
        $this->FmfsOrder = $fmfsOrder;

        return $this;
    }

    /**
     * Get FmfsOrder
     *
     * @return integer 
     */
    public function getFmfsOrder()
    {
        return $this->FmfsOrder;
    }

    /**
     * Set FmfsRoles
     *
     * @param string $fmfsRoles
     * @return Formfieldsets
     */
    public function setFmfsRoles($fmfsRoles)
    {
        $this->FmfsRoles = $fmfsRoles;

        return $this;
    }

    /**
     * Get FmfsRoles
     *
     * @return string 
     */
    public function getFmfsRoles()
    {
        return $this->FmfsRoles;
    }

    /**
     * Set FmfsRegattas
     *
     * @param string $fmfsRegattas
     * @return Formfieldsets
     */
    public function setFmfsRegattas($fmfsRegattas)
    {
        $this->FmfsRegattas = $fmfsRegattas;

        return $this;
    }

    /**
     * Get FmfsRegattas
     *
     * @return string 
     */
    public function getFmfsRegattas()
    {
        return $this->FmfsRegattas;
    }

    /**
     * Set FmfsClasses
     *
     * @param string $fmfsClasses
     * @return Formfieldsets
     */
    public function setFmfsClasses($fmfsClasses)
    {
        $this->FmfsClasses = $fmfsClasses;

        return $this;
    }

    /**
     * Get FmfsClasses
     *
     * @return string 
     */
    public function getFmfsClasses()
    {
        return $this->FmfsClasses;
    }

    /**
     * Get FmfsId
     *
     * @return integer 
     */
    public function getFmfsId()
    {
        return $this->FmfsId;
    }
}
