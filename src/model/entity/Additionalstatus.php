<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Additionalstatus
 *
 * @ORM\Table(name="AdditionalStatus")
 * @ORM\Entity
 */
class Additionalstatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="AddMembId", type="integer", nullable=false)
     */
    protected $AddMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="AddOfstId", type="integer", nullable=false)
     */
    protected $AddOfstId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="AddStart", type="date", nullable=false)
     */
    protected $AddStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="AddEnd", type="date", nullable=false)
     */
    protected $AddEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="AddType", type="string", nullable=true)
     */
    protected $AddType;

    /**
     * @var integer
     *
     * @ORM\Column(name="AddCposId", type="integer", nullable=false)
     */
    protected $AddCposId;

    /**
     * @var integer
     *
     * @ORM\Column(name="AddId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $AddId;



    /**
     * Set AddMembId
     *
     * @param integer $addMembId
     * @return Additionalstatus
     */
    public function setAddMembId($addMembId)
    {
        $this->AddMembId = $addMembId;

        return $this;
    }

    /**
     * Get AddMembId
     *
     * @return integer 
     */
    public function getAddMembId()
    {
        return $this->AddMembId;
    }

    /**
     * Set AddOfstId
     *
     * @param integer $addOfstId
     * @return Additionalstatus
     */
    public function setAddOfstId($addOfstId)
    {
        $this->AddOfstId = $addOfstId;

        return $this;
    }

    /**
     * Get AddOfstId
     *
     * @return integer 
     */
    public function getAddOfstId()
    {
        return $this->AddOfstId;
    }

    /**
     * Set AddStart
     *
     * @param \DateTime $addStart
     * @return Additionalstatus
     */
    public function setAddStart($addStart)
    {
        $this->AddStart = $addStart;

        return $this;
    }

    /**
     * Get AddStart
     *
     * @return \DateTime 
     */
    public function getAddStart()
    {
        return $this->AddStart;
    }

    /**
     * Set AddEnd
     *
     * @param \DateTime $addEnd
     * @return Additionalstatus
     */
    public function setAddEnd($addEnd)
    {
        $this->AddEnd = $addEnd;

        return $this;
    }

    /**
     * Get AddEnd
     *
     * @return \DateTime 
     */
    public function getAddEnd()
    {
        return $this->AddEnd;
    }

    /**
     * Set AddType
     *
     * @param string $addType
     * @return Additionalstatus
     */
    public function setAddType($addType)
    {
        $this->AddType = $addType;

        return $this;
    }

    /**
     * Get AddType
     *
     * @return string 
     */
    public function getAddType()
    {
        return $this->AddType;
    }

    /**
     * Set AddCposId
     *
     * @param integer $addCposId
     * @return Additionalstatus
     */
    public function setAddCposId($addCposId)
    {
        $this->AddCposId = $addCposId;

        return $this;
    }

    /**
     * Get AddCposId
     *
     * @return integer 
     */
    public function getAddCposId()
    {
        return $this->AddCposId;
    }

    /**
     * Get AddId
     *
     * @return integer 
     */
    public function getAddId()
    {
        return $this->AddId;
    }
}
