<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Translationlocales
 *
 * @ORM\Table(name="translationlocales")
 * @ORM\Entity
 */
class Translationlocales
{
    /**
     * @var string
     *
     * @ORM\Column(name="LocaleName", type="string", length=10, nullable=true)
     */
    protected $LocaleName;

    /**
     * @var string
     *
     * @ORM\Column(name="LocaleDescription", type="string", length=50, nullable=true)
     */
    protected $LocaleDescription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    protected $Enabled;

    /**
     * @var integer
     *
     * @ORM\Column(name="LocaleId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $LocaleId;



    /**
     * Set LocaleName
     *
     * @param string $localeName
     * @return Translationlocales
     */
    public function setLocaleName($localeName)
    {
        $this->LocaleName = $localeName;

        return $this;
    }

    /**
     * Get LocaleName
     *
     * @return string 
     */
    public function getLocaleName()
    {
        return $this->LocaleName;
    }

    /**
     * Set LocaleDescription
     *
     * @param string $localeDescription
     * @return Translationlocales
     */
    public function setLocaleDescription($localeDescription)
    {
        $this->LocaleDescription = $localeDescription;

        return $this;
    }

    /**
     * Get LocaleDescription
     *
     * @return string 
     */
    public function getLocaleDescription()
    {
        return $this->LocaleDescription;
    }

    /**
     * Set Enabled
     *
     * @param boolean $enabled
     * @return Translationlocales
     */
    public function setEnabled($enabled)
    {
        $this->Enabled = $enabled;

        return $this;
    }

    /**
     * Get Enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->Enabled;
    }

    /**
     * Get LocaleId
     *
     * @return integer 
     */
    public function getLocaleId()
    {
        return $this->LocaleId;
    }
}
