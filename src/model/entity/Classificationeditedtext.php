<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationeditedtext
 *
 * @ORM\Table(name="ClassificationEditedText")
 * @ORM\Entity
 */
class Classificationeditedtext
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEditTextClassName", type="string", length=150, nullable=false)
     */
    protected $CfcnEditTextClassName;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEditTextValue", type="text", nullable=false)
     */
    protected $CfcnEditTextValue;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEditTextConstName", type="string", length=150, nullable=false)
     */
    protected $CfcnEditTextConstName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnEditTextChangeDate", type="datetime", nullable=false)
     */
    protected $CfcnEditTextChangeDate;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEditTextChangedBy", type="string", length=150, nullable=false)
     */
    protected $CfcnEditTextChangedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEditTextLanguage", type="string", length=100, nullable=false)
     */
    protected $CfcnEditTextLanguage;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnEditTextId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CfcnEditTextId;



    /**
     * Set CfcnEditTextClassName
     *
     * @param string $cfcnEditTextClassName
     * @return Classificationeditedtext
     */
    public function setCfcnEditTextClassName($cfcnEditTextClassName)
    {
        $this->CfcnEditTextClassName = $cfcnEditTextClassName;

        return $this;
    }

    /**
     * Get CfcnEditTextClassName
     *
     * @return string 
     */
    public function getCfcnEditTextClassName()
    {
        return $this->CfcnEditTextClassName;
    }

    /**
     * Set CfcnEditTextValue
     *
     * @param string $cfcnEditTextValue
     * @return Classificationeditedtext
     */
    public function setCfcnEditTextValue($cfcnEditTextValue)
    {
        $this->CfcnEditTextValue = $cfcnEditTextValue;

        return $this;
    }

    /**
     * Get CfcnEditTextValue
     *
     * @return string 
     */
    public function getCfcnEditTextValue()
    {
        return $this->CfcnEditTextValue;
    }

    /**
     * Set CfcnEditTextConstName
     *
     * @param string $cfcnEditTextConstName
     * @return Classificationeditedtext
     */
    public function setCfcnEditTextConstName($cfcnEditTextConstName)
    {
        $this->CfcnEditTextConstName = $cfcnEditTextConstName;

        return $this;
    }

    /**
     * Get CfcnEditTextConstName
     *
     * @return string 
     */
    public function getCfcnEditTextConstName()
    {
        return $this->CfcnEditTextConstName;
    }

    /**
     * Set CfcnEditTextChangeDate
     *
     * @param \DateTime $cfcnEditTextChangeDate
     * @return Classificationeditedtext
     */
    public function setCfcnEditTextChangeDate($cfcnEditTextChangeDate)
    {
        $this->CfcnEditTextChangeDate = $cfcnEditTextChangeDate;

        return $this;
    }

    /**
     * Get CfcnEditTextChangeDate
     *
     * @return \DateTime 
     */
    public function getCfcnEditTextChangeDate()
    {
        return $this->CfcnEditTextChangeDate;
    }

    /**
     * Set CfcnEditTextChangedBy
     *
     * @param string $cfcnEditTextChangedBy
     * @return Classificationeditedtext
     */
    public function setCfcnEditTextChangedBy($cfcnEditTextChangedBy)
    {
        $this->CfcnEditTextChangedBy = $cfcnEditTextChangedBy;

        return $this;
    }

    /**
     * Get CfcnEditTextChangedBy
     *
     * @return string 
     */
    public function getCfcnEditTextChangedBy()
    {
        return $this->CfcnEditTextChangedBy;
    }

    /**
     * Set CfcnEditTextLanguage
     *
     * @param string $cfcnEditTextLanguage
     * @return Classificationeditedtext
     */
    public function setCfcnEditTextLanguage($cfcnEditTextLanguage)
    {
        $this->CfcnEditTextLanguage = $cfcnEditTextLanguage;

        return $this;
    }

    /**
     * Get CfcnEditTextLanguage
     *
     * @return string 
     */
    public function getCfcnEditTextLanguage()
    {
        return $this->CfcnEditTextLanguage;
    }

    /**
     * Get CfcnEditTextId
     *
     * @return integer 
     */
    public function getCfcnEditTextId()
    {
        return $this->CfcnEditTextId;
    }
}
