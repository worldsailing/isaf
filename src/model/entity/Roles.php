<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roles
 *
 * @ORM\Table(name="roles")
 * @ORM\Entity
 */
class Roles
{
    /**
     * @var string
     *
     * @ORM\Column(name="RoleName", type="string", length=30, nullable=false)
     */
    protected $RoleName;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleDescription", type="text", length=65535, nullable=true)
     */
    protected $RoleDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleInherits", type="string", length=30, nullable=true)
     */
    protected $RoleInherits;

    /**
     * @var string
     *
     * @ORM\Column(name="RoleEntity", type="string", length=30, nullable=true)
     */
    protected $RoleEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="RoleId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $RoleId;



    /**
     * Set RoleName
     *
     * @param string $roleName
     * @return Roles
     */
    public function setRoleName($roleName)
    {
        $this->RoleName = $roleName;

        return $this;
    }

    /**
     * Get RoleName
     *
     * @return string 
     */
    public function getRoleName()
    {
        return $this->RoleName;
    }

    /**
     * Set RoleDescription
     *
     * @param string $roleDescription
     * @return Roles
     */
    public function setRoleDescription($roleDescription)
    {
        $this->RoleDescription = $roleDescription;

        return $this;
    }

    /**
     * Get RoleDescription
     *
     * @return string 
     */
    public function getRoleDescription()
    {
        return $this->RoleDescription;
    }

    /**
     * Set RoleInherits
     *
     * @param string $roleInherits
     * @return Roles
     */
    public function setRoleInherits($roleInherits)
    {
        $this->RoleInherits = $roleInherits;

        return $this;
    }

    /**
     * Get RoleInherits
     *
     * @return string 
     */
    public function getRoleInherits()
    {
        return $this->RoleInherits;
    }

    /**
     * Set RoleEntity
     *
     * @param string $roleEntity
     * @return Roles
     */
    public function setRoleEntity($roleEntity)
    {
        $this->RoleEntity = $roleEntity;

        return $this;
    }

    /**
     * Get RoleEntity
     *
     * @return string 
     */
    public function getRoleEntity()
    {
        return $this->RoleEntity;
    }

    /**
     * Get RoleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->RoleId;
    }
}
