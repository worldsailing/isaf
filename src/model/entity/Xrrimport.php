<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Xrrimport
 *
 * @ORM\Table(name="XRRImport")
 * @ORM\Entity
 */
class Xrrimport
{
    /**
     * @var string
     *
     * @ORM\Column(name="XRRDocContent", type="text", nullable=false)
     */
    protected $XRRDocContent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="XRRDocCreateDate", type="datetime", nullable=false)
     */
    protected $XRRDocCreateDate;

    /**
     * @var string
     *
     * @ORM\Column(name="XRRDocRemoteAddr", type="string", length=20, nullable=false)
     */
    protected $XRRDocRemoteAddr;

    /**
     * @var boolean
     *
     * @ORM\Column(name="XRRDocProcessed", type="boolean", nullable=false)
     */
    protected $XRRDocProcessed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="XRRDocVetoed", type="boolean", nullable=false)
     */
    protected $XRRDocVetoed;

    /**
     * @var string
     *
     * @ORM\Column(name="XRRDescription", type="text", length=65535, nullable=true)
     */
    protected $XRRDescription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="XRRIsXLR", type="boolean", nullable=true)
     */
    protected $XRRIsXLR;

    /**
     * @var integer
     *
     * @ORM\Column(name="XRRDocId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $XRRDocId;



    /**
     * Set XRRDocContent
     *
     * @param string $xRRDocContent
     * @return Xrrimport
     */
    public function setXRRDocContent($xRRDocContent)
    {
        $this->XRRDocContent = $xRRDocContent;

        return $this;
    }

    /**
     * Get XRRDocContent
     *
     * @return string 
     */
    public function getXRRDocContent()
    {
        return $this->XRRDocContent;
    }

    /**
     * Set XRRDocCreateDate
     *
     * @param \DateTime $xRRDocCreateDate
     * @return Xrrimport
     */
    public function setXRRDocCreateDate($xRRDocCreateDate)
    {
        $this->XRRDocCreateDate = $xRRDocCreateDate;

        return $this;
    }

    /**
     * Get XRRDocCreateDate
     *
     * @return \DateTime 
     */
    public function getXRRDocCreateDate()
    {
        return $this->XRRDocCreateDate;
    }

    /**
     * Set XRRDocRemoteAddr
     *
     * @param string $xRRDocRemoteAddr
     * @return Xrrimport
     */
    public function setXRRDocRemoteAddr($xRRDocRemoteAddr)
    {
        $this->XRRDocRemoteAddr = $xRRDocRemoteAddr;

        return $this;
    }

    /**
     * Get XRRDocRemoteAddr
     *
     * @return string 
     */
    public function getXRRDocRemoteAddr()
    {
        return $this->XRRDocRemoteAddr;
    }

    /**
     * Set XRRDocProcessed
     *
     * @param boolean $xRRDocProcessed
     * @return Xrrimport
     */
    public function setXRRDocProcessed($xRRDocProcessed)
    {
        $this->XRRDocProcessed = $xRRDocProcessed;

        return $this;
    }

    /**
     * Get XRRDocProcessed
     *
     * @return boolean 
     */
    public function getXRRDocProcessed()
    {
        return $this->XRRDocProcessed;
    }

    /**
     * Set XRRDocVetoed
     *
     * @param boolean $xRRDocVetoed
     * @return Xrrimport
     */
    public function setXRRDocVetoed($xRRDocVetoed)
    {
        $this->XRRDocVetoed = $xRRDocVetoed;

        return $this;
    }

    /**
     * Get XRRDocVetoed
     *
     * @return boolean 
     */
    public function getXRRDocVetoed()
    {
        return $this->XRRDocVetoed;
    }

    /**
     * Set XRRDescription
     *
     * @param string $xRRDescription
     * @return Xrrimport
     */
    public function setXRRDescription($xRRDescription)
    {
        $this->XRRDescription = $xRRDescription;

        return $this;
    }

    /**
     * Get XRRDescription
     *
     * @return string 
     */
    public function getXRRDescription()
    {
        return $this->XRRDescription;
    }

    /**
     * Set XRRIsXLR
     *
     * @param boolean $xRRIsXLR
     * @return Xrrimport
     */
    public function setXRRIsXLR($xRRIsXLR)
    {
        $this->XRRIsXLR = $xRRIsXLR;

        return $this;
    }

    /**
     * Get XRRIsXLR
     *
     * @return boolean 
     */
    public function getXRRIsXLR()
    {
        return $this->XRRIsXLR;
    }

    /**
     * Get XRRDocId
     *
     * @return integer 
     */
    public function getXRRDocId()
    {
        return $this->XRRDocId;
    }
}
