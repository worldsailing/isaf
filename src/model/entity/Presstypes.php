<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Presstypes
 *
 * @ORM\Table(name="PressTypes")
 * @ORM\Entity
 */
class Presstypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="PtypOrigId", type="integer", nullable=false)
     */
    protected $PtypOrigId;

    /**
     * @var string
     *
     * @ORM\Column(name="PtypCode", type="string", length=15, nullable=true)
     */
    protected $PtypCode;

    /**
     * @var string
     *
     * @ORM\Column(name="PtypDesc", type="string", length=100, nullable=true)
     */
    protected $PtypDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="PtypId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $PtypId;



    /**
     * Set PtypOrigId
     *
     * @param integer $ptypOrigId
     * @return Presstypes
     */
    public function setPtypOrigId($ptypOrigId)
    {
        $this->PtypOrigId = $ptypOrigId;

        return $this;
    }

    /**
     * Get PtypOrigId
     *
     * @return integer 
     */
    public function getPtypOrigId()
    {
        return $this->PtypOrigId;
    }

    /**
     * Set PtypCode
     *
     * @param string $ptypCode
     * @return Presstypes
     */
    public function setPtypCode($ptypCode)
    {
        $this->PtypCode = $ptypCode;

        return $this;
    }

    /**
     * Get PtypCode
     *
     * @return string 
     */
    public function getPtypCode()
    {
        return $this->PtypCode;
    }

    /**
     * Set PtypDesc
     *
     * @param string $ptypDesc
     * @return Presstypes
     */
    public function setPtypDesc($ptypDesc)
    {
        $this->PtypDesc = $ptypDesc;

        return $this;
    }

    /**
     * Get PtypDesc
     *
     * @return string 
     */
    public function getPtypDesc()
    {
        return $this->PtypDesc;
    }

    /**
     * Get PtypId
     *
     * @return integer 
     */
    public function getPtypId()
    {
        return $this->PtypId;
    }
}
