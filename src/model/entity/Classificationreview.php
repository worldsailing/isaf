<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationreview
 *
 * @ORM\Table(name="ClassificationReview", indexes={@ORM\Index(name="fk_rvw_action", columns={"CfcnRvwActionId"})})
 * @ORM\Entity
 */
class Classificationreview
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfcnRvwRequester", type="string", length=1024, nullable=true)
     */
    protected $CfcnRvwRequester;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnRvwEventName", type="string", length=1024, nullable=true)
     */
    protected $CfcnRvwEventName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnRvwDate", type="datetime", nullable=true)
     */
    protected $CfcnRvwDate;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnRvwFactsFound", type="text", length=65535, nullable=true)
     */
    protected $CfcnRvwFactsFound;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnRvwConclusion", type="text", length=65535, nullable=true)
     */
    protected $CfcnRvwConclusion;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnRvwDecision", type="text", length=65535, nullable=true)
     */
    protected $CfcnRvwDecision;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnRvwActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnRvwActionId;



    /**
     * Set CfcnRvwRequester
     *
     * @param string $cfcnRvwRequester
     * @return Classificationreview
     */
    public function setCfcnRvwRequester($cfcnRvwRequester)
    {
        $this->CfcnRvwRequester = $cfcnRvwRequester;

        return $this;
    }

    /**
     * Get CfcnRvwRequester
     *
     * @return string 
     */
    public function getCfcnRvwRequester()
    {
        return $this->CfcnRvwRequester;
    }

    /**
     * Set CfcnRvwEventName
     *
     * @param string $cfcnRvwEventName
     * @return Classificationreview
     */
    public function setCfcnRvwEventName($cfcnRvwEventName)
    {
        $this->CfcnRvwEventName = $cfcnRvwEventName;

        return $this;
    }

    /**
     * Get CfcnRvwEventName
     *
     * @return string 
     */
    public function getCfcnRvwEventName()
    {
        return $this->CfcnRvwEventName;
    }

    /**
     * Set CfcnRvwDate
     *
     * @param \DateTime $cfcnRvwDate
     * @return Classificationreview
     */
    public function setCfcnRvwDate($cfcnRvwDate)
    {
        $this->CfcnRvwDate = $cfcnRvwDate;

        return $this;
    }

    /**
     * Get CfcnRvwDate
     *
     * @return \DateTime 
     */
    public function getCfcnRvwDate()
    {
        return $this->CfcnRvwDate;
    }

    /**
     * Set CfcnRvwFactsFound
     *
     * @param string $cfcnRvwFactsFound
     * @return Classificationreview
     */
    public function setCfcnRvwFactsFound($cfcnRvwFactsFound)
    {
        $this->CfcnRvwFactsFound = $cfcnRvwFactsFound;

        return $this;
    }

    /**
     * Get CfcnRvwFactsFound
     *
     * @return string 
     */
    public function getCfcnRvwFactsFound()
    {
        return $this->CfcnRvwFactsFound;
    }

    /**
     * Set CfcnRvwConclusion
     *
     * @param string $cfcnRvwConclusion
     * @return Classificationreview
     */
    public function setCfcnRvwConclusion($cfcnRvwConclusion)
    {
        $this->CfcnRvwConclusion = $cfcnRvwConclusion;

        return $this;
    }

    /**
     * Get CfcnRvwConclusion
     *
     * @return string 
     */
    public function getCfcnRvwConclusion()
    {
        return $this->CfcnRvwConclusion;
    }

    /**
     * Set CfcnRvwDecision
     *
     * @param string $cfcnRvwDecision
     * @return Classificationreview
     */
    public function setCfcnRvwDecision($cfcnRvwDecision)
    {
        $this->CfcnRvwDecision = $cfcnRvwDecision;

        return $this;
    }

    /**
     * Get CfcnRvwDecision
     *
     * @return string 
     */
    public function getCfcnRvwDecision()
    {
        return $this->CfcnRvwDecision;
    }

    /**
     * Set cfcnRvwActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnRvwActionId
     * @return Classificationreview
     */
    public function setCfcnRvwActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnRvwActionId)
    {
        $this->cfcnRvwActionId = $cfcnRvwActionId;

        return $this;
    }

    /**
     * Get cfcnRvwActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnRvwActionId()
    {
        return $this->cfcnRvwActionId;
    }
}
