<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Officialnotices
 *
 * @ORM\Table(name="OfficialNotices")
 * @ORM\Entity
 */
class Officialnotices
{
    /**
     * @var string
     *
     * @ORM\Column(name="NtceSource", type="string", length=20, nullable=true)
     */
    protected $NtceSource;

    /**
     * @var string
     *
     * @ORM\Column(name="NtceSrcId", type="string", length=20, nullable=false)
     */
    protected $NtceSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="NtceRgtaId", type="integer", nullable=false)
     */
    protected $NtceRgtaId;

    /**
     * @var string
     *
     * @ORM\Column(name="NtceAffects", type="string", nullable=false)
     */
    protected $NtceAffects;

    /**
     * @var string
     *
     * @ORM\Column(name="NtceSubtitle", type="text", length=65535, nullable=true)
     */
    protected $NtceSubtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="NtceHeading", type="text", length=65535, nullable=true)
     */
    protected $NtceHeading;

    /**
     * @var string
     *
     * @ORM\Column(name="NtceDecision", type="text", length=65535, nullable=true)
     */
    protected $NtceDecision;

    /**
     * @var string
     *
     * @ORM\Column(name="NtceIssuedBy", type="text", length=65535, nullable=true)
     */
    protected $NtceIssuedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NtceDateTime", type="datetime", nullable=false)
     */
    protected $NtceDateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="NtceNote", type="text", length=65535, nullable=true)
     */
    protected $NtceNote;

    /**
     * @var integer
     *
     * @ORM\Column(name="NtceId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $NtceId;



    /**
     * Set NtceSource
     *
     * @param string $ntceSource
     * @return Officialnotices
     */
    public function setNtceSource($ntceSource)
    {
        $this->NtceSource = $ntceSource;

        return $this;
    }

    /**
     * Get NtceSource
     *
     * @return string 
     */
    public function getNtceSource()
    {
        return $this->NtceSource;
    }

    /**
     * Set NtceSrcId
     *
     * @param string $ntceSrcId
     * @return Officialnotices
     */
    public function setNtceSrcId($ntceSrcId)
    {
        $this->NtceSrcId = $ntceSrcId;

        return $this;
    }

    /**
     * Get NtceSrcId
     *
     * @return string 
     */
    public function getNtceSrcId()
    {
        return $this->NtceSrcId;
    }

    /**
     * Set NtceRgtaId
     *
     * @param integer $ntceRgtaId
     * @return Officialnotices
     */
    public function setNtceRgtaId($ntceRgtaId)
    {
        $this->NtceRgtaId = $ntceRgtaId;

        return $this;
    }

    /**
     * Get NtceRgtaId
     *
     * @return integer 
     */
    public function getNtceRgtaId()
    {
        return $this->NtceRgtaId;
    }

    /**
     * Set NtceAffects
     *
     * @param string $ntceAffects
     * @return Officialnotices
     */
    public function setNtceAffects($ntceAffects)
    {
        $this->NtceAffects = $ntceAffects;

        return $this;
    }

    /**
     * Get NtceAffects
     *
     * @return string 
     */
    public function getNtceAffects()
    {
        return $this->NtceAffects;
    }

    /**
     * Set NtceSubtitle
     *
     * @param string $ntceSubtitle
     * @return Officialnotices
     */
    public function setNtceSubtitle($ntceSubtitle)
    {
        $this->NtceSubtitle = $ntceSubtitle;

        return $this;
    }

    /**
     * Get NtceSubtitle
     *
     * @return string 
     */
    public function getNtceSubtitle()
    {
        return $this->NtceSubtitle;
    }

    /**
     * Set NtceHeading
     *
     * @param string $ntceHeading
     * @return Officialnotices
     */
    public function setNtceHeading($ntceHeading)
    {
        $this->NtceHeading = $ntceHeading;

        return $this;
    }

    /**
     * Get NtceHeading
     *
     * @return string 
     */
    public function getNtceHeading()
    {
        return $this->NtceHeading;
    }

    /**
     * Set NtceDecision
     *
     * @param string $ntceDecision
     * @return Officialnotices
     */
    public function setNtceDecision($ntceDecision)
    {
        $this->NtceDecision = $ntceDecision;

        return $this;
    }

    /**
     * Get NtceDecision
     *
     * @return string 
     */
    public function getNtceDecision()
    {
        return $this->NtceDecision;
    }

    /**
     * Set NtceIssuedBy
     *
     * @param string $ntceIssuedBy
     * @return Officialnotices
     */
    public function setNtceIssuedBy($ntceIssuedBy)
    {
        $this->NtceIssuedBy = $ntceIssuedBy;

        return $this;
    }

    /**
     * Get NtceIssuedBy
     *
     * @return string 
     */
    public function getNtceIssuedBy()
    {
        return $this->NtceIssuedBy;
    }

    /**
     * Set NtceDateTime
     *
     * @param \DateTime $ntceDateTime
     * @return Officialnotices
     */
    public function setNtceDateTime($ntceDateTime)
    {
        $this->NtceDateTime = $ntceDateTime;

        return $this;
    }

    /**
     * Get NtceDateTime
     *
     * @return \DateTime 
     */
    public function getNtceDateTime()
    {
        return $this->NtceDateTime;
    }

    /**
     * Set NtceNote
     *
     * @param string $ntceNote
     * @return Officialnotices
     */
    public function setNtceNote($ntceNote)
    {
        $this->NtceNote = $ntceNote;

        return $this;
    }

    /**
     * Get NtceNote
     *
     * @return string 
     */
    public function getNtceNote()
    {
        return $this->NtceNote;
    }

    /**
     * Get NtceId
     *
     * @return integer 
     */
    public function getNtceId()
    {
        return $this->NtceId;
    }
}
