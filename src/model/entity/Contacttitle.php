<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contacttitle
 *
 * @ORM\Table(name="ContactTitle")
 * @ORM\Entity
 */
class Contacttitle
{
    /**
     * @var string
     *
     * @ORM\Column(name="CntName", type="string", length=256, nullable=true)
     */
    protected $CntName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CntPrimary", type="boolean", nullable=false)
     */
    protected $CntPrimary;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CntSecondary", type="boolean", nullable=false)
     */
    protected $CntSecondary;

    /**
     * @var integer
     *
     * @ORM\Column(name="CntId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CntId;



    /**
     * Set CntName
     *
     * @param string $cntName
     * @return Contacttitle
     */
    public function setCntName($cntName)
    {
        $this->CntName = $cntName;

        return $this;
    }

    /**
     * Get CntName
     *
     * @return string 
     */
    public function getCntName()
    {
        return $this->CntName;
    }

    /**
     * Set CntPrimary
     *
     * @param boolean $cntPrimary
     * @return Contacttitle
     */
    public function setCntPrimary($cntPrimary)
    {
        $this->CntPrimary = $cntPrimary;

        return $this;
    }

    /**
     * Get CntPrimary
     *
     * @return boolean 
     */
    public function getCntPrimary()
    {
        return $this->CntPrimary;
    }

    /**
     * Set CntSecondary
     *
     * @param boolean $cntSecondary
     * @return Contacttitle
     */
    public function setCntSecondary($cntSecondary)
    {
        $this->CntSecondary = $cntSecondary;

        return $this;
    }

    /**
     * Get CntSecondary
     *
     * @return boolean 
     */
    public function getCntSecondary()
    {
        return $this->CntSecondary;
    }

    /**
     * Get CntId
     *
     * @return integer 
     */
    public function getCntId()
    {
        return $this->CntId;
    }
}
