<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tourndiscardscores
 *
 * @ORM\Table(name="TournDiscardScores")
 * @ORM\Entity
 */
class Tourndiscardscores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="TdscDisId", type="integer", nullable=false)
     */
    protected $TdscDisId;

    /**
     * @var string
     *
     * @ORM\Column(name="TdscCriteria", type="string", length=100, nullable=true)
     */
    protected $TdscCriteria;

    /**
     * @var integer
     *
     * @ORM\Column(name="TdscOrd", type="integer", nullable=false)
     */
    protected $TdscOrd;

    /**
     * @var integer
     *
     * @ORM\Column(name="TdscId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $TdscId;



    /**
     * Set TdscDisId
     *
     * @param integer $tdscDisId
     * @return Tourndiscardscores
     */
    public function setTdscDisId($tdscDisId)
    {
        $this->TdscDisId = $tdscDisId;

        return $this;
    }

    /**
     * Get TdscDisId
     *
     * @return integer 
     */
    public function getTdscDisId()
    {
        return $this->TdscDisId;
    }

    /**
     * Set TdscCriteria
     *
     * @param string $tdscCriteria
     * @return Tourndiscardscores
     */
    public function setTdscCriteria($tdscCriteria)
    {
        $this->TdscCriteria = $tdscCriteria;

        return $this;
    }

    /**
     * Get TdscCriteria
     *
     * @return string 
     */
    public function getTdscCriteria()
    {
        return $this->TdscCriteria;
    }

    /**
     * Set TdscOrd
     *
     * @param integer $tdscOrd
     * @return Tourndiscardscores
     */
    public function setTdscOrd($tdscOrd)
    {
        $this->TdscOrd = $tdscOrd;

        return $this;
    }

    /**
     * Get TdscOrd
     *
     * @return integer 
     */
    public function getTdscOrd()
    {
        return $this->TdscOrd;
    }

    /**
     * Get TdscId
     *
     * @return integer 
     */
    public function getTdscId()
    {
        return $this->TdscId;
    }
}
