<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Raceresults
 *
 * @ORM\Table(name="RaceResults")
 * @ORM\Entity
 */
class Raceresults
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RresBstsId", type="integer", nullable=false)
     */
    protected $RresBstsId;

    /**
     * @var float
     *
     * @ORM\Column(name="RresFinishTime", type="float", precision=10, scale=0, nullable=false)
     */
    protected $RresFinishTime;

    /**
     * @var string
     *
     * @ORM\Column(name="RresFinishCode", type="string", length=10, nullable=false)
     */
    protected $RresFinishCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="RresResPlace", type="integer", nullable=false)
     */
    protected $RresResPlace;

    /**
     * @var integer
     *
     * @ORM\Column(name="RresAwdPlace", type="integer", nullable=false)
     */
    protected $RresAwdPlace;

    /**
     * @var float
     *
     * @ORM\Column(name="RresAvgPoints", type="float", precision=10, scale=0, nullable=false)
     */
    protected $RresAvgPoints;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RresCorrectedTime", type="time", nullable=false)
     */
    protected $RresCorrectedTime;

    /**
     * @var string
     *
     * @ORM\Column(name="RresHandicap", type="text", length=65535, nullable=false)
     */
    protected $RresHandicap;

    /**
     * @var integer
     *
     * @ORM\Column(name="RresBoatPos", type="integer", nullable=false)
     */
    protected $RresBoatPos;

    /**
     * @var float
     *
     * @ORM\Column(name="RresBoatPts", type="float", precision=10, scale=0, nullable=false)
     */
    protected $RresBoatPts;

    /**
     * @var string
     *
     * @ORM\Column(name="RresEntry", type="string", nullable=true)
     */
    protected $RresEntry;

    /**
     * @var string
     *
     * @ORM\Column(name="RresDiscard", type="string", nullable=true)
     */
    protected $RresDiscard;

    /**
     * @var string
     *
     * @ORM\Column(name="RresUpdated", type="string", nullable=false)
     */
    protected $RresUpdated;

    /**
     * @var integer
     *
     * @ORM\Column(name="RresRaceId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $RresRaceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RresBoatId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $RresBoatId;



    /**
     * Set RresBstsId
     *
     * @param integer $rresBstsId
     * @return Raceresults
     */
    public function setRresBstsId($rresBstsId)
    {
        $this->RresBstsId = $rresBstsId;

        return $this;
    }

    /**
     * Get RresBstsId
     *
     * @return integer 
     */
    public function getRresBstsId()
    {
        return $this->RresBstsId;
    }

    /**
     * Set RresFinishTime
     *
     * @param float $rresFinishTime
     * @return Raceresults
     */
    public function setRresFinishTime($rresFinishTime)
    {
        $this->RresFinishTime = $rresFinishTime;

        return $this;
    }

    /**
     * Get RresFinishTime
     *
     * @return float 
     */
    public function getRresFinishTime()
    {
        return $this->RresFinishTime;
    }

    /**
     * Set RresFinishCode
     *
     * @param string $rresFinishCode
     * @return Raceresults
     */
    public function setRresFinishCode($rresFinishCode)
    {
        $this->RresFinishCode = $rresFinishCode;

        return $this;
    }

    /**
     * Get RresFinishCode
     *
     * @return string 
     */
    public function getRresFinishCode()
    {
        return $this->RresFinishCode;
    }

    /**
     * Set RresResPlace
     *
     * @param integer $rresResPlace
     * @return Raceresults
     */
    public function setRresResPlace($rresResPlace)
    {
        $this->RresResPlace = $rresResPlace;

        return $this;
    }

    /**
     * Get RresResPlace
     *
     * @return integer 
     */
    public function getRresResPlace()
    {
        return $this->RresResPlace;
    }

    /**
     * Set RresAwdPlace
     *
     * @param integer $rresAwdPlace
     * @return Raceresults
     */
    public function setRresAwdPlace($rresAwdPlace)
    {
        $this->RresAwdPlace = $rresAwdPlace;

        return $this;
    }

    /**
     * Get RresAwdPlace
     *
     * @return integer 
     */
    public function getRresAwdPlace()
    {
        return $this->RresAwdPlace;
    }

    /**
     * Set RresAvgPoints
     *
     * @param float $rresAvgPoints
     * @return Raceresults
     */
    public function setRresAvgPoints($rresAvgPoints)
    {
        $this->RresAvgPoints = $rresAvgPoints;

        return $this;
    }

    /**
     * Get RresAvgPoints
     *
     * @return float 
     */
    public function getRresAvgPoints()
    {
        return $this->RresAvgPoints;
    }

    /**
     * Set RresCorrectedTime
     *
     * @param \DateTime $rresCorrectedTime
     * @return Raceresults
     */
    public function setRresCorrectedTime($rresCorrectedTime)
    {
        $this->RresCorrectedTime = $rresCorrectedTime;

        return $this;
    }

    /**
     * Get RresCorrectedTime
     *
     * @return \DateTime 
     */
    public function getRresCorrectedTime()
    {
        return $this->RresCorrectedTime;
    }

    /**
     * Set RresHandicap
     *
     * @param string $rresHandicap
     * @return Raceresults
     */
    public function setRresHandicap($rresHandicap)
    {
        $this->RresHandicap = $rresHandicap;

        return $this;
    }

    /**
     * Get RresHandicap
     *
     * @return string 
     */
    public function getRresHandicap()
    {
        return $this->RresHandicap;
    }

    /**
     * Set RresBoatPos
     *
     * @param integer $rresBoatPos
     * @return Raceresults
     */
    public function setRresBoatPos($rresBoatPos)
    {
        $this->RresBoatPos = $rresBoatPos;

        return $this;
    }

    /**
     * Get RresBoatPos
     *
     * @return integer 
     */
    public function getRresBoatPos()
    {
        return $this->RresBoatPos;
    }

    /**
     * Set RresBoatPts
     *
     * @param float $rresBoatPts
     * @return Raceresults
     */
    public function setRresBoatPts($rresBoatPts)
    {
        $this->RresBoatPts = $rresBoatPts;

        return $this;
    }

    /**
     * Get RresBoatPts
     *
     * @return float 
     */
    public function getRresBoatPts()
    {
        return $this->RresBoatPts;
    }

    /**
     * Set RresEntry
     *
     * @param string $rresEntry
     * @return Raceresults
     */
    public function setRresEntry($rresEntry)
    {
        $this->RresEntry = $rresEntry;

        return $this;
    }

    /**
     * Get RresEntry
     *
     * @return string 
     */
    public function getRresEntry()
    {
        return $this->RresEntry;
    }

    /**
     * Set RresDiscard
     *
     * @param string $rresDiscard
     * @return Raceresults
     */
    public function setRresDiscard($rresDiscard)
    {
        $this->RresDiscard = $rresDiscard;

        return $this;
    }

    /**
     * Get RresDiscard
     *
     * @return string 
     */
    public function getRresDiscard()
    {
        return $this->RresDiscard;
    }

    /**
     * Set RresUpdated
     *
     * @param string $rresUpdated
     * @return Raceresults
     */
    public function setRresUpdated($rresUpdated)
    {
        $this->RresUpdated = $rresUpdated;

        return $this;
    }

    /**
     * Get RresUpdated
     *
     * @return string 
     */
    public function getRresUpdated()
    {
        return $this->RresUpdated;
    }

    /**
     * Set RresRaceId
     *
     * @param integer $rresRaceId
     * @return Raceresults
     */
    public function setRresRaceId($rresRaceId)
    {
        $this->RresRaceId = $rresRaceId;

        return $this;
    }

    /**
     * Get RresRaceId
     *
     * @return integer 
     */
    public function getRresRaceId()
    {
        return $this->RresRaceId;
    }

    /**
     * Set RresBoatId
     *
     * @param integer $rresBoatId
     * @return Raceresults
     */
    public function setRresBoatId($rresBoatId)
    {
        $this->RresBoatId = $rresBoatId;

        return $this;
    }

    /**
     * Get RresBoatId
     *
     * @return integer 
     */
    public function getRresBoatId()
    {
        return $this->RresBoatId;
    }
}
