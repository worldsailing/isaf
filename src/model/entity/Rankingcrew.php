<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rankingcrew
 *
 * @ORM\Table(name="RankingCrew")
 * @ORM\Entity
 */
class Rankingcrew
{
    /**
     * @var integer
     *
     * @ORM\Column(name="RankId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $RankId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankSkipMembId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $RankSkipMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RankCrewMembId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $RankCrewMembId;



    /**
     * Set RankId
     *
     * @param integer $rankId
     * @return Rankingcrew
     */
    public function setRankId($rankId)
    {
        $this->RankId = $rankId;

        return $this;
    }

    /**
     * Get RankId
     *
     * @return integer 
     */
    public function getRankId()
    {
        return $this->RankId;
    }

    /**
     * Set RankSkipMembId
     *
     * @param integer $rankSkipMembId
     * @return Rankingcrew
     */
    public function setRankSkipMembId($rankSkipMembId)
    {
        $this->RankSkipMembId = $rankSkipMembId;

        return $this;
    }

    /**
     * Get RankSkipMembId
     *
     * @return integer 
     */
    public function getRankSkipMembId()
    {
        return $this->RankSkipMembId;
    }

    /**
     * Set RankCrewMembId
     *
     * @param integer $rankCrewMembId
     * @return Rankingcrew
     */
    public function setRankCrewMembId($rankCrewMembId)
    {
        $this->RankCrewMembId = $rankCrewMembId;

        return $this;
    }

    /**
     * Get RankCrewMembId
     *
     * @return integer 
     */
    public function getRankCrewMembId()
    {
        return $this->RankCrewMembId;
    }
}
