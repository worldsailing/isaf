<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eventdisciplines
 *
 * @ORM\Table(name="EventDisciplines")
 * @ORM\Entity
 */
class Eventdisciplines
{
    /**
     * @var integer
     *
     * @ORM\Column(name="DisId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $DisId;

    /**
     * @var string
     *
     * @ORM\Column(name="DisName", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $DisName;



    /**
     * Set DisId
     *
     * @param integer $disId
     * @return Eventdisciplines
     */
    public function setDisId($disId)
    {
        $this->DisId = $disId;

        return $this;
    }

    /**
     * Get DisId
     *
     * @return integer 
     */
    public function getDisId()
    {
        return $this->DisId;
    }

    /**
     * Set DisName
     *
     * @param string $disName
     * @return Eventdisciplines
     */
    public function setDisName($disName)
    {
        $this->DisName = $disName;

        return $this;
    }

    /**
     * Get DisName
     *
     * @return string 
     */
    public function getDisName()
    {
        return $this->DisName;
    }
}
