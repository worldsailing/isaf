<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formgroups
 *
 * @ORM\Table(name="formgroups")
 * @ORM\Entity
 */
class Formgroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="FmgpFrmId", type="integer", nullable=false)
     */
    protected $FmgpFrmId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmgpFmfsId", type="integer", nullable=false)
     */
    protected $FmgpFmfsId;

    /**
     * @var string
     *
     * @ORM\Column(name="FmgpName", type="string", length=100, nullable=false)
     */
    protected $FmgpName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FmgpCollapsed", type="boolean", nullable=false)
     */
    protected $FmgpCollapsed;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmgpOrder", type="integer", nullable=false)
     */
    protected $FmgpOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmgpOrderBackup", type="integer", nullable=false)
     */
    protected $FmgpOrderBackup;

    /**
     * @var integer
     *
     * @ORM\Column(name="FmgpId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $FmgpId;



    /**
     * Set FmgpFrmId
     *
     * @param integer $fmgpFrmId
     * @return Formgroups
     */
    public function setFmgpFrmId($fmgpFrmId)
    {
        $this->FmgpFrmId = $fmgpFrmId;

        return $this;
    }

    /**
     * Get FmgpFrmId
     *
     * @return integer 
     */
    public function getFmgpFrmId()
    {
        return $this->FmgpFrmId;
    }

    /**
     * Set FmgpFmfsId
     *
     * @param integer $fmgpFmfsId
     * @return Formgroups
     */
    public function setFmgpFmfsId($fmgpFmfsId)
    {
        $this->FmgpFmfsId = $fmgpFmfsId;

        return $this;
    }

    /**
     * Get FmgpFmfsId
     *
     * @return integer 
     */
    public function getFmgpFmfsId()
    {
        return $this->FmgpFmfsId;
    }

    /**
     * Set FmgpName
     *
     * @param string $fmgpName
     * @return Formgroups
     */
    public function setFmgpName($fmgpName)
    {
        $this->FmgpName = $fmgpName;

        return $this;
    }

    /**
     * Get FmgpName
     *
     * @return string 
     */
    public function getFmgpName()
    {
        return $this->FmgpName;
    }

    /**
     * Set FmgpCollapsed
     *
     * @param boolean $fmgpCollapsed
     * @return Formgroups
     */
    public function setFmgpCollapsed($fmgpCollapsed)
    {
        $this->FmgpCollapsed = $fmgpCollapsed;

        return $this;
    }

    /**
     * Get FmgpCollapsed
     *
     * @return boolean 
     */
    public function getFmgpCollapsed()
    {
        return $this->FmgpCollapsed;
    }

    /**
     * Set FmgpOrder
     *
     * @param integer $fmgpOrder
     * @return Formgroups
     */
    public function setFmgpOrder($fmgpOrder)
    {
        $this->FmgpOrder = $fmgpOrder;

        return $this;
    }

    /**
     * Get FmgpOrder
     *
     * @return integer 
     */
    public function getFmgpOrder()
    {
        return $this->FmgpOrder;
    }

    /**
     * Set FmgpOrderBackup
     *
     * @param integer $fmgpOrderBackup
     * @return Formgroups
     */
    public function setFmgpOrderBackup($fmgpOrderBackup)
    {
        $this->FmgpOrderBackup = $fmgpOrderBackup;

        return $this;
    }

    /**
     * Get FmgpOrderBackup
     *
     * @return integer 
     */
    public function getFmgpOrderBackup()
    {
        return $this->FmgpOrderBackup;
    }

    /**
     * Get FmgpId
     *
     * @return integer 
     */
    public function getFmgpId()
    {
        return $this->FmgpId;
    }
}
