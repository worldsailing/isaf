<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Buildersclass
 *
 * @ORM\Table(name="BuildersClass")
 * @ORM\Entity
 */
class Buildersclass
{
    /**
     * @var string
     *
     * @ORM\Column(name="BclsComponentType", type="string", nullable=true)
     */
    protected $BclsComponentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="BclsBuildOrgId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $BclsBuildOrgId;

    /**
     * @var integer
     *
     * @ORM\Column(name="BclsClassId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $BclsClassId;

    /**
     * @var string
     *
     * @ORM\Column(name="BclsClassType", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $BclsClassType;



    /**
     * Set BclsComponentType
     *
     * @param string $bclsComponentType
     * @return Buildersclass
     */
    public function setBclsComponentType($bclsComponentType)
    {
        $this->BclsComponentType = $bclsComponentType;

        return $this;
    }

    /**
     * Get BclsComponentType
     *
     * @return string 
     */
    public function getBclsComponentType()
    {
        return $this->BclsComponentType;
    }

    /**
     * Set BclsBuildOrgId
     *
     * @param integer $bclsBuildOrgId
     * @return Buildersclass
     */
    public function setBclsBuildOrgId($bclsBuildOrgId)
    {
        $this->BclsBuildOrgId = $bclsBuildOrgId;

        return $this;
    }

    /**
     * Get BclsBuildOrgId
     *
     * @return integer 
     */
    public function getBclsBuildOrgId()
    {
        return $this->BclsBuildOrgId;
    }

    /**
     * Set BclsClassId
     *
     * @param integer $bclsClassId
     * @return Buildersclass
     */
    public function setBclsClassId($bclsClassId)
    {
        $this->BclsClassId = $bclsClassId;

        return $this;
    }

    /**
     * Get BclsClassId
     *
     * @return integer 
     */
    public function getBclsClassId()
    {
        return $this->BclsClassId;
    }

    /**
     * Set BclsClassType
     *
     * @param string $bclsClassType
     * @return Buildersclass
     */
    public function setBclsClassType($bclsClassType)
    {
        $this->BclsClassType = $bclsClassType;

        return $this;
    }

    /**
     * Get BclsClassType
     *
     * @return string 
     */
    public function getBclsClassType()
    {
        return $this->BclsClassType;
    }
}
