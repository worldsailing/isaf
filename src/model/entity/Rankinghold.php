<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rankinghold
 *
 * @ORM\Table(name="RankingHold")
 * @ORM\Entity
 */
class Rankinghold
{
    /**
     * @var integer
     *
     * @ORM\Column(name="HoldDisId", type="integer", nullable=false)
     */
    protected $HoldDisId;

    /**
     * @var integer
     *
     * @ORM\Column(name="HoldTypeId", type="integer", nullable=false)
     */
    protected $HoldTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="HoldClassId", type="integer", nullable=false)
     */
    protected $HoldClassId;

    /**
     * @var integer
     *
     * @ORM\Column(name="HoldMembId", type="integer", nullable=false)
     */
    protected $HoldMembId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="HoldStart", type="date", nullable=false)
     */
    protected $HoldStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="HoldRecoveryStart", type="date", nullable=false)
     */
    protected $HoldRecoveryStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="HoldEnd", type="date", nullable=false)
     */
    protected $HoldEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="HoldId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $HoldId;



    /**
     * Set HoldDisId
     *
     * @param integer $holdDisId
     * @return Rankinghold
     */
    public function setHoldDisId($holdDisId)
    {
        $this->HoldDisId = $holdDisId;

        return $this;
    }

    /**
     * Get HoldDisId
     *
     * @return integer 
     */
    public function getHoldDisId()
    {
        return $this->HoldDisId;
    }

    /**
     * Set HoldTypeId
     *
     * @param integer $holdTypeId
     * @return Rankinghold
     */
    public function setHoldTypeId($holdTypeId)
    {
        $this->HoldTypeId = $holdTypeId;

        return $this;
    }

    /**
     * Get HoldTypeId
     *
     * @return integer 
     */
    public function getHoldTypeId()
    {
        return $this->HoldTypeId;
    }

    /**
     * Set HoldClassId
     *
     * @param integer $holdClassId
     * @return Rankinghold
     */
    public function setHoldClassId($holdClassId)
    {
        $this->HoldClassId = $holdClassId;

        return $this;
    }

    /**
     * Get HoldClassId
     *
     * @return integer 
     */
    public function getHoldClassId()
    {
        return $this->HoldClassId;
    }

    /**
     * Set HoldMembId
     *
     * @param integer $holdMembId
     * @return Rankinghold
     */
    public function setHoldMembId($holdMembId)
    {
        $this->HoldMembId = $holdMembId;

        return $this;
    }

    /**
     * Get HoldMembId
     *
     * @return integer 
     */
    public function getHoldMembId()
    {
        return $this->HoldMembId;
    }

    /**
     * Set HoldStart
     *
     * @param \DateTime $holdStart
     * @return Rankinghold
     */
    public function setHoldStart($holdStart)
    {
        $this->HoldStart = $holdStart;

        return $this;
    }

    /**
     * Get HoldStart
     *
     * @return \DateTime 
     */
    public function getHoldStart()
    {
        return $this->HoldStart;
    }

    /**
     * Set HoldRecoveryStart
     *
     * @param \DateTime $holdRecoveryStart
     * @return Rankinghold
     */
    public function setHoldRecoveryStart($holdRecoveryStart)
    {
        $this->HoldRecoveryStart = $holdRecoveryStart;

        return $this;
    }

    /**
     * Get HoldRecoveryStart
     *
     * @return \DateTime 
     */
    public function getHoldRecoveryStart()
    {
        return $this->HoldRecoveryStart;
    }

    /**
     * Set HoldEnd
     *
     * @param \DateTime $holdEnd
     * @return Rankinghold
     */
    public function setHoldEnd($holdEnd)
    {
        $this->HoldEnd = $holdEnd;

        return $this;
    }

    /**
     * Get HoldEnd
     *
     * @return \DateTime 
     */
    public function getHoldEnd()
    {
        return $this->HoldEnd;
    }

    /**
     * Get HoldId
     *
     * @return integer 
     */
    public function getHoldId()
    {
        return $this->HoldId;
    }
}
