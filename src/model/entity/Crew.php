<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Crew
 *
 * @ORM\Table(name="Crew", indexes={@ORM\Index(name="idCrewBoatMembId", columns={"CrewBoatId", "CrewMembId"}), @ORM\Index(name="idCrewBoatId", columns={"CrewBoatId"}), @ORM\Index(name="CrewMembId", columns={"CrewMembId"})})
 * @ORM\Entity
 */
class Crew
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CrewMembId", type="integer", nullable=true)
     */
    protected $CrewMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CrewNonMembId", type="integer", nullable=true)
     */
    protected $CrewNonMembId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="OldCrewSkipper", type="boolean", nullable=false)
     */
    protected $OldCrewSkipper;

    /**
     * @var integer
     *
     * @ORM\Column(name="CrewPosition", type="integer", nullable=true)
     */
    protected $CrewPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="CrewUpdated", type="string", nullable=false)
     */
    protected $CrewUpdated;

    /**
     * @var string
     *
     * @ORM\Column(name="CrewSrcId", type="string", length=45, nullable=true)
     */
    protected $CrewSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CrewCtryId", type="integer", nullable=true)
     */
    protected $CrewCtryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CrewBoatId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CrewBoatId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CrewIndex", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $CrewIndex;



    /**
     * Set CrewMembId
     *
     * @param integer $crewMembId
     * @return Crew
     */
    public function setCrewMembId($crewMembId)
    {
        $this->CrewMembId = $crewMembId;

        return $this;
    }

    /**
     * Get CrewMembId
     *
     * @return integer 
     */
    public function getCrewMembId()
    {
        return $this->CrewMembId;
    }

    /**
     * Set CrewNonMembId
     *
     * @param integer $crewNonMembId
     * @return Crew
     */
    public function setCrewNonMembId($crewNonMembId)
    {
        $this->CrewNonMembId = $crewNonMembId;

        return $this;
    }

    /**
     * Get CrewNonMembId
     *
     * @return integer 
     */
    public function getCrewNonMembId()
    {
        return $this->CrewNonMembId;
    }

    /**
     * Set OldCrewSkipper
     *
     * @param boolean $oldCrewSkipper
     * @return Crew
     */
    public function setOldCrewSkipper($oldCrewSkipper)
    {
        $this->OldCrewSkipper = $oldCrewSkipper;

        return $this;
    }

    /**
     * Get OldCrewSkipper
     *
     * @return boolean 
     */
    public function getOldCrewSkipper()
    {
        return $this->OldCrewSkipper;
    }

    /**
     * Set CrewPosition
     *
     * @param integer $crewPosition
     * @return Crew
     */
    public function setCrewPosition($crewPosition)
    {
        $this->CrewPosition = $crewPosition;

        return $this;
    }

    /**
     * Get CrewPosition
     *
     * @return integer 
     */
    public function getCrewPosition()
    {
        return $this->CrewPosition;
    }

    /**
     * Set CrewUpdated
     *
     * @param string $crewUpdated
     * @return Crew
     */
    public function setCrewUpdated($crewUpdated)
    {
        $this->CrewUpdated = $crewUpdated;

        return $this;
    }

    /**
     * Get CrewUpdated
     *
     * @return string 
     */
    public function getCrewUpdated()
    {
        return $this->CrewUpdated;
    }

    /**
     * Set CrewSrcId
     *
     * @param string $crewSrcId
     * @return Crew
     */
    public function setCrewSrcId($crewSrcId)
    {
        $this->CrewSrcId = $crewSrcId;

        return $this;
    }

    /**
     * Get CrewSrcId
     *
     * @return string 
     */
    public function getCrewSrcId()
    {
        return $this->CrewSrcId;
    }

    /**
     * Set CrewCtryId
     *
     * @param integer $crewCtryId
     * @return Crew
     */
    public function setCrewCtryId($crewCtryId)
    {
        $this->CrewCtryId = $crewCtryId;

        return $this;
    }

    /**
     * Get CrewCtryId
     *
     * @return integer 
     */
    public function getCrewCtryId()
    {
        return $this->CrewCtryId;
    }

    /**
     * Set CrewBoatId
     *
     * @param integer $crewBoatId
     * @return Crew
     */
    public function setCrewBoatId($crewBoatId)
    {
        $this->CrewBoatId = $crewBoatId;

        return $this;
    }

    /**
     * Get CrewBoatId
     *
     * @return integer 
     */
    public function getCrewBoatId()
    {
        return $this->CrewBoatId;
    }

    /**
     * Set CrewIndex
     *
     * @param integer $crewIndex
     * @return Crew
     */
    public function setCrewIndex($crewIndex)
    {
        $this->CrewIndex = $crewIndex;

        return $this;
    }

    /**
     * Get CrewIndex
     *
     * @return integer 
     */
    public function getCrewIndex()
    {
        return $this->CrewIndex;
    }
}
