<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Templates
 *
 * @ORM\Table(name="templates")
 * @ORM\Entity
 */
class Templates
{
    /**
     * @var string
     *
     * @ORM\Column(name="TpltName", type="string", length=100, nullable=false)
     */
    protected $TpltName;

    /**
     * @var string
     *
     * @ORM\Column(name="TpltEntity", type="string", nullable=false)
     */
    protected $TpltEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="TpltLang", type="string", length=10, nullable=true)
     */
    protected $TpltLang;

    /**
     * @var string
     *
     * @ORM\Column(name="TpltDescription", type="text", length=65535, nullable=true)
     */
    protected $TpltDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="TpltTemplate", type="text", length=65535, nullable=false)
     */
    protected $TpltTemplate;

    /**
     * @var integer
     *
     * @ORM\Column(name="TpltParentId", type="integer", nullable=true)
     */
    protected $TpltParentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TpltId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $TpltId;



    /**
     * Set TpltName
     *
     * @param string $tpltName
     * @return Templates
     */
    public function setTpltName($tpltName)
    {
        $this->TpltName = $tpltName;

        return $this;
    }

    /**
     * Get TpltName
     *
     * @return string 
     */
    public function getTpltName()
    {
        return $this->TpltName;
    }

    /**
     * Set TpltEntity
     *
     * @param string $tpltEntity
     * @return Templates
     */
    public function setTpltEntity($tpltEntity)
    {
        $this->TpltEntity = $tpltEntity;

        return $this;
    }

    /**
     * Get TpltEntity
     *
     * @return string 
     */
    public function getTpltEntity()
    {
        return $this->TpltEntity;
    }

    /**
     * Set TpltLang
     *
     * @param string $tpltLang
     * @return Templates
     */
    public function setTpltLang($tpltLang)
    {
        $this->TpltLang = $tpltLang;

        return $this;
    }

    /**
     * Get TpltLang
     *
     * @return string 
     */
    public function getTpltLang()
    {
        return $this->TpltLang;
    }

    /**
     * Set TpltDescription
     *
     * @param string $tpltDescription
     * @return Templates
     */
    public function setTpltDescription($tpltDescription)
    {
        $this->TpltDescription = $tpltDescription;

        return $this;
    }

    /**
     * Get TpltDescription
     *
     * @return string 
     */
    public function getTpltDescription()
    {
        return $this->TpltDescription;
    }

    /**
     * Set TpltTemplate
     *
     * @param string $tpltTemplate
     * @return Templates
     */
    public function setTpltTemplate($tpltTemplate)
    {
        $this->TpltTemplate = $tpltTemplate;

        return $this;
    }

    /**
     * Get TpltTemplate
     *
     * @return string 
     */
    public function getTpltTemplate()
    {
        return $this->TpltTemplate;
    }

    /**
     * Set TpltParentId
     *
     * @param integer $tpltParentId
     * @return Templates
     */
    public function setTpltParentId($tpltParentId)
    {
        $this->TpltParentId = $tpltParentId;

        return $this;
    }

    /**
     * Get TpltParentId
     *
     * @return integer 
     */
    public function getTpltParentId()
    {
        return $this->TpltParentId;
    }

    /**
     * Get TpltId
     *
     * @return integer 
     */
    public function getTpltId()
    {
        return $this->TpltId;
    }
}
