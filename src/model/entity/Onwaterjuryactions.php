<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Onwaterjuryactions
 *
 * @ORM\Table(name="OnWaterJuryActions")
 * @ORM\Entity
 */
class Onwaterjuryactions
{
    /**
     * @var string
     *
     * @ORM\Column(name="WatSource", type="string", length=20, nullable=true)
     */
    protected $WatSource;

    /**
     * @var string
     *
     * @ORM\Column(name="WatSrcId", type="string", length=50, nullable=true)
     */
    protected $WatSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="WatRgtaId", type="integer", nullable=false)
     */
    protected $WatRgtaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="WatBoatId", type="integer", nullable=false)
     */
    protected $WatBoatId;

    /**
     * @var integer
     *
     * @ORM\Column(name="WatRaceId", type="integer", nullable=false)
     */
    protected $WatRaceId;

    /**
     * @var string
     *
     * @ORM\Column(name="WatCompAction", type="string", length=255, nullable=true)
     */
    protected $WatCompAction;

    /**
     * @var string
     *
     * @ORM\Column(name="WatRule", type="string", length=255, nullable=true)
     */
    protected $WatRule;

    /**
     * @var string
     *
     * @ORM\Column(name="WatJuryAction", type="string", length=255, nullable=true)
     */
    protected $WatJuryAction;

    /**
     * @var integer
     *
     * @ORM\Column(name="WatInfNo", type="integer", nullable=false)
     */
    protected $WatInfNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="WatId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $WatId;



    /**
     * Set WatSource
     *
     * @param string $watSource
     * @return Onwaterjuryactions
     */
    public function setWatSource($watSource)
    {
        $this->WatSource = $watSource;

        return $this;
    }

    /**
     * Get WatSource
     *
     * @return string 
     */
    public function getWatSource()
    {
        return $this->WatSource;
    }

    /**
     * Set WatSrcId
     *
     * @param string $watSrcId
     * @return Onwaterjuryactions
     */
    public function setWatSrcId($watSrcId)
    {
        $this->WatSrcId = $watSrcId;

        return $this;
    }

    /**
     * Get WatSrcId
     *
     * @return string 
     */
    public function getWatSrcId()
    {
        return $this->WatSrcId;
    }

    /**
     * Set WatRgtaId
     *
     * @param integer $watRgtaId
     * @return Onwaterjuryactions
     */
    public function setWatRgtaId($watRgtaId)
    {
        $this->WatRgtaId = $watRgtaId;

        return $this;
    }

    /**
     * Get WatRgtaId
     *
     * @return integer 
     */
    public function getWatRgtaId()
    {
        return $this->WatRgtaId;
    }

    /**
     * Set WatBoatId
     *
     * @param integer $watBoatId
     * @return Onwaterjuryactions
     */
    public function setWatBoatId($watBoatId)
    {
        $this->WatBoatId = $watBoatId;

        return $this;
    }

    /**
     * Get WatBoatId
     *
     * @return integer 
     */
    public function getWatBoatId()
    {
        return $this->WatBoatId;
    }

    /**
     * Set WatRaceId
     *
     * @param integer $watRaceId
     * @return Onwaterjuryactions
     */
    public function setWatRaceId($watRaceId)
    {
        $this->WatRaceId = $watRaceId;

        return $this;
    }

    /**
     * Get WatRaceId
     *
     * @return integer 
     */
    public function getWatRaceId()
    {
        return $this->WatRaceId;
    }

    /**
     * Set WatCompAction
     *
     * @param string $watCompAction
     * @return Onwaterjuryactions
     */
    public function setWatCompAction($watCompAction)
    {
        $this->WatCompAction = $watCompAction;

        return $this;
    }

    /**
     * Get WatCompAction
     *
     * @return string 
     */
    public function getWatCompAction()
    {
        return $this->WatCompAction;
    }

    /**
     * Set WatRule
     *
     * @param string $watRule
     * @return Onwaterjuryactions
     */
    public function setWatRule($watRule)
    {
        $this->WatRule = $watRule;

        return $this;
    }

    /**
     * Get WatRule
     *
     * @return string 
     */
    public function getWatRule()
    {
        return $this->WatRule;
    }

    /**
     * Set WatJuryAction
     *
     * @param string $watJuryAction
     * @return Onwaterjuryactions
     */
    public function setWatJuryAction($watJuryAction)
    {
        $this->WatJuryAction = $watJuryAction;

        return $this;
    }

    /**
     * Get WatJuryAction
     *
     * @return string 
     */
    public function getWatJuryAction()
    {
        return $this->WatJuryAction;
    }

    /**
     * Set WatInfNo
     *
     * @param integer $watInfNo
     * @return Onwaterjuryactions
     */
    public function setWatInfNo($watInfNo)
    {
        $this->WatInfNo = $watInfNo;

        return $this;
    }

    /**
     * Get WatInfNo
     *
     * @return integer 
     */
    public function getWatInfNo()
    {
        return $this->WatInfNo;
    }

    /**
     * Get WatId
     *
     * @return integer 
     */
    public function getWatId()
    {
        return $this->WatId;
    }
}
