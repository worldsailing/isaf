<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Osrentry
 *
 * @ORM\Table(name="OSREntry")
 * @ORM\Entity
 */
class Osrentry
{
    /**
     * @var string
     *
     * @ORM\Column(name="OSRType", type="string", length=2, nullable=true)
     */
    protected $OSRType;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFontColorScheme", type="string", length=20, nullable=true)
     */
    protected $OSRFontColorScheme;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFontItalic", type="string", length=2, nullable=true)
     */
    protected $OSRFontItalic;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFontBold", type="string", length=2, nullable=true)
     */
    protected $OSRFontBold;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRHeading", type="string", length=200, nullable=true)
     */
    protected $OSRHeading;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRContent", type="text", length=65535, nullable=true)
     */
    protected $OSRContent;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRChecklist", type="text", length=65535, nullable=true)
     */
    protected $OSRChecklist;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRChecklistOrder", type="string", length=20, nullable=true)
     */
    protected $OSRChecklistOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFormat", type="text", length=65535, nullable=true)
     */
    protected $OSRFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRCatOnComplete", type="string", length=200, nullable=true)
     */
    protected $OSRCatOnComplete;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRCatOnRorc", type="string", length=200, nullable=true)
     */
    protected $OSRCatOnRorc;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMo0", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMo0;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMo1", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMo1;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMo2", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMo2;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMo3", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMo3;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMo3life", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMo3life;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMo4", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMo4;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMu0", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMu0;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMu1", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMu1;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMu2", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMu2;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMu3", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMu3;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMu3life", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMu3life;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagMu4", type="string", length=2, nullable=true)
     */
    protected $OSRFlagMu4;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagAll", type="string", length=2, nullable=true)
     */
    protected $OSRFlagAll;

    /**
     * @var string
     *
     * @ORM\Column(name="OSRFlagRORC", type="string", length=2, nullable=true)
     */
    protected $OSRFlagRORC;

    /**
     * @var integer
     *
     * @ORM\Column(name="OSRId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $OSRId;



    /**
     * Set OSRType
     *
     * @param string $oSRType
     * @return Osrentry
     */
    public function setOSRType($oSRType)
    {
        $this->OSRType = $oSRType;

        return $this;
    }

    /**
     * Get OSRType
     *
     * @return string 
     */
    public function getOSRType()
    {
        return $this->OSRType;
    }

    /**
     * Set OSRFontColorScheme
     *
     * @param string $oSRFontColorScheme
     * @return Osrentry
     */
    public function setOSRFontColorScheme($oSRFontColorScheme)
    {
        $this->OSRFontColorScheme = $oSRFontColorScheme;

        return $this;
    }

    /**
     * Get OSRFontColorScheme
     *
     * @return string 
     */
    public function getOSRFontColorScheme()
    {
        return $this->OSRFontColorScheme;
    }

    /**
     * Set OSRFontItalic
     *
     * @param string $oSRFontItalic
     * @return Osrentry
     */
    public function setOSRFontItalic($oSRFontItalic)
    {
        $this->OSRFontItalic = $oSRFontItalic;

        return $this;
    }

    /**
     * Get OSRFontItalic
     *
     * @return string 
     */
    public function getOSRFontItalic()
    {
        return $this->OSRFontItalic;
    }

    /**
     * Set OSRFontBold
     *
     * @param string $oSRFontBold
     * @return Osrentry
     */
    public function setOSRFontBold($oSRFontBold)
    {
        $this->OSRFontBold = $oSRFontBold;

        return $this;
    }

    /**
     * Get OSRFontBold
     *
     * @return string 
     */
    public function getOSRFontBold()
    {
        return $this->OSRFontBold;
    }

    /**
     * Set OSRHeading
     *
     * @param string $oSRHeading
     * @return Osrentry
     */
    public function setOSRHeading($oSRHeading)
    {
        $this->OSRHeading = $oSRHeading;

        return $this;
    }

    /**
     * Get OSRHeading
     *
     * @return string 
     */
    public function getOSRHeading()
    {
        return $this->OSRHeading;
    }

    /**
     * Set OSRContent
     *
     * @param string $oSRContent
     * @return Osrentry
     */
    public function setOSRContent($oSRContent)
    {
        $this->OSRContent = $oSRContent;

        return $this;
    }

    /**
     * Get OSRContent
     *
     * @return string 
     */
    public function getOSRContent()
    {
        return $this->OSRContent;
    }

    /**
     * Set OSRChecklist
     *
     * @param string $oSRChecklist
     * @return Osrentry
     */
    public function setOSRChecklist($oSRChecklist)
    {
        $this->OSRChecklist = $oSRChecklist;

        return $this;
    }

    /**
     * Get OSRChecklist
     *
     * @return string 
     */
    public function getOSRChecklist()
    {
        return $this->OSRChecklist;
    }

    /**
     * Set OSRChecklistOrder
     *
     * @param string $oSRChecklistOrder
     * @return Osrentry
     */
    public function setOSRChecklistOrder($oSRChecklistOrder)
    {
        $this->OSRChecklistOrder = $oSRChecklistOrder;

        return $this;
    }

    /**
     * Get OSRChecklistOrder
     *
     * @return string 
     */
    public function getOSRChecklistOrder()
    {
        return $this->OSRChecklistOrder;
    }

    /**
     * Set OSRFormat
     *
     * @param string $oSRFormat
     * @return Osrentry
     */
    public function setOSRFormat($oSRFormat)
    {
        $this->OSRFormat = $oSRFormat;

        return $this;
    }

    /**
     * Get OSRFormat
     *
     * @return string 
     */
    public function getOSRFormat()
    {
        return $this->OSRFormat;
    }

    /**
     * Set OSRCatOnComplete
     *
     * @param string $oSRCatOnComplete
     * @return Osrentry
     */
    public function setOSRCatOnComplete($oSRCatOnComplete)
    {
        $this->OSRCatOnComplete = $oSRCatOnComplete;

        return $this;
    }

    /**
     * Get OSRCatOnComplete
     *
     * @return string 
     */
    public function getOSRCatOnComplete()
    {
        return $this->OSRCatOnComplete;
    }

    /**
     * Set OSRCatOnRorc
     *
     * @param string $oSRCatOnRorc
     * @return Osrentry
     */
    public function setOSRCatOnRorc($oSRCatOnRorc)
    {
        $this->OSRCatOnRorc = $oSRCatOnRorc;

        return $this;
    }

    /**
     * Get OSRCatOnRorc
     *
     * @return string 
     */
    public function getOSRCatOnRorc()
    {
        return $this->OSRCatOnRorc;
    }

    /**
     * Set OSRFlagMo0
     *
     * @param string $oSRFlagMo0
     * @return Osrentry
     */
    public function setOSRFlagMo0($oSRFlagMo0)
    {
        $this->OSRFlagMo0 = $oSRFlagMo0;

        return $this;
    }

    /**
     * Get OSRFlagMo0
     *
     * @return string 
     */
    public function getOSRFlagMo0()
    {
        return $this->OSRFlagMo0;
    }

    /**
     * Set OSRFlagMo1
     *
     * @param string $oSRFlagMo1
     * @return Osrentry
     */
    public function setOSRFlagMo1($oSRFlagMo1)
    {
        $this->OSRFlagMo1 = $oSRFlagMo1;

        return $this;
    }

    /**
     * Get OSRFlagMo1
     *
     * @return string 
     */
    public function getOSRFlagMo1()
    {
        return $this->OSRFlagMo1;
    }

    /**
     * Set OSRFlagMo2
     *
     * @param string $oSRFlagMo2
     * @return Osrentry
     */
    public function setOSRFlagMo2($oSRFlagMo2)
    {
        $this->OSRFlagMo2 = $oSRFlagMo2;

        return $this;
    }

    /**
     * Get OSRFlagMo2
     *
     * @return string 
     */
    public function getOSRFlagMo2()
    {
        return $this->OSRFlagMo2;
    }

    /**
     * Set OSRFlagMo3
     *
     * @param string $oSRFlagMo3
     * @return Osrentry
     */
    public function setOSRFlagMo3($oSRFlagMo3)
    {
        $this->OSRFlagMo3 = $oSRFlagMo3;

        return $this;
    }

    /**
     * Get OSRFlagMo3
     *
     * @return string 
     */
    public function getOSRFlagMo3()
    {
        return $this->OSRFlagMo3;
    }

    /**
     * Set OSRFlagMo3life
     *
     * @param string $oSRFlagMo3life
     * @return Osrentry
     */
    public function setOSRFlagMo3life($oSRFlagMo3life)
    {
        $this->OSRFlagMo3life = $oSRFlagMo3life;

        return $this;
    }

    /**
     * Get OSRFlagMo3life
     *
     * @return string 
     */
    public function getOSRFlagMo3life()
    {
        return $this->OSRFlagMo3life;
    }

    /**
     * Set OSRFlagMo4
     *
     * @param string $oSRFlagMo4
     * @return Osrentry
     */
    public function setOSRFlagMo4($oSRFlagMo4)
    {
        $this->OSRFlagMo4 = $oSRFlagMo4;

        return $this;
    }

    /**
     * Get OSRFlagMo4
     *
     * @return string 
     */
    public function getOSRFlagMo4()
    {
        return $this->OSRFlagMo4;
    }

    /**
     * Set OSRFlagMu0
     *
     * @param string $oSRFlagMu0
     * @return Osrentry
     */
    public function setOSRFlagMu0($oSRFlagMu0)
    {
        $this->OSRFlagMu0 = $oSRFlagMu0;

        return $this;
    }

    /**
     * Get OSRFlagMu0
     *
     * @return string 
     */
    public function getOSRFlagMu0()
    {
        return $this->OSRFlagMu0;
    }

    /**
     * Set OSRFlagMu1
     *
     * @param string $oSRFlagMu1
     * @return Osrentry
     */
    public function setOSRFlagMu1($oSRFlagMu1)
    {
        $this->OSRFlagMu1 = $oSRFlagMu1;

        return $this;
    }

    /**
     * Get OSRFlagMu1
     *
     * @return string 
     */
    public function getOSRFlagMu1()
    {
        return $this->OSRFlagMu1;
    }

    /**
     * Set OSRFlagMu2
     *
     * @param string $oSRFlagMu2
     * @return Osrentry
     */
    public function setOSRFlagMu2($oSRFlagMu2)
    {
        $this->OSRFlagMu2 = $oSRFlagMu2;

        return $this;
    }

    /**
     * Get OSRFlagMu2
     *
     * @return string 
     */
    public function getOSRFlagMu2()
    {
        return $this->OSRFlagMu2;
    }

    /**
     * Set OSRFlagMu3
     *
     * @param string $oSRFlagMu3
     * @return Osrentry
     */
    public function setOSRFlagMu3($oSRFlagMu3)
    {
        $this->OSRFlagMu3 = $oSRFlagMu3;

        return $this;
    }

    /**
     * Get OSRFlagMu3
     *
     * @return string 
     */
    public function getOSRFlagMu3()
    {
        return $this->OSRFlagMu3;
    }

    /**
     * Set OSRFlagMu3life
     *
     * @param string $oSRFlagMu3life
     * @return Osrentry
     */
    public function setOSRFlagMu3life($oSRFlagMu3life)
    {
        $this->OSRFlagMu3life = $oSRFlagMu3life;

        return $this;
    }

    /**
     * Get OSRFlagMu3life
     *
     * @return string 
     */
    public function getOSRFlagMu3life()
    {
        return $this->OSRFlagMu3life;
    }

    /**
     * Set OSRFlagMu4
     *
     * @param string $oSRFlagMu4
     * @return Osrentry
     */
    public function setOSRFlagMu4($oSRFlagMu4)
    {
        $this->OSRFlagMu4 = $oSRFlagMu4;

        return $this;
    }

    /**
     * Get OSRFlagMu4
     *
     * @return string 
     */
    public function getOSRFlagMu4()
    {
        return $this->OSRFlagMu4;
    }

    /**
     * Set OSRFlagAll
     *
     * @param string $oSRFlagAll
     * @return Osrentry
     */
    public function setOSRFlagAll($oSRFlagAll)
    {
        $this->OSRFlagAll = $oSRFlagAll;

        return $this;
    }

    /**
     * Get OSRFlagAll
     *
     * @return string 
     */
    public function getOSRFlagAll()
    {
        return $this->OSRFlagAll;
    }

    /**
     * Set OSRFlagRORC
     *
     * @param string $oSRFlagRORC
     * @return Osrentry
     */
    public function setOSRFlagRORC($oSRFlagRORC)
    {
        $this->OSRFlagRORC = $oSRFlagRORC;

        return $this;
    }

    /**
     * Get OSRFlagRORC
     *
     * @return string 
     */
    public function getOSRFlagRORC()
    {
        return $this->OSRFlagRORC;
    }

    /**
     * Get OSRId
     *
     * @return integer 
     */
    public function getOSRId()
    {
        return $this->OSRId;
    }
}
