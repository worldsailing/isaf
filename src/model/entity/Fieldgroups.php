<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fieldgroups
 *
 * @ORM\Table(name="fieldgroups")
 * @ORM\Entity
 */
class Fieldgroups
{
    /**
     * @var string
     *
     * @ORM\Column(name="GrpTable", type="string", length=100, nullable=true)
     */
    protected $GrpTable;

    /**
     * @var string
     *
     * @ORM\Column(name="GrpName", type="string", length=100, nullable=false)
     */
    protected $GrpName;

    /**
     * @var integer
     *
     * @ORM\Column(name="GrpOrder", type="integer", nullable=false)
     */
    protected $GrpOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="GrpId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $GrpId;



    /**
     * Set GrpTable
     *
     * @param string $grpTable
     * @return Fieldgroups
     */
    public function setGrpTable($grpTable)
    {
        $this->GrpTable = $grpTable;

        return $this;
    }

    /**
     * Get GrpTable
     *
     * @return string 
     */
    public function getGrpTable()
    {
        return $this->GrpTable;
    }

    /**
     * Set GrpName
     *
     * @param string $grpName
     * @return Fieldgroups
     */
    public function setGrpName($grpName)
    {
        $this->GrpName = $grpName;

        return $this;
    }

    /**
     * Get GrpName
     *
     * @return string 
     */
    public function getGrpName()
    {
        return $this->GrpName;
    }

    /**
     * Set GrpOrder
     *
     * @param integer $grpOrder
     * @return Fieldgroups
     */
    public function setGrpOrder($grpOrder)
    {
        $this->GrpOrder = $grpOrder;

        return $this;
    }

    /**
     * Get GrpOrder
     *
     * @return integer 
     */
    public function getGrpOrder()
    {
        return $this->GrpOrder;
    }

    /**
     * Get GrpId
     *
     * @return integer 
     */
    public function getGrpId()
    {
        return $this->GrpId;
    }
}
