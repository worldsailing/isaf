<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationreviewer
 *
 * @ORM\Table(name="ClassificationReviewer", indexes={@ORM\Index(name="rvwr_actn", columns={"CfcnRvwrActionId"}), @ORM\Index(name="rvwr_biog", columns={"CfcnRvwrMembId"})})
 * @ORM\Entity
 */
class Classificationreviewer
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnRvwrDateAssigned", type="datetime", nullable=true)
     */
    protected $CfcnRvwrDateAssigned;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnRvwrDateUnassigned", type="datetime", nullable=true)
     */
    protected $CfcnRvwrDateUnassigned;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CfcnRvwrIsChairman", type="boolean", nullable=true)
     */
    protected $CfcnRvwrIsChairman;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnRvwrAssignedBy", type="integer", nullable=true)
     */
    protected $CfcnRvwrAssignedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnRvwrUnassignedBy", type="integer", nullable=true)
     */
    protected $CfcnRvwrUnassignedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $Id;

    /**
     * @var \worldsailing\Isaf\model\Entity\Memberbiogs
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Memberbiogs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnRvwrMembId", referencedColumnName="BiogMembId")
     * })
     */
    protected $cfcnRvwrMembId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnRvwrActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnRvwrActionId;



    /**
     * Set CfcnRvwrDateAssigned
     *
     * @param \DateTime $cfcnRvwrDateAssigned
     * @return Classificationreviewer
     */
    public function setCfcnRvwrDateAssigned($cfcnRvwrDateAssigned)
    {
        $this->CfcnRvwrDateAssigned = $cfcnRvwrDateAssigned;

        return $this;
    }

    /**
     * Get CfcnRvwrDateAssigned
     *
     * @return \DateTime 
     */
    public function getCfcnRvwrDateAssigned()
    {
        return $this->CfcnRvwrDateAssigned;
    }

    /**
     * Set CfcnRvwrDateUnassigned
     *
     * @param \DateTime $cfcnRvwrDateUnassigned
     * @return Classificationreviewer
     */
    public function setCfcnRvwrDateUnassigned($cfcnRvwrDateUnassigned)
    {
        $this->CfcnRvwrDateUnassigned = $cfcnRvwrDateUnassigned;

        return $this;
    }

    /**
     * Get CfcnRvwrDateUnassigned
     *
     * @return \DateTime 
     */
    public function getCfcnRvwrDateUnassigned()
    {
        return $this->CfcnRvwrDateUnassigned;
    }

    /**
     * Set CfcnRvwrIsChairman
     *
     * @param boolean $cfcnRvwrIsChairman
     * @return Classificationreviewer
     */
    public function setCfcnRvwrIsChairman($cfcnRvwrIsChairman)
    {
        $this->CfcnRvwrIsChairman = $cfcnRvwrIsChairman;

        return $this;
    }

    /**
     * Get CfcnRvwrIsChairman
     *
     * @return boolean 
     */
    public function getCfcnRvwrIsChairman()
    {
        return $this->CfcnRvwrIsChairman;
    }

    /**
     * Set CfcnRvwrAssignedBy
     *
     * @param integer $cfcnRvwrAssignedBy
     * @return Classificationreviewer
     */
    public function setCfcnRvwrAssignedBy($cfcnRvwrAssignedBy)
    {
        $this->CfcnRvwrAssignedBy = $cfcnRvwrAssignedBy;

        return $this;
    }

    /**
     * Get CfcnRvwrAssignedBy
     *
     * @return integer 
     */
    public function getCfcnRvwrAssignedBy()
    {
        return $this->CfcnRvwrAssignedBy;
    }

    /**
     * Set CfcnRvwrUnassignedBy
     *
     * @param integer $cfcnRvwrUnassignedBy
     * @return Classificationreviewer
     */
    public function setCfcnRvwrUnassignedBy($cfcnRvwrUnassignedBy)
    {
        $this->CfcnRvwrUnassignedBy = $cfcnRvwrUnassignedBy;

        return $this;
    }

    /**
     * Get CfcnRvwrUnassignedBy
     *
     * @return integer 
     */
    public function getCfcnRvwrUnassignedBy()
    {
        return $this->CfcnRvwrUnassignedBy;
    }

    /**
     * Get Id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * Set cfcnRvwrMembId
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $cfcnRvwrMembId
     * @return Classificationreviewer
     */
    public function setCfcnRvwrMembId(\worldsailing\Isaf\model\Entity\Memberbiogs $cfcnRvwrMembId = null)
    {
        $this->cfcnRvwrMembId = $cfcnRvwrMembId;

        return $this;
    }

    /**
     * Get cfcnRvwrMembId
     *
     * @return \worldsailing\Isaf\model\Entity\Memberbiogs
     */
    public function getCfcnRvwrMembId()
    {
        return $this->cfcnRvwrMembId;
    }

    /**
     * Set cfcnRvwrActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnRvwrActionId
     * @return Classificationreviewer
     */
    public function setCfcnRvwrActionId(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnRvwrActionId = null)
    {
        $this->cfcnRvwrActionId = $cfcnRvwrActionId;

        return $this;
    }

    /**
     * Get cfcnRvwrActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnRvwrActionId()
    {
        return $this->cfcnRvwrActionId;
    }
}
