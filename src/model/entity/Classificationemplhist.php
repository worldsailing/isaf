<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationemplhist
 *
 * @ORM\Table(name="ClassificationEmplHist", indexes={@ORM\Index(name="fk_ehist_appl_1", columns={"CfcnEmplHistAppId"})})
 * @ORM\Entity
 */
class Classificationemplhist
{
    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEmplHistStatusType", type="string", length=20, nullable=true)
     */
    protected $CfcnEmplHistStatusType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnEmplHistDateFrom", type="date", nullable=true)
     */
    protected $CfcnEmplHistDateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnEmplHistDateTo", type="date", nullable=true)
     */
    protected $CfcnEmplHistDateTo;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEmplHistBusName", type="string", length=500, nullable=true)
     */
    protected $CfcnEmplHistBusName;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEmplHistBusNature", type="string", length=500, nullable=true)
     */
    protected $CfcnEmplHistBusNature;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEmplHistWebAddress", type="string", length=500, nullable=true)
     */
    protected $CfcnEmplHistWebAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEmplHistBusAddress", type="text", length=65535, nullable=true)
     */
    protected $CfcnEmplHistBusAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEmplHistUndertaking", type="text", length=65535, nullable=true)
     */
    protected $CfcnEmplHistUndertaking;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnEmplHistLegacyStartDate", type="string", length=45, nullable=true)
     */
    protected $CfcnEmplHistLegacyStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnEmplHistStudyExpend", type="date", nullable=true)
     */
    protected $CfcnEmplHistStudyExpend;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnEmplHistId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CfcnEmplHistId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationapplication
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationapplication")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnEmplHistAppId", referencedColumnName="CfcnAppId")
     * })
     */
    protected $cfcnEmplHistAppId;



    /**
     * Set CfcnEmplHistStatusType
     *
     * @param string $cfcnEmplHistStatusType
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistStatusType($cfcnEmplHistStatusType)
    {
        $this->CfcnEmplHistStatusType = $cfcnEmplHistStatusType;

        return $this;
    }

    /**
     * Get CfcnEmplHistStatusType
     *
     * @return string 
     */
    public function getCfcnEmplHistStatusType()
    {
        return $this->CfcnEmplHistStatusType;
    }

    /**
     * Set CfcnEmplHistDateFrom
     *
     * @param \DateTime $cfcnEmplHistDateFrom
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistDateFrom($cfcnEmplHistDateFrom)
    {
        $this->CfcnEmplHistDateFrom = $cfcnEmplHistDateFrom;

        return $this;
    }

    /**
     * Get CfcnEmplHistDateFrom
     *
     * @return \DateTime 
     */
    public function getCfcnEmplHistDateFrom()
    {
        return $this->CfcnEmplHistDateFrom;
    }

    /**
     * Set CfcnEmplHistDateTo
     *
     * @param \DateTime $cfcnEmplHistDateTo
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistDateTo($cfcnEmplHistDateTo)
    {
        $this->CfcnEmplHistDateTo = $cfcnEmplHistDateTo;

        return $this;
    }

    /**
     * Get CfcnEmplHistDateTo
     *
     * @return \DateTime 
     */
    public function getCfcnEmplHistDateTo()
    {
        return $this->CfcnEmplHistDateTo;
    }

    /**
     * Set CfcnEmplHistBusName
     *
     * @param string $cfcnEmplHistBusName
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistBusName($cfcnEmplHistBusName)
    {
        $this->CfcnEmplHistBusName = $cfcnEmplHistBusName;

        return $this;
    }

    /**
     * Get CfcnEmplHistBusName
     *
     * @return string 
     */
    public function getCfcnEmplHistBusName()
    {
        return $this->CfcnEmplHistBusName;
    }

    /**
     * Set CfcnEmplHistBusNature
     *
     * @param string $cfcnEmplHistBusNature
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistBusNature($cfcnEmplHistBusNature)
    {
        $this->CfcnEmplHistBusNature = $cfcnEmplHistBusNature;

        return $this;
    }

    /**
     * Get CfcnEmplHistBusNature
     *
     * @return string 
     */
    public function getCfcnEmplHistBusNature()
    {
        return $this->CfcnEmplHistBusNature;
    }

    /**
     * Set CfcnEmplHistWebAddress
     *
     * @param string $cfcnEmplHistWebAddress
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistWebAddress($cfcnEmplHistWebAddress)
    {
        $this->CfcnEmplHistWebAddress = $cfcnEmplHistWebAddress;

        return $this;
    }

    /**
     * Get CfcnEmplHistWebAddress
     *
     * @return string 
     */
    public function getCfcnEmplHistWebAddress()
    {
        return $this->CfcnEmplHistWebAddress;
    }

    /**
     * Set CfcnEmplHistBusAddress
     *
     * @param string $cfcnEmplHistBusAddress
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistBusAddress($cfcnEmplHistBusAddress)
    {
        $this->CfcnEmplHistBusAddress = $cfcnEmplHistBusAddress;

        return $this;
    }

    /**
     * Get CfcnEmplHistBusAddress
     *
     * @return string 
     */
    public function getCfcnEmplHistBusAddress()
    {
        return $this->CfcnEmplHistBusAddress;
    }

    /**
     * Set CfcnEmplHistUndertaking
     *
     * @param string $cfcnEmplHistUndertaking
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistUndertaking($cfcnEmplHistUndertaking)
    {
        $this->CfcnEmplHistUndertaking = $cfcnEmplHistUndertaking;

        return $this;
    }

    /**
     * Get CfcnEmplHistUndertaking
     *
     * @return string 
     */
    public function getCfcnEmplHistUndertaking()
    {
        return $this->CfcnEmplHistUndertaking;
    }

    /**
     * Set CfcnEmplHistLegacyStartDate
     *
     * @param string $cfcnEmplHistLegacyStartDate
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistLegacyStartDate($cfcnEmplHistLegacyStartDate)
    {
        $this->CfcnEmplHistLegacyStartDate = $cfcnEmplHistLegacyStartDate;

        return $this;
    }

    /**
     * Get CfcnEmplHistLegacyStartDate
     *
     * @return string 
     */
    public function getCfcnEmplHistLegacyStartDate()
    {
        return $this->CfcnEmplHistLegacyStartDate;
    }

    /**
     * Set CfcnEmplHistStudyExpend
     *
     * @param \DateTime $cfcnEmplHistStudyExpend
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistStudyExpend($cfcnEmplHistStudyExpend)
    {
        $this->CfcnEmplHistStudyExpend = $cfcnEmplHistStudyExpend;

        return $this;
    }

    /**
     * Get CfcnEmplHistStudyExpend
     *
     * @return \DateTime 
     */
    public function getCfcnEmplHistStudyExpend()
    {
        return $this->CfcnEmplHistStudyExpend;
    }

    /**
     * Get CfcnEmplHistId
     *
     * @return integer 
     */
    public function getCfcnEmplHistId()
    {
        return $this->CfcnEmplHistId;
    }

    /**
     * Set cfcnEmplHistAppId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationapplication $cfcnEmplHistAppId
     * @return Classificationemplhist
     */
    public function setCfcnEmplHistAppId(\worldsailing\Isaf\model\Entity\Classificationapplication $cfcnEmplHistAppId = null)
    {
        $this->cfcnEmplHistAppId = $cfcnEmplHistAppId;

        return $this;
    }

    /**
     * Get cfcnEmplHistAppId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationapplication
     */
    public function getCfcnEmplHistAppId()
    {
        return $this->cfcnEmplHistAppId;
    }
}
