<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Protestsummary
 *
 * @ORM\Table(name="ProtestSummary")
 * @ORM\Entity
 */
class Protestsummary
{
    /**
     * @var string
     *
     * @ORM\Column(name="ProSource", type="string", length=20, nullable=true)
     */
    protected $ProSource;

    /**
     * @var integer
     *
     * @ORM\Column(name="ProSrcId", type="integer", nullable=false)
     */
    protected $ProSrcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ProRgtaId", type="integer", nullable=false)
     */
    protected $ProRgtaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ProEvntId", type="integer", nullable=false)
     */
    protected $ProEvntId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ProRaceId", type="integer", nullable=false)
     */
    protected $ProRaceId;

    /**
     * @var string
     *
     * @ORM\Column(name="ProStatus", type="string", nullable=false)
     */
    protected $ProStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="ProBy", type="string", length=255, nullable=true)
     */
    protected $ProBy;

    /**
     * @var string
     *
     * @ORM\Column(name="ProByName", type="string", length=255, nullable=true)
     */
    protected $ProByName;

    /**
     * @var string
     *
     * @ORM\Column(name="ProAbout", type="string", length=255, nullable=true)
     */
    protected $ProAbout;

    /**
     * @var string
     *
     * @ORM\Column(name="ProAboutName", type="string", length=255, nullable=true)
     */
    protected $ProAboutName;

    /**
     * @var string
     *
     * @ORM\Column(name="ProWitnesses", type="text", length=65535, nullable=true)
     */
    protected $ProWitnesses;

    /**
     * @var string
     *
     * @ORM\Column(name="ProType", type="text", length=65535, nullable=true)
     */
    protected $ProType;

    /**
     * @var string
     *
     * @ORM\Column(name="ProDetails", type="text", length=65535, nullable=true)
     */
    protected $ProDetails;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ProHearingDateTime", type="datetime", nullable=true)
     */
    protected $ProHearingDateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="ProDescription", type="text", length=65535, nullable=true)
     */
    protected $ProDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="ProFactsFound", type="text", length=65535, nullable=true)
     */
    protected $ProFactsFound;

    /**
     * @var string
     *
     * @ORM\Column(name="ProConclusion", type="text", length=65535, nullable=true)
     */
    protected $ProConclusion;

    /**
     * @var string
     *
     * @ORM\Column(name="ProRules", type="text", length=65535, nullable=true)
     */
    protected $ProRules;

    /**
     * @var string
     *
     * @ORM\Column(name="ProDecision", type="text", length=65535, nullable=true)
     */
    protected $ProDecision;

    /**
     * @var string
     *
     * @ORM\Column(name="ProDecisionShort", type="text", length=65535, nullable=true)
     */
    protected $ProDecisionShort;

    /**
     * @var string
     *
     * @ORM\Column(name="ProJury", type="text", length=65535, nullable=true)
     */
    protected $ProJury;

    /**
     * @var string
     *
     * @ORM\Column(name="ProNote", type="text", length=65535, nullable=true)
     */
    protected $ProNote;

    /**
     * @var integer
     *
     * @ORM\Column(name="ProId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $ProId;



    /**
     * Set ProSource
     *
     * @param string $proSource
     * @return Protestsummary
     */
    public function setProSource($proSource)
    {
        $this->ProSource = $proSource;

        return $this;
    }

    /**
     * Get ProSource
     *
     * @return string 
     */
    public function getProSource()
    {
        return $this->ProSource;
    }

    /**
     * Set ProSrcId
     *
     * @param integer $proSrcId
     * @return Protestsummary
     */
    public function setProSrcId($proSrcId)
    {
        $this->ProSrcId = $proSrcId;

        return $this;
    }

    /**
     * Get ProSrcId
     *
     * @return integer 
     */
    public function getProSrcId()
    {
        return $this->ProSrcId;
    }

    /**
     * Set ProRgtaId
     *
     * @param integer $proRgtaId
     * @return Protestsummary
     */
    public function setProRgtaId($proRgtaId)
    {
        $this->ProRgtaId = $proRgtaId;

        return $this;
    }

    /**
     * Get ProRgtaId
     *
     * @return integer 
     */
    public function getProRgtaId()
    {
        return $this->ProRgtaId;
    }

    /**
     * Set ProEvntId
     *
     * @param integer $proEvntId
     * @return Protestsummary
     */
    public function setProEvntId($proEvntId)
    {
        $this->ProEvntId = $proEvntId;

        return $this;
    }

    /**
     * Get ProEvntId
     *
     * @return integer 
     */
    public function getProEvntId()
    {
        return $this->ProEvntId;
    }

    /**
     * Set ProRaceId
     *
     * @param integer $proRaceId
     * @return Protestsummary
     */
    public function setProRaceId($proRaceId)
    {
        $this->ProRaceId = $proRaceId;

        return $this;
    }

    /**
     * Get ProRaceId
     *
     * @return integer 
     */
    public function getProRaceId()
    {
        return $this->ProRaceId;
    }

    /**
     * Set ProStatus
     *
     * @param string $proStatus
     * @return Protestsummary
     */
    public function setProStatus($proStatus)
    {
        $this->ProStatus = $proStatus;

        return $this;
    }

    /**
     * Get ProStatus
     *
     * @return string 
     */
    public function getProStatus()
    {
        return $this->ProStatus;
    }

    /**
     * Set ProBy
     *
     * @param string $proBy
     * @return Protestsummary
     */
    public function setProBy($proBy)
    {
        $this->ProBy = $proBy;

        return $this;
    }

    /**
     * Get ProBy
     *
     * @return string 
     */
    public function getProBy()
    {
        return $this->ProBy;
    }

    /**
     * Set ProByName
     *
     * @param string $proByName
     * @return Protestsummary
     */
    public function setProByName($proByName)
    {
        $this->ProByName = $proByName;

        return $this;
    }

    /**
     * Get ProByName
     *
     * @return string 
     */
    public function getProByName()
    {
        return $this->ProByName;
    }

    /**
     * Set ProAbout
     *
     * @param string $proAbout
     * @return Protestsummary
     */
    public function setProAbout($proAbout)
    {
        $this->ProAbout = $proAbout;

        return $this;
    }

    /**
     * Get ProAbout
     *
     * @return string 
     */
    public function getProAbout()
    {
        return $this->ProAbout;
    }

    /**
     * Set ProAboutName
     *
     * @param string $proAboutName
     * @return Protestsummary
     */
    public function setProAboutName($proAboutName)
    {
        $this->ProAboutName = $proAboutName;

        return $this;
    }

    /**
     * Get ProAboutName
     *
     * @return string 
     */
    public function getProAboutName()
    {
        return $this->ProAboutName;
    }

    /**
     * Set ProWitnesses
     *
     * @param string $proWitnesses
     * @return Protestsummary
     */
    public function setProWitnesses($proWitnesses)
    {
        $this->ProWitnesses = $proWitnesses;

        return $this;
    }

    /**
     * Get ProWitnesses
     *
     * @return string 
     */
    public function getProWitnesses()
    {
        return $this->ProWitnesses;
    }

    /**
     * Set ProType
     *
     * @param string $proType
     * @return Protestsummary
     */
    public function setProType($proType)
    {
        $this->ProType = $proType;

        return $this;
    }

    /**
     * Get ProType
     *
     * @return string 
     */
    public function getProType()
    {
        return $this->ProType;
    }

    /**
     * Set ProDetails
     *
     * @param string $proDetails
     * @return Protestsummary
     */
    public function setProDetails($proDetails)
    {
        $this->ProDetails = $proDetails;

        return $this;
    }

    /**
     * Get ProDetails
     *
     * @return string 
     */
    public function getProDetails()
    {
        return $this->ProDetails;
    }

    /**
     * Set ProHearingDateTime
     *
     * @param \DateTime $proHearingDateTime
     * @return Protestsummary
     */
    public function setProHearingDateTime($proHearingDateTime)
    {
        $this->ProHearingDateTime = $proHearingDateTime;

        return $this;
    }

    /**
     * Get ProHearingDateTime
     *
     * @return \DateTime 
     */
    public function getProHearingDateTime()
    {
        return $this->ProHearingDateTime;
    }

    /**
     * Set ProDescription
     *
     * @param string $proDescription
     * @return Protestsummary
     */
    public function setProDescription($proDescription)
    {
        $this->ProDescription = $proDescription;

        return $this;
    }

    /**
     * Get ProDescription
     *
     * @return string 
     */
    public function getProDescription()
    {
        return $this->ProDescription;
    }

    /**
     * Set ProFactsFound
     *
     * @param string $proFactsFound
     * @return Protestsummary
     */
    public function setProFactsFound($proFactsFound)
    {
        $this->ProFactsFound = $proFactsFound;

        return $this;
    }

    /**
     * Get ProFactsFound
     *
     * @return string 
     */
    public function getProFactsFound()
    {
        return $this->ProFactsFound;
    }

    /**
     * Set ProConclusion
     *
     * @param string $proConclusion
     * @return Protestsummary
     */
    public function setProConclusion($proConclusion)
    {
        $this->ProConclusion = $proConclusion;

        return $this;
    }

    /**
     * Get ProConclusion
     *
     * @return string 
     */
    public function getProConclusion()
    {
        return $this->ProConclusion;
    }

    /**
     * Set ProRules
     *
     * @param string $proRules
     * @return Protestsummary
     */
    public function setProRules($proRules)
    {
        $this->ProRules = $proRules;

        return $this;
    }

    /**
     * Get ProRules
     *
     * @return string 
     */
    public function getProRules()
    {
        return $this->ProRules;
    }

    /**
     * Set ProDecision
     *
     * @param string $proDecision
     * @return Protestsummary
     */
    public function setProDecision($proDecision)
    {
        $this->ProDecision = $proDecision;

        return $this;
    }

    /**
     * Get ProDecision
     *
     * @return string 
     */
    public function getProDecision()
    {
        return $this->ProDecision;
    }

    /**
     * Set ProDecisionShort
     *
     * @param string $proDecisionShort
     * @return Protestsummary
     */
    public function setProDecisionShort($proDecisionShort)
    {
        $this->ProDecisionShort = $proDecisionShort;

        return $this;
    }

    /**
     * Get ProDecisionShort
     *
     * @return string 
     */
    public function getProDecisionShort()
    {
        return $this->ProDecisionShort;
    }

    /**
     * Set ProJury
     *
     * @param string $proJury
     * @return Protestsummary
     */
    public function setProJury($proJury)
    {
        $this->ProJury = $proJury;

        return $this;
    }

    /**
     * Get ProJury
     *
     * @return string 
     */
    public function getProJury()
    {
        return $this->ProJury;
    }

    /**
     * Set ProNote
     *
     * @param string $proNote
     * @return Protestsummary
     */
    public function setProNote($proNote)
    {
        $this->ProNote = $proNote;

        return $this;
    }

    /**
     * Get ProNote
     *
     * @return string 
     */
    public function getProNote()
    {
        return $this->ProNote;
    }

    /**
     * Get ProId
     *
     * @return integer 
     */
    public function getProId()
    {
        return $this->ProId;
    }
}
