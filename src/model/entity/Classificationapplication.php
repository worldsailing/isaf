<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationapplication
 *
 * @ORM\Table(name="ClassificationApplication", uniqueConstraints={@ORM\UniqueConstraint(name="CfcnAppId_UNIQUE", columns={"CfcnAppId"})}, indexes={@ORM\Index(name="biog_fk_1", columns={"CfcnMembId"}), @ORM\Index(name="action_fk_1", columns={"CfcnAppActionID"})})
 * @ORM\Entity
 */
class Classificationapplication
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnCreationDate", type="datetime", nullable=true)
     */
    protected $CfcnCreationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnOtherWork", type="text", length=65535, nullable=true)
     */
    protected $CfcnOtherWork;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnRequestedGroup", type="string", length=5, nullable=true)
     */
    protected $CfcnRequestedGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnAppStatus", type="string", nullable=true)
     */
    protected $CfcnAppStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnEffectiveDate", type="date", nullable=true)
     */
    protected $CfcnEffectiveDate;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnDecidedGroup", type="string", length=5, nullable=true)
     */
    protected $CfcnDecidedGroup;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CfcnExpiryDate", type="date", nullable=true)
     */
    protected $CfcnExpiryDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CfcnAppExpiryReminderSent", type="boolean", nullable=false)
     */
    protected $CfcnAppExpiryReminderSent;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnAppSelectedLanguage", type="string", length=45, nullable=true)
     */
    protected $CfcnAppSelectedLanguage;

    /**
     * @var string
     *
     * @ORM\Column(name="CfcnAppReviewCompleteDate", type="string", length=45, nullable=true)
     */
    protected $CfcnAppReviewCompleteDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="CfcnAppId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CfcnAppId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Memberbiogs
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Memberbiogs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnMembId", referencedColumnName="BiogMembId")
     * })
     */
    protected $cfcnMembId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CfcnAppActionID", referencedColumnName="CfcnActionId")
     * })
     */
    protected $cfcnAppActionID;



    /**
     * Set CfcnCreationDate
     *
     * @param \DateTime $cfcnCreationDate
     * @return Classificationapplication
     */
    public function setCfcnCreationDate($cfcnCreationDate)
    {
        $this->CfcnCreationDate = $cfcnCreationDate;

        return $this;
    }

    /**
     * Get CfcnCreationDate
     *
     * @return \DateTime 
     */
    public function getCfcnCreationDate()
    {
        return $this->CfcnCreationDate;
    }

    /**
     * Set CfcnOtherWork
     *
     * @param string $cfcnOtherWork
     * @return Classificationapplication
     */
    public function setCfcnOtherWork($cfcnOtherWork)
    {
        $this->CfcnOtherWork = $cfcnOtherWork;

        return $this;
    }

    /**
     * Get CfcnOtherWork
     *
     * @return string 
     */
    public function getCfcnOtherWork()
    {
        return $this->CfcnOtherWork;
    }

    /**
     * Set CfcnRequestedGroup
     *
     * @param string $cfcnRequestedGroup
     * @return Classificationapplication
     */
    public function setCfcnRequestedGroup($cfcnRequestedGroup)
    {
        $this->CfcnRequestedGroup = $cfcnRequestedGroup;

        return $this;
    }

    /**
     * Get CfcnRequestedGroup
     *
     * @return string 
     */
    public function getCfcnRequestedGroup()
    {
        return $this->CfcnRequestedGroup;
    }

    /**
     * Set CfcnAppStatus
     *
     * @param string $cfcnAppStatus
     * @return Classificationapplication
     */
    public function setCfcnAppStatus($cfcnAppStatus)
    {
        $this->CfcnAppStatus = $cfcnAppStatus;

        return $this;
    }

    /**
     * Get CfcnAppStatus
     *
     * @return string 
     */
    public function getCfcnAppStatus()
    {
        return $this->CfcnAppStatus;
    }

    /**
     * Set CfcnEffectiveDate
     *
     * @param \DateTime $cfcnEffectiveDate
     * @return Classificationapplication
     */
    public function setCfcnEffectiveDate($cfcnEffectiveDate)
    {
        $this->CfcnEffectiveDate = $cfcnEffectiveDate;

        return $this;
    }

    /**
     * Get CfcnEffectiveDate
     *
     * @return \DateTime 
     */
    public function getCfcnEffectiveDate()
    {
        return $this->CfcnEffectiveDate;
    }

    /**
     * Set CfcnDecidedGroup
     *
     * @param string $cfcnDecidedGroup
     * @return Classificationapplication
     */
    public function setCfcnDecidedGroup($cfcnDecidedGroup)
    {
        $this->CfcnDecidedGroup = $cfcnDecidedGroup;

        return $this;
    }

    /**
     * Get CfcnDecidedGroup
     *
     * @return string 
     */
    public function getCfcnDecidedGroup()
    {
        return $this->CfcnDecidedGroup;
    }

    /**
     * Set CfcnExpiryDate
     *
     * @param \DateTime $cfcnExpiryDate
     * @return Classificationapplication
     */
    public function setCfcnExpiryDate($cfcnExpiryDate)
    {
        $this->CfcnExpiryDate = $cfcnExpiryDate;

        return $this;
    }

    /**
     * Get CfcnExpiryDate
     *
     * @return \DateTime 
     */
    public function getCfcnExpiryDate()
    {
        return $this->CfcnExpiryDate;
    }

    /**
     * Set CfcnAppExpiryReminderSent
     *
     * @param boolean $cfcnAppExpiryReminderSent
     * @return Classificationapplication
     */
    public function setCfcnAppExpiryReminderSent($cfcnAppExpiryReminderSent)
    {
        $this->CfcnAppExpiryReminderSent = $cfcnAppExpiryReminderSent;

        return $this;
    }

    /**
     * Get CfcnAppExpiryReminderSent
     *
     * @return boolean 
     */
    public function getCfcnAppExpiryReminderSent()
    {
        return $this->CfcnAppExpiryReminderSent;
    }

    /**
     * Set CfcnAppSelectedLanguage
     *
     * @param string $cfcnAppSelectedLanguage
     * @return Classificationapplication
     */
    public function setCfcnAppSelectedLanguage($cfcnAppSelectedLanguage)
    {
        $this->CfcnAppSelectedLanguage = $cfcnAppSelectedLanguage;

        return $this;
    }

    /**
     * Get CfcnAppSelectedLanguage
     *
     * @return string 
     */
    public function getCfcnAppSelectedLanguage()
    {
        return $this->CfcnAppSelectedLanguage;
    }

    /**
     * Set CfcnAppReviewCompleteDate
     *
     * @param string $cfcnAppReviewCompleteDate
     * @return Classificationapplication
     */
    public function setCfcnAppReviewCompleteDate($cfcnAppReviewCompleteDate)
    {
        $this->CfcnAppReviewCompleteDate = $cfcnAppReviewCompleteDate;

        return $this;
    }

    /**
     * Get CfcnAppReviewCompleteDate
     *
     * @return string 
     */
    public function getCfcnAppReviewCompleteDate()
    {
        return $this->CfcnAppReviewCompleteDate;
    }

    /**
     * Get CfcnAppId
     *
     * @return integer 
     */
    public function getCfcnAppId()
    {
        return $this->CfcnAppId;
    }

    /**
     * Set cfcnMembId
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $cfcnMembId
     * @return Classificationapplication
     */
    public function setCfcnMembId(\worldsailing\Isaf\model\Entity\Memberbiogs $cfcnMembId = null)
    {
        $this->cfcnMembId = $cfcnMembId;

        return $this;
    }

    /**
     * Get cfcnMembId
     *
     * @return \worldsailing\Isaf\model\Entity\Memberbiogs
     */
    public function getCfcnMembId()
    {
        return $this->cfcnMembId;
    }

    /**
     * Set cfcnAppActionID
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $cfcnAppActionID
     * @return Classificationapplication
     */
    public function setCfcnAppActionID(\worldsailing\Isaf\model\Entity\Classificationaction $cfcnAppActionID = null)
    {
        $this->cfcnAppActionID = $cfcnAppActionID;

        return $this;
    }

    /**
     * Get cfcnAppActionID
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getCfcnAppActionID()
    {
        return $this->cfcnAppActionID;
    }
}
