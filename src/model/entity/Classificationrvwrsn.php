<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classificationrvwrsn
 *
 * @ORM\Table(name="ClassificationRvwRsn", indexes={@ORM\Index(name="rvw_action", columns={"ReviewActionId"})})
 * @ORM\Entity
 */
class Classificationrvwrsn
{
    /**
     * @var string
     *
     * @ORM\Column(name="ReviewReason", type="string", length=75, nullable=true)
     */
    protected $ReviewReason;

    /**
     * @var string
     *
     * @ORM\Column(name="ReviewRsnDetail", type="text", length=65535, nullable=true)
     */
    protected $ReviewRsnDetail;

    /**
     * @var integer
     *
     * @ORM\Column(name="ReviewRsnId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $ReviewRsnId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ReviewActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $reviewActionId;



    /**
     * Set ReviewReason
     *
     * @param string $reviewReason
     * @return Classificationrvwrsn
     */
    public function setReviewReason($reviewReason)
    {
        $this->ReviewReason = $reviewReason;

        return $this;
    }

    /**
     * Get ReviewReason
     *
     * @return string 
     */
    public function getReviewReason()
    {
        return $this->ReviewReason;
    }

    /**
     * Set ReviewRsnDetail
     *
     * @param string $reviewRsnDetail
     * @return Classificationrvwrsn
     */
    public function setReviewRsnDetail($reviewRsnDetail)
    {
        $this->ReviewRsnDetail = $reviewRsnDetail;

        return $this;
    }

    /**
     * Get ReviewRsnDetail
     *
     * @return string 
     */
    public function getReviewRsnDetail()
    {
        return $this->ReviewRsnDetail;
    }

    /**
     * Get ReviewRsnId
     *
     * @return integer 
     */
    public function getReviewRsnId()
    {
        return $this->ReviewRsnId;
    }

    /**
     * Set reviewActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $reviewActionId
     * @return Classificationrvwrsn
     */
    public function setReviewActionId(\worldsailing\Isaf\model\Entity\Classificationaction $reviewActionId = null)
    {
        $this->reviewActionId = $reviewActionId;

        return $this;
    }

    /**
     * Get reviewActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getReviewActionId()
    {
        return $this->reviewActionId;
    }
}
