<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Groups
 *
 * @ORM\Table(name="Groups")
 * @ORM\Entity
 */
class Groups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="GrpOrigId", type="integer", nullable=true)
     */
    protected $GrpOrigId;

    /**
     * @var string
     *
     * @ORM\Column(name="GrpName", type="string", length=128, nullable=true)
     */
    protected $GrpName;

    /**
     * @var integer
     *
     * @ORM\Column(name="GrpId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $GrpId;



    /**
     * Set GrpOrigId
     *
     * @param integer $grpOrigId
     * @return Groups
     */
    public function setGrpOrigId($grpOrigId)
    {
        $this->GrpOrigId = $grpOrigId;

        return $this;
    }

    /**
     * Get GrpOrigId
     *
     * @return integer 
     */
    public function getGrpOrigId()
    {
        return $this->GrpOrigId;
    }

    /**
     * Set GrpName
     *
     * @param string $grpName
     * @return Groups
     */
    public function setGrpName($grpName)
    {
        $this->GrpName = $grpName;

        return $this;
    }

    /**
     * Get GrpName
     *
     * @return string 
     */
    public function getGrpName()
    {
        return $this->GrpName;
    }

    /**
     * Get GrpId
     *
     * @return integer 
     */
    public function getGrpId()
    {
        return $this->GrpId;
    }
}
