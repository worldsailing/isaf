<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organisationmembermap
 *
 * @ORM\Table(name="OrganisationMemberMap", uniqueConstraints={@ORM\UniqueConstraint(name="idMmapMembOrg", columns={"MmapOrgId", "MmapMembId", "MmapCntId"})}, indexes={@ORM\Index(name="idMmapOrgId", columns={"MmapOrgId"})})
 * @ORM\Entity
 */
class Organisationmembermap
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MmapOrgId", type="integer", nullable=false)
     */
    protected $MmapOrgId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MmapCntId", type="smallint", nullable=false)
     */
    protected $MmapCntId;

    /**
     * @var string
     *
     * @ORM\Column(name="MmapContactType", type="string", nullable=false)
     */
    protected $MmapContactType;

    /**
     * @var integer
     *
     * @ORM\Column(name="MmapMembId", type="integer", nullable=false)
     */
    protected $MmapMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MmapId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $MmapId;



    /**
     * Set MmapOrgId
     *
     * @param integer $mmapOrgId
     * @return Organisationmembermap
     */
    public function setMmapOrgId($mmapOrgId)
    {
        $this->MmapOrgId = $mmapOrgId;

        return $this;
    }

    /**
     * Get MmapOrgId
     *
     * @return integer 
     */
    public function getMmapOrgId()
    {
        return $this->MmapOrgId;
    }

    /**
     * Set MmapCntId
     *
     * @param integer $mmapCntId
     * @return Organisationmembermap
     */
    public function setMmapCntId($mmapCntId)
    {
        $this->MmapCntId = $mmapCntId;

        return $this;
    }

    /**
     * Get MmapCntId
     *
     * @return integer 
     */
    public function getMmapCntId()
    {
        return $this->MmapCntId;
    }

    /**
     * Set MmapContactType
     *
     * @param string $mmapContactType
     * @return Organisationmembermap
     */
    public function setMmapContactType($mmapContactType)
    {
        $this->MmapContactType = $mmapContactType;

        return $this;
    }

    /**
     * Get MmapContactType
     *
     * @return string 
     */
    public function getMmapContactType()
    {
        return $this->MmapContactType;
    }

    /**
     * Set MmapMembId
     *
     * @param integer $mmapMembId
     * @return Organisationmembermap
     */
    public function setMmapMembId($mmapMembId)
    {
        $this->MmapMembId = $mmapMembId;

        return $this;
    }

    /**
     * Get MmapMembId
     *
     * @return integer 
     */
    public function getMmapMembId()
    {
        return $this->MmapMembId;
    }

    /**
     * Get MmapId
     *
     * @return integer 
     */
    public function getMmapId()
    {
        return $this->MmapId;
    }
}
