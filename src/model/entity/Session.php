<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="Session")
 * @ORM\Entity
 */
class Session
{
    /**
     * @var string
     *
     * @ORM\Column(name="session_content", type="text", nullable=true)
     */
    protected $Session_content;

    /**
     * @var integer
     *
     * @ORM\Column(name="session_timestamp", type="integer", nullable=false)
     */
    protected $Session_timestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="string", length=100)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $Session_id;



    /**
     * Set Session_content
     *
     * @param string $sessionContent
     * @return Session
     */
    public function setSessionContent($sessionContent)
    {
        $this->Session_content = $sessionContent;

        return $this;
    }

    /**
     * Get Session_content
     *
     * @return string 
     */
    public function getSessionContent()
    {
        return $this->Session_content;
    }

    /**
     * Set Session_timestamp
     *
     * @param integer $sessionTimestamp
     * @return Session
     */
    public function setSessionTimestamp($sessionTimestamp)
    {
        $this->Session_timestamp = $sessionTimestamp;

        return $this;
    }

    /**
     * Get Session_timestamp
     *
     * @return integer 
     */
    public function getSessionTimestamp()
    {
        return $this->Session_timestamp;
    }

    /**
     * Get Session_id
     *
     * @return string 
     */
    public function getSessionId()
    {
        return $this->Session_id;
    }
}
