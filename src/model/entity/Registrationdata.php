<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Registrationdata
 *
 * @ORM\Table(name="RegistrationData")
 * @ORM\Entity
 */
class Registrationdata
{
    /**
     * @var string
     *
     * @ORM\Column(name="DataValue", type="text", length=65535, nullable=true)
     */
    protected $DataValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="DataRegId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $DataRegId;

    /**
     * @var integer
     *
     * @ORM\Column(name="DataFieldId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $DataFieldId;



    /**
     * Set DataValue
     *
     * @param string $dataValue
     * @return Registrationdata
     */
    public function setDataValue($dataValue)
    {
        $this->DataValue = $dataValue;

        return $this;
    }

    /**
     * Get DataValue
     *
     * @return string 
     */
    public function getDataValue()
    {
        return $this->DataValue;
    }

    /**
     * Set DataRegId
     *
     * @param integer $dataRegId
     * @return Registrationdata
     */
    public function setDataRegId($dataRegId)
    {
        $this->DataRegId = $dataRegId;

        return $this;
    }

    /**
     * Get DataRegId
     *
     * @return integer 
     */
    public function getDataRegId()
    {
        return $this->DataRegId;
    }

    /**
     * Set DataFieldId
     *
     * @param integer $dataFieldId
     * @return Registrationdata
     */
    public function setDataFieldId($dataFieldId)
    {
        $this->DataFieldId = $dataFieldId;

        return $this;
    }

    /**
     * Get DataFieldId
     *
     * @return integer 
     */
    public function getDataFieldId()
    {
        return $this->DataFieldId;
    }
}
