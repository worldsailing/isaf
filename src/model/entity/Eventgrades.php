<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eventgrades
 *
 * @ORM\Table(name="EventGrades")
 * @ORM\Entity
 */
class Eventgrades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="GradeDisId", type="integer", nullable=false)
     */
    protected $GradeDisId;

    /**
     * @var string
     *
     * @ORM\Column(name="GradeCode", type="string", length=5, nullable=true)
     */
    protected $GradeCode;

    /**
     * @var string
     *
     * @ORM\Column(name="GradeName", type="string", length=20, nullable=true)
     */
    protected $GradeName;

    /**
     * @var float
     *
     * @ORM\Column(name="GradeMultiplier", type="float", precision=10, scale=0, nullable=true)
     */
    protected $GradeMultiplier;

    /**
     * @var integer
     *
     * @ORM\Column(name="GradePercent", type="integer", nullable=true)
     */
    protected $GradePercent;

    /**
     * @var integer
     *
     * @ORM\Column(name="GradeEventRank", type="integer", nullable=true)
     */
    protected $GradeEventRank;

    /**
     * @var integer
     *
     * @ORM\Column(name="GradeId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $GradeId;



    /**
     * Set GradeDisId
     *
     * @param integer $gradeDisId
     * @return Eventgrades
     */
    public function setGradeDisId($gradeDisId)
    {
        $this->GradeDisId = $gradeDisId;

        return $this;
    }

    /**
     * Get GradeDisId
     *
     * @return integer 
     */
    public function getGradeDisId()
    {
        return $this->GradeDisId;
    }

    /**
     * Set GradeCode
     *
     * @param string $gradeCode
     * @return Eventgrades
     */
    public function setGradeCode($gradeCode)
    {
        $this->GradeCode = $gradeCode;

        return $this;
    }

    /**
     * Get GradeCode
     *
     * @return string 
     */
    public function getGradeCode()
    {
        return $this->GradeCode;
    }

    /**
     * Set GradeName
     *
     * @param string $gradeName
     * @return Eventgrades
     */
    public function setGradeName($gradeName)
    {
        $this->GradeName = $gradeName;

        return $this;
    }

    /**
     * Get GradeName
     *
     * @return string 
     */
    public function getGradeName()
    {
        return $this->GradeName;
    }

    /**
     * Set GradeMultiplier
     *
     * @param float $gradeMultiplier
     * @return Eventgrades
     */
    public function setGradeMultiplier($gradeMultiplier)
    {
        $this->GradeMultiplier = $gradeMultiplier;

        return $this;
    }

    /**
     * Get GradeMultiplier
     *
     * @return float 
     */
    public function getGradeMultiplier()
    {
        return $this->GradeMultiplier;
    }

    /**
     * Set GradePercent
     *
     * @param integer $gradePercent
     * @return Eventgrades
     */
    public function setGradePercent($gradePercent)
    {
        $this->GradePercent = $gradePercent;

        return $this;
    }

    /**
     * Get GradePercent
     *
     * @return integer 
     */
    public function getGradePercent()
    {
        return $this->GradePercent;
    }

    /**
     * Set GradeEventRank
     *
     * @param integer $gradeEventRank
     * @return Eventgrades
     */
    public function setGradeEventRank($gradeEventRank)
    {
        $this->GradeEventRank = $gradeEventRank;

        return $this;
    }

    /**
     * Get GradeEventRank
     *
     * @return integer 
     */
    public function getGradeEventRank()
    {
        return $this->GradeEventRank;
    }

    /**
     * Get GradeId
     *
     * @return integer 
     */
    public function getGradeId()
    {
        return $this->GradeId;
    }
}
