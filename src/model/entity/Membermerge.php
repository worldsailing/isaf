<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Membermerge
 *
 * @ORM\Table(name="MemberMerge", indexes={@ORM\Index(name="merge_date", columns={"MergeDate"}), @ORM\Index(name="cfcn_mrg_cfcn", columns={"MergeCfcnActionId"})})
 * @ORM\Entity
 */
class Membermerge
{
    /**
     * @var string
     *
     * @ORM\Column(name="MergeReverse", type="text", nullable=false)
     */
    protected $MergeReverse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MergeDate", type="datetime", nullable=false)
     */
    protected $MergeDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="MergeId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $MergeId;

    /**
     * @var \worldsailing\Isaf\model\Entity\Classificationaction
     *
     * @ORM\ManyToOne(targetEntity="worldsailing\Isaf\model\Entity\Classificationaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="MergeCfcnActionId", referencedColumnName="CfcnActionId")
     * })
     */
    protected $mergeCfcnActionId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="worldsailing\Isaf\model\Entity\Memberbiogs", mappedBy="mrgMergeId")
     */
    protected $mrgMembId;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mrgMembId = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set MergeReverse
     *
     * @param string $mergeReverse
     * @return Membermerge
     */
    public function setMergeReverse($mergeReverse)
    {
        $this->MergeReverse = $mergeReverse;

        return $this;
    }

    /**
     * Get MergeReverse
     *
     * @return string 
     */
    public function getMergeReverse()
    {
        return $this->MergeReverse;
    }

    /**
     * Set MergeDate
     *
     * @param \DateTime $mergeDate
     * @return Membermerge
     */
    public function setMergeDate($mergeDate)
    {
        $this->MergeDate = $mergeDate;

        return $this;
    }

    /**
     * Get MergeDate
     *
     * @return \DateTime 
     */
    public function getMergeDate()
    {
        return $this->MergeDate;
    }

    /**
     * Get MergeId
     *
     * @return integer 
     */
    public function getMergeId()
    {
        return $this->MergeId;
    }

    /**
     * Set mergeCfcnActionId
     *
     * @param \worldsailing\Isaf\model\Entity\Classificationaction $mergeCfcnActionId
     * @return Membermerge
     */
    public function setMergeCfcnActionId(\worldsailing\Isaf\model\Entity\Classificationaction $mergeCfcnActionId = null)
    {
        $this->mergeCfcnActionId = $mergeCfcnActionId;

        return $this;
    }

    /**
     * Get mergeCfcnActionId
     *
     * @return \worldsailing\Isaf\model\Entity\Classificationaction
     */
    public function getMergeCfcnActionId()
    {
        return $this->mergeCfcnActionId;
    }

    /**
     * Add mrgMembId
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $mrgMembId
     * @return Membermerge
     */
    public function addMrgMembId(\worldsailing\Isaf\model\Entity\Memberbiogs $mrgMembId)
    {
        $this->mrgMembId[] = $mrgMembId;

        return $this;
    }

    /**
     * Remove mrgMembId
     *
     * @param \worldsailing\Isaf\model\Entity\Memberbiogs $mrgMembId
     */
    public function removeMrgMembId(\worldsailing\Isaf\model\Entity\Memberbiogs $mrgMembId)
    {
        $this->mrgMembId->removeElement($mrgMembId);
    }

    /**
     * Get mrgMembId
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMrgMembId()
    {
        return $this->mrgMembId;
    }
}
