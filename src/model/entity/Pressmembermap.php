<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pressmembermap
 *
 * @ORM\Table(name="PressMemberMap")
 * @ORM\Entity
 */
class Pressmembermap
{
    /**
     * @var integer
     *
     * @ORM\Column(name="PmapMembId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $PmapMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PmapPtypId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $PmapPtypId;



    /**
     * Set PmapMembId
     *
     * @param integer $pmapMembId
     * @return Pressmembermap
     */
    public function setPmapMembId($pmapMembId)
    {
        $this->PmapMembId = $pmapMembId;

        return $this;
    }

    /**
     * Get PmapMembId
     *
     * @return integer 
     */
    public function getPmapMembId()
    {
        return $this->PmapMembId;
    }

    /**
     * Set PmapPtypId
     *
     * @param integer $pmapPtypId
     * @return Pressmembermap
     */
    public function setPmapPtypId($pmapPtypId)
    {
        $this->PmapPtypId = $pmapPtypId;

        return $this;
    }

    /**
     * Get PmapPtypId
     *
     * @return integer 
     */
    public function getPmapPtypId()
    {
        return $this->PmapPtypId;
    }
}
