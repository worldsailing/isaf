<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Protesttimelimit
 *
 * @ORM\Table(name="ProtestTimeLimit", uniqueConstraints={@ORM\UniqueConstraint(name="LimUq", columns={"LimDate", "LimEvntId", "LimEvtSplit"})})
 * @ORM\Entity
 */
class Protesttimelimit
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LimDate", type="date", nullable=false)
     */
    protected $LimDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="LimEvntId", type="integer", nullable=false)
     */
    protected $LimEvntId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LimCutOff", type="time", nullable=false)
     */
    protected $LimCutOff;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LimAsOf", type="datetime", nullable=false)
     */
    protected $LimAsOf;

    /**
     * @var string
     *
     * @ORM\Column(name="LimEvtSplit", type="string", length=45, nullable=true)
     */
    protected $LimEvtSplit;

    /**
     * @var integer
     *
     * @ORM\Column(name="LimId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $LimId;



    /**
     * Set LimDate
     *
     * @param \DateTime $limDate
     * @return Protesttimelimit
     */
    public function setLimDate($limDate)
    {
        $this->LimDate = $limDate;

        return $this;
    }

    /**
     * Get LimDate
     *
     * @return \DateTime 
     */
    public function getLimDate()
    {
        return $this->LimDate;
    }

    /**
     * Set LimEvntId
     *
     * @param integer $limEvntId
     * @return Protesttimelimit
     */
    public function setLimEvntId($limEvntId)
    {
        $this->LimEvntId = $limEvntId;

        return $this;
    }

    /**
     * Get LimEvntId
     *
     * @return integer 
     */
    public function getLimEvntId()
    {
        return $this->LimEvntId;
    }

    /**
     * Set LimCutOff
     *
     * @param \DateTime $limCutOff
     * @return Protesttimelimit
     */
    public function setLimCutOff($limCutOff)
    {
        $this->LimCutOff = $limCutOff;

        return $this;
    }

    /**
     * Get LimCutOff
     *
     * @return \DateTime 
     */
    public function getLimCutOff()
    {
        return $this->LimCutOff;
    }

    /**
     * Set LimAsOf
     *
     * @param \DateTime $limAsOf
     * @return Protesttimelimit
     */
    public function setLimAsOf($limAsOf)
    {
        $this->LimAsOf = $limAsOf;

        return $this;
    }

    /**
     * Get LimAsOf
     *
     * @return \DateTime 
     */
    public function getLimAsOf()
    {
        return $this->LimAsOf;
    }

    /**
     * Set LimEvtSplit
     *
     * @param string $limEvtSplit
     * @return Protesttimelimit
     */
    public function setLimEvtSplit($limEvtSplit)
    {
        $this->LimEvtSplit = $limEvtSplit;

        return $this;
    }

    /**
     * Get LimEvtSplit
     *
     * @return string 
     */
    public function getLimEvtSplit()
    {
        return $this->LimEvtSplit;
    }

    /**
     * Get LimId
     *
     * @return integer 
     */
    public function getLimId()
    {
        return $this->LimId;
    }
}
