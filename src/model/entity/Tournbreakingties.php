<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tournbreakingties
 *
 * @ORM\Table(name="TournBreakingTies")
 * @ORM\Entity
 */
class Tournbreakingties
{
    /**
     * @var integer
     *
     * @ORM\Column(name="TtieTournId", type="integer", nullable=false)
     */
    protected $TtieTournId;

    /**
     * @var string
     *
     * @ORM\Column(name="TtieCriteria", type="string", length=100, nullable=true)
     */
    protected $TtieCriteria;

    /**
     * @var string
     *
     * @ORM\Column(name="TtieIncDiscarded", type="string", nullable=false)
     */
    protected $TtieIncDiscarded;

    /**
     * @var integer
     *
     * @ORM\Column(name="TtieOrd", type="integer", nullable=false)
     */
    protected $TtieOrd;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $Id;



    /**
     * Set TtieTournId
     *
     * @param integer $ttieTournId
     * @return Tournbreakingties
     */
    public function setTtieTournId($ttieTournId)
    {
        $this->TtieTournId = $ttieTournId;

        return $this;
    }

    /**
     * Get TtieTournId
     *
     * @return integer 
     */
    public function getTtieTournId()
    {
        return $this->TtieTournId;
    }

    /**
     * Set TtieCriteria
     *
     * @param string $ttieCriteria
     * @return Tournbreakingties
     */
    public function setTtieCriteria($ttieCriteria)
    {
        $this->TtieCriteria = $ttieCriteria;

        return $this;
    }

    /**
     * Get TtieCriteria
     *
     * @return string 
     */
    public function getTtieCriteria()
    {
        return $this->TtieCriteria;
    }

    /**
     * Set TtieIncDiscarded
     *
     * @param string $ttieIncDiscarded
     * @return Tournbreakingties
     */
    public function setTtieIncDiscarded($ttieIncDiscarded)
    {
        $this->TtieIncDiscarded = $ttieIncDiscarded;

        return $this;
    }

    /**
     * Get TtieIncDiscarded
     *
     * @return string 
     */
    public function getTtieIncDiscarded()
    {
        return $this->TtieIncDiscarded;
    }

    /**
     * Set TtieOrd
     *
     * @param integer $ttieOrd
     * @return Tournbreakingties
     */
    public function setTtieOrd($ttieOrd)
    {
        $this->TtieOrd = $ttieOrd;

        return $this;
    }

    /**
     * Get TtieOrd
     *
     * @return integer 
     */
    public function getTtieOrd()
    {
        return $this->TtieOrd;
    }

    /**
     * Get Id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->Id;
    }
}
