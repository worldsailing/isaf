<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Raceupdates
 *
 * @ORM\Table(name="RaceUpdates")
 * @ORM\Entity
 */
class Raceupdates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="UpdtRaceId", type="integer", nullable=false)
     */
    protected $UpdtRaceId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UpdtDateTime", type="datetime", nullable=false)
     */
    protected $UpdtDateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="UpdtType", type="string", nullable=false)
     */
    protected $UpdtType;

    /**
     * @var string
     *
     * @ORM\Column(name="UpdtData", type="string", length=200, nullable=true)
     */
    protected $UpdtData;

    /**
     * @var integer
     *
     * @ORM\Column(name="UpdtId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $UpdtId;



    /**
     * Set UpdtRaceId
     *
     * @param integer $updtRaceId
     * @return Raceupdates
     */
    public function setUpdtRaceId($updtRaceId)
    {
        $this->UpdtRaceId = $updtRaceId;

        return $this;
    }

    /**
     * Get UpdtRaceId
     *
     * @return integer 
     */
    public function getUpdtRaceId()
    {
        return $this->UpdtRaceId;
    }

    /**
     * Set UpdtDateTime
     *
     * @param \DateTime $updtDateTime
     * @return Raceupdates
     */
    public function setUpdtDateTime($updtDateTime)
    {
        $this->UpdtDateTime = $updtDateTime;

        return $this;
    }

    /**
     * Get UpdtDateTime
     *
     * @return \DateTime 
     */
    public function getUpdtDateTime()
    {
        return $this->UpdtDateTime;
    }

    /**
     * Set UpdtType
     *
     * @param string $updtType
     * @return Raceupdates
     */
    public function setUpdtType($updtType)
    {
        $this->UpdtType = $updtType;

        return $this;
    }

    /**
     * Get UpdtType
     *
     * @return string 
     */
    public function getUpdtType()
    {
        return $this->UpdtType;
    }

    /**
     * Set UpdtData
     *
     * @param string $updtData
     * @return Raceupdates
     */
    public function setUpdtData($updtData)
    {
        $this->UpdtData = $updtData;

        return $this;
    }

    /**
     * Get UpdtData
     *
     * @return string 
     */
    public function getUpdtData()
    {
        return $this->UpdtData;
    }

    /**
     * Get UpdtId
     *
     * @return integer 
     */
    public function getUpdtId()
    {
        return $this->UpdtId;
    }
}
