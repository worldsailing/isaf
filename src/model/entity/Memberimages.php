<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Memberimages
 *
 * @ORM\Table(name="MemberImages", indexes={@ORM\Index(name="profilepic", columns={"ImgMembId", "ImgProfile"}), @ORM\Index(name="avatar", columns={"ImgMembId", "ImgProfile"})})
 * @ORM\Entity
 */
class Memberimages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ImgOrigId", type="integer", nullable=true)
     */
    protected $ImgOrigId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ImgMembId", type="integer", nullable=false)
     */
    protected $ImgMembId;

    /**
     * @var string
     *
     * @ORM\Column(name="ImgType", type="string", length=255, nullable=true)
     */
    protected $ImgType;

    /**
     * @var string
     *
     * @ORM\Column(name="ImgExtension", type="string", length=5, nullable=true)
     */
    protected $ImgExtension;

    /**
     * @var integer
     *
     * @ORM\Column(name="ImgSize", type="integer", nullable=true)
     */
    protected $ImgSize;

    /**
     * @var integer
     *
     * @ORM\Column(name="ImgWidth", type="integer", nullable=false)
     */
    protected $ImgWidth;

    /**
     * @var integer
     *
     * @ORM\Column(name="ImgHeight", type="integer", nullable=false)
     */
    protected $ImgHeight;

    /**
     * @var string
     *
     * @ORM\Column(name="ImgCaption", type="text", length=65535, nullable=true)
     */
    protected $ImgCaption;

    /**
     * @var string
     *
     * @ORM\Column(name="ImgDescription", type="text", length=65535, nullable=true)
     */
    protected $ImgDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="ImgPhotographer", type="string", length=200, nullable=true)
     */
    protected $ImgPhotographer;

    /**
     * @var string
     *
     * @ORM\Column(name="ImgKeywords", type="string", length=200, nullable=true)
     */
    protected $ImgKeywords;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ImgTaken", type="date", nullable=true)
     */
    protected $ImgTaken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ImgUploaded", type="datetime", nullable=false)
     */
    protected $ImgUploaded;

    /**
     * @var integer
     *
     * @ORM\Column(name="ImgOrd", type="integer", nullable=false)
     */
    protected $ImgOrd;

    /**
     * @var string
     *
     * @ORM\Column(name="ImgContent", type="blob", nullable=true)
     */
    protected $ImgContent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ImgProfile", type="boolean", nullable=false)
     */
    protected $ImgProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ImgApproved", type="boolean", nullable=false)
     */
    protected $ImgApproved;

    /**
     * @var integer
     *
     * @ORM\Column(name="ImgId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $ImgId;



    /**
     * Set ImgOrigId
     *
     * @param integer $imgOrigId
     * @return Memberimages
     */
    public function setImgOrigId($imgOrigId)
    {
        $this->ImgOrigId = $imgOrigId;

        return $this;
    }

    /**
     * Get ImgOrigId
     *
     * @return integer 
     */
    public function getImgOrigId()
    {
        return $this->ImgOrigId;
    }

    /**
     * Set ImgMembId
     *
     * @param integer $imgMembId
     * @return Memberimages
     */
    public function setImgMembId($imgMembId)
    {
        $this->ImgMembId = $imgMembId;

        return $this;
    }

    /**
     * Get ImgMembId
     *
     * @return integer 
     */
    public function getImgMembId()
    {
        return $this->ImgMembId;
    }

    /**
     * Set ImgType
     *
     * @param string $imgType
     * @return Memberimages
     */
    public function setImgType($imgType)
    {
        $this->ImgType = $imgType;

        return $this;
    }

    /**
     * Get ImgType
     *
     * @return string 
     */
    public function getImgType()
    {
        return $this->ImgType;
    }

    /**
     * Set ImgExtension
     *
     * @param string $imgExtension
     * @return Memberimages
     */
    public function setImgExtension($imgExtension)
    {
        $this->ImgExtension = $imgExtension;

        return $this;
    }

    /**
     * Get ImgExtension
     *
     * @return string 
     */
    public function getImgExtension()
    {
        return $this->ImgExtension;
    }

    /**
     * Set ImgSize
     *
     * @param integer $imgSize
     * @return Memberimages
     */
    public function setImgSize($imgSize)
    {
        $this->ImgSize = $imgSize;

        return $this;
    }

    /**
     * Get ImgSize
     *
     * @return integer 
     */
    public function getImgSize()
    {
        return $this->ImgSize;
    }

    /**
     * Set ImgWidth
     *
     * @param integer $imgWidth
     * @return Memberimages
     */
    public function setImgWidth($imgWidth)
    {
        $this->ImgWidth = $imgWidth;

        return $this;
    }

    /**
     * Get ImgWidth
     *
     * @return integer 
     */
    public function getImgWidth()
    {
        return $this->ImgWidth;
    }

    /**
     * Set ImgHeight
     *
     * @param integer $imgHeight
     * @return Memberimages
     */
    public function setImgHeight($imgHeight)
    {
        $this->ImgHeight = $imgHeight;

        return $this;
    }

    /**
     * Get ImgHeight
     *
     * @return integer 
     */
    public function getImgHeight()
    {
        return $this->ImgHeight;
    }

    /**
     * Set ImgCaption
     *
     * @param string $imgCaption
     * @return Memberimages
     */
    public function setImgCaption($imgCaption)
    {
        $this->ImgCaption = $imgCaption;

        return $this;
    }

    /**
     * Get ImgCaption
     *
     * @return string 
     */
    public function getImgCaption()
    {
        return $this->ImgCaption;
    }

    /**
     * Set ImgDescription
     *
     * @param string $imgDescription
     * @return Memberimages
     */
    public function setImgDescription($imgDescription)
    {
        $this->ImgDescription = $imgDescription;

        return $this;
    }

    /**
     * Get ImgDescription
     *
     * @return string 
     */
    public function getImgDescription()
    {
        return $this->ImgDescription;
    }

    /**
     * Set ImgPhotographer
     *
     * @param string $imgPhotographer
     * @return Memberimages
     */
    public function setImgPhotographer($imgPhotographer)
    {
        $this->ImgPhotographer = $imgPhotographer;

        return $this;
    }

    /**
     * Get ImgPhotographer
     *
     * @return string 
     */
    public function getImgPhotographer()
    {
        return $this->ImgPhotographer;
    }

    /**
     * Set ImgKeywords
     *
     * @param string $imgKeywords
     * @return Memberimages
     */
    public function setImgKeywords($imgKeywords)
    {
        $this->ImgKeywords = $imgKeywords;

        return $this;
    }

    /**
     * Get ImgKeywords
     *
     * @return string 
     */
    public function getImgKeywords()
    {
        return $this->ImgKeywords;
    }

    /**
     * Set ImgTaken
     *
     * @param \DateTime $imgTaken
     * @return Memberimages
     */
    public function setImgTaken($imgTaken)
    {
        $this->ImgTaken = $imgTaken;

        return $this;
    }

    /**
     * Get ImgTaken
     *
     * @return \DateTime 
     */
    public function getImgTaken()
    {
        return $this->ImgTaken;
    }

    /**
     * Set ImgUploaded
     *
     * @param \DateTime $imgUploaded
     * @return Memberimages
     */
    public function setImgUploaded($imgUploaded)
    {
        $this->ImgUploaded = $imgUploaded;

        return $this;
    }

    /**
     * Get ImgUploaded
     *
     * @return \DateTime 
     */
    public function getImgUploaded()
    {
        return $this->ImgUploaded;
    }

    /**
     * Set ImgOrd
     *
     * @param integer $imgOrd
     * @return Memberimages
     */
    public function setImgOrd($imgOrd)
    {
        $this->ImgOrd = $imgOrd;

        return $this;
    }

    /**
     * Get ImgOrd
     *
     * @return integer 
     */
    public function getImgOrd()
    {
        return $this->ImgOrd;
    }

    /**
     * Set ImgContent
     *
     * @param string $imgContent
     * @return Memberimages
     */
    public function setImgContent($imgContent)
    {
        $this->ImgContent = $imgContent;

        return $this;
    }

    /**
     * Get ImgContent
     *
     * @return string 
     */
    public function getImgContent()
    {
        return $this->ImgContent;
    }

    /**
     * Set ImgProfile
     *
     * @param boolean $imgProfile
     * @return Memberimages
     */
    public function setImgProfile($imgProfile)
    {
        $this->ImgProfile = $imgProfile;

        return $this;
    }

    /**
     * Get ImgProfile
     *
     * @return boolean 
     */
    public function getImgProfile()
    {
        return $this->ImgProfile;
    }

    /**
     * Set ImgApproved
     *
     * @param boolean $imgApproved
     * @return Memberimages
     */
    public function setImgApproved($imgApproved)
    {
        $this->ImgApproved = $imgApproved;

        return $this;
    }

    /**
     * Get ImgApproved
     *
     * @return boolean 
     */
    public function getImgApproved()
    {
        return $this->ImgApproved;
    }

    /**
     * Get ImgId
     *
     * @return integer 
     */
    public function getImgId()
    {
        return $this->ImgId;
    }
}
