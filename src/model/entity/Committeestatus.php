<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model\entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Committeestatus
 *
 * @ORM\Table(name="CommitteeStatus")
 * @ORM\Entity
 */
class Committeestatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CommMembId", type="integer", nullable=false)
     */
    protected $CommMembId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CommOfstId", type="integer", nullable=false)
     */
    protected $CommOfstId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CommStart", type="date", nullable=true)
     */
    protected $CommStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CommEnd", type="date", nullable=true)
     */
    protected $CommEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="CommType", type="string", nullable=true)
     */
    protected $CommType;

    /**
     * @var integer
     *
     * @ORM\Column(name="CommCposId", type="integer", nullable=false)
     */
    protected $CommCposId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CommId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $CommId;



    /**
     * Set CommMembId
     *
     * @param integer $commMembId
     * @return Committeestatus
     */
    public function setCommMembId($commMembId)
    {
        $this->CommMembId = $commMembId;

        return $this;
    }

    /**
     * Get CommMembId
     *
     * @return integer 
     */
    public function getCommMembId()
    {
        return $this->CommMembId;
    }

    /**
     * Set CommOfstId
     *
     * @param integer $commOfstId
     * @return Committeestatus
     */
    public function setCommOfstId($commOfstId)
    {
        $this->CommOfstId = $commOfstId;

        return $this;
    }

    /**
     * Get CommOfstId
     *
     * @return integer 
     */
    public function getCommOfstId()
    {
        return $this->CommOfstId;
    }

    /**
     * Set CommStart
     *
     * @param \DateTime $commStart
     * @return Committeestatus
     */
    public function setCommStart($commStart)
    {
        $this->CommStart = $commStart;

        return $this;
    }

    /**
     * Get CommStart
     *
     * @return \DateTime 
     */
    public function getCommStart()
    {
        return $this->CommStart;
    }

    /**
     * Set CommEnd
     *
     * @param \DateTime $commEnd
     * @return Committeestatus
     */
    public function setCommEnd($commEnd)
    {
        $this->CommEnd = $commEnd;

        return $this;
    }

    /**
     * Get CommEnd
     *
     * @return \DateTime 
     */
    public function getCommEnd()
    {
        return $this->CommEnd;
    }

    /**
     * Set CommType
     *
     * @param string $commType
     * @return Committeestatus
     */
    public function setCommType($commType)
    {
        $this->CommType = $commType;

        return $this;
    }

    /**
     * Get CommType
     *
     * @return string 
     */
    public function getCommType()
    {
        return $this->CommType;
    }

    /**
     * Set CommCposId
     *
     * @param integer $commCposId
     * @return Committeestatus
     */
    public function setCommCposId($commCposId)
    {
        $this->CommCposId = $commCposId;

        return $this;
    }

    /**
     * Get CommCposId
     *
     * @return integer 
     */
    public function getCommCposId()
    {
        return $this->CommCposId;
    }

    /**
     * Get CommId
     *
     * @return integer 
     */
    public function getCommId()
    {
        return $this->CommId;
    }
}
