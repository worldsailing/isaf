<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Isaf\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JsonSerializable;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Races
 *
 * @ORM\Table(name="Races", indexes={@ORM\Index(name="RaceSource", columns={"RaceSource", "RaceSrcId"}), @ORM\Index(name="RaceEvntId", columns={"RaceEvntId"})})
 * @ORM\Entity
 */
class Races extends \worldsailing\Isaf\model\entity\Races  implements JsonSerializable, IsafOrmModelInterface
{
    /**
     * @return array
     */
    public static function getDefaultValues()
    {
        return [
            'RaceNumber' => 0,
            'RaceMedalRace' => 0,
            'RaceMultiplier' => 1
        ];
    }

    /**
     * @return Assert\Collection
     */
    public static function getConstraints()
    {
        $constraints = new Assert\Collection([
            'fields' => [
                'RaceEvntId' => [new Assert\NotBlank()],
                'RaceNumber' => [new Assert\NotBlank()],
                'RaceCourseId' => [new Assert\NotBlank()],
                'RaceShowResults' => [new Assert\NotBlank()],
                'RaceMultiplier' => [new Assert\NotBlank()],
                'RaceMedalRace' => [new Assert\NotBlank()],
                'RaceStatus' => [new Assert\NotBlank(), new Assert\Collection(['planned','scheduled','startseq','inprogress','recall','finished','result','delayed','cancelled','protested','postponed','abandoned','rescheduled','provisional','final'])],
                'RaceStage' => [new Assert\Length(['max' => 100])],
                'RaceFlight' => [new Assert\Length(['max' => 100])],
                'RaceMatch' => [new Assert\Length(['max' => 100])],
                'RaceStartDate' => [new Assert\Callback('isValidDate')],
                'RaceStartTime' => [new Assert\Callback('isValidTime')]
            ],
            'allowExtraFields' => true
        ]);

        return $constraints;
    }

    /**
     * TODO: Validation. Move custom validations out from main class to a validation utility class!
     * @param $object
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public static function isValidDate($object, ExecutionContextInterface $context, $payload)
    {
        try {
            if (!is_object(\DateTime::createFromFormat('Y-m-d', $object->getRaceStartDate()))) {
                $context->buildViolation('Race Start Date is not valid date')
                    ->atPath('RaceStartDate')
                    ->addViolation();
            }
        } catch(\Exception $e) {
            $context->buildViolation('Race Start Date is not valid date')
                ->atPath('RaceStartDate')
                ->addViolation();
        }
    }

    /**
     * TODO: Validation. Move custom validations out from main class to a validation utility class!
     * @param $object
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public static function isValidTime($object, ExecutionContextInterface $context, $payload)
    {
        $passed = true;
        try {
            if (!is_object(\DateTime::createFromFormat('H:i', $object->getRaceStartDate()))) {
                $passed = false;
            }
        } catch(\Exception $e) {
            $passed = false;
        }
        if (! $passed) {
            try {
                if (is_object(\DateTime::createFromFormat('H:i:s', $object->getRaceStartDate()))) {
                    $passed = true;
                }
            } catch (\Exception $e) {
                $passed = false;
            }
        }

        if (! $passed) {
            $context->buildViolation('Race Start Time is not valid time')
                ->atPath('RaceStartTime')
                ->addViolation();
        }
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = self::getDefaultValues();
        if (isset($defaults[$key])) {
            if (is_callable($defaults[$key])) {
                return $defaults[$key]();
            } else {
                return $defaults[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param bool $useDefaults
     * @return IsafOrmModelInterface
     */
    public function setData($data, $useDefaults = false)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    if ($useDefaults === true) {
                        $this->{$key} = $this->getDefaultValue($key);
                    }
                } else {
                    $this->{$key} = $data[$key];
                }
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}