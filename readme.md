# worldsailing Isaf 

### PHP library description
Doctrine ORM provider and entities for custom World Sailing database.

  
### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://worldsailing@bitbucket.org/worldsailing/isaf.git"
    }
  ],
  "require": {
    "worldsailing/isaf": "dev-master"
  }
}
```

### Usage
````php
use worldsailing\Isaf\provider\IsafOrmProvider;

if (! isset($app['orm.ems'])) {
    $app->register(new IsafOrmProvider());
}
````
 
### License

All right reserved
